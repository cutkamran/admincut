﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClickUrHotel_Master.Master" AutoEventWireup="true" CodeBehind="contact-us.aspx.cs" Inherits="ClickUrHotel.contact_us" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<section id="themo_page_header_1" class="light-text parallax-preload parallax-bg full-header-img" data-stellar-background-ratio="0.15" data-stellar-vertical-offset="145" style="padding-top: 238px; background-position: 50% -21.75px;">
					<div class="container">
						<div class="row">
							<div class="page-title centered">
								<h1 class="page-title-h1 "></h1>
								<div class="page-title-button "><a href="#map" class="btn btn-ghost   th-btn">MAP &amp; DIRECTIONS</a></div>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container -->
				</section>--%>

				<section id="themo_service_block_1" class=" icon-blocks ">
					<div class="container">
						<div class="row">
							<div class="icon-block icon-block-1 col-md-6">
								<div class="circle-lrg-icon"><a href="#map"><i class="accent glyphicons glyphicons-map"></i></a></div>
								<h3><a href="#map">Map &amp; Directions</a></h3>
								<p>Are you wondering how to get here?</p>
							</div>
							<div class="icon-block icon-block-2 col-md-6">
								<div class="circle-lrg-icon"><a href="#contact"><i class="accent glyphicons conversation"></i></a></div>
								<h3><a href="#contact">Contact Us</a></h3>
								<p>Call, Email or use our Contact Form</p>
							</div>
						</div>
						<!--/row-->
					</div>
					<!-- /.container -->
				</section>
				
				<%--<div id="map">
					<section id="themo_map_1" class=" full-map ">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<div class="aligncenter">
										<div id="map_canvas_75" class="googlemap" style="height:300px;width:100%"><input class="title" type="hidden" value="Our Location"><input class="location" type="hidden" value="Mohammed Ali Rd, Pydhonie, Mandvi, Mumbai, Maharashtra 400003, India"><input class="zoom" type="hidden" value="8">
											<div class="map_canvas"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.container -->
					</section>
				</div>--%>
				
				<div id="contact">
					<section id="themo_service_block_split_1" class=" service-split ">
						<div class="container">
							<div class="row">
								<div class="section-header col-xs-12 centered">
									<h2>Get in Touch</h2>
								</div>
								<!-- /.section-header -->
							</div>
							<!-- /.row -->
							<div class="row">
								<section class="split-blocks col-sm-6">
									<div class="service-block">
										<div class="service-block service-block-0">
											<div class="med-icon"><i class="accent glyphicons inbox"></i></div>
											<div class="service-block-text">
												<h3>Contact us by Email</h3>
												<p><a href="mailto:info@arabian.co.in">info@arabian.co.in</a></p>
											</div>
										</div>
										<div class="service-block service-block-1">
											<div class="med-icon"><i class="accent glyphicons iphone"></i></div>
											<div class="service-block-text">
												<h3>Contact us by Phone</h3>
												<p>Office: +91 22 4062 7860<br> Fax: +91 22 2344 5282</p>
											</div>
										</div>
										<div class="service-block service-block-2">
											<div class="med-icon"><i class="accent glyphicons glyphicons-map"></i></div>
											<div class="service-block-text">
												<h3>Contact Info</h3>
												<p>95, Memonwada Road, Amina Apartment, Ground Floor, Shop No.2, Mumbai – 400 003. INDIA</p>
											</div>
										</div>
									</div>
								</section>
								<!-- /.contact-blocks -->
								<section class="contact-form col-sm-6">
									<div class="frm_forms  with_frm_style frm_style_formidable-style-2" id="frm_form_5_container">
										<form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_contact3">
											<div class="frm_form_fields ">
												<fieldset>

													<input type="hidden" name="frm_action" value="create">
													<input type="hidden" name="form_id" value="5">
													<input type="hidden" name="frm_hide_fields_5" id="frm_hide_fields_5" value="">
													<input type="hidden" name="form_key" value="contact3">
													<input type="hidden" name="item_meta[0]" value="">
													<input type="hidden" id="frm_submit_entry_5" name="frm_submit_entry_5" value="fe09a7d03e"><input type="hidden" name="_wp_http_referer" value="/contact-us/">
													<div id="frm_field_22_container" class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
														<label for="field_xipbjr3" class="frm_primary_label">Name
						                                    <span class="frm_required">*</span>
					                                    </label>
														<input type="text" id="field_xipbjr3" name="item_meta[22]" value="" placeholder="Name" data-reqmsg="This field cannot be blank." style="width:100%">
													</div>
													<div id="frm_field_23_container" class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
														<label for="field_jaehy83" class="frm_primary_label">Email
						                                    <span class="frm_required">*</span>
					                                    </label>
														<input type="text" id="field_jaehy83" name="item_meta[23]" value="" placeholder="Email" data-reqmsg="This field cannot be blank." style="width:100%">
													</div>
													<div id="frm_field_24_container" class="frm_form_field form-field  frm_none_container frm_full">
														<label for="field_cqpguu3" class="frm_primary_label">Mobile Number
						                                    <span class="frm_required"></span>
					                                    </label>
														<input type="text" id="field_cqpguu3" name="item_meta[24]" value="" placeholder="Mobile Number" style="width:100%">
													</div>
													<div id="frm_field_25_container" class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
														<label for="field_w7ueyk3" class="frm_primary_label">Subject
						                                    <span class="frm_required">*</span>
					                                    </label>
														<input type="text" id="field_w7ueyk3" name="item_meta[25]" value="" placeholder="Subject" data-reqmsg="This field cannot be blank." style="width:100%">
													</div>
													<div id="frm_field_26_container" class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
														<label for="field_kggkvh3" class="frm_primary_label">Message
						                                    <span class="frm_required">*</span>
					                                    </label>
														<textarea name="item_meta[26]" id="field_kggkvh3" rows="5" placeholder="Message" data-reqmsg="This field cannot be blank." style="width:100%"></textarea>
													</div>
													<input type="hidden" name="item_key" value="" />
													<div class="frm_submit">
														<input type="submit" value="Send">
														<img class="frm_ajax_loading" src="assets/images/ajax_loader.gif" alt="Sending" style="visibility:hidden;">
													</div>
												</fieldset>
											</div>

										</form>
									</div>

								</section>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.container -->
					</section>
				</div>
</asp:Content>
