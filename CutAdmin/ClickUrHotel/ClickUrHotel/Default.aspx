﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ClickUrHotel.Default" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arabian Tours & Travels</title>
	<link rel="stylesheet" href="assets/css/preloader.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor.css">
	<link rel="stylesheet" href="assets/css/app.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<link rel="stylesheet" href="assets/css/custom.css">
	<script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5bc5909461d0b770925158fc/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
    <!--End of Tawk.to Script-->
   <%-- <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">--%>
</head>

<body class="page-template-default loaded" data-elementor-device-mode="desktop" data-spy="scroll" data-offset="50">
    <header class="banner navbar navbar-default navbar-static-top dark-header headhesive--clone headhesive--unstick" role="banner" data-transparent-header="true">
        <!-- top navigation -->
        <div class="top-nav">
            <div class="container">
                <div class="row col-md-12">
                    <div class="top-nav-text">
                        <p>Welcome to Arabian Tours & Travels!</p>
                    </div>
                    <div class="top-nav-icon-blocks">
                        <div class="icon-block">
                            <p>
								<a href="mailto:info@arabian.co.in">
									<i class="glyphicons glyphicons-envelope"></i>
									<span>info@arabian.co.in</span>
								</a>
                            </p>
                        </div>
                        <div class="icon-block">
                            <p><a href="#"><i class="glyphicons glyphicons-google-maps"></i><span>How to Find Us</span></a></p>
                        </div>
                        <div class="icon-block">
                            <p><a href="tel:+91 22 4062 7860"><i class="glyphicons glyphicons-iphone"></i><span>+91 22 4062 7860</span></a></p>
                        </div>
                        <div class="icon-block">
                            <p><a href="fax:+91 22 2344 5282" href="#"><i class="glyphicons glyphicons-fax"></i><span>+91 22 2344 5282</span></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END top navigation -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <div id="logo">
                    <a href="#">
                   	  <img class="logo-trans logo-reg" src="assets/images/logo/Arabian-final-logo1.png" alt="Arabian">
                      <img class="logo-main logo-reg" src="assets/images/logo/Arabian-final-logo1.png" alt="Arabian">
					</a>
                </div>
            </div>

            <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                <ul id="menu-main-menu" class="nav navbar-nav">
                    <li class="active menu-home"><a href="Default.aspx">Home </a>                        
                    </li>
                    <li class="menu-about-us"><a href="about-us.aspx">About Us </a>
                        
                    </li>
                    <li class="menu-services"><a href="services.aspx">Services </a>
                        
                    </li>
                    <li class="dropdown menu-our-packages"><a class="dropdown-toggle" href="#">Our Packages <b class="caret"></b></a>
                        <ul class="dropdown-menu">
							<li class="menu-hajj-tour-package">
								<a href="#">Hajj Tour Package</a>
							</li>
							<li class="menu-umrah-tour-package">
								<a href="#">Umrah Tour Package</a>
							</li>
						</ul>
                    </li>
                    <li class="menu-contact-us"><a href="contact-us.aspx">Contact Us </a>
                        
                    </li>
                    <li class="th-accent menu-download-theme"><a target="_blank" href="#">Get in Touch</a></li>
                    <li class="th-accent menu-download-theme"><a target="_blank" href="Login.aspx">Agent Login</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <!-- Preloader Start -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- Preloader End -->

    <header class="banner navbar navbar-default navbar-static-top dark-header" role="banner" data-transparent-header="true">

        <!-- top navigation -->
        <div class="top-nav">
            <div class="container">
                <div class="row col-md-12">
                    <div class="top-nav-text">
                        <p>Welcome to Arabian Tours & Travels!</p>
                    </div>
                    <div class="top-nav-icon-blocks">
                        <div class="icon-block">
                            <p><a href="mailto:info@arabian.co.in"><i class="glyphicons glyphicons-envelope"></i><span>info@arabian.co.in</span></a></p>
                        </div>
                        <div class="icon-block">
                            <p><a href="#"><i class="glyphicons glyphicons-google-maps"></i><span>How to Find Us</span></a></p>
                        </div>
                        <div class="icon-block">
                            <p><a href="tel:+91 22 4062 7860"><i class="glyphicons glyphicons-iphone"></i><span>+91 22 4062 7860</span></a></p>
                        </div>
                        <div class="icon-block">
                            <p><a href="fax:+91 22 2344 5282" href="#"><i class="glyphicons glyphicons-fax"></i><span>+91 22 2344 5282</span></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END top navigation -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <div id="logo">
                    <a href="#">
                   	    <img class="logo-trans logo-reg" src="assets/images/logo/Arabian-final-logo1.png" alt="Arabian">
                        <img class="logo-main logo-reg" src="assets/images/logo/Arabian-final-logo1.png" alt="Arabian">
				</a>
                </div>
            </div>

            <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                <ul id="menu-main-menu" class="nav navbar-nav">
                    <li class="active menu-home"><a href="Default.aspx">Home </a></li>
                    <li class="menu-pages"><a href="about-us.aspx">About Us </a></li>
                    <li class="menu-features"><a href="services.aspx">Services </a></li>
                    <li class="menu-blog"><a href="#">Our Packages </a></li>
                    <li class="menu-blog"><a href="#">Gallery </a></li>
                    <li class="menu-blog"><a href="contact-us.aspx">Contact Us </a></li>
                    <li class="th-accent menu-download-theme"><a target="_blank" href="#">Get in Touch</a></li>
                    <li class="th-accent menu-download-theme"><a target="_blank" href="Login.aspx">Agent Login</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <div class="wrap" role="document">

        <div class="content">

            <div class="inner-container">

                <div id="home-slider">
					<style scoped="">
						#main-flex-slider .themo_slider_0{padding-top:130px ; padding-bottom:265px }
						#main-flex-slider .themo_slider_1{padding-top:130px ; padding-bottom:265px }
					</style>
					<div id="main-flex-slider" class="flexslider" style="height: 769px;">
						<ul class="slides">
							<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
								<div class="slider-bg slide-cal-center light-text themo_slider_0">
									<div class="container">
										<div class="row lrg-txt ">
											<div class="slider-content col-sm-6">
												<div class="slider-subtitle ">
													<h1 class="slider-title"> Labbaik Allahumma Labbaik</h1>
													<div class="slider-subtitle ">
														<p>‘Best Tours &amp; Travels in the World’</p>
													</div>
												</div>
											</div>
											<!-- /.row -->
										</div>
										<!-- /.container -->
									</div>
									<!-- /.slider-bg -->
								</div>
							</li>
							<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
								<div class="slider-bg slide-cal-center light-text themo_slider_1">
									<div class="container">
										<div class="row lrg-txt ">
											<div class="slider-content col-sm-6">
												<div class="slider-subtitle ">
													<h1>Perform your hajj with a</h1>
													<div class="slider-subtitle ">
														<p>Well-known Islamic Scholar</p>
													</div>
												</div>
											</div>
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container -->
								</div>
								<!-- /.slider-bg -->
							</li>
						</ul><a href="#the-chalet" target="_self" class="slider-scroll-down th-icon th-i-down"></a>
						<ul class="flex-direction-nav">
							<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li>
							<li class="flex-nav-next"><a class="flex-next" href="#"></a></li>
						</ul>
					</div>
					<!-- /.main-flex-slider -->
				</div>

				<div id="the-chalet">
					<section id="themo_html_2" class=" content-editor ">
						<div class="container">
							<div class="row">
								<div class="section-header col-xs-12 centered">
													<h2>TOP DESTINATIONS</h2>	
								</div><!-- /.section-header -->        
							</div><!-- /.row -->   
						</div><!-- /.container -->
					</section>
				</div>
				
				<section id="themo_thumb_slider_1" class=" thumb-slider ">
					<div class="container">
						<div class="row">
							<div id="themo_thumb_slider_1_inner" class="thumb-flex-slider flexslider flex-landscape col-xs-12"><!-- /.slides -->
								<div class="flex-viewport" style="overflow: hidden; position: relative;">
									<ul class="slides gallery" style="width: 1000%; transition-duration: 0s; transform: translate3d(-295px, 0px, 0px);">
										<li class="thumb-flex-slider-img-1" style="width: 255px; float: left; display: block;">
											<img width="255" height="170" src="assets/images/hotel_home_04.jpg" class="attachment-themo_thumb_slider size-themo_thumb_slider wp-post-image" alt="hotel_home_04" srcset="assets/images/hotel_home_04.jpg 255w, assets/images/hotel_home_04-120x80.jpg 120w, assets/images/hotel_home_04-60x40.jpg 60w" sizes="(max-width: 255px) 100vw, 255px" draggable="false" style="display: inline;">
										</li>
										<li class="thumb-flex-slider-img-2" style="width: 255px; float: left; display: block;">
											<img width="255" height="170" src="assets/images/hotel_home_03.jpg" class="attachment-themo_thumb_slider size-themo_thumb_slider wp-post-image" alt="hotel_home_03" srcset="assets/images/hotel_home_03.jpg 255w, assets/images/hotel_home_03-120x80.jpg 120w, assets/images/hotel_home_03-60x40.jpg 60w" sizes="(max-width: 255px) 100vw, 255px" draggable="false" style="display: inline;">
										</li>
										<li class="thumb-flex-slider-img-3" style="width: 255px; float: left; display: block;">
											<img width="255" height="170" src="assets/images/hotel_home_02.jpg" class="attachment-themo_thumb_slider size-themo_thumb_slider wp-post-image" alt="hotel_home_02" srcset="assets/images/hotel_home_02.jpg 255w, assets/images/hotel_home_02-120x80.jpg 120w, assets/images/hotel_home_02-60x40.jpg 60w" sizes="(max-width: 255px) 100vw, 255px" draggable="false" style="display: inline;">
										</li>
										<li class="thumb-flex-slider-img-4" style="width: 255px; float: left; display: block;">
											<img width="255" height="170" src="assets/images/hotel_home_01.jpg" class="attachment-themo_thumb_slider size-themo_thumb_slider wp-post-image" alt="hotel_home_01" srcset="assets/images/hotel_home_01.jpg 255w, assets/images/hotel_home_01-120x80.jpg 120w, assets/images/hotel_home_01-60x40.jpg 60w" sizes="(max-width: 255px) 100vw, 255px" draggable="false" style="display: inline;">
										</li>
										<li class="thumb-flex-slider-img-5" style="width: 255px; float: left; display: block;">
											<img width="255" height="170" src="assets/images/hotel_home_04.jpg" class="attachment-themo_thumb_slider size-themo_thumb_slider wp-post-image" alt="hotel_home_04" srcset="assets/images/hotel_home_04.jpg 255w, assets/images/hotel_home_04-120x80.jpg 120w, assets/images/hotel_home_04-60x40.jpg 60w" sizes="(max-width: 255px) 100vw, 255px" draggable="false" style="display: inline;">
										</li>
									</ul>
								</div>
								<ul class="flex-direction-nav">
									<li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li>
									<li class="flex-nav-next"><a class="flex-next" href="#"></a></li>
								</ul>
							</div> <!-- /.thumb-flex-slider -->
						</div><!-- /.row -->			
						<script>
							jQuery(window).load(function() {
								jQuery('.thumb-flex-slider').show();
								themo_start_thumb_slider('#themo_thumb_slider_1_inner');
							});
						</script>
					</div><!-- /.container -->
				</section>
				
				<section id="themo_html_3" class=" content-editor ">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<h2>About Arabian Tours &amp; Travels</h2>
										<p style="text-align: justify">Arabian Tours &amp; Travels was established as a Organizers of Hajj, Umrah and Ziyarat Tours. Since 2001, we have our Head Office in Mumbai (Bombay) India &amp; Branch Offices in UK Bolton, Malawi Blantyre, Canada Toronto and inshallah
											in coming year 2010 we are opening our Branch Offices in South Africa and Pakistan too we mainly concentrate on the tourism package for Hajj &amp; Umrah. Our Organization has increased the quality of the Hajj and Umrah sector
											by providing luxury accommodation, comfortable transportation, delicious foods, and reliable services. We have taken religious travel and raised it to the standards of the International Tourism and Travel Industry. » <a href="http://arabian.co.in/about-us/"
												style="color:#46263d">Read More</a>&nbsp;</p>
									</div>
									<div class="col-md-6">
										<iframe src="https://player.vimeo.com/video/139131761?color=6ec3b3&amp;title=0&amp;byline=0&amp;portrait=0" width="550" height="309" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.container -->
				</section>
				
				<section id="themo_rooms_1" class=" rooms ">
					<div class="container">
						<div class="row">
							<div class="section-header col-xs-12 centered">
								<h2>Our Packages</h2>
								<p>Choose from some of the most popular packages.</p>
							</div>
							<!-- /.section-header -->
						</div>
						<!-- /.row -->
						<div id="filters" class="rooms-filters"><span>Sort:</span><a href="#" data-filter="*" class="current">All</a></div>
						<div id="rooms-row" class="rooms-row row themo_rooms_1 three-columns">
                            <div  class="themo_rooms type-themo_rooms rooms-item item col-lg-2 col-md-2 col-sm-6  post-383 status-publish format-standard has-post-thumbnail hentry" style="height: 380px;"></div>
							<div id="themo_rooms_1-post-383" class="themo_rooms type-themo_rooms rooms-item item col-lg-4 col-md-4 col-sm-6  post-383 status-publish format-standard has-post-thumbnail hentry" style="height: 380px;">
								<div class="room-wrap"><img width="380" height="380" src="assets/images/hotel_room_03-380x380.jpg" class="img-responsive room-img wp-post-image" alt="hotel_room_03-380x380" srcset="assets/images/hotel_room_03-380x380.jpg 380w, assets/images/hotel_room_03-380x380-150x150.jpg 150w, assets/images/hotel_room_03-380x380-300x300.jpg 300w, assets/images/hotel_room_03-380x380-180x180.jpg 180w, assets/images/hotel_room_03-380x380-255x255.jpg 255w, assets/images/hotel_room_03-380x380-80x80.jpg 80w, assets/images/hotel_room_03-380x380-40x40.jpg 40w, assets/images/hotel_room_03-380x380-60x60.jpg 60w, assets/images/hotel_room_03-380x380-360x360.jpg 360w"
										sizes="(max-width: 380px) 100vw, 380px">
									<div class="room-overlay"></div>
									<div class="room-inner" style="height: 380px;">
										<div class="room-center" style="visibility: visible;">
											<h3 class="room-title">Hajj Tour Package</h3>
											<div class="pricing-cost"><span></span></div>
										</div>
										<!-- /.room-center --><a class="room-link" href="#"></a></div>
									<!-- /.room-inner -->
								</div>
								<!-- /.room-wrap -->
							</div>
							<!-- /.col-md -->
							<div id="themo_rooms_1-post-1187" class="themo_rooms type-themo_rooms rooms-item item col-lg-4 col-md-4 col-sm-6  post-1187 status-publish format-standard has-post-thumbnail hentry" style="height: 380px;">
								<div class="room-wrap"><img width="380" height="380" src="assets/images/umrah-tour-package.jpg" class="img-responsive room-img wp-post-image" alt="umrah-tour-package" srcset="assets/images/umrah-tour-package.jpg 380w, assets/images/umrah-tour-package-150x150.jpg 150w, assets/images/umrah-tour-package-300x300.jpg 300w, assets/images/umrah-tour-package-180x180.jpg 180w, assets/images/umrah-tour-package-255x255.jpg 255w, assets/images/umrah-tour-package-80x80.jpg 80w, assets/images/umrah-tour-package-40x40.jpg 40w, assets/images/umrah-tour-package-60x60.jpg 60w, assets/images/umrah-tour-package-360x360.jpg 360w"
										sizes="(max-width: 380px) 100vw, 380px">
									<div class="room-overlay"></div>
									<div class="room-inner" style="height: 380px;">
										<div class="room-center" style="visibility: visible;">
											<h3 class="room-title">Umrah Tour Package</h3>
											<div class="pricing-cost"><span></span></div>
										</div>
										<!-- /.room-center --><a class="room-link" href="#"></a></div>
									<!-- /.room-inner -->
								</div>
								<!-- /.room-wrap -->
							</div>
							<!-- /.col-md -->
                            <div  class="themo_rooms type-themo_rooms rooms-item item col-lg-2 col-md-2 col-sm-6  post-383 status-publish format-standard has-post-thumbnail hentry" style="height: 380px;"></div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container -->
				</section>
				
				<div id="why-us">
					<section id="themo_tour_1" class="floated-blocks">
						
						<div>
							<section id="themo_tour_1_0" class=" float-block img-right light-text full-header-img" style="height: 342px;">
								<div class="container">
									<div class="row">
										<div class="float-content col-sm-8">
											<div class="center-table-con">
												<div class="center-table-cell">
													<h2 class="tour-content-title tour-title-0">5 STAR HOSPITALITY</h2>
													<div class="tour-content-0">
														<p>Nulla at mauris accumsan eros ullamcorper tincidunt at nec ipsum. In iaculis est ut sapien ultrices, vel feugiat nulla lobortis. Donec nec quam accumsan, lobortis.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.container -->
							</section>
						</div>

						<script>
							jQuery(document).ready(function($) {
														"use strict";
							
														if (themo_is_touch_device()) {
															$("section#themo_tour_1_0").backstretch("assets/images/hotel_tour_01.jpg");        }
													});
						</script>
						
						<div class="preloader loaded">
							<section id="themo_tour_1_1" class=" float-block img-left light-text parallax-preload parallax-bg full-header-img" data-stellar-background-ratio="0.15" data-stellar-vertical-offset="145" style="height: 342px; background-position: 50% 40.8px;">
								<div class="container">
									<div class="row">
										<div class="float-content col-sm-8">
											<div class="center-table-con">
												<div class="center-table-cell">
													<h2 class="tour-content-title tour-title-1">TRANSPORTATION INCLUDED</h2>
													<div class="tour-content-1">
														<p>Nulla at mauris accumsan eros ullamcorper tincidunt at nec ipsum. In iaculis est ut sapien ultrices, vel feugiat nulla lobortis. Donec nec quam accumsan, lobortis.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.container -->
							</section>
						</div>

						<script>
							jQuery(document).ready(function($) {
														"use strict";
							
														if (themo_is_touch_device()) {
															$("section#themo_tour_1_1").backstretch("assets/images/hotel_tour_02_2.jpg");        }
													});
						</script>
						

						<div>
							<section id="themo_tour_1_2" class=" float-block img-right light-text full-header-img" style="height: 342px;">
								<div class="container">
									<div class="row">
										<div class="float-content col-sm-8">
											<div class="center-table-con">
												<div class="center-table-cell">
													<h2 class="tour-content-title tour-title-2">CONFERENCE CENTRE</h2>
													<div class="tour-content-2">
														<p>Nulla at mauris accumsan eros ullamcorper tincidunt at nec ipsum. In iaculis est ut sapien ultrices, vel feugiat nulla lobortis. Donec nec quam accumsan, lobortis.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.container -->
							</section>
						</div>

						<script>
							jQuery(document).ready(function($) {
									"use strict";
		
									if (themo_is_touch_device()) {
										$("section#themo_tour_1_2").backstretch("assets/images/hotel_tour_03_1.jpg");        }
								});
						</script>
					</section>
				</div>
				
				<section id="themo_conversion_form_1" class=" conversion-form ">
					<div class="container">
						<div class="row">
							<div class="section-header col-xs-12 centered">
								<h2>Confused regarding Tickets &amp; Packages? - Quick Enquiry Form</h2>
							</div>
							<!-- /.section-header -->
						</div><!-- /.row -->
						<div class="row">
							<div class="col-xs-12">
								<div class="simple-conversion">
									<div class="frm_forms  with_frm_style frm_style_formidable-style-2" id="frm_form_3_container">
										<form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_2ssykv">
											<div class="frm_form_fields ">
												<fieldset>

													<input type="hidden" name="frm_action" value="create">
													<input type="hidden" name="form_id" value="3">
													<input type="hidden" name="frm_hide_fields_3" id="frm_hide_fields_3" value="">
													<input type="hidden" name="form_key" value="2ssykv">
													<input type="hidden" name="item_meta[0]" value="">
													<input type="hidden" id="frm_submit_entry_3" name="frm_submit_entry_3" value="621c30d9da"><input type="hidden" name="_wp_http_referer" value="/">
													<div id="frm_field_16_container" class="frm_form_field form-field  frm_none_container">
														<label for="field_qy05f8" class="frm_primary_label">Name
						<span class="frm_required"></span>
					</label>
														<input type="text" id="field_qy05f8" name="item_meta[16]" value="" placeholder="Name">



													</div>
													<div id="frm_field_17_container" class="frm_form_field form-field  frm_required_field frm_none_container">
														<label for="field_3asv29" class="frm_primary_label">Email Address
						<span class="frm_required">*</span>
					</label>
														<input type="text" id="field_3asv29" name="item_meta[17]" value="" placeholder="Email Address" data-reqmsg="This field cannot be blank.">



													</div>
													<div id="frm_field_18_container" class="frm_form_field form-field  frm_none_container">
														<label for="field_2ywico" class="frm_primary_label">Enquiry
						<span class="frm_required"></span>
					</label>
														<input type="text" id="field_2ywico" name="item_meta[18]" value="" placeholder="Enquiry">



													</div>
													<input type="hidden" name="item_key" value="">
													<div class="frm_submit">

														<input type="submit" value="Submit">


													</div>
												</fieldset>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>
					</div><!-- /.container -->
				</section>
				
				<section id="themo_testimonials_1" class=" testimonials light-text">
					<div class="container">
						<div class="row">
							<div class="section-header col-xs-12 centered">
								<h2>Guest Reviews</h2>
							</div>
							<!-- /.section-header -->
						</div><!-- /.row -->
						<div class="row ">
							<div class="col-md-4">
								<figure class="quote">
									<blockquote class="tweet blockquote-1">I would like to thank Arabian team for the excellent service throughout my Umrah trip, everything was perfect - the hotels and luxury group coaches were amazing, we are very satisfied with everything. I shall also recommend Arabian
										to friends.</blockquote><img width="60" height="60" src="assets/images/testimonial-1-1.jpg" class="circle circle-1 wp-post-image" alt="testimonial-1" srcset="assets/images/testimonial-1-1.jpg 60w, assets/images/testimonial-1-1-40x40.jpg 40w"
										sizes="(max-width: 60px) 100vw, 60px">
									<figcaption class="figcaption-1">Ahmad Dayan<span>Satisfied Customer</span></figcaption>
								</figure>
							</div><!-- /.col-md -->
							<div class="col-md-4">
								<figure class="quote">
									<blockquote class="tweet blockquote-2">I went to Umrah with number of different tour operator, last month first time myself, wife and kids travel with Arabian Umrah group. Mashallah great services in Makkah and Madinah. Luxury vip coach throughout the journey, very friendly
										Arabian team.</blockquote><img width="60" height="60" src="assets/images/testimonial-1-1.jpg" class="circle circle-2 wp-post-image" alt="testimonial-1" srcset="assets/images/testimonial-1-1.jpg 60w, assets/images/testimonial-1-1-40x40.jpg 40w"
										sizes="(max-width: 60px) 100vw, 60px">
									<figcaption class="figcaption-2">Mohid<span>Satisfied Customer</span></figcaption>
								</figure>
							</div><!-- /.col-md -->
							<div class="col-md-4">
								<figure class="quote">
									<blockquote class="tweet blockquote-3">Asalam o alaikum, <br> Jazak Allah khair to the team for providing a lovely service for myself and my father... I thoroughly enjoyed myself and so did my daddy :) Alhamdulillah I will also recommend Islam freedom to friends..</blockquote>
									<img
										width="60" height="60" src="assets/images/testimonial-1-1.jpg" class="circle circle-3 wp-post-image" alt="testimonial-1" srcset="assets/images/testimonial-1-1.jpg 60w, assets/images/testimonial-1-1-40x40.jpg 40w"
										sizes="(max-width: 60px) 100vw, 60px">
										<figcaption class="figcaption-3">Khayum<span>Satisfied Customer</span></figcaption>
								</figure>
							</div><!-- /.col-md -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>
				
            </div>
            <!-- /.inner-container -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.wrap -->


    <div class="prefooter"></div>

    <footer class="footer" role="contentinfo">
        <div class="container">
            <div class="footer-widgets row">
                <div class="footer-area-1 col-md-3 col-sm-6">
					<section class="widget text-2 widget_text">
						<div class="widget-inner">
							<h3 class="widget-title">About Us</h3>
							<div class="textwidget">
								<p align="justify">Arabian Tours &amp; Travels is proud to announce that they are the leaders in their industry of religious tourism. Apart from our competitive rates and convenient locations with a network of hotel in Makkah &amp; Madina. <br>» <a href="http://arabian.co.in/about-us/"
										style="color:#eee;">Read More</a></p>
							</div>
						</div>
					</section>
				</div>
                <div class="footer-area-2 col-md-3 col-sm-6">
					<section class="widget nav_menu-2 widget_nav_menu">
						<div class="widget-inner">
							<h3 class="widget-title">Quick Menu</h3>
							<ul id="menu-footer-menu" class="menu">
								<li class="active menu-home"><a href="Default.aspx">Home</a></li>
								<li class="menu-about-us"><a href="about-us.aspx">About Us</a></li>
								<li class="menu-services"><a href="services.aspx">Services</a></li>
								<li class="menu-gallery"><a href="#">Gallery</a></li>
							</ul>
						</div>
                        <div class="icon-block">
                                <div class="row">
                                    <div class="col-md-2 col-sm-12"">
                                        <p><a href="https://www.facebook.com/tourarabian/"><i class="fa fa-facebook-square"></i><span></span></a></p>
                                    </div>
                                    <div class="col-md-2 col-sm-12"">
                                        <p><a href="https://twitter.com/tourarabian"><i class="fa fa-twitter-square"></i><span></span></a></p>
                                    </div>
                                    <div class="col-md-2 col-sm-12"">
                                        <p><a href="https://www.instagram.com/tourarabian/"><i class="fa fa-instagram"></i><span></span></a></p>
                                    </div>
                                    <div class="col-md-2 col-sm-12"">
                                        <p><a href="https://plus.google.com/106039359983799112610"><i class="fa fa-google-plus-square"></i><span></span></a></p>
                                    </div>
                                </div>
                            </div>
					</section>
				</div>
                <div class="footer-area-3 col-md-3 col-sm-6">
					<section class="widget widget-th-contact-info">
						<div class="widget-inner">
							<h3 class="widget-title">Contact Info</h3>
							<div class="th-contact-info-widget">
								<div class="icon-blocks">
									<div class="icon-block">
										<p><a href="mailto:info@arabian.co.in"><i class="glyphicons glyphicons-envelope"></i><span>info@arabian.co.in</span></a></p>
									</div>
									<div class="icon-block">
										<p><a href="tel:+91%2022%204062%207860%20"><i class="glyphicons glyphicons-iphone"></i><span>+91 22 4062 7860 </span></a></p>
									</div>
									<div class="icon-block">
										<p><a href="fax:+91%2022%202344%205282"><i class="glyphicons glyphicons-fax"></i><span>+91 22 2344 5282</span></a></p>
									</div>
									<div class="icon-block">
										<p><a href="#"><i class="glyphicons glyphicons-google-maps"></i><span>95, Memonwada Road, Amina Apartment,  Ground Floor, Shop No.2, Mumbai - 400 003. INDIA</span></a></p>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
                <div class="footer-area-4 col-md-3 col-sm-6">
                    <section class="widget recent-posts-5 widget_recent_entries">
                        <div class="widget-inner">
                            <h3 class="widget-title">Photo Gallery</h3>
                            
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="footer-btm-bar">
            <div class="container">
                <div class="footer-copyright row">
                    <div class="col-xs-12">
						<p><span class="footer_copy">&nbsp;© 2016 Arabian Tours &amp; Travels</span> - <span class="footer_credit">| All Rights Reserved | Powered by : <a target="_blank" href="http://dreamzvision.com/">Dreamzvision</a></span></p>
					</div>
                </div>
            </div>
        </div>
    </footer>
	
    <button onclick="topFunction()" id="myBtn" title="Go to top"><span class="glyphicon glyphicon-chevron-up"></span></button>     
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		function onReady(callback) {
		var intervalID = window.setInterval(checkReady, 1000);

			function checkReady() {
				if (document.getElementsByTagName('body')[0] !== undefined) {
					window.clearInterval(intervalID);
					callback.call(this);
				}
			}
		}

		function show(id, value) {
			document.getElementById(id).style.display = value ? 'block' : 'none';
		}

		onReady(function () {
			show('page', true);
			show('loader', false);
		});
	</script>
	<!--Scroll top javascript-->
	<script>
		// When the user scrolls down 20px from the top of the document, show the button
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		// When the user clicks on the button, scroll to the top of the document
		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</body>
</html>