﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClickUrHotel.Agent
{
    public partial class ViewInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ReservationID = Request.QueryString["ReservationId"];
            if (ReservationID != "")
            {
                DataSet ds = new DataSet();
                GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = Convert.ToInt64(Request.QueryString["Uid"]);
                string invoice = "";
                maincontainer.InnerHtml = CutAdmin.DataLayer.InvoiceManager.GetInvoice(ReservationID, Uid, out invoice);

            }
        }
    }
}