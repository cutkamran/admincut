﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClickUrHotel.Agent
{
    public partial class ViewVoucher : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ReservationID = Request.QueryString["ReservationID"];
            string Uid = Request.QueryString["Uid"];
            string Status = Request.QueryString["Status"];
            if (ReservationID != "")
            {
                maincontainer.InnerHtml = CutAdmin.VoucherManager.GenrateVoucher(ReservationID, Uid, Status);
            }
        }
    }
}