﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClickUrHotel_Master.Master" AutoEventWireup="true" CodeBehind="about-us.aspx.cs" Inherits="ClickUrHotel.about_us" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
				<section id="themo_html_1" class=" content-editor ">
					<div class="container">
						<div class="row">
							<div class="section-header col-xs-12 centered">
								<h2>Arabian Tours &amp; Travels</h2>
							</div>
							<!-- /.section-header -->
						</div>
						<!-- /.row -->

						<div class="row">
							<div class="col-md-12">
								<p style="text-align: justify">Arabian Tours &amp; Travels was established as a Organizers of Hajj, Umrah and Ziyarat Tours. Since 2001, we have our Head Office in Mumbai (Bombay) India &amp; Branch Offices in UK Bolton, Malawi Blantyre, Canada Toronto and inshallah
									in coming year 2010 we are opening our Branch Offices in South Africa and Pakistan too we mainly concentrate on the tourism package for Hajj &amp; Umrah. Our Organization has increased the quality of the Hajj and Umrah sector by providing
									luxury accommodation, comfortable transportation, delicious foods, and reliable services. We have taken religious travel and raised it to the standards of the International Tourism and Travel Industry.</p>
								<p style="text-align: justify">Arabian Tours &amp; Travels is proud to announce that they are the leaders in their industry of religious tourism. Apart from our competitive rates and convenient locations with a network of hotel in Makkah &amp; Madina. We offer you a
									clean homely environment, all elegantly design with state of the art amenities and backed by friendliness and grace of our staff and personnel.</p>
							</div>
						</div>

					</div>
					<!-- /.container -->
				</section>
</asp:Content>
