﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClickUrHotel_Master.Master" AutoEventWireup="true" CodeBehind="services.aspx.cs" Inherits="ClickUrHotel.services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="themo_showcase_1" class=" showcase ">
					<div class="container">
						<div class="row">
							<div class="section-header col-xs-12 centered">
								<h2>Our Services</h2>
							</div>
							<!-- /.section-header -->
						</div>
						<!-- /.row -->
						<div class="service-blocks">
							<div class="row">
								<div class="service-block col-sm-6 service-block-1">
									<div class="med-icon"><i class="accent glyphicons glyphicons-money"></i></div>
									<h3>Money Exchange</h3>
									<p>Arabian enables you to deal with an array of foreign exchange transactions for every purpose. Be it overseas business or a leisure trip.</p>
								</div>
								<div class="service-block col-sm-6 service-block-2">
									<div class="med-icon"><i class="accent glyphicons glyphicons-nameplate"></i></div>
									<h3>Visas Services</h3>
									<p>Welcome to the new world of fastest and the most secured visa processing medium with an expertise of more than 18&nbsp;years in the Travel Industry.</p>
								</div>
							</div>
							<!-- /.row -->
							<div class="row">
								<div class="service-block col-sm-6 service-block-3">
									<div class="med-icon"><i class="accent glyphicons glyphicons-check"></i></div>
									<h3>Air Ticketing</h3>
									<p>We at Arabian&nbsp;have facilities for ticketing with all International and Domestic Airlines around the globe to provide the best possible services to our clients.</p>
								</div>
								<div class="service-block col-sm-6 service-block-4">
									<div class="med-icon"><i class="accent glyphicons glyphicons-globe"></i></div>
									<h3>Hotel Reservations</h3>
									<p>Arabian Tours &amp; Travels is proud to announce that they are the leaders in their industry of religious tourism. Apart from our competitive rates and convenient locations with a network of hotel in Makkah &amp; Madina. </p>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.service-blocks -->
					</div>
					<!-- /.container -->
				</section>
</asp:Content>
