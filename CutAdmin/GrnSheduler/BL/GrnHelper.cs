﻿using CutAdmin;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrnSheduler.BL
{
    public class GrnHelper : WebClient
    {
        public bool isResponse { get; set; }
        public string session { get; set; }
        public GRNStatusCode objStatusCode { get; set; }
        public GlobalDefault objGlobalDefault { get; set; }
        public CutAdmin.Models.GRNHotels objHotel { get; set; }
      

        public void AvailReq()
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<tbl_GRN_Country> CountryList =(from objCountry in DB.tbl_GRN_Countries select objCountry).ToList();


                foreach (var Country in CountryList)
                {
                    int Status = 0;
                    string RequestHeader;
                    string m_response; ;
                    string ResponseHeader;
                    string TypeOfRequest = "?country=" + Country.Country2Code;
                    ParseGRNResponse ObjParseGRNResponse = new ParseGRNResponse();
                    isResponse = Post(TypeOfRequest, out  m_response, out  RequestHeader, out  ResponseHeader, out Status);
                    if (isResponse)
                      ParseHotel.GetHotels(m_response);

                    Console.WriteLine(ParseHotel.TotalCount);
                }
             
                Console.ReadLine();
            
            }
            catch (Exception ex)
            {
                objStatusCode = null;
                isResponse = false;
            }

        }
    }
}
