﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;

namespace GrnSheduler.BL
{
   public  class WebClient
    {
       public bool Post(string TypeOfRequest, out string m_response, out string RequestHeader, out string ResponseHeader, out int Status)
       {
           m_response = "";
           RequestHeader = "";
           ResponseHeader = "";
           Status = 0;
           try
           {
               var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["GRNAPIURL"] + TypeOfRequest);
               httpWebRequest.ContentType = "application/json";
               httpWebRequest.Method = "GET";
               httpWebRequest.Accept = "application/json";
               httpWebRequest.Headers["api-key"] = ConfigurationManager.AppSettings["GRNAPIKey"];
               //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
               //{
               //    streamWriter.Write(m_data); streamWriter.Flush();
               //    streamWriter.Close();
               //}
               var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
               {
                   m_response = streamReader.ReadToEnd();
              }
               if (m_response.Contains("errors"))
                   return false;
               else
                    return true;
           }
           catch (WebException oWebException)
           {
               if (oWebException.Response != null)
               {
                   using (var oResponseStream = oWebException.Response.GetResponseStream())
                   {
                       var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                       System.IO.StreamReader sr = new System.IO.StreamReader(response);
                       m_response = sr.ReadToEnd();
                       ResponseHeader = "Content-Type:" + oWebException.Response.ContentType + " Content-Length:" + oWebException.Response.ContentLength;
                   }
               }
               return false;
           }
           catch (Exception oException)
           {
               m_response = oException.Message;
               return false;
           }
       }

    }
}
