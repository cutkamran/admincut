﻿using CutAdmin;
using CutAdmin.BL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace GrnSheduler.BL
{
   public class ParseHotel
   {
       public  static Int64 TotalCount { get; set; }
       public static void GetHotels(string Response)
       {
           int i = 0;
           try
           {
               DBHandlerDataContext DB = new DBHandlerDataContext();
               List<tbl_GRN_Hotel> ListHotel = new List<tbl_GRN_Hotel>();
               Int64 HotelCount = 0;
               var Respose = JsonConvert.DeserializeObject<dynamic>(Response);
               HotelCount = Convert.ToInt64(Respose.total.Value);
               //string hcode = Respose.hotels[i].Value.ToString();
               TotalCount += HotelCount;
               for (i = 0; i < HotelCount; i++)
               {
                   string hcode = Respose.hotels[i].code.Value.ToString();
                   // var Maincode = Respose.hotels[i].code;
                   var MainName = Respose.hotels[i].name;
                   var MainCategory = Respose.hotels[i].category;
                   var Mainpostalcode = Respose.hotels[i].postal_code;
                   var Maindescription = Respose.hotels[i].description;
                   var Maincountry = Respose.hotels[i].country;
                   var Maincity_code = Respose.hotels[i].city_code;
                   var Mainfacilities = Respose.hotels[i].facilities;
                   var Mainlongitude = Respose.hotels[i].longitude;
                   var Mainlatitude = Respose.hotels[i].latitude;
                   var Maindest_code = Respose.hotels[i].dest_code;
                   var Mainaddress = Respose.hotels[i].address;
                   string hname = "";
                   if (MainName != null)
                   {
                       hname = MainName.Value.ToString();
                   }

                   //if (Maincode != null)
                   //{
                   //    string hcode = Maincode.Value.ToString();
                   //}
                   string hpostalcode = "";
                   if (Mainpostalcode != null)
                   {
                       hpostalcode = Mainpostalcode.Value.ToString();
                   }

                   string hdescription = "";
                   if (Maindescription != null)
                   {
                       hdescription = Maindescription.Value.ToString();
                   }

                   string hcountry = "";
                   if (Maincountry != null)
                   {
                       hcountry = Maincountry.Value.ToString();
                   }

                   string hcity_code = "";
                   if (Maincity_code != null)
                   {
                       hcity_code = Maincity_code.Value.ToString();
                   }

                   string hcategory = "";
                   if (MainCategory != null)
                   {
                       hcategory = MainCategory.Value.ToString();
                   }

                   string hfacilities = "";
                   if (Mainfacilities != null)
                   {
                       hfacilities = Mainfacilities.Value.ToString();
                   }

                   string hlongitude = "";
                   if (Mainlongitude != null)
                   {
                       hlongitude = Mainlongitude.Value.ToString();
                   }

                   string hlatitude = "";
                   if (Mainlatitude != null)
                   {
                       hlatitude = Mainlatitude.Value.ToString();
                   }

                   string hdest_code = "";
                   if (Maindest_code != null)
                   {
                       hdest_code = Maindest_code.Value.ToString();
                   }


                   string haddress = "";
                   if (Mainaddress != null)
                   {
                       haddress = Mainaddress.Value.ToString();
                   }

                   //var OldHotelscount = (from obj in DB.tbl_GRN_Hotels where obj.code == hcode select obj).ToList().Count();

                   if (0 == 0)
                   {
                       ListHotel.Add(new tbl_GRN_Hotel
                       {
                           name = hname,
                           postal_code = hpostalcode,
                           description = hdescription,
                           country = hcountry,
                           city_code = hcity_code,
                           category = hcategory,
                           facilities = hfacilities,
                           longitude = hlongitude,
                           latitude = hlatitude,
                           dest_code = hdest_code,
                           code = hcode,
                           address = haddress,
                           sid = 0,
                       });


                   }
               }
               DB.tbl_GRN_Hotels.InsertAllOnSubmit(ListHotel);
               DB.SubmitChanges();
           }
           catch
           {

           }
          
       }

                

       }
    }

