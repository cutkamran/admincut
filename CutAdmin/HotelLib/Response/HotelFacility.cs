﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class HotelFacility    
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int Count { get; set; }
    }
}
