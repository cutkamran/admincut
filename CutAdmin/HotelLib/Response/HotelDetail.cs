﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelLib.Response
{
    public class HotelDetail
    {
        public string AvailToken{get;set;}
        public string DateTo{get;set;}
        public string DateFrom{get;set;}
        public string Code { get; set; }
        public string Name { get; set; }
        public string ServiceType { get; set; }
        public string HotelType { get; set; }
        public string Description { get; set; }
        public List<Image> Image { get; set; }
        public float Price { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public float UserRating { get; set; }
        public int Review { get; set; }
        public string Category { get; set; }
        public int CategoryType { get; set; }
        public List<String> Facility { get; set; }
        public List<String> Location { get; set; }
        public string Langitude { get; set; }
        public string Latitude { get; set; }
        public string ChildAgeTo { get; set; }
        public string ChildAgeFrom { get; set; }
        public string Currency { get; set; }
        public List<AvailableRooms> Rooms { get; set; }
        public List<Contract> ContractList { get; set; }
        public string DestinationCode { get; set; }
        public string DestionationType { get; set; }
        public string Address { get; set; }
        public List<RoomOccupancy> sRooms { get; set; }
        public string HotelID { get; set; }
        public string Supplier { get; set; }
    }
}