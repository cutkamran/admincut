﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class DiscountList
    {
        public int UnitCount { get; set; }
        public int PaxCount { get; set; }
        public float Amount { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Description { get; set; }
    }
}
