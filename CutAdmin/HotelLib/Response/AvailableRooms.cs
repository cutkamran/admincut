﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Response
{
    public class AvailableRooms
    {
        public HotelOccupancy sHotelOccupancy { get; set; }
        public HotelRoom sHotelRoom { get; set; }
    }
}
