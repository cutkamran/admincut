﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HotelLib.Response
{
    public class Board
    {
        public string type { get; set; }
        public string code { get; set; }
        public string shortname { get; set; }
        public string text { get; set; }
    }
}
