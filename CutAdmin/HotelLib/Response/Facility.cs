﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelLib.Response
{
    public class Facility
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}