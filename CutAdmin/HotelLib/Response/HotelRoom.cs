﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Response
{
    public class HotelRoom
    {
        public string onRequest { get; set; }
        public string availCount { get; set; }
        public string SHRUI { get; set; }
        public Board sBoard { get; set; }
        public RoomType sRoomType { get; set; }
        public float sPrice { get; set; }   
        public string Roomdate { get; set; }
        public string Roomtime { get; set; }
        public CancellationPolicy sCancellationPolicy { get; set; }
        public List<HotelRoomExtraInfo> HotelRoomExtraInfo { get; set; }
        public List<Facility> RoomFacility { get; set; }
    }
}
