﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Holder
    {
        public string type { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
