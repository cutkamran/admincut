﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace HotelLib.Response
{
    public class CancellationPolicy
    {
        public List<float> Amount { get; set; }
        public DateTime NoChargeDate { get; set; }
        public List<DateTime> ChargeDate { get; set; }
        public string sNoChargeDate { get; set; }
        public List<string> sChargeDate { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
