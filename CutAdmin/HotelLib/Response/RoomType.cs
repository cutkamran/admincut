﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HotelLib.Response
{
    public class RoomType
    {
        public string type { get; set; }
        public string code { get; set; }
        public string characteristic { get; set; }
        public string text { get; set; }
    }
}
