﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HotelLib.Response
{
    public class Image
    {
        public string Type { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
    }
}
