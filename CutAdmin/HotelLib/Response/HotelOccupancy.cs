﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HotelLib.Response
{
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }
        public Occupancy sOccupancy { get; set; }
    }
}
