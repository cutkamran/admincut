﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class ServiceResponse
    {
        public string PurchaseToken { get; set; }
        public string timeToExpiration { get; set; }
        public string Status { get; set; }
        public Agency Agency { get; set; }
        public string Language { get; set; }
        public string CreationUser { get; set; }
        public string SPUI { get; set; }
        public List<Service> Service { get; set; }
        public string CurrencyCode { get; set; }
        public float TotalPrice { get; set; }
    }
}
