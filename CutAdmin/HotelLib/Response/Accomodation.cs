﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelLib.Response
{
    public class Accomodation
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}