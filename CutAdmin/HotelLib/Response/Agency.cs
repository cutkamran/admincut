﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Agency
    {
        public int Code { get; set; }
        public int Branch { get; set; }
    }
}
