﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class PurchaseConfirmResponse
    {
        public string PurchaseToken { get; set; }
        public string timeToExpiration { get; set; }
        public Reference Reference { get; set; }
        public string Status { get; set; }
        public Agency Agency { get; set; }
        public string Language { get; set; }
        public string CreationDate { get; set; }
        public string CreationUser { get; set; }
        public Holder Holder { get; set; }
        public string AgencyReference { get; set; }
        public List<Service> Service { get; set; }
        public string CurrencyCode { get; set; }
        public PaymentData PaymentData { get; set; }
        public float TotalPrice { get; set; }
    }
}
