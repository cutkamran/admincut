﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Service
    {
        public string type { get; set; }
        public string SPUI { get; set; }
        public Reference Reference { get; set; }
        public string Status { get; set; }
        public Contract Contract { get; set; }
        public Supplier Supplier { get; set; }
        //public List<Comments> Comments { get; set; }
        public Comments Comments { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public Currency CurrencyCode { get; set; }
        public float TotalAmount { get; set; }
        public List<DiscountList> DiscountList { get; set; } //or SupplementList
        public List<AdditionalCostList> AdditionalCostList { get; set; }
        public List<string> ModificationPolicyList { get; set; }
        public HotelInfo HotelInfo { get; set; }
        public List<AvailableRooms> AvailableRooms { get; set; }
    }
}
