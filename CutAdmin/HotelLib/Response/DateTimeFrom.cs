﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace HotelLib.Response
{
    public class DateTimeFrom
    {
        public string date { get; set; }
        public string time { get; set; }
    }
}
