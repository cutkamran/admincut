﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Supplier
    {
        public string Name { get; set; }
        public string VatNumber { get; set; }
    }
}
