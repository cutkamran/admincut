﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class PriceList
    {
        public DateTime Date { get; set; }
        public float Price { get; set; }
    }
}
