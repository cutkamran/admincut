﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Reference
    {
        public string FileNumber { get; set; }
        public int IncomingOfficeCode { get; set; }
    }
}
