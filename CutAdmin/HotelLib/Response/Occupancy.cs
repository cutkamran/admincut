﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Response
{
    public class Occupancy
    {
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public List<GuestList> GuestList { get; set; }
    }
}
