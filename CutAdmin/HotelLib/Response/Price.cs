﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HotelLib.Response
{
    public class Price
    {
        public float Amount { get; set; }
        public DateTimeFrom sDateTimeFrom { get; set; }
    }
}
