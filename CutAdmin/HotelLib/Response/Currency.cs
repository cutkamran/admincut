﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class Currency
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}
