﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Response
{
    public class RoomOccupancy
    {
        public string RoomID { get; set; }
        public List<Room> sRoom { get; set; }
        public float RoomPrice { get; set; }
        public float CUTRoomPrice { get; set; }
        public float AgentMarkup { get; set; }
        public float StaffMarkup { get; set; }
    }
}
