﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Request
{
    public class Credential
    {
        [XmlElement(IsNullable=false)]
        public string User { get; set; }
        [XmlElement(IsNullable = false)]
        public string Password { get; set; }
    }
}
