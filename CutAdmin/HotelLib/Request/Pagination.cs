﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Request
{
    
    public class Pagination
    {
        [XmlAttribute]
        public int pageNumber { get; set; }
        [XmlAttribute]
        public int itemsPerPage { get; set; }
    }
}
