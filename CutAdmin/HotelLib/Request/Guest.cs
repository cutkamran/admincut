﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelLib.Request
{
    public class Guest
    {
        public string ID { get; set; }
        public int RoomNumber { get; set; }
        public List<Customer> sCustomer { get; set; }
    }
}
