﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Request
{
    public class Date
    {
        [XmlAttribute]
        public string date { get; set; }
    }
}
