﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.Xml;
namespace HotelLib.Request
{
    public class HotelValuedAvailRQ : Common.WebClient
    {
        public string Language { get; set; }
        public string SessionID { get; set; }
        public string EchoToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int PageNumber { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationType { get; set; }
        public List<HotelOccupancy> HotelOccupancy { get; set; }
        public HotelCodeList sHotelCodeList { get; set; }
        public string ShowCancellationPolicies { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            string result = "999";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("HotelValuedAvailRQ");
                textWriter.WriteAttributeString("echoToken", EchoToken);
                textWriter.WriteAttributeString("sessionId", SessionID);
                textWriter.WriteAttributeString("version", "2013/12");
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages HotelValuedAvailRQ.xsd");

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue(Language);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Credentials");

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteStartElement("PaginationData");
                textWriter.WriteAttributeString("pageNumber", PageNumber.ToString());
                //textWriter.WriteAttributeString("itemsPerPage", "999");
                textWriter.WriteAttributeString("itemsPerPage", result);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("CheckInDate");
                textWriter.WriteAttributeString("date", CheckInDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("CheckOutDate");
                textWriter.WriteAttributeString("date", CheckOutDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("ShowCancellationPolicies");
                textWriter.WriteValue("Y");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Destination");
                textWriter.WriteAttributeString("code", DestinationCode);
                textWriter.WriteAttributeString("type", DestinationType);
                textWriter.WriteEndElement();

                if (sHotelCodeList != null)
                {
                    textWriter.WriteStartElement("HotelCodeList");
                    textWriter.WriteAttributeString("withinResults", sHotelCodeList.withinResults);
                    textWriter.WriteStartElement("ProductCode");
                    textWriter.WriteValue(sHotelCodeList.ProductCode);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();
                }
                //List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
                textWriter.WriteStartElement("OccupancyList");
                int RoomCount = 0;
                int ChildCount = 0;
                //int AdultCount = 0;
                //var Occupancy_List = new List<HotelOccupancy>(); ;
                foreach (HotelOccupancy objHotelOccupancy in HotelOccupancy)
                {
                    //RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                    RoomCount = objHotelOccupancy.RoomCount;

                    ChildCount = objHotelOccupancy.ChildCount;
                    //AdultCount = AdultCount + objHotelOccupancy.AdultCount;
                }
                //int j = 0;
                //int k = 0;
                List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
                List<HotelOccupancy> DISTINCT_OCCUPANCY = new List<HotelOccupancy>();

                List<Occupancy> IDENTICAL_OCCUPANCY = new List<Occupancy>();
                foreach (HotelOccupancy Check_Duplicate_Occupancy in HotelOccupancy)
                {
                    //var Occupancy_List = DISTINCT_OCCUPANCY.Where(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount && data.GuestList == Check_Duplicate_Occupancy.GuestList).FirstOrDefault();
                    //int j = 0;
                    if (!DISTINCT_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount && data.GuestList.Count == Check_Duplicate_Occupancy.GuestList.Count))
                        DISTINCT_OCCUPANCY.Add(new HotelOccupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount, GuestList = Check_Duplicate_Occupancy.GuestList });
                }
                foreach (HotelOccupancy objHotelOccupancy in DISTINCT_OCCUPANCY)
                {
                    textWriter.WriteStartElement("HotelOccupancy");

                    textWriter.WriteStartElement("RoomCount");
                    textWriter.WriteValue(objHotelOccupancy.RoomCount);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Occupancy");

                    textWriter.WriteStartElement("AdultCount");
                    textWriter.WriteValue(objHotelOccupancy.AdultCount);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("ChildCount");
                    textWriter.WriteValue(objHotelOccupancy.ChildCount);
                    textWriter.WriteEndElement();

                    if (objHotelOccupancy.GuestList != null && objHotelOccupancy.GuestList.Count > 0)
                    {
                        textWriter.WriteStartElement("GuestList");
                        foreach (Customer objCustomer in objHotelOccupancy.GuestList)
                        {
                            textWriter.WriteStartElement("Customer");
                            textWriter.WriteAttributeString("type", objCustomer.type);

                            textWriter.WriteStartElement("Age");
                            textWriter.WriteValue(objCustomer.Age);
                            textWriter.WriteEndElement();

                            textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();

                /*textWriter.WriteStartElement("ExtraParamList");

                textWriter.WriteStartElement("ExtendedData");
                textWriter.WriteAttributeString("type", "EXT_ADDITIONAL_PARAM");
                textWriter.WriteStartElement("Name");
                textWriter.WriteValue("PARAM_KEY_PRICE_BREAKDOWN");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Value");
                textWriter.WriteValue("Y");
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();
                textWriter.WriteEndElement();*/

                /*textWriter.WriteStartElement("ShowonRequest");
                textWriter.WriteValue("Y");
                textWriter.WriteEndElement();*/

                textWriter.WriteEndElement();
                textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
