﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HotelLib.Request
{
    public class ServiceAddRequest : Common.WebClient
    {
        public string Language { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationType { get; set; }
        public string AvailToken { get; set; }
        public string ServiceType { get; set; }
        public List<Response.Contract> ContractList { get; set; }
        public string HotelType { get; set; }
        public string HotelCode { get; set; }
        public List<Guest> CustomerList { get; set; }
        public string BoardType { get; set; }
        public string BoardCode { get; set; }
        public string RoomType { get; set; }
        public string RoomCode { get; set; }
        public string RoomCharacteristic { get; set; }
        public int RoomCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public string onRequest { get; set; }
        public string RoomID { get; set; }
        public string EchoToken { get; set; }
        public string Currency { get; set; }
        public List<HotelLib.Response.Room> HotelOccupancy { get; set; }
        public string GenerateXML(List<Guest> lstGuests)
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("ServiceAddRQ");
                textWriter.WriteAttributeString("echoToken", EchoToken);
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("version", "2013/12");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd");

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue(Language);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Credentials");

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Service");
                textWriter.WriteAttributeString("availToken", AvailToken);
                textWriter.WriteAttributeString("xsi:type", ServiceType);

                textWriter.WriteStartElement("ContractList");
                foreach (Response.Contract contract in ContractList)
                {
                    textWriter.WriteStartElement("Contract");

                    textWriter.WriteStartElement("Name");
                    textWriter.WriteValue(contract.Name);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("IncomingOffice");
                    textWriter.WriteAttributeString("code", contract.Code);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("DateFrom");
                textWriter.WriteAttributeString("date", CheckInDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("DateTo");
                textWriter.WriteAttributeString("date", CheckOutDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Currency");
                textWriter.WriteAttributeString("code", Currency);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("HotelInfo");
                textWriter.WriteAttributeString("xsi:type", HotelType);
                textWriter.WriteStartElement("Code");
                textWriter.WriteValue(HotelCode);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("Destination");
                textWriter.WriteAttributeString("code", DestinationCode);
                textWriter.WriteAttributeString("type", DestinationType);
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();

                //.......BBB..under test by maqsood............//

                int RoomCount = 0;
                int ChildCount = 0;
                //int AdultCount = 0;
                //var Occupancy_List = new List<HotelOccupancy>(); ;
                foreach (HotelLib.Response.Room objHotelOccupancy in HotelOccupancy)
                {
                    //RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                    RoomCount = objHotelOccupancy.RoomCount;

                    ChildCount = objHotelOccupancy.ChildCount;
                    //AdultCount = AdultCount + objHotelOccupancy.AdultCount;
                }
                //int j = 0;
                //int k = 0;
                List<HotelLib.Response.Room> DISTINCT_OCCUPANCY = new List<HotelLib.Response.Room>();
                foreach (HotelLib.Response.Room Check_Duplicate_Occupancy in HotelOccupancy)
                {
                    //var Occupancy_List = DISTINCT_OCCUPANCY.Where(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount && data.GuestList == Check_Duplicate_Occupancy.GuestList).FirstOrDefault();
                    //int j = 0;
                    if (!DISTINCT_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                        //DISTINCT_OCCUPANCY.Add(new HotelLib.Response.Room { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount});
                        DISTINCT_OCCUPANCY.Add(Check_Duplicate_Occupancy);
                }
                int z = 0;
                foreach (HotelLib.Response.Room objHotelOccupancy in DISTINCT_OCCUPANCY)
                {
                    textWriter.WriteStartElement("AvailableRoom");

                    textWriter.WriteStartElement("HotelOccupancy");

                    textWriter.WriteStartElement("RoomCount");
                    textWriter.WriteValue(objHotelOccupancy.RoomCount);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Occupancy");

                    textWriter.WriteStartElement("AdultCount");
                    textWriter.WriteValue(objHotelOccupancy.AdultCount);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("ChildCount");
                    textWriter.WriteValue(objHotelOccupancy.ChildCount);
                    textWriter.WriteEndElement();

                    if (objHotelOccupancy.ChildCount != null && objHotelOccupancy.ChildCount > 0)
                    {
                        textWriter.WriteStartElement("GuestList");
                        List<Customer> List_Customer = new List<Customer>();
                        if (lstGuests[z].ID == objHotelOccupancy.SHRUI)
                        {
                            List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                            foreach (Customer objCustomer in List_Customer)
                            {
                                if (objCustomer.type == "CH")
                                {
                                    textWriter.WriteStartElement("Customer");
                                    textWriter.WriteAttributeString("type", objCustomer.type);

                                    textWriter.WriteStartElement("Age");
                                    textWriter.WriteValue(objCustomer.Age);
                                    textWriter.WriteEndElement();
                                    textWriter.WriteEndElement();
                                }
                            }
                        }
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("HotelRoom");
                    textWriter.WriteStartElement("Board");
                    textWriter.WriteAttributeString("code", objHotelOccupancy.Boardcode);
                    textWriter.WriteAttributeString("type", objHotelOccupancy.Boardtype);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("RoomType");
                    textWriter.WriteAttributeString("characteristic", objHotelOccupancy.Roomcharacteristic);
                    textWriter.WriteAttributeString("code", objHotelOccupancy.Roomcode);
                    textWriter.WriteAttributeString("type", objHotelOccupancy.Roomtype);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//for ending AvailableRoom
                    z++;
                }
                /////////////////////////////////////textWriter.WriteEndElement();

                #region comment existing
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //int k = 0;

                //foreach (HotelLib.Response.Room objHotelOccupancy in HotelOccupancy)
                //{
                //    //var Occupancy_List = DISTINST_OCCUPANCY.Where(data => data.AdultCount == objHotelOccupancy.AdultCount && data.ChildCount == objHotelOccupancy.ChildCount).ToList();
                //    //if (!Occupancy_List.Exists(data => data.RoomCount == objHotelOccupancy.RoomCount && data.AdultCount == objHotelOccupancy.AdultCount && data.ChildCount == objHotelOccupancy.ChildCount))
                //    //{
                //    if (HotelOccupancy.Count == 4 && (HotelOccupancy[0].ChildCount != HotelOccupancy[1].ChildCount) && (HotelOccupancy[0].ChildCount != HotelOccupancy[2].ChildCount) && (HotelOccupancy[0].ChildCount != HotelOccupancy[3].ChildCount))
                //    {
                //        textWriter.WriteStartElement("AvailableRoom");

                //        textWriter.WriteStartElement("HotelOccupancy");

                //        textWriter.WriteStartElement("RoomCount");
                //        textWriter.WriteValue(objHotelOccupancy.RoomCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("Occupancy");

                //        textWriter.WriteStartElement("AdultCount");
                //        textWriter.WriteValue(objHotelOccupancy.AdultCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("ChildCount");
                //        textWriter.WriteValue(objHotelOccupancy.ChildCount);
                //        textWriter.WriteEndElement();

                //        if (objHotelOccupancy.ChildCount != null && objHotelOccupancy.ChildCount > 0)
                //        {
                //            textWriter.WriteStartElement("GuestList");
                //            //under test//
                //            List<Customer> List_Customer = new List<Customer>();
                //            //int z = 0;
                //            //foreach (var objGuest in lstGuests)
                //            //{
                //            if (lstGuests[z].ID == objHotelOccupancy.SHRUI)
                //            {
                //                List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                //                foreach (Customer objCustomer in List_Customer)
                //                {
                //                    if (objCustomer.type == "CH")
                //                    {
                //                        textWriter.WriteStartElement("Customer");
                //                        textWriter.WriteAttributeString("type", objCustomer.type);

                //                        textWriter.WriteStartElement("Age");
                //                        textWriter.WriteValue(objCustomer.Age);
                //                        textWriter.WriteEndElement();



                //                        textWriter.WriteEndElement();
                //                    }
                //                }
                //                //z++;
                //            }
                //            //}
                //            //under test//
                //            textWriter.WriteEndElement();
                //        }
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("HotelRoom");
                //        textWriter.WriteStartElement("Board");
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Boardcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Boardtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteStartElement("RoomType");
                //        textWriter.WriteAttributeString("characteristic", objHotelOccupancy.Roomcharacteristic);
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Roomcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Roomtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();//for ending AvailableRoom
                //    }
                //    if (HotelOccupancy.Count == 3 && (HotelOccupancy[0].ChildCount != HotelOccupancy[1].ChildCount) && (HotelOccupancy[0].ChildCount != HotelOccupancy[2].ChildCount))
                //    {
                //        textWriter.WriteStartElement("AvailableRoom");

                //        textWriter.WriteStartElement("HotelOccupancy");

                //        textWriter.WriteStartElement("RoomCount");
                //        textWriter.WriteValue(objHotelOccupancy.RoomCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("Occupancy");

                //        textWriter.WriteStartElement("AdultCount");
                //        textWriter.WriteValue(objHotelOccupancy.AdultCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("ChildCount");
                //        textWriter.WriteValue(objHotelOccupancy.ChildCount);
                //        textWriter.WriteEndElement();

                //        if (objHotelOccupancy.ChildCount != null && objHotelOccupancy.ChildCount > 0)
                //        {
                //            textWriter.WriteStartElement("GuestList");
                //            //under test//
                //            List<Customer> List_Customer = new List<Customer>();
                //            //int z = 0;
                //            //foreach (var objGuest in lstGuests)
                //            //{
                //            if (lstGuests[z].ID == objHotelOccupancy.SHRUI)
                //            {
                //                List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                //                foreach (Customer objCustomer in List_Customer)
                //                {
                //                    if (objCustomer.type == "CH")
                //                    {
                //                        textWriter.WriteStartElement("Customer");
                //                        textWriter.WriteAttributeString("type", objCustomer.type);

                //                        textWriter.WriteStartElement("Age");
                //                        textWriter.WriteValue(objCustomer.Age);
                //                        textWriter.WriteEndElement();



                //                        textWriter.WriteEndElement();
                //                    }
                //                }
                //                //z++;
                //            }
                //            //}
                //            //under test//
                //            textWriter.WriteEndElement();
                //        }
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("HotelRoom");
                //        textWriter.WriteStartElement("Board");
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Boardcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Boardtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteStartElement("RoomType");
                //        textWriter.WriteAttributeString("characteristic", objHotelOccupancy.Roomcharacteristic);
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Roomcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Roomtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();//for ending AvailableRoom
                //    }
                //    if (HotelOccupancy.Count == 2 && (HotelOccupancy[0].ChildCount != HotelOccupancy[1].ChildCount))
                //    {
                //        textWriter.WriteStartElement("AvailableRoom");

                //        textWriter.WriteStartElement("HotelOccupancy");

                //        textWriter.WriteStartElement("RoomCount");
                //        textWriter.WriteValue(objHotelOccupancy.RoomCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("Occupancy");

                //        textWriter.WriteStartElement("AdultCount");
                //        textWriter.WriteValue(objHotelOccupancy.AdultCount);
                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("ChildCount");
                //        textWriter.WriteValue(objHotelOccupancy.ChildCount);
                //        textWriter.WriteEndElement();

                //        if (objHotelOccupancy.ChildCount != null && objHotelOccupancy.ChildCount > 0)
                //        {
                //            textWriter.WriteStartElement("GuestList");
                //            //under test//
                //            List<Customer> List_Customer = new List<Customer>();
                //            //int z = 0;
                //            //foreach (var objGuest in lstGuests)
                //            //{
                //            if (lstGuests[z].ID == objHotelOccupancy.SHRUI)
                //            {
                //                List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                //                foreach (Customer objCustomer in List_Customer)
                //                {
                //                    if (objCustomer.type == "CH")
                //                    {
                //                        textWriter.WriteStartElement("Customer");
                //                        textWriter.WriteAttributeString("type", objCustomer.type);

                //                        textWriter.WriteStartElement("Age");
                //                        textWriter.WriteValue(objCustomer.Age);
                //                        textWriter.WriteEndElement();



                //                        textWriter.WriteEndElement();
                //                    }
                //                }
                //                //z++;
                //            }
                //            //}
                //            //under test//
                //            textWriter.WriteEndElement();
                //        }
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();

                //        textWriter.WriteStartElement("HotelRoom");
                //        textWriter.WriteStartElement("Board");
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Boardcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Boardtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteStartElement("RoomType");
                //        textWriter.WriteAttributeString("characteristic", objHotelOccupancy.Roomcharacteristic);
                //        textWriter.WriteAttributeString("code", objHotelOccupancy.Roomcode);
                //        textWriter.WriteAttributeString("type", objHotelOccupancy.Roomtype);
                //        textWriter.WriteEndElement();
                //        textWriter.WriteEndElement();

                //        textWriter.WriteEndElement();//for ending AvailableRoom

                //    }
                //    else
                //    {
                //        if (k == 0)
                //        {
                //            textWriter.WriteStartElement("AvailableRoom");
                //            textWriter.WriteStartElement("HotelOccupancy");


                //            textWriter.WriteStartElement("RoomCount");
                //            textWriter.WriteValue(RoomCount);
                //            textWriter.WriteEndElement();

                //            textWriter.WriteStartElement("Occupancy");

                //            textWriter.WriteStartElement("AdultCount");
                //            textWriter.WriteValue(objHotelOccupancy.AdultCount);
                //            textWriter.WriteEndElement();

                //            textWriter.WriteStartElement("ChildCount");
                //            textWriter.WriteValue(objHotelOccupancy.ChildCount);
                //            textWriter.WriteEndElement();

                //            //if (objHotelOccupancy.GuestList != null && objHotelOccupancy.GuestList.Count > 0)
                //            //{
                //            if (Convert.ToInt32(objHotelOccupancy.ChildCount) > 0)
                //            {
                //                textWriter.WriteStartElement("GuestList");
                //            }


                //        }
                //        if (k >= 0)
                //        {
                //            if (Convert.ToInt32(objHotelOccupancy.ChildCount) > 0)
                //            {
                //                //under test//
                //                List<Customer> List_Customer = new List<Customer>();
                //                //int z = 0;
                //                //foreach (var objGuest in lstGuests)
                //                //{
                //                if (lstGuests[z].ID == objHotelOccupancy.SHRUI)
                //                {
                //                    List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                //                    foreach (Customer objCustomer in List_Customer)
                //                    {
                //                        if (objCustomer.type == "CH")
                //                        {
                //                            textWriter.WriteStartElement("Customer");
                //                            textWriter.WriteAttributeString("type", objCustomer.type);

                //                            textWriter.WriteStartElement("Age");
                //                            textWriter.WriteValue(objCustomer.Age);
                //                            textWriter.WriteEndElement();



                //                            textWriter.WriteEndElement();
                //                        }
                //                    }
                //                    //z++;
                //                }
                //                //}
                //                //under test//
                //            }
                //        }
                //        if (k == HotelOccupancy.Count - 1)
                //        {
                //            if (Convert.ToInt32(objHotelOccupancy.ChildCount) > 0)
                //            {
                //                textWriter.WriteEndElement();
                //            }

                //            //}
                //            textWriter.WriteEndElement();

                //            textWriter.WriteEndElement();

                //            textWriter.WriteStartElement("HotelRoom");
                //            textWriter.WriteStartElement("Board");
                //            textWriter.WriteAttributeString("code", objHotelOccupancy.Boardcode);
                //            textWriter.WriteAttributeString("type", objHotelOccupancy.Boardtype);
                //            textWriter.WriteEndElement();
                //            textWriter.WriteStartElement("RoomType");
                //            textWriter.WriteAttributeString("characteristic", objHotelOccupancy.Roomcharacteristic);
                //            textWriter.WriteAttributeString("code", objHotelOccupancy.Roomcode);
                //            textWriter.WriteAttributeString("type", objHotelOccupancy.Roomtype);
                //            textWriter.WriteEndElement();
                //            textWriter.WriteEndElement();

                //            textWriter.WriteEndElement();//for ending AvailableRoom


                //        }
                //        k++;
                //    }
                //    z++;
                //}
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                #endregion comment existing

                #region comment old
                ////.......EEE..under test by maqsood............//
                //foreach (HotelLib.Response.Room objRoom in HotelOccupancy)
                //{
                //    //int sPreviousRoomNumber = 0;

                //    textWriter.WriteStartElement("AvailableRoom");
                //    textWriter.WriteStartElement("HotelOccupancy");
                //    textWriter.WriteStartElement("RoomCount");
                //    textWriter.WriteValue(objRoom.RoomCount);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("Occupancy");

                //    textWriter.WriteStartElement("AdultCount");
                //    textWriter.WriteValue(objRoom.AdultCount);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("ChildCount");
                //    textWriter.WriteValue(objRoom.ChildCount);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("GuestList");
                //    //........code by others................//

                //    //List<Customer> List_Customer = CustomerList.Where(data => data.ID == objRoom.SHRUI).Select(data => data.sCustomer).FirstOrDefault();
                //    List<Customer> List_Customer = new List<Customer>();
                //    //if (!List_Customer.Any())
                //    //{
                //    int z = 0;
                //    //for (int i = 0; i < objRoom.RoomCount; i++)
                //    //{
                //    //List_Customer = CustomerList.Where(data => data.ID == objRoom.SHRUI).Select(data => data.sCustomer).FirstOrDefault();
                //    foreach (var objGuest in lstGuests)
                //    {
                //        if (objGuest.ID == objRoom.SHRUI)
                //        {

                //            //if (sPreviousRoomNumber == 0)
                //            //    sPreviousRoomNumber = objGuest.RoomNumber;
                //            //sPreviousRoomNumber--;
                //            //if (sPreviousRoomNumber != objGuest.RoomNumber)
                //            List_Customer = lstGuests[z].sCustomer;// objGuest.sCustomer;
                //            foreach (Customer objCustomer in List_Customer)
                //            {
                //                //test //
                //                if (objCustomer.type == "CH")
                //                {
                //                    //test //


                //                    textWriter.WriteStartElement("Customer");
                //                    textWriter.WriteAttributeString("type", objCustomer.type);

                //                    textWriter.WriteStartElement("Age");
                //                    textWriter.WriteValue(objCustomer.Age);
                //                    textWriter.WriteEndElement();

                //                    //textWriter.WriteStartElement("Name");
                //                    //textWriter.WriteValue(objCustomer.Name);
                //                    //textWriter.WriteEndElement();

                //                    //textWriter.WriteStartElement("LastName");
                //                    //if (objCustomer.LastName != "")
                //                    //    textWriter.WriteValue(objCustomer.LastName);
                //                    //else
                //                    //    textWriter.WriteValue(objCustomer.Name);
                //                    //textWriter.WriteEndElement();

                //                    textWriter.WriteEndElement();

                //                    //test //
                //                }
                //                //test //
                //            }
                //            z++;
                //        }
                //    }
                //    //}


                //    //}



                //    textWriter.WriteEndElement();
                //    textWriter.WriteEndElement();
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("HotelRoom");
                //    textWriter.WriteAttributeString("SHRUI", objRoom.SHRUI);
                //    textWriter.WriteAttributeString("onRequest", objRoom.onRequest);
                //    textWriter.WriteStartElement("Board");
                //    textWriter.WriteAttributeString("code", objRoom.Boardcode);
                //    textWriter.WriteAttributeString("type", objRoom.Boardtype);
                //    textWriter.WriteEndElement();
                //    textWriter.WriteStartElement("RoomType");
                //    textWriter.WriteAttributeString("characteristic", objRoom.Roomcharacteristic);
                //    textWriter.WriteAttributeString("code", objRoom.Roomcode);
                //    textWriter.WriteAttributeString("type", objRoom.Roomtype);
                //    textWriter.WriteEndElement();
                //    textWriter.WriteEndElement();

                //    textWriter.WriteEndElement();

                //}
                ////........code by others................//
                #endregion comment old

                textWriter.WriteEndElement();

                textWriter.WriteEndElement();
                textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
