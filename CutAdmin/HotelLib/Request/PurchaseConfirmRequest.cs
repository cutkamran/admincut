﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HotelLib.Request
{
    public class PurchaseConfirmRequest : Common.WebClient
    {
        public string Language { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AgencyReference { get; set; }
        public List<Customer> CustomerList { get; set; }
        public string RoomID { get; set; }
        public string EchoToken { get; set; }
        public string PurchaseToken { get; set; }
        public string HolderAge { get; set; }
        public string HolderName { get; set; }
        public string HolderLastName { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("PurchaseConfirmRQ");
                textWriter.WriteAttributeString("echoToken", EchoToken);
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd");

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue(Language);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Credentials");

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteStartElement("ConfirmationData");
                textWriter.WriteAttributeString("purchaseToken", PurchaseToken);
                textWriter.WriteStartElement("Holder");
                textWriter.WriteAttributeString("type", "AD");
                textWriter.WriteStartElement("CustomerId");
                textWriter.WriteValue("Hldr");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("Age");
                //textWriter.WriteValue(HolderAge);
                textWriter.WriteValue("30");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("Name");
                textWriter.WriteValue(HolderName);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("LastName");
                textWriter.WriteValue(HolderLastName);
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("AgencyReference");
                textWriter.WriteValue(AgencyReference);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("ConfirmationServiceDataList");
                textWriter.WriteStartElement("ServiceData");
                textWriter.WriteAttributeString("xsi:type", "ConfirmationServiceDataHotel");
                textWriter.WriteAttributeString("SPUI", RoomID);

                textWriter.WriteStartElement("CustomerList");
                int i = 1;
                foreach (Customer objCustomer in CustomerList)
                {
                    textWriter.WriteStartElement("Customer");
                    textWriter.WriteAttributeString("type", objCustomer.type);

                    textWriter.WriteStartElement("CustomerId");
                    textWriter.WriteValue(i);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Age");
                    textWriter.WriteValue(objCustomer.Age);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Name");
                    textWriter.WriteValue(objCustomer.Name);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("LastName");
                    if (objCustomer.LastName != "")
                        textWriter.WriteValue(objCustomer.LastName);
                    else
                        textWriter.WriteValue(objCustomer.Name);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();

                    i = i + 1;
                }
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();
                textWriter.WriteEndElement();
                textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
