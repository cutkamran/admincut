﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace HotelLib.Request
{
    public class CancelConfirmRequest : Common.WebClient
    {
        public string Language { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EchoToken { get; set; }
        public string FileNumber { get; set; }
        public string IncomingOffice { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();

                textWriter.WriteStartElement("PurchaseCancelRQ");
                textWriter.WriteAttributeString("type", "C");
                textWriter.WriteAttributeString("echoToken", EchoToken);
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd");

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue(Language);
                textWriter.WriteEndElement();


                textWriter.WriteStartElement("Credentials");

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteStartElement("PurchaseReference");

                textWriter.WriteStartElement("FileNumber");
                textWriter.WriteValue(FileNumber);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("IncomingOffice");
                textWriter.WriteAttributeString("code", IncomingOffice);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteEndElement();

                textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
