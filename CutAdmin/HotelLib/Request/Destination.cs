﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace HotelLib.Request
{
    public class Destination
    {
        [XmlAttribute]
        public string code { get; set; }
        [XmlAttribute]
        public string type { get; set; }
    }
}
