﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Facilities
    {
        public Int64 FacilitiesId { get; set; }
        public string FacilitiesName { get; set; }
        public int Count { get; set; }
    }
}
