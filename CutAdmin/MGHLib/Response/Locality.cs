﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Locality
    {
        public Int64 LocalityId { get; set; }
        public string LocalityName { get; set; }
    }
}
