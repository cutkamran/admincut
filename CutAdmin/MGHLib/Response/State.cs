﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class State
    {
        public Int64 StateId { get; set; }
        public string StateName { get; set; }
    }
}
