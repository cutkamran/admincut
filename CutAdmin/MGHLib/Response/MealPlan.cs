﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class MealPlan
    {
        public string code { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string text { get; set; }

    }
}
