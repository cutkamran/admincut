﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RoomCategory
    {
        public Int64 RoomCategoryId { get; set; }
        public string RoomCategoryName { get; set; }
        public Int64 RoomCategoryCapacity { get; set; }
        public List<RoomFacilities> roomFacilities { get; set; }
        public List<Image> MediaList { get; set; }
        //Below tags are only used to save details for room details utility
        public string NoOfChild { get; set; }
        public string ExtraBed { get; set; }
        public bool Offer { get; set; }
        public Int64 PromoRate { get; set; }
        public List<Offers> OfferList { get; set; }
    }
}
