﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RoomFacilities
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }
}
