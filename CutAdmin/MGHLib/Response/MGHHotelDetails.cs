﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class MGHHotelDetails
    {
        public Int64 HotelId { get; set; }
        public string HotelName { get; set; }
        public List<ChannelCommission> ChannelComission { get; set; }
        public List<MasterInclusions> MasterInclusions { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public List<Category> Category { get; set; }
        public List<Location> Location { get; set; }
        public Locality Locality { get; set; }
        public State State { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string Currency { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Contact ContactDetails { get; set; }
        public List<Image> MediaList { get; set; }
        public List<Facilities> Facilities { get; set; }
        public string Description { get; set; }
        public List<RoomType> RoomType { get; set; }
        public List<RoomCategory> RoomCategory { get; set; }
        public List<AccomodationPolicies> AccomodationPolicies { get; set; }
        public string PolicyCheckInTime { get; set; }
        public string PolicyCheckOutTime { get; set; }
        public List<Rate> rate { get; set; }
        public int rating { get; set; }
        public float Price { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public List<float> StaffMarkup { get; set; }
        
    }
}
