﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Category
    {
        public Int64 CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int Count { get; set; }
    }
}
