﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RoomType
    {
        public Int64 RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
    }
}
