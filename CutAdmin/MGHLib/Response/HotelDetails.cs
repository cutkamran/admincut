﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class HotelDetails
    {
        public Int64 HotelId { get; set; }
        public string HotelName { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public List<Category> Category { get; set; }
        public List<Location> Location { get; set; }
        public List<Locality> Locality { get; set; }
        public List<State> State { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string Currency { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public List<Contact> ContactDetails { get; set; }
        public List<Image> MediaList { get; set; }
        public List<Facilities> Facilities { get; set; }
        public string Description { get; set; }
        public List<RoomType> RoomType { get; set; }
        public List<RoomCategory> RoomCategory { get; set; }
        public List<Attribute> AccomodationPolicies { get; set; }
        public string PolicyCheckInTime { get; set; }
        public string PolicyCheckOutTime { get; set; }
    }
}
