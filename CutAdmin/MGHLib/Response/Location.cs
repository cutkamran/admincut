﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Location
    {
        public Int64 LocationId { get; set; }
        public string LocationName { get; set; }
        public string Country { get; set; }
        public int Count { get; set; }
    }
}
