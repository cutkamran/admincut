﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class ChannelCommission
    {
        public bool Included { get; set; }
        public string unit { get; set; }
        public float value { get; set; }
    }
}
