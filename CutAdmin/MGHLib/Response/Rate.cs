﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Rate
    {
       public List<RoomCategory> roomCategory { get; set; }
       public List<RoomType> roomType { get; set; }
       public Int64 availCount { get; set; }
       public string rateType { get; set; }
       public string checkin { get; set; }
       public string checkout { get; set; }
       public string taxIncluded { get; set; }
       public List<RoomInfo> roomInfo { get; set; }
       public List<MealPlan> mealPlan { get; set; }
       public List<RefundPolicy> refundPolicy { get; set; }
       public float Price { get; set; }
       public float CUTPrice { get; set; }
       public float AgentMarkup { get; set; }
    }
}
