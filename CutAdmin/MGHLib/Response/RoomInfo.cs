﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RoomInfo
    {
        public int roomNumber { get; set; }
        public List<RoomRate> roomRate { get; set; }
    }
}
