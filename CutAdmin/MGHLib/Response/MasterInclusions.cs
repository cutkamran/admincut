﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class MasterInclusions
    {
        public Int64 InclusionId { get; set; }
        public string InclusionDetails { get; set; }
    }
}
