﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class Attribute
    {
        public string AttributeName { get; set; }
        public string AttributeDetails { get; set; }
    }
}
