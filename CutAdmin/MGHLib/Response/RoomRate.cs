﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RoomRate
    {
        public string type { get; set; }
        public float amount { get; set; }
        public float RoomAmount { get; set; }
        public float CutRoomAmount { get; set; }
        public float AgentRoomMarkup { get; set; }
        public List<RefundPolicy> refundPolicy { get; set; }
    }
}
