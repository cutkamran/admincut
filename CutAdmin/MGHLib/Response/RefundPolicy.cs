﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Response
{
    public class RefundPolicy
    {
        public bool nonRefundable { get; set; }
        public string dayMin { get; set; }
        public string dayMax { get; set; }
        public float deduction { get; set; }
        public string unit { get; set; }

        public float CancellationAmount { get; set; }
        public float CUTCancellationAmount { get; set; }
        public float AgentCancellationMarkup { get; set; }
        public float StaffCancellationMarkup { get; set; }
        public float CutRoomAmount { get; set; }
        public float AgentRoomMarkup { get; set; }
    }
}
