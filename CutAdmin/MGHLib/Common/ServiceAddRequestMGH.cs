﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Common
{
    public class ServiceAddRequestMGH : Common.MGHWebClient
    {

        public string Language { get; set; }
        public string SessionID { get; set; }
        public string EchoToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int PageNumber { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationType { get; set; }

        public string ShowCancellationPolicies { get; set; }
    }
}
