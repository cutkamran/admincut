﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MGHLib.Common
{
    public class MGHWebClient
    {
        public bool Post(string mgh_data, out string mgh_response, out int mgh_status)
        {
            mgh_response = "";
            mgh_status = 0;
            try
            {
                string APIURL = ConfigurationManager.AppSettings["MGHAPIURL"];
                var oRequest = WebRequest.Create(APIURL + mgh_data);
                oRequest.Method = "GET";
                using (var oResponse = oRequest.GetResponse())
                {
                    using (var oResponseStream = oResponse.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        mgh_response = sr.ReadToEnd();
                        mgh_status = 1;
                        return true;
                    }
                }

                //using (var oResponse = oRequest.GetResponse())
                //{
                //    using (var oResponseStream = oResponse.GetResponseStream())
                //    {
                //        var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                //        System.IO.StreamReader sr = new System.IO.StreamReader(response);
                //        m_response = sr.ReadToEnd();
                //        ResponseHeader = "Content-Type:" + oResponse.ContentType + " Content-Length:" + oResponse.ContentLength;
                //        if (m_response.Contains("ErrorList"))
                //        {
                //            Status = 0;
                //            return false;
                //        }
                //        else
                //        {
                //            Status = 1;
                //            return true;
                //        }
                //    }
                //}
            }
            catch (WebException oWebException)
            {
                if (oWebException.Response != null)
                {
                    using (var oResponseStream = oWebException.Response.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        mgh_response = sr.ReadToEnd();
                        mgh_status = 0;
                    }
                }
                return false;
            }
            catch (Exception oException)
            {
                mgh_response = oException.Message;
                mgh_status = 0;
                return false;
            }
        }
    }
}
