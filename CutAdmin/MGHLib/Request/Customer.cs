﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGHLib.Request
{
    public class Customer
    {
        public int Age { get; set; }
        public string type { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
