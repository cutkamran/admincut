﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace MGHLib.Request
{
    public class SearchRequest : Common.MGHWebClient
    {
        public string Language { get; set; }
        public string SessionID { get; set; }
        public string EchoToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int PageNumber { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationType { get; set; }
        public List<HotelOccupancy> HotelOccupancy { get; set; }
        public HotelCodeList sHotelCodeList { get; set; }
        public string ShowCancellationPolicies { get; set; }
        //public string Nationality { get; set; }

        public string GenerateRequest(string session)
        {
            //string m_xml = "";
            string[] array_input = session.Split('_');
            string occpancy = array_input[5].ToString();
            string[] array_occupancy = occpancy.Split('$');
            int adult = 0;
            int child = 0;

            List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Customer> list_Customer = new List<Customer>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        //...............................under test by maqsood...........................................//
                        int Age = Convert.ToInt32(n_array[i]);
                        //...............................under test by maqsood...........................................//
                        if (Age > 0)
                            list_Customer.Add(new Customer { Age = Age, type = "CH" });
                    }
                }
                list_Occupancy.Add(new HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
            }


            int RoomCount = 0;




            string ChildCount = "";
            string AdultCount = "";
            string occupant_child = "";

       
            int j = 0;
            int k = 0;
            List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
            List<HotelOccupancy> DISTINCT_OCCUPANCY = new List<HotelOccupancy>();

            List<Occupancy> IDENTICAL_OCCUPANCY = new List<Occupancy>();
            foreach (HotelOccupancy Check_Duplicate_Occupancy in HotelOccupancy)
            {
              
                if (!DISTINCT_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount && data.GuestList.Count == Check_Duplicate_Occupancy.GuestList.Count))
                    DISTINCT_OCCUPANCY.Add(new HotelOccupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount, GuestList = Check_Duplicate_Occupancy.GuestList });
            }

         

            for (int i = 0; i < list_Occupancy.Count; i++)
            {
                if (i == 0)
                {
                    AdultCount += list_Occupancy[i].AdultCount;
                }
                else
                {
                    AdultCount += "," + list_Occupancy[i].AdultCount;
                }
            }

            for (int i = 0; i < list_Occupancy.Count; i++)
            {


                if (list_Occupancy[i].ChildCount > 0)
                {
                    if (list_Occupancy[i].ChildCount == 1)
                    {
                        occupant_child += list_Occupancy[i].ChildCount + "-" + list_Occupancy[i].GuestList[0].Age;
                    }
                    if (list_Occupancy[i].ChildCount == 2)
                    {
                        occupant_child += list_Occupancy[i].ChildCount + "-" + list_Occupancy[i].GuestList[0].Age + ";" + list_Occupancy[i].GuestList[1].Age;
                    }
                    if (list_Occupancy[i].ChildCount == 3)
                    {
                        occupant_child += list_Occupancy[i].ChildCount + "-" + list_Occupancy[i].GuestList[0].Age + ";" + list_Occupancy[i].GuestList[1].Age + ";" + list_Occupancy[i].GuestList[2].Age;
                    }
                    if (list_Occupancy[i].ChildCount == 4)
                    {
                        occupant_child += list_Occupancy[i].ChildCount + "-" + list_Occupancy[i].GuestList[0].Age + ";" + list_Occupancy[i].GuestList[1].Age + ";" + list_Occupancy[i].GuestList[2].Age + ";" + list_Occupancy[i].GuestList[3].Age;
                    }
                }
                else
                {
                    occupant_child += "0";
                }
                if (i != (list_Occupancy.Count - 1))
                {
                    occupant_child += ",";
                }
            }


            //RoomCount = HotelOccupancy[0].RoomCount;
            RoomCount = HotelOccupancy.Count;
            string UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
            string Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];
            if (DestinationCode == "20570")
            {
                DestinationCode = "20490";
            }
            string mghData = "search&rooms=" + RoomCount + "&token=" + Password + "&location_id=" + DestinationCode + "&locality_id=&facility=1&checkin_date=" + CheckInDate + "&checkout_date=" + CheckOutDate + "&occupant_adult=" + AdultCount + "&occupant_child=" + occupant_child;
            //string mghData = "search&rooms=" + RoomCount + "&token=clickurtrip351087a0ee4089c7clickurtrip&location_id=" + DestinationCode + "&locality_id=&facility=1&checkin_date=" + CheckInDate + "&checkout_date=" + CheckOutDate + "&occupant_adult=" + AdultCount + "&occupant_child=" + occupant_child;
            //m_xml = sw.ToString();
            //mghData = "search&rooms=2&token=8d75e20b9e2e2c96dfc94a284fde249d&location_id=26&locality_id=&facility=1&checkin_date=2015-12-12&checkout_date=2015-12-13&occupant_adult=2,2&occupant_child=0,0";
            return mghData;
        }
    }
}
