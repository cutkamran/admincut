﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="CutAdmin.HotelAdmin.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/Dashboard.js?v=1.15"></script>
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="standard-tabs margin-bottom">
           
            <!--Black Strip -->
                    <div class="dashboard">
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile">
                                <div class="twelve-columns">
                                    <div class="silver">Total Sales</div>
                                    <div class="large-text-shadow align-central">
                                       <h3 class="mid-margin-bottom" id="TotalSale"> </h3>
                                    </div>

                                    <div class="ten-columns" style="border-top: 1.2px solid #cccccc; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);"></div>
                                    <div class="columns mid-margin-top">
                                        <div class="six-columns">
                                            <h4 class="align-left" id="noRooms">0</h4>
                                            <span class="align-left silver">Rooms</span>
                                        </div>
                                        <div class="six-columns">
                                            <h4 class="align-left" id="noNights">0</h4>
                                            <span class="align-left silver">Nights</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <div class="twelve-columns">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile"></div>
                            <div class="three-columns twelve-columns-mobile new-row-mobile">
                                <ul class="stats split-on-mobile">
                                    <li style="padding-bottom:0">
                                        <a href="HotelList.aspx">
                                            <strong>
                                                <lable id="hotel_Count"></lable>
                                            </strong>
                                            <br>
                                            Hotels 
                                        </a>
                                    </li>
                                    <li style="padding-bottom:0">
                                        <a href="SupplierDetails.aspx">
                                            <strong>
                                                <lable id="Agents"></lable>
                                            </strong>
                                            <br>
                                            Suppliers
                                        </a>
                                    </li>
                                     <li style="padding-bottom:0">
                                        <a href="CommissionStatement.aspx">
                                            <strong>
                                                <lable id="Commission">0</lable>
                                            </strong>
                                            <br>
                                            Commisson Reports 
                                        </a>
                                    </li>

                                </ul>
                            </div>

                        </div>

                    </div>
                    <div class="with-padding">
                    <!--First Row -->
                    <div class="columns">
                        <!--Booking Reconfirmations -->
                        <div class="three-columns  six-columns-mobile large-box-shadow orange-bg glossy " id="BookingsReconfirm">
                        </div>
                        <!--On Request Bookings-->
                        <div class="three-columns six-columns-mobile large-box-shadow blue-bg glossy " id="BookingsRequests">
                        </div>
                        <!--Bookings On Hold-->
                        <div class="three-columns six-columns-mobile large-box-shadow green-bg glossy" id="BookingOnHold">
                        </div>
                        <!--Group Request-->
                        <div class="three-columns six-columns-mobile large-box-shadow linen" id="GroupRequest">
                        </div>
                    </div>

                    <!--Second Row -->
                    <div class="columns">
                        <div class="three-columns six-columns-mobile large-box-shadow orange-bg glossy ">
                            <div class="with-mid-padding" style="background-color: darkgoldenrod; border-bottom: 2px solid navajowhite">
                                <h4 class="icon-line-graph icon-size2">Top Buyer</h4>

                            </div>

                            <div class="with-mid-padding" id="div_Buyer">
                            </div>
                        </div>
                        <div class="three-columns six-columns-mobile large-box-shadow blue-bg">
                            <div class="with-mid-padding" style="background-color: steelblue; border-bottom: 2px solid lightblue">
                                <h4 class="icon-link icon-size2">Top Supplier</h4>
                            </div>
                            <div class="with-mid-padding" id="div_Supplier">
                            </div>
                        </div>

                        <div class="three-columns six-columns-mobile large-box-shadow green-bg">
                            <div class="with-mid-padding" style="background-color: darkolivegreen; border-bottom: 2px solid greenyellow">
                                <h4 class="icon-globe icon-size2">Top Destinations</h4>

                            </div>
                            <div class="with-mid-padding" id="div_Destination">
                            </div>
                        </div>
                        <div class="three-columns six-columns-mobile large-box-shadow linen">
                            <div class="with-mid-padding" style="background-color: grey; border-bottom: 2px solid darkgrey">
                                <h4 class="icon-home icon-size2">Top Hotels</h4>
                            </div>
                            <div class="with-mid-padding" id="div_Hotels">
                            </div>
                        </div>

                    </div>
                    <div class="columns datepicker" id="">
                    </div>
                </div>
        </div>
</asp:Content>
