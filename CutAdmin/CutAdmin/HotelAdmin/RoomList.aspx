﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="RoomList.aspx.cs" Inherits="CutAdmin.HotelAdmin.RoomList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../HotelAdmin/Scripts/RoomList.js?v=1.4"></script>

    <!-- Additional styles -->
    <link rel="stylesheet" href="../css/styles/form.css?v=1">
    <link rel="stylesheet" href="../css/styles/switches.css?v=1">
    <link rel="stylesheet" href="../css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
    <hgroup id="main-title" class="thin">
        <h1>Room List</h1>
        <h2 class="addromtxt"><b class="grey" id="HtlName"></b><a onclick="NewRoom()" class="button anthracite-gradient" style="cursor: pointer">Add Room</a></h2>
        <hr />
    </hgroup>
    <div class="with-padding">
        <table class="table responsive-table" id="tbl_RoomList">
            <thead>
                <tr>
                    <th scope="col">Room Type</th>
                    <th scope="col">Max Occupancy</th>
                    <th scope="col" class="align-center">Room Details</th>
                    <th scope="col">Rates</th>
                    <th scope="col">Manage</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- End main content -->
    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="../js/setup.js"></script>
    <link rel="stylesheet" href="../css/styles/modal.css?v=1">
    <script src="../js/developr.modal.js"></script>

    <!-- Template functions -->
    <script src="../js/developr.input.js"></script>
    <script src="../js/developr.navigable.js"></script>
    <script src="../js/developr.notify.js"></script>
    <script src="../js/developr.scroll.js"></script>
    <script src="../js/developr.tooltip.js"></script>
    <script src="../js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="../js/libs/jquery.tablesorter.min.js"></script>
    <script src="../js/libs/DataTables/jquery.dataTables.min.js"></script>

</asp:Content>
