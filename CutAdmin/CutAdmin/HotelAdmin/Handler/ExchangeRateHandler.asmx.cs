﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for ExchangeRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ExchangeRateHandler : System.Web.Services.WebService
    {
        string json = "";
        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //[WebMethod(EnableSession = true)]
        //public string GetExchangeLog()
        //{
        //    objSerializer = new JavaScriptSerializer();
        //    try
        //    {
        //        using (var db = new ClickUrHotel_DBEntities())
        //        {
        //            var arrCodes = db.tbl_CurrencyCodeExchange;
        //            var arrRates = (from objUser in db.tbl_UserCurrency
        //                            join
        //                            objCurrency in db.tbl_CurrencyCodeExchange on objUser.CurrencyID equals objCurrency.ID
        //                            where objUser.ParentID == AccountManager.GetSupplierByUser()
        //                            select new
        //                            {
        //                                objUser.ID,
        //                                objUser.Rate,
        //                                objUser.Markup,
        //                                TotalExchange = Convert.ToSingle(objUser.Rate + objUser.Markup),
        //                                objUser.UpdateDate,
        //                                objUser.UpdateBy,
        //                                objUser.CurrencyID,
        //                                objCurrency.CurrencyCode,
        //                                objCurrency.CurrencyDetails
        //                            }).ToList();

        //            return objSerializer.Serialize(new { retCode = 1, sLastLogs = arrRates, arrCurrences = arrCodes });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
        //    }
        //}

        #region Exchangelog
        [WebMethod(EnableSession = true)]
        public string GetExchangeLog()
        {
            objSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var arrCodes = db.tbl_CurrencyCodeExchange;
                    var arrExchange = CutAdmin.Common.Common.Exchange;
                    return objSerializer.Serialize(new { retCode = 1, sLastLogs = arrExchange, arrCurrences = arrCodes });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(Context).Raise(ex);
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Update Excnage from Online
        [WebMethod(EnableSession = true)]
        public string GetOnlineRate()
        {
            JavaScriptSerializer ocjserialize = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = ExchangeRateManager.GetExchangeRate();
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                return ocjserialize.Serialize(new { retCode = 1, Session = 1 });
            else
                return ocjserialize.Serialize(new { retCode = 0, Session = 1 });
        }
        #endregion

        #region Search Exchange Rate
        [WebMethod(EnableSession = true)]
        public string SearchExchangeUpdate(string currency, string updatedBy, string updateDate)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHandlerDataContext db = new DBHandlerDataContext();
            try
            {
                var data = (from obj in db.tbl_ExchangeRateLogs where obj.UpdateBy == updatedBy select obj).ToList();

                List<string> extractDate = new List<string>();

                for (var i = 0; i < data.Count; i++)
                {
                    extractDate.Add(data[i].UpdateDate.ToString().Split(' ')[0]);
                }

                List<object> exchangeRates = new List<object>();
                if (currency != "All Currency")
                {
                    for (var i = 0; i < extractDate.Count; i++)
                    {
                        if (updateDate == extractDate[i] && data[i].Currency == currency)
                        {
                            exchangeRates.Add(data[i]);
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < extractDate.Count; i++)
                    {
                        if (updateDate == extractDate[i])
                        {
                            exchangeRates.Add(data[i]);
                        }
                    }
                }

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, exchangeRate = exchangeRates });

            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #endregion

        #region Add Update
        [WebMethod(EnableSession = true)]
        public string GetExchangeRate(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            decimal[] Amt, TotalExchange;
            ExchangeRateManager.GetMarkupAmt(ExchangeRate, MarkupAmt, MarkupPer, out Amt, out TotalExchange);
            return jsSerializer.Serialize(new { retCode = 1, Session = 1, MarkupAmt = Amt, ExchangeRate = TotalExchange });
        }
        [WebMethod(EnableSession = true)]
        public string UpdateExchangeRate(Int64[] MarkupSid, Int64[] LogSid, string[] Currency, decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = ExchangeRateManager.UpdateExchangeRate(MarkupSid, LogSid, Currency, ExchangeRate, MarkupAmt, MarkupPer);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

      

        [WebMethod(EnableSession = true)]
        public string AddExchangeRate(tbl_UserCurrency arrRate)
        {
            objSerializer = new JavaScriptSerializer();
            try
            {
                var parentid = Convert.ToInt64(AccountManager.GetSupplierByUser());
                using (var db = new ClickUrHotel_DBEntities())
                {
                    if (db.tbl_UserCurrency.Where(D => D.CurrencyID == arrRate.CurrencyID && D.ParentID == parentid).FirstOrDefault() != null)
                        throw new Exception("Currency Already Added.");
                    var ID = Convert.ToString(AccountManager.GetSupplierByUser());
                    arrRate.UpdateBy = Convert.ToString(AccountManager.GetSupplierByUser()) ;
                    arrRate.UpdateDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm");
                    arrRate.ParentID = AccountManager.GetSupplierByUser();
                    db.tbl_UserCurrency.Add(arrRate);
                    db.SaveChanges();
                }
                return objSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(Context).Raise(ex);
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #endregion
    }
}
