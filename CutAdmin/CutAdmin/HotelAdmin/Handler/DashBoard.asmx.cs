﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for DashBoard
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DashBoard : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        DBHandlerDataContext db = new DBHandlerDataContext();
        ClickUrHotel_DBEntities dbc = new ClickUrHotel_DBEntities();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string GetCount()
        {
            try
            {
                /*if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");

                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = AccountManager.GetUserByLogin();
                //if (objGlobalDefault.UserType != "Supplier")
                //   Uid = objGlobalDefault.ParentId;
                var CommissionReports = (from obj in DB.Comm_CommissionReports select obj).Select(x => x.SupplierID).Distinct().ToList().Count;
                var SupplierList = (from obj in DB.tbl_AdminLogins where obj.ParentID == Uid && obj.UserType == "Supplier" select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CommissionReports = CommissionReports, Supplier = SupplierList });*/
                using (var DB = new AdminDBHandlerDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Hotel = 0, Agent = 0;
                    Int64 Uid = AccountManager.GetUserByLogin();
                    ClickUrHotel_DBEntities Db = new ClickUrHotel_DBEntities();
                    using (var db = new DBHandlerDataContext())
                    {
                        Hotel = (from obj in Db.tbl_CommonHotelMaster  select obj).ToList().Count;
                        Agent = (from obj in db.tbl_AdminLogins where obj.UserType == "Supplier" select obj).ToList().Count;
                    }
                    var arrBookings = Reservations.AdminBookingList();
                    List<Reservation> arrBuyerRevt = new List<Reservation>();
                    List<Reservation> arrSupplierRevt = new List<Reservation>();
                    List<Reservation> arrDestinationsRevt = new List<Reservation>();
                    List<Reservation> arrHotelsRevt = new List<Reservation>();

                    arrBookings.Where(d => d.Source.Contains("HOTELBEDS")).ToList().ForEach(r => r.Source = "HOTELBEDS");
                    foreach (Int64 AgentID in arrBookings.Select(D => D.Uid).ToList().Distinct())
                    {
                        arrBuyerRevt.Add(new Reservation
                        {
                            Buyer = arrBookings.Where(d => d.Uid == AgentID).FirstOrDefault().AgencyName,
                            Count = arrBookings.Where(d => d.Uid == AgentID).Count()
                        });
                    }
                    foreach (string Supplier in arrBookings.Select(D => D.Source).ToList().Distinct())
                    {
                        arrSupplierRevt.Add(new Reservation
                        {
                            Supplier = arrBookings.Where(d => d.Source == Supplier).FirstOrDefault().Source,
                            Count = arrBookings.Where(d => d.Source == Supplier).Count()
                        });
                    }
                    foreach (string Country in arrBookings.Select(D => D.City).ToList().Distinct())
                    {
                        arrDestinationsRevt.Add(new Reservation
                        {
                            Destination = arrBookings.Where(d => d.City == Country).FirstOrDefault().City,
                            Count = arrBookings.Where(d => d.City == Country).Count()
                        });
                    }
                    foreach (string HotelName in arrBookings.Select(D => D.HotelName).ToList().Distinct())
                    {
                        arrHotelsRevt.Add(new Reservation
                        {
                            HotelName = HotelName,
                            Count = arrBookings.Where(d => d.HotelName == HotelName).Count()
                        });
                    }

                    var arrOnRequest = new
                    {
                        OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                        OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                        OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrOnHold = new
                    {
                        OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                        OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                        OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrReconfirmed = new
                    {
                        ReconfirmPending = arrBookings.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().Count,
                        ReconfirmRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().Count,
                        ReconfirmReject = arrBookings.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrGroupRequest = new
                    {
                        GroupRequestPending = arrBookings.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestReject = arrBookings.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().Count,
                    };
                    var CommissionReports = (from obj in DB.Comm_CommissionReports select obj).Select(x => x.SupplierID).Distinct().ToList().Count;
                    IQueryable<tbl_commonRoomDetails> arrRoomDetails = from obj in dbc.tbl_commonRoomDetails select obj;
                    int arrRooms = arrRoomDetails.Count();

                    json = jsSerializer.Serialize(new
                    {
                        Session = 1,
                        retCode = 1,
                        HotelList = Hotel,
                        AgentCount = Agent,
                        arrBuyerRervation = arrBuyerRevt.OrderByDescending(d => d.Count).Take(5),
                        arrSupplierRevt = arrSupplierRevt.OrderByDescending(d => d.Count).Take(5),
                        arrDestinationsRevt = arrDestinationsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrHotelsRevt = arrHotelsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrOnRequest = arrOnRequest,
                        arrOnHold = arrOnHold,
                        arrReconfirmed = arrReconfirmed,
                        arrGroupRequest = arrGroupRequest,
                        arrBookings = arrBookings,
                        CommissionReports = CommissionReports,
                        noRooms = arrBookings.Where(d => d.Status == "Vouchered").Select(d => d.TotalRooms).ToList().Sum(),
                        noNights = arrBookings.Where(d => d.Status == "Vouchered").Select(d => d.noNights).ToList().Sum(),
                        TotalSale = arrBookings.Where(d => d.Status == "Vouchered").Select(d => d.TotalFare).ToList().Sum(),
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelUnApproved()
        {
            try
            {               
                var HotelListCount = (from obj in dbc.tbl_CommonHotelMaster where obj.Approved == false select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelListCount = HotelListCount});
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRoomUnApproved()
        {
            try
            {
                var RoomListCount = (from obj in dbc.tbl_commonRoomDetails where obj.Approved == false select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, RoomListCount = RoomListCount });
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelsData()
        {
            try
            {
                var HotelList = (from obj in dbc.tbl_CommonHotelMaster
                                 join AgName in db.tbl_AdminLogins on obj.ParentID equals AgName.sid
                                 where obj.Approved == false
                                 select new
                                     {
                                         obj.HotelName,
                                         obj.sid,
                                         obj.Approved,
                                         AgName.AgencyName,
                                         obj.ParentID
                                     }).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelList = HotelList });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRoomsData()
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var Admin = DB.tbl_AdminLogins.AsEnumerable();
                var RoomList = (from obj in dbc.tbl_commonRoomDetails
                                join AgName in Admin on obj.CreatedBy equals AgName.sid
                                join Room in dbc.tbl_commonRoomType on obj.RoomTypeId equals Room.RoomTypeID
                                join Hotel in dbc.tbl_CommonHotelMaster on obj.HotelId equals Hotel.sid
                                where obj.Approved == false
                                select new
                                {
                                    obj.RoomId,
                                    obj.RoomTypeId,
                                    obj.Approved,
                                    AgName.AgencyName,
                                    obj.CreatedBy,
                                    Room.RoomType,
                                    Hotel.HotelName,
                                    Hotel.sid
                                }).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Unapproved(Int32 HotelCode)
        {
            try
            {
                ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                using (var Unapproved = db.Database.BeginTransaction())
                {
                    tbl_CommonHotelMaster Delete = dbc.tbl_CommonHotelMaster.Single(x => x.sid == HotelCode);
                    dbc.tbl_CommonHotelMaster.Remove(Delete);
                    dbc.SaveChanges();
                    Unapproved.Commit();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UnapprovedRoom(Int32 RoomId)
        {
            try
            {
                ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                using (var Unapproved = db.Database.BeginTransaction())
                {
                    tbl_commonRoomDetails Delete = dbc.tbl_commonRoomDetails.Single(x => x.RoomId == RoomId);
                    dbc.tbl_commonRoomDetails.Remove(Delete);
                    dbc.SaveChanges();
                    Unapproved.Commit();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingRequests()
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                var arrBookings = Reservations.AdminBookingList();
                var BookingPending = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList().Count;
                var BookingConfirmed = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().Count;
                var BookingRejected = arrBookings.Where(d => d.Status == "Cancelled").ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingPending = BookingPending, BookingConfirmed = BookingConfirmed, BookingRejected = BookingRejected });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingOnHold()
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                var arrBookings = Reservations.AdminBookingList();
                var OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count;
                var OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count;
                var OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count;

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, OnHoldBookings = OnHoldBookings, OnHoldRequested = OnHoldRequested, OnHoldConfirmed = OnHoldConfirmed, arrBookings = arrBookings });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingReconfirmation()
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                var arrBookings = Reservations.AdminBookingList();
                var ReconfirmPending = arrBookings.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().Count;
                var ReconfirmRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().Count;
                var ReconfirmReject = arrBookings.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().Count;

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, ReconfirmPending = ReconfirmPending, ReconfirmRequested = ReconfirmRequested, ReconfirmReject = ReconfirmReject, arrBookings = arrBookings });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingGroupRequest()
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                var arrBookings = Reservations.AdminGroupBookingList();
                var GroupRequestPending = arrBookings.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().Count;
                var GroupRequestRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().Count;
                var GroupRequestReject = arrBookings.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().Count;

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, GroupRequestPending = GroupRequestPending, GroupRequestRequested = GroupRequestRequested, GroupRequestReject = GroupRequestReject, arrBookings = arrBookings });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }
    }
}
