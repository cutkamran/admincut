﻿$(function () {
    LoadBankDetails();
    GetCountryy();
    $('#sel_Nationality').change(function () {
        var sndcountry = $('#sel_Nationality').val();
        GetCity(sndcountry);
    });
    $('#ddlcountry').change(function () {
        var sndcountry = $('#ddlcountry').val();
        GetCity(sndcountry);


    });


    $('#ddlcity').change(function () {
        var sndcity = $('#ddlcity').val();
        for (var i = 0; i < sndcity.length; i++) {

            SetActCities(sndcity[i]);
        }

    });
});

function SetActCities(sndcity) {

    GetActLocation(sndcity);

}
function LoadBankDetails() {
    $("#tbl_BankDetails").dataTable().fnClearTable();
    $("#tbl_BankDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetBankDetails",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
              
            }
            else if (result.retCode == 1) {
                var BankDetails = result.BankDetails;
                console.log(BankDetails);
                var row = '';
                for (var i = 0; i < BankDetails.length; i++) {
                    row += '<tr>'
                    row += '<td class="align-center">' + (i + 1) + '</td>'
                    row += '<td>' + BankDetails[i].BankName + '</td>'
                    row += '<td>' + BankDetails[i].AccountNo + '</td>'
                    row += '<td>' + BankDetails[i].Branch + '</td>'
                    row += '<td>' + BankDetails[i].SwiftCode + '</td>'
                    row += '<td>' + BankDetails[i].IFSCCode + '</td>'
                    row += '<td>' + BankDetails[i].Country + '</td>'
                    row += '<td>' + BankDetails[i].City + '</td>'
                    row += '<td class="align-center"><span class="button-group children-tooltip actiontab">' 
                    row += '<a href="#" class="button" title="Edit" onclick="EditBankDetail(\'' + BankDetails[i].sid + '\',\'' + BankDetails[i].BankName + '\',\'' + BankDetails[i].AccountNo + '\',\'' + BankDetails[i].Branch + '\',\'' + BankDetails[i].SwiftCode + '\',\'' + BankDetails[i].IFSCCode + '\',\'' + BankDetails[i].Country + '\',\'' + BankDetails[i].City + '\')"><span class="icon-pencil"></span></a>'
                    row += '<a href="#" class="button" title="Delete" onclick="DeleteBankDetail(\'' + BankDetails[i].sid + '\')"><span class="icon-trash"></span></a>'
                    row += '</span></td>'
                    row += '</tr>'
                }
                $('#tbl_BankDetails tbody').html(row);
                $("#tbl_BankDetails").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });
}

function GetCountryy() {
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#sel_Nationality").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#sel_Nationality").append(ddlRequest);
                    $("#ddlcountry").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

var contryname;
var cityname;
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();

function GetCity(reccountry) {
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetCity1",
        data: '{"country":"' + reccountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.CityList;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    $("#ddlcity").empty();

                    //var ddlRequest = '<option selected="" value="-">Select Any City</option>';
                    var ddlRequest = "";
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Description + '">' + arrCity[i].Description + '</option>';
                        //cityname = arrCity[i].Description;
                    }
                    $("#selCity").append(ddlRequest);

                    $("#ddlcity").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
        }
    });
}

var arrActLocation;
function GetActLocation(sndcity) {
    //$("#tbl_GetLocation").dataTable().fnClearTable();
    //$("#tbl_GetLocation").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();



    $.ajax({
        url: "../ActivityHandller.asmx/GetLocationByCity",
        type: "post",
        data: '{"city":"' + sndcity + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {

                arrActLocation = result.ArrLocation;
                var ddlRequest = "";
                var ddlRequestt = "";
                if (arrActLocation != 0) {

                    // $("#ddlLocation").empty();
                    //  var ddlRequest = '<option selected="selected" value="-">Select Any Location</option>';
                    for (i = 0; i < arrActLocation.length; i++) {
                        ddlRequest += '<option value="' + arrActLocation[i].LocationName + '">' + arrActLocation[i].Lid + '</option>';
                        ddlRequestt += '<option value="' + arrActLocation[i].LocationName + '">' + arrActLocation[i].LocationName + '</option>';

                        //if ($("#txt_location").val() == arrActLocation[i].Location_Name)
                        //{
                        //    $("#AddLocation").hide();
                        //}
                        //else
                        //{
                        //    $("#AddLocation").show();
                        //}
                        //contryname = arrCountry[i].Countryname;
                    }

                    $("#Select_Location").append(ddlRequest);
                    //$("#txt_PickUpFrom" + ploc + "").append(ddlRequestt);
                    //$("#txt_DropOffAt" + ploc + "").append(ddlRequestt);

                }
            }
        },
    });
}

function AddBankDetails() {
    var BankName = $('#txtBankName').val();
    var AccountNo = $('#txtAccountNo').val();
    var Branch = $('#txtBranch').val();
    var SwiftCode = $('#txtSwiftCode').val();
    var IFSCCode = $('#txtIFSCCode').val();
    var Country = $('#sel_Nationality').val();
    var City = $('#selCity').val();
    if (!BankName) {
        Success('Please enter Bank Name.');
        return false;
    } else if (!AccountNo) {
        Success('Please enter Account No.');
        return false;
    } else if (!Branch) {
        Success('Please enter Branch.');
        return false;
    } else if (!SwiftCode) {
        Success('Please enter Swift Code.');
        return false;
    } else if (!IFSCCode) {
        Success('Please enter IFSC Code.');
        return false;
    }
    else if (Country == '-') {
        Success('Please select country.');
        return false;
    } else if (City == '-') {
        Success('Please select City.');
        return false;
    }

    var data = {
        BankName: BankName,
        AccountNo: AccountNo,
        Branch: Branch,
        SwiftCode: SwiftCode,
        IFSCCode:IFSCCode,
        Country: Country,
        City: City
    }

    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/AddBankDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
              
            }
            else if (result.retCode == 1) {
                Success("Bank details saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

var ID = "";

function EditBankDetail(sid, BankName, AccountNo, Branch, SwiftCode, IFSCCode, Country, City) {
    ID = sid;
    $('#txtBankName').val(BankName);
    $('#txtAccountNo').val(AccountNo);
    $('#txtBranch').val(Branch);
    $('#txtSwiftCode').val(SwiftCode);
    $('#txtIFSCCode').val(IFSCCode);
    GetNationality(Country);
    GetCity(Country);
    setTimeout(function () {
        BindCity(City);
    }, 2000);

    $('#btnAddBankDetails').attr("onclick","UpdateBankDetail()");
    $('#btnAddBankDetails').attr("value", "Update");
    $('#btn_Cancel').css("display","");
}

function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#sel_Nationality").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function BindCity(Description) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var City = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Description; });

        $("#DivCity .select span")[0].textContent = Description;
        for (var i = 0; i <= City.length - 1; i++) {
            $('input[value="' + Description + '"][class="City"]').prop("selected", true);
            $("#selCity").val(Description);
        }
    }
    catch (ex)
    { }
}
function UpdateBankDetail() {
    var BankName = $('#txtBankName').val();
    var AccountNo = $('#txtAccountNo').val();
    var Branch = $('#txtBranch').val();
    var SwiftCode = $('#txtSwiftCode').val();
    var IFSCCode = $('#txtIFSCCode').val();
    var Country = $('#sel_Nationality').val();
    var City = $('#selCity option:selected').val();

    if (!BankName) {
        Success('Please enter Bank Name.');
        return false;
    } else if (!AccountNo) {
        Success('Please enter Account No.');
        return false;
    } else if (!Branch) {
        Success('Please enter Branch.');
        return false;
    } else if (!SwiftCode) {
        Success('Please enter Swift Code.');
        return false;
    } else if (!IFSCCode) {
        Success('Please enter IFSC Code.');
        return false;
    } else if (Country == '-') {
        Success('Please select country.');
        return false;
    } else if (City == '-') {
        Success('Please select City.');
        return false;
    }

    var data = {
        sid:ID,
        BankName: BankName,
        AccountNo: AccountNo,
        Branch: Branch,
        SwiftCode: SwiftCode,
        IFSCCode:IFSCCode,
        Country: Country,
        City: City
    }

    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/UpdateBankDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
               
            }
            else if (result.retCode == 1) {
                Success("Bank details updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
               
            }
        },
        error: function () {
            Success("An error occured while updating details.");
        }
    });
}

function Cancel() {
    window.location.reload();
}

function DeleteBankDetail(sid) {
    $.modal({
        content: '<p class="avtiveDea">Are you sure you want to delete this bank detail?</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Delete('+ sid +')">OK</button></p>',


        width: 300,
        scrolling: false,
        actions: {
            'Cancel': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function Delete(sid) {
    var data = {
        sid:sid
    }
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/DeleteBankDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
                
            }
            else if (result.retCode == 1) {
                Success("Bank detail deleted successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
                
            }
        },
        error: function () {
            Success("An error occured while deleting details.");
        }
    });
}
