﻿$(document).ready(function () {
    var Status = getParameterByName('Status');
    var type = getParameterByName('Type');
    BookingListAll();
    GetSupplier();
    if (type != "") {
        BookingListFilter(Status, type);
    }
    $("#Check-In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Check-Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#CheckIn").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#CheckOut").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#Check_In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#Check_Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var BookingList;
var SupplierList;
function BookingListAll() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    // $("#tbl_BookingList tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/BookingList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                SupplierList = result.SupplierList;
                htmlGenrator();
                GetBookingListSupplier(SupplierList);
            }
            else {
                $("#tbl_BookingList").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function BookingListFilter(status, type) {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var data = {
        status: status,
        type: type
    }
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/BookingListFilter",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                // BookingList = new Array();
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else {
                $("#tbl_BookingList").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function Search() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();

    //  var Agency = $("#txt_Agency").val();
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    // var SupplierRef = $("#txt_Supplier").val();
    //  var Supplier = $("#selSupplier option:selected").val();
    //  var Supplier = $("#selSupplier option:selected").text();
    var Supplier = $("#selAgency option:selected").text();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();

    var data = {
        // Agency: Agency,
        CheckIn: CheckIn,
        CheckOut: CheckOut,
        Passenger: Passenger,
        BookingDate: BookingDate,
        Reference: Reference,
        // SupplierRef: SupplierRef,
        Supplier: Supplier,
        HotelName: HotelName,
        Location: Location,
        ReservationStatus: ReservationStatus
    }

    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                htmlGenrator();

            }
            else if (result.retCode == 0) {
                $("#tbl_BookingList").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.")
        }
    });
}

function Reset() {
    $("#txt_Agency").val('');
    $("#Check-In").val('');
    $("#Check-Out").val('');
    $("#txt_Passenger").val('');
    $("#Bookingdate").val('');
    $("#txt_Reference").val('');
    $("#txt_Supplier").val('');
    $("#selSupplier").val('All');
    $("#txt_Hotel").val('');
    $("#txt_Location").val('');
    $("#selReservation").val('All');
}


function ArrivalReport() {
    var CheckIn = $("#CheckIn").val();
    var CheckOut = $("#CheckOut").val();
    var HotelName = $("#txt_HotelName").val();
    window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=ArrivalReportForAdmin&CheckIn=" + CheckIn + "&CheckOut=" + CheckOut + "&HotelName=" + HotelName;
}

function ArrivalReportForClient() {
    var CheckIn = $("#Check_In").val();
    var CheckOut = $("#Check_Out").val();
    var HotelName = $("#txt-HotelName").val();
    var Agency = $("#Agency_List option:selected").val();
    if (Agency == "-") {
        Agency = "";
    }
    window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=ClientReservationStatementForAdmin&CheckIn=" + CheckIn + "&CheckOut=" + CheckOut + "&HotelName=" + HotelName + "&Agency=" + Agency;
}

function htmlGenrator() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++)
    {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;

        if (BookingList[i].IsConfirm == "True")
            BookingList[i].IsConfirm = true;

        if (BookingList[i].IsConfirm == "False")
            BookingList[i].IsConfirm = false;

        trow += '<tr>';
        trow += '<td style="max-width:25px; vertical-align: middle;" class="center">' + (i + 1) + '</td>';
        trow += '<td style="min-width:60px; vertical-align: middle;" class="center">' + BookingList[i].ReservationDate + ' </td>';
        trow += '<td style="max-width: 115px; vertical-align: middle;" class="center">'
        if (BookingList[i].Status == 'Cancelled')
        {
            trow += '<a style="cursor:pointer" title="Booking Cancelled">' + BookingList[i].ReservationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
        }
        else if (BookingList[i].BookStatus == 'OnRequest' && BookingList[i].IsConfirm == false)
        {
            trow += '<a style="cursor:pointer" title="Booking On Request" onclick="ConfirmOnRequestBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
        }
        else if (BookingList[i].BookStatus == 'Rejected' && BookingList[i].IsConfirm == false)
        {
            trow += '<a style="cursor:pointer" title="Booking Rejected" >' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
        }
        else if (BookingList[i].Status == 'OnHold' && BookingList[i].IsConfirm == false)
        {
            trow += '<a style="cursor:pointer" title="Booking On Hold" onclick="ConfirmBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
        }
        else if (BookingList[i].IsConfirm)
        {
            trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ConfirmHoldBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-reconfirmed">Reconfirmed</span>';
        }
        else
        {
            trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="REConfirmBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
        }
        trow += '</td>'
        trow += '<td style="vertical-align: middle; width: 100px;">' + BookingList[i].AgencyName + ' </td>';
        trow += '<td style="vertical-align: middle;width: 100px;">' + BookingList[i].bookingname + '</td>';
        trow += '<td style="min-width: 100px; vertical-align: middle;">' + BookingList[i].HotelName + '</br><b>' + BookingList[i].City + '</b></td>';
        trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckIn + '<br> to </br>' + BookingList[i].CheckOut + ' </td>';
        /* trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckOut + ' </td>';*/
        trow += '<td style="max-width: 20px; vertical-align: middle;" class="center">' + BookingList[i].TotalRooms + ' </td>';
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-cancelled">Cancelled</span></td>';
        } else if (BookingList[i].Status == 'Vouchered') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-vouchered">Vouchered</span></td>';
        } else if (BookingList[i].Status == 'OnRequest') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onrequest">On Request</span></td>';
        } else if (BookingList[i].Status == 'OnHold') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onhold">On Hold</span></td>';
        } else {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-rejected">Rejected</span></td>';
        }

        trow += '<td style="vertical-align: middle; min-width: 60px;" class="align-right"><span class="strong"><i class="' + GetCurrency(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        trow += '<td style="max-width: 55px; vertical-align: middle;" class="align-center action-button">';
        trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Voucher</a >';
        trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Invoice</a >';
        trow += ' </td>';
        trow += '</tr>';
    }
    trow += '</tbody>'
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()
    $("#tbl_BookingList").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $("#tbl_BookingList").removeAttr("style");
}

function GetSupplier() {
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetSupplier",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Supplier = obj.Arr;
            var ul = '';
            $("#CustomerDetails").empty();
            if (obj.retCode == 1) {
                if (Supplier.length > 0) {
                    $("#selSupplier").empty();
                    var ddlRequest = '<option selected="selected">All</option>';
                    for (i = 0; i < Supplier.length; i++) {
                        ddlRequest += '<option value="' + Supplier[i].sid + '">' + Supplier[i].AgencyName + '</option>';
                    }
                    $("#selSupplier").append(ddlRequest);
                    $("#Agency_List").append(ddlRequest);
                }
            }
            else {

            }
        }
    });
}

function GetAgent(SupplierId) {
    var data = {
        SupplierId: SupplierId
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetAgent",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Agent = obj.Arr;
            var ul = '';
            $("#CustomerDetails").empty();
            if (obj.retCode == 1) {
                if (Agent.length > 0)
                {
                    $("#selAgency").empty();
                    var ddlRequest = '<option selected="selected">All</option>';
                    for (i = 0; i < Agent.length; i++) {
                        ddlRequest += '<option value="' + Agent[i].sid + '">' + Agent[i].AgencyName + '</option>';
                    }
                    $("#selAgency").append(ddlRequest);
                }
            }
            else {

            }
        }
    });
}

function Suppliers() {
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/Suppliers",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;0
            Suppliers = obj.Arr;
            var ul = '';
            if (obj.retCode == 1) {
                if (Suppliers.length > 0) {
                    $("#Agency_List").empty();
                    var ddlRequest = '<option value="-" selected="selected">Select Supplier</option>';
                    for (i = 0; i < Suppliers.length; i++) {
                        ddlRequest += '<option value="' + Suppliers[i].sid + '">' + Suppliers[i].AgencyName + '</option>';
                    }
                    $("#Agency_List").append(ddlRequest);
                }
            }
            else {

            }
        }
    });
}

function GetBookingListSupplier(SupplierList) {

    var data = {
        SupplierList: SupplierList
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetBookingListSupplier",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Supplier = obj.Arr;
            var ul = '';
            $("#CustomerDetails").empty();
            if (obj.retCode == 1) {
                if (Supplier.length > 0) {
                    $("#selSupplier").empty();
                    var ddlRequest = '<option selected="selected">All</option>';
                    for (i = 0; i < Supplier.length; i++) {
                        ddlRequest += '<option value="' + Supplier[i].sid + '">' + Supplier[i].AgencyName + '</option>';
                    }
                    $("#selSupplier").append(ddlRequest);

                }
            }
            else {

            }
        }
    });
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {
    //OpenCancellationPopup(ReservationID, Status);
    $("#hdn_Supplier").val(Source);
    //$("#hdn_AffiliateCode").val(AffilateCode);
    OpenHoldPopup(ReservationID, Status);

    //$("#hdn_HoldDate").val(HoldDate);
    //$("#hdn_DeadLineDate").val(DeadLineDate);
    //$('#ConfirmAlertForOnHoldModel').modal('show');
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {
    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].Name + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" style="width: 100%">' +
                   '<tr>' +
                  '<h4>Re-Confirmation Detail</h4>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Staff Name :&nbsp;&nbsp; <input type="text" id="StaffName" class="input" > </span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +

                  '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                  '<tr>' +
                    '<td style="border-bottom: none;" >' +
                  '<span class="text-left">Comment :  <input type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                   '' +
                  '</td>' +
                  '</tr>' +
                   '</table>' +

                   '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button anthracite-gradient" style="width: 7%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                  '</div>',
                    title: 'Re-confirm Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'No': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }

                    },

                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },

                });

                $("#ConfirmDate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    //onSelect: insertDepartureDate,
                    //minDate: "dateToday",
                    //maxDate: "+3M +10D"
                });
            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}

function SaveConfirmDetail(ReservationID, status, HotelName) {
    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").val();
    if (ReconfirmThrough == "-") {
        Success('Please Select Reconfirm Through.');
        return false;
    }

    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,
    }

    $.ajax({
        type: "POST",
        url: "BookingHandler.asmx/SaveConfirmDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Confirm Detail Save.");
                $("#ConfirmDate").val('');
                $("#HotelConfirmationNo").val('');
                $("#StaffName").val('');
                $("#ReconfirmThrough option:selected").val('-');
                $("#Comment").val('');
                window.location.href = "BookingList.aspx";
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });


}

function ExportBookingDetailsToExcel(Document) {
    debugger;

    var Agency = $("#txt_Agency").val();
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var SupplierRef = $("#txt_Supplier").val();
    var Supplier = $("#selSupplier option:selected").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();
    var Type = "All";
    if (Agency == "" && CheckIn == "" && CheckOut == "" && Passenger == "" && BookingDate == "" && Reference == "" && SupplierRef == "" && Supplier == "All" && HotelName == "" && Location == "" && ReservationStatus == "All") {
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AllBookingDetails&Type=" + Type + "&Document=" + Document;
    }
    else {
        Type = "Search";
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AllBookingDetails&Type=" + Type + "&Document=" + Document;
    }
}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}