﻿var arrLocationCode = new Array();
var dLat, dLong;
google.maps.event.addDomListener(window, 'load', function () {
    var places2 = new google.maps.places.Autocomplete(document.getElementById('txtSource'),
        {
        icon: true,
        types: ['(cities)'],
    });
    google.maps.event.addListener(places2, 'place_changed', function () {
        debugger;
        var place = places2.getPlace();
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var Placeid = place.place_id;
        $('#hdnlatitude').val(latitude);
        $('#hdnlongitude').val(longitude);
        $('#hdnPlaceid').val(Placeid);
        dLat = latitude;
        dLong = longitude;
        $("#Area_Name").val(place.address_components[0].short_name);
        var arrLocationCode = place.address_components;
        var arrCountryCode = '';
        for (var i = 0; i < arrLocationCode.length; i++) {
            var arrCodes = $.grep(arrLocationCode[i].types, function (H) { return H == "country" })
                .map(function (H) { return H; });
            if (arrCodes.length != 0) {
                arrCountryCode = arrLocationCode[i].short_name;
                arrCountryName = arrLocationCode[i].long_name;
                $('#hdCountryCode').val(arrCountryCode);
                $('#hdnCountryName').val(arrCountryName);
            }
        }
        GetLocation(arrCountryCode, arrCountryName)
    });
});

google.maps.event.addDomListener(window, 'load', function () {
    var places2 = new google.maps.places.Autocomplete(document.getElementById('txt_location'));
    google.maps.event.addListener(places2, 'place_changed', function () {
        debugger;
        var place = places2.getPlace();
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var Placeid = place.place_id;
        $('#hdnlatitude').val(latitude);
        $('#hdnlongitude').val(longitude);
        $('#hdnPlaceid').val(Placeid);
        $('#hdnName').val(place.name);

        dLat = latitude;
        dLong = longitude;
        var arrLocationCode = place.address_components;
        var arrCountryCode = '';
        var arrCityCode = '';
        for (var i = 0; i < arrLocationCode.length; i++) {
            var arrCodes = $.grep(arrLocationCode[i].types, function (H) { return H == "country" })
                .map(function (H) { return H; });
            if (arrCodes.length != 0) {
                arrCountryCode = arrLocationCode[i].short_name;
                arrCountryName = arrLocationCode[i].long_name;
                $('#hdCountryCode').val(arrCountryCode);
                $('#lbl_country').text(arrCountryName);
            }
            var arrcityCodes = $.grep(arrLocationCode[i].types, function (k) { return k == "locality" })
                .map(function (k) { return k; });
            if (arrcityCodes.length != 0) {
                arrCityCode = arrLocationCode[i].short_name;
                arrCityName = arrLocationCode[i].long_name;
                $('#hdCityCode').val(arrCityCode);
                $('#lbl_city').text(arrCityName);
            }
        }
        codeAddress($("#lbl_city").text() + "," + $("#lbl_country").text())
    });
});

function codeAddress(address) {
    debugger
    geocoder = new google.maps.Geocoder();
    var AddNew = address;
    var address = AddNew.split(",");
    address = address[(address.length - 1)];
    address = address.trim();
    geocoder.geocode({ 'address': AddNew }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var a = results[0].geometry.location.lat();
            var b = results[0].geometry.location.lng();
            $("#City_Name").val(results[0].address_components[0].short_name);
            $("#lat_City").val(a);
            $("#log_City").val(b);
            $("#CityPlaceID").val(results[0].place_id);
        }
    });
}