﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="ActivityMails.aspx.cs" Inherits="CutAdmin.HotelAdmin.ActivityMails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <link href="../js/semantic.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.2/semantic.min.js"></script>
    <link href="../js/multiple-emails.css" rel="stylesheet" />
    <script src="../js/multiple-emails.js"></script>
    
    <%--<script type="text/javascript">
        $(function () {
            //To render the input device to multiple email input using SemanticUI icon
            $('#example_emailSUI').multiple_emails({ theme: "SemanticUI" });
            //Shows the value of the input device, which is in JSON format
            $('#current_emailsSUI').text($('#example_emailSUI').val());
            $('#example_emailSUI').change(function () {
                var Departure = document.getElementsByClassName('remove icon'), i;
                for (i = 0; i < Departure.length; i += 1) {
                    Departure[i].className = "glyphicon glyphicon-remove"
                }
                $('#current_emailsSUI').text($(this).val());
            });

        });
	</script>--%>
    <script src="Scripts/ActivityMails.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
           <hgroup id="main-title" class="thin">
            <h1>Hotel Mails</h1>
            <hr />
        </hgroup>
        <div class="with-small-padding">
            <%--<div class="standard-tabs margin-bottom" id="add-tabs">--%>
                <%--<ul class="tabs">--%>
                    <%--<li class="active"><a href="#VisaMails">Visa Mails</a></li>
                    <li><a href="#OTBMails">OTB Mails</a></li>--%>
                    <%--<li class="active"><a href="#HotelMails">Hotel Mails</a></li>--%>
                <%--</ul>--%>
                <%--<div class="tabs-content">--%>
                    <div id="HotelMails">
                        <div class="with-small-padding">
                            <div class="respTable">
                                <table class="table responsive-table" id="tbl_HotelDetails">
                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Error Message </th>
                                            <th scope="col" class="align-center">Add | Edit </th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
        </div>
</asp:Content>
