﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="CommissionStatement.aspx.cs" Inherits="CutAdmin.HotelAdmin.CommissionStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <%--  <script src="Scripts/CommissionReport.js"></script>--%>
    <script src="Scripts/CommissionStatement.js?v=1.5"></script>
    <script src="../Scripts/Alert.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <hgroup id="main-title" class="thin">
        <h1>Commision Statement</h1>
        <hr />
    </hgroup>

    <div class="with-padding">
        <div class="columns ">
            <div class="four-columns ">
                <label><b>Supplier </b></label>
                <select id="selSupplier" class="select Supplier">
                    <option value="-">-Select Any Supplier-</option>
                    <%-- <option value="0"> All Supplier </option>--%>
                </select>
            </div>
            <div class="three-columns">
                <label><b>Year </b></label>
                <select id="selMonth" class="select">
                    <option value="-">-Select Any Year-</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                </select>
            </div>
            <div class="two-columns">
                <button type="button" class="button anthracite-gradient" onclick="Search()" id="btn_Search">Search</button>

                <span class="icon-pdf right" onclick="ExportCommissionDetailToExcel('PDF')" >
                    <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer;" title="Export To Pdf" height="35" width="35">
                </span>
                <span class="icon-excel right" onclick="ExportCommissionDetailToExcel('excel')" >
                    <img src="../img/Excel-2-icon.png" style="cursor: pointer;" title="Export To Excel" id="btnPDF" height="35" width="35">
                </span>
            </div>
            <div class="three-columns ">
                <label><b>Total Amount : </b></label>
                <label id="UnPaidAmt"></label>
            </div>
        </div>
        <div class="respTable">
            <table class="table responsive-table" id="tbl_SupplierDetails">
                <thead>
                    <tr>
                        <th scope="col" class="align-center hide-on-mobile">Invoice No</th>
                        <th scope="col" class="align-center hide-on-mobile">Invoice Date</th>
                        <th scope="col" class="align-center hide-on-mobile">Description</th>
                        <th scope="col" class="align-center hide-on-mobile-portrait">Invoice Amount</th>
                        <th scope="col" class="align-center hide-on-mobile-portrait">Status</th>
                        <th scope="col" width="150" class="align-center">View Invoice</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</asp:Content>
