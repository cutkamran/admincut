﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="StaffDetails.aspx.cs" Inherits="CutAdmin.HotelAdmin.StaffDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/StaffDetails.js?v=1.5"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <hgroup id="main-title" class="thin">
            <h1>Staff Details</h1>
                <h2><a href="AddStaff.aspx" class="button anthracite-gradient" style="float: right;"><span class="icon-user"></span>Add New</a> 
                 <a onclick="ExportStaffDetailsToExcel()" style="">
                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35">
                    </a>
            </h2>
            <hr />
        </hgroup>
        <div class="with-small-padding">
            <div class="respTable">
                <table class="table responsive-table font10" id="tbl_StaffDetails">
                    <thead>
                        <tr>
                            <th scope="col">Staff Detail</th>
                            <th scope="col" class="align-center hide-on-mobile">Email | Password Manage </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Unique Code </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Live</th>
                            <th scope="col" width="150" class="align-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
</asp:Content>
