﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="BankDepositDetails.aspx.cs" Inherits="CutAdmin.HotelAdmin.BankDepositDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/BankDepositDetails.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <hgroup id="main-title" class="thin">
            <h1>Deposit Request</h1>
            <hr />
        </hgroup>
        <div class="with-small-padding">
            <div class="respTable">
                <table class="table responsive-table font10" id="tbl_BankDepositDetails">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Name</th>
                            <th scope="col" class="align-center">Unique Code</th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Remarks</th>
                            <th scope="col" class="align-center">Mobile</th>
                            <th scope="col" class="align-center">Amount</th>
                            <th scope="col" class="align-center">Approve | Unapprove</th>

                        </tr>
                    </thead>

                </table>
            </div>
        </div>

</asp:Content>
