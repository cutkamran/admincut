﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for LocationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        string json = "";
        private string escapejsondata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }


        [WebMethod(EnableSession = true)]
        public string Save(tbl_Location arrLocation, tbl_MappedArea arrCity)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                LocationManager.SaveLocation(arrLocation, arrCity);
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveLocation(string Location, string Country, string City)
        {
            ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
            using (var SaveLoc = DB.Database.BeginTransaction())
            {
                var RoomList = (from obj in DB.tbl_CommonLocations where (obj.LocationName.Contains(Location) && obj.City == City) select obj).ToList();
                if (RoomList.Count == 0)
                {
                    try
                    {
                        tbl_CommonLocations Locatn = new tbl_CommonLocations();
                        Locatn.LocationName = Location;
                        Locatn.City = City;
                        Locatn.Country = Country;

                        DB.tbl_CommonLocations.Add(Locatn);
                        DB.SaveChanges();
                        SaveLoc.Commit();
                          json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Locat\":\"1\"}";

                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }


                return json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetLocation()
        {
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    //var LocationList = ((from Loct in DB.tbl_CommonLocations select Loct).Distinct()).ToList();
                    var LocationList = DB.tbl_Location.GroupBy(x => x.LocationName).Select(x=>x.FirstOrDefault()).ToList();

                    if (LocationList.Count > 0)
                    {
                        json = jsSerializer.Serialize(LocationList);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"LocationList\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
            }

            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string DeleteLocation(Int64 LocationId)
        {
            try
            {
                LocationManager.Delete(LocationId);
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

    }
}
