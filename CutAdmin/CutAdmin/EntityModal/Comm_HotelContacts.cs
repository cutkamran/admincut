//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comm_HotelContacts
    {
        public long ID { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string ContactPerson { get; set; }
        public string email { get; set; }
        public string MobileNo { get; set; }
        public Nullable<long> HotelCode { get; set; }
        public Nullable<long> SupplierCode { get; set; }
    }
}
