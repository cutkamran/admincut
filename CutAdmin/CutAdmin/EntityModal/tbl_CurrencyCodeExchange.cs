//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_CurrencyCodeExchange
    {
        public long ID { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyDetails { get; set; }
    }
}
