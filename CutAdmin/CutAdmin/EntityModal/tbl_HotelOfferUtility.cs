//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_HotelOfferUtility
    {
        public long HotelOfferId { get; set; }
        public string HotelCountry { get; set; }
        public string HotelCity { get; set; }
        public Nullable<long> Hotel_Id { get; set; }
        public Nullable<long> Room_Id { get; set; }
        public Nullable<long> Supplier_Id { get; set; }
        public string OfferNationality { get; set; }
        public string FromInventory { get; set; }
        public string BlockDays { get; set; }
        public string OfferOn { get; set; }
        public string OfferType { get; set; }
        public string MinNights { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string MinRooms { get; set; }
        public string DaysPrior { get; set; }
        public string BookBefore { get; set; }
        public string Discount { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> NewRate { get; set; }
        public string FreebiItem { get; set; }
        public string FreebiItemDetail { get; set; }
        public string OfferTerms { get; set; }
        public string OfferNote { get; set; }
        public string HotelOfferCode { get; set; }
        public string OfferCode { get; set; }
    }
}
