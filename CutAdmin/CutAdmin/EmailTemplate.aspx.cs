﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CutAdmin.Models;
using CutAdmin.DataLayer;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    public partial class EmailTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ul_Folder.InnerHtml = TemplateManager.GetManu();
        }

        public string RemoveSpace(string Name)
        {
            string path = "";

            path = Name.Replace(" ", "%20");

            return path;
        }

        public string RemovePercent(string Name)
        {
            string path = "";

            path = Name.Replace("%20", " ");

            return path;
        }

        public static List<CutAdmin.Models.MenuItem> GenTemplateFolder()
        {
            List<CutAdmin.Models.MenuItem> arrMenu = new List<CutAdmin.Models.MenuItem>();
            string pathDownload = Common.Common.GetAppServerFolder;
            DirectoryInfo directory = new DirectoryInfo(pathDownload);
            DirectoryInfo[] directories = directory.GetDirectories();
            try
            {
                foreach (DirectoryInfo folder in directories)
                {
                    var arrList = GetSubMenu(folder.Name);
                    foreach (var Menu in arrList)
                    {
                        arrMenu.Add(Menu);
                    }
                }
            }
            catch (Exception)
            {

            }
            return arrMenu.OrderBy(d => d.Name).ToList();
        }

        public static List<CutAdmin.Models.MenuItem> GetSubMenu(string Folder)
        {
            List<CutAdmin.Models.MenuItem> ListMenu = new List<Models.MenuItem>();
            try
            {
                string pathDownload = Common.Common.GetAppServerFolder + "\\" + Folder;
                DirectoryInfo directory = new DirectoryInfo(pathDownload);
                DirectoryInfo[] directories = directory.GetDirectories();
                ListMenu.Add(new Models.MenuItem
                {
                    Name = Folder,
                    ID = 0,
                    Path = "",
                    ChildItem = new List<Models.MenuItem>(),
                });
                foreach (DirectoryInfo folder in directories)
                {
                    CutAdmin.Models.MenuItem arrMenu = new Models.MenuItem();
                    arrMenu = new CutAdmin.Models.MenuItem { Name = folder.Name.Split('.')[0], ChildItem = new List<CutAdmin.Models.MenuItem>(), Path = string.Empty };
                    List<FileInfo> files = new List<FileInfo>();
                    files = folder.GetFiles().ToList();
                    if (files.Count != 0)
                    {
                        if (files.Count == 1)
                            arrMenu.Path = Common.Common.GetAppRootFolder + "/" + Folder + "/" + folder + "/" + files[0].Name;
                        else
                            foreach (var File in files)
                            {
                                arrMenu.ChildItem.Add(new Models.MenuItem
                                {
                                    Name = File.Name.Split('.')[0],
                                    ChildItem = new List<CutAdmin.Models.MenuItem>(),
                                    Path = Common.Common.GetAppRootFolder + "/" + Folder + "/" + folder + "/" + File.Name,
                                });
                            }
                    }
                    ListMenu.LastOrDefault().ChildItem.Add(arrMenu);
                }
            }
            catch (Exception)
            {
            }
            return ListMenu;
        }
    }
}