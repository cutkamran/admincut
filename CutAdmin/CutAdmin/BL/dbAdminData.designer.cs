﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.BL
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Trivo_Ams")]
	public partial class dbAdminDataDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void Inserttbl_Admin(tbl_Admin instance);
    partial void Updatetbl_Admin(tbl_Admin instance);
    partial void Deletetbl_Admin(tbl_Admin instance);
    partial void Inserttbl_SendInTemplate(tbl_SendInTemplate instance);
    partial void Updatetbl_SendInTemplate(tbl_SendInTemplate instance);
    partial void Deletetbl_SendInTemplate(tbl_SendInTemplate instance);
    partial void Inserttbl_ExchangeLog(tbl_ExchangeLog instance);
    partial void Updatetbl_ExchangeLog(tbl_ExchangeLog instance);
    partial void Deletetbl_ExchangeLog(tbl_ExchangeLog instance);
    partial void Inserttbl_CurrencyShedule(tbl_CurrencyShedule instance);
    partial void Updatetbl_CurrencyShedule(tbl_CurrencyShedule instance);
    partial void Deletetbl_CurrencyShedule(tbl_CurrencyShedule instance);
    partial void Inserttbl_CurrencyRate(tbl_CurrencyRate instance);
    partial void Updatetbl_CurrencyRate(tbl_CurrencyRate instance);
    partial void Deletetbl_CurrencyRate(tbl_CurrencyRate instance);
    partial void Inserttbl_CurrencyCode(tbl_CurrencyCode instance);
    partial void Updatetbl_CurrencyCode(tbl_CurrencyCode instance);
    partial void Deletetbl_CurrencyCode(tbl_CurrencyCode instance);
    #endregion
		
		public dbAdminDataDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["Trivo_AmsConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public dbAdminDataDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbAdminDataDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbAdminDataDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbAdminDataDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<tbl_Admin> tbl_Admins
		{
			get
			{
				return this.GetTable<tbl_Admin>();
			}
		}
		
		public System.Data.Linq.Table<tbl_SendInTemplate> tbl_SendInTemplates
		{
			get
			{
				return this.GetTable<tbl_SendInTemplate>();
			}
		}
		
		public System.Data.Linq.Table<tbl_ExchangeLog> tbl_ExchangeLogs
		{
			get
			{
				return this.GetTable<tbl_ExchangeLog>();
			}
		}
		
		public System.Data.Linq.Table<tbl_CurrencyShedule> tbl_CurrencyShedules
		{
			get
			{
				return this.GetTable<tbl_CurrencyShedule>();
			}
		}
		
		public System.Data.Linq.Table<tbl_CurrencyRate> tbl_CurrencyRates
		{
			get
			{
				return this.GetTable<tbl_CurrencyRate>();
			}
		}
		
		public System.Data.Linq.Table<tbl_CurrencyCode> tbl_CurrencyCodes
		{
			get
			{
				return this.GetTable<tbl_CurrencyCode>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_Admin")]
	public partial class tbl_Admin : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private string _CompanyName;
		
		private string _CompanyAddress;
		
		private string _CityName;
		
		private string _CityID;
		
		private string _CountryName;
		
		private string _UserName;
		
		private string _EmailID;
		
		private string _ConntectionString;
		
		private string _Currency;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnCompanyNameChanging(string value);
    partial void OnCompanyNameChanged();
    partial void OnCompanyAddressChanging(string value);
    partial void OnCompanyAddressChanged();
    partial void OnCityNameChanging(string value);
    partial void OnCityNameChanged();
    partial void OnCityIDChanging(string value);
    partial void OnCityIDChanged();
    partial void OnCountryNameChanging(string value);
    partial void OnCountryNameChanged();
    partial void OnUserNameChanging(string value);
    partial void OnUserNameChanged();
    partial void OnEmailIDChanging(string value);
    partial void OnEmailIDChanged();
    partial void OnConntectionStringChanging(string value);
    partial void OnConntectionStringChanged();
    partial void OnCurrencyChanging(string value);
    partial void OnCurrencyChanged();
    #endregion
		
		public tbl_Admin()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CompanyName", DbType="NVarChar(MAX)")]
		public string CompanyName
		{
			get
			{
				return this._CompanyName;
			}
			set
			{
				if ((this._CompanyName != value))
				{
					this.OnCompanyNameChanging(value);
					this.SendPropertyChanging();
					this._CompanyName = value;
					this.SendPropertyChanged("CompanyName");
					this.OnCompanyNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CompanyAddress", DbType="NVarChar(MAX)")]
		public string CompanyAddress
		{
			get
			{
				return this._CompanyAddress;
			}
			set
			{
				if ((this._CompanyAddress != value))
				{
					this.OnCompanyAddressChanging(value);
					this.SendPropertyChanging();
					this._CompanyAddress = value;
					this.SendPropertyChanged("CompanyAddress");
					this.OnCompanyAddressChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityName", DbType="NVarChar(MAX)")]
		public string CityName
		{
			get
			{
				return this._CityName;
			}
			set
			{
				if ((this._CityName != value))
				{
					this.OnCityNameChanging(value);
					this.SendPropertyChanging();
					this._CityName = value;
					this.SendPropertyChanged("CityName");
					this.OnCityNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CityID", DbType="NVarChar(50)")]
		public string CityID
		{
			get
			{
				return this._CityID;
			}
			set
			{
				if ((this._CityID != value))
				{
					this.OnCityIDChanging(value);
					this.SendPropertyChanging();
					this._CityID = value;
					this.SendPropertyChanged("CityID");
					this.OnCityIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CountryName", DbType="NVarChar(MAX)")]
		public string CountryName
		{
			get
			{
				return this._CountryName;
			}
			set
			{
				if ((this._CountryName != value))
				{
					this.OnCountryNameChanging(value);
					this.SendPropertyChanging();
					this._CountryName = value;
					this.SendPropertyChanged("CountryName");
					this.OnCountryNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserName", DbType="NVarChar(MAX)")]
		public string UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				if ((this._UserName != value))
				{
					this.OnUserNameChanging(value);
					this.SendPropertyChanging();
					this._UserName = value;
					this.SendPropertyChanged("UserName");
					this.OnUserNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_EmailID", DbType="NVarChar(MAX)")]
		public string EmailID
		{
			get
			{
				return this._EmailID;
			}
			set
			{
				if ((this._EmailID != value))
				{
					this.OnEmailIDChanging(value);
					this.SendPropertyChanging();
					this._EmailID = value;
					this.SendPropertyChanged("EmailID");
					this.OnEmailIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ConntectionString", DbType="NVarChar(MAX)")]
		public string ConntectionString
		{
			get
			{
				return this._ConntectionString;
			}
			set
			{
				if ((this._ConntectionString != value))
				{
					this.OnConntectionStringChanging(value);
					this.SendPropertyChanging();
					this._ConntectionString = value;
					this.SendPropertyChanged("ConntectionString");
					this.OnConntectionStringChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Currency", DbType="NVarChar(50)")]
		public string Currency
		{
			get
			{
				return this._Currency;
			}
			set
			{
				if ((this._Currency != value))
				{
					this.OnCurrencyChanging(value);
					this.SendPropertyChanging();
					this._Currency = value;
					this.SendPropertyChanged("Currency");
					this.OnCurrencyChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_SendInTemplate")]
	public partial class tbl_SendInTemplate : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private string _sName;
		
		private string _dte_CreatedOn;
		
		private System.Nullable<bool> _b_Active;
		
		private string _s_html;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnsNameChanging(string value);
    partial void OnsNameChanged();
    partial void Ondte_CreatedOnChanging(string value);
    partial void Ondte_CreatedOnChanged();
    partial void Onb_ActiveChanging(System.Nullable<bool> value);
    partial void Onb_ActiveChanged();
    partial void Ons_htmlChanging(string value);
    partial void Ons_htmlChanged();
    #endregion
		
		public tbl_SendInTemplate()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sName", DbType="NVarChar(MAX)")]
		public string sName
		{
			get
			{
				return this._sName;
			}
			set
			{
				if ((this._sName != value))
				{
					this.OnsNameChanging(value);
					this.SendPropertyChanging();
					this._sName = value;
					this.SendPropertyChanged("sName");
					this.OnsNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dte_CreatedOn", DbType="NVarChar(50)")]
		public string dte_CreatedOn
		{
			get
			{
				return this._dte_CreatedOn;
			}
			set
			{
				if ((this._dte_CreatedOn != value))
				{
					this.Ondte_CreatedOnChanging(value);
					this.SendPropertyChanging();
					this._dte_CreatedOn = value;
					this.SendPropertyChanged("dte_CreatedOn");
					this.Ondte_CreatedOnChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_b_Active", DbType="Bit")]
		public System.Nullable<bool> b_Active
		{
			get
			{
				return this._b_Active;
			}
			set
			{
				if ((this._b_Active != value))
				{
					this.Onb_ActiveChanging(value);
					this.SendPropertyChanging();
					this._b_Active = value;
					this.SendPropertyChanged("b_Active");
					this.Onb_ActiveChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_s_html", DbType="NVarChar(MAX)")]
		public string s_html
		{
			get
			{
				return this._s_html;
			}
			set
			{
				if ((this._s_html != value))
				{
					this.Ons_htmlChanging(value);
					this.SendPropertyChanging();
					this._s_html = value;
					this.SendPropertyChanged("s_html");
					this.Ons_htmlChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_ExchangeLog")]
	public partial class tbl_ExchangeLog : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private System.Nullable<long> _SoureCurrency;
		
		private string _SourceName;
		
		private System.Nullable<long> _Currency;
		
		private string _CurrencyName;
		
		private System.Nullable<double> _Markup;
		
		private System.Nullable<double> _Rate;
		
		private System.Nullable<double> _Total;
		
		private System.Nullable<long> _UID;
		
		private string _InsertDate;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnSoureCurrencyChanging(System.Nullable<long> value);
    partial void OnSoureCurrencyChanged();
    partial void OnSourceNameChanging(string value);
    partial void OnSourceNameChanged();
    partial void OnCurrencyChanging(System.Nullable<long> value);
    partial void OnCurrencyChanged();
    partial void OnCurrencyNameChanging(string value);
    partial void OnCurrencyNameChanged();
    partial void OnMarkupChanging(System.Nullable<double> value);
    partial void OnMarkupChanged();
    partial void OnRateChanging(System.Nullable<double> value);
    partial void OnRateChanged();
    partial void OnTotalChanging(System.Nullable<double> value);
    partial void OnTotalChanged();
    partial void OnUIDChanging(System.Nullable<long> value);
    partial void OnUIDChanged();
    partial void OnInsertDateChanging(string value);
    partial void OnInsertDateChanged();
    #endregion
		
		public tbl_ExchangeLog()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", DbType="BigInt NOT NULL", IsPrimaryKey=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SoureCurrency", DbType="BigInt")]
		public System.Nullable<long> SoureCurrency
		{
			get
			{
				return this._SoureCurrency;
			}
			set
			{
				if ((this._SoureCurrency != value))
				{
					this.OnSoureCurrencyChanging(value);
					this.SendPropertyChanging();
					this._SoureCurrency = value;
					this.SendPropertyChanged("SoureCurrency");
					this.OnSoureCurrencyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SourceName", DbType="NVarChar(50)")]
		public string SourceName
		{
			get
			{
				return this._SourceName;
			}
			set
			{
				if ((this._SourceName != value))
				{
					this.OnSourceNameChanging(value);
					this.SendPropertyChanging();
					this._SourceName = value;
					this.SendPropertyChanged("SourceName");
					this.OnSourceNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Currency", DbType="BigInt")]
		public System.Nullable<long> Currency
		{
			get
			{
				return this._Currency;
			}
			set
			{
				if ((this._Currency != value))
				{
					this.OnCurrencyChanging(value);
					this.SendPropertyChanging();
					this._Currency = value;
					this.SendPropertyChanged("Currency");
					this.OnCurrencyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CurrencyName", DbType="NVarChar(50)")]
		public string CurrencyName
		{
			get
			{
				return this._CurrencyName;
			}
			set
			{
				if ((this._CurrencyName != value))
				{
					this.OnCurrencyNameChanging(value);
					this.SendPropertyChanging();
					this._CurrencyName = value;
					this.SendPropertyChanged("CurrencyName");
					this.OnCurrencyNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Markup", DbType="Float")]
		public System.Nullable<double> Markup
		{
			get
			{
				return this._Markup;
			}
			set
			{
				if ((this._Markup != value))
				{
					this.OnMarkupChanging(value);
					this.SendPropertyChanging();
					this._Markup = value;
					this.SendPropertyChanged("Markup");
					this.OnMarkupChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Rate", DbType="Float")]
		public System.Nullable<double> Rate
		{
			get
			{
				return this._Rate;
			}
			set
			{
				if ((this._Rate != value))
				{
					this.OnRateChanging(value);
					this.SendPropertyChanging();
					this._Rate = value;
					this.SendPropertyChanged("Rate");
					this.OnRateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Total", DbType="Float")]
		public System.Nullable<double> Total
		{
			get
			{
				return this._Total;
			}
			set
			{
				if ((this._Total != value))
				{
					this.OnTotalChanging(value);
					this.SendPropertyChanging();
					this._Total = value;
					this.SendPropertyChanged("Total");
					this.OnTotalChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="BigInt")]
		public System.Nullable<long> UID
		{
			get
			{
				return this._UID;
			}
			set
			{
				if ((this._UID != value))
				{
					this.OnUIDChanging(value);
					this.SendPropertyChanging();
					this._UID = value;
					this.SendPropertyChanged("UID");
					this.OnUIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsertDate", DbType="NVarChar(MAX)")]
		public string InsertDate
		{
			get
			{
				return this._InsertDate;
			}
			set
			{
				if ((this._InsertDate != value))
				{
					this.OnInsertDateChanging(value);
					this.SendPropertyChanging();
					this._InsertDate = value;
					this.SendPropertyChanged("InsertDate");
					this.OnInsertDateChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_CurrencyShedule")]
	public partial class tbl_CurrencyShedule : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private string _ExchangeDate;
		
		private string _NextSchedule;
		
		private System.Nullable<long> _UserID;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnExchangeDateChanging(string value);
    partial void OnExchangeDateChanged();
    partial void OnNextScheduleChanging(string value);
    partial void OnNextScheduleChanged();
    partial void OnUserIDChanging(System.Nullable<long> value);
    partial void OnUserIDChanged();
    #endregion
		
		public tbl_CurrencyShedule()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExchangeDate", DbType="NVarChar(MAX)")]
		public string ExchangeDate
		{
			get
			{
				return this._ExchangeDate;
			}
			set
			{
				if ((this._ExchangeDate != value))
				{
					this.OnExchangeDateChanging(value);
					this.SendPropertyChanging();
					this._ExchangeDate = value;
					this.SendPropertyChanged("ExchangeDate");
					this.OnExchangeDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_NextSchedule", DbType="NVarChar(MAX)")]
		public string NextSchedule
		{
			get
			{
				return this._NextSchedule;
			}
			set
			{
				if ((this._NextSchedule != value))
				{
					this.OnNextScheduleChanging(value);
					this.SendPropertyChanging();
					this._NextSchedule = value;
					this.SendPropertyChanged("NextSchedule");
					this.OnNextScheduleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserID", DbType="BigInt")]
		public System.Nullable<long> UserID
		{
			get
			{
				return this._UserID;
			}
			set
			{
				if ((this._UserID != value))
				{
					this.OnUserIDChanging(value);
					this.SendPropertyChanging();
					this._UserID = value;
					this.SendPropertyChanged("UserID");
					this.OnUserIDChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_CurrencyRate")]
	public partial class tbl_CurrencyRate : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private string _CurrencyID;
		
		private string _SourceCurrency;
		
		private System.Nullable<double> _ExchangeRate;
		
		private System.Nullable<long> _Userid;
		
		private string _UpdateDate;
		
		private string _UpdateBy;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnCurrencyIDChanging(string value);
    partial void OnCurrencyIDChanged();
    partial void OnSourceCurrencyChanging(string value);
    partial void OnSourceCurrencyChanged();
    partial void OnExchangeRateChanging(System.Nullable<double> value);
    partial void OnExchangeRateChanged();
    partial void OnUseridChanging(System.Nullable<long> value);
    partial void OnUseridChanged();
    partial void OnUpdateDateChanging(string value);
    partial void OnUpdateDateChanged();
    partial void OnUpdateByChanging(string value);
    partial void OnUpdateByChanged();
    #endregion
		
		public tbl_CurrencyRate()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CurrencyID", DbType="NVarChar(50)")]
		public string CurrencyID
		{
			get
			{
				return this._CurrencyID;
			}
			set
			{
				if ((this._CurrencyID != value))
				{
					this.OnCurrencyIDChanging(value);
					this.SendPropertyChanging();
					this._CurrencyID = value;
					this.SendPropertyChanged("CurrencyID");
					this.OnCurrencyIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SourceCurrency", DbType="NVarChar(50)")]
		public string SourceCurrency
		{
			get
			{
				return this._SourceCurrency;
			}
			set
			{
				if ((this._SourceCurrency != value))
				{
					this.OnSourceCurrencyChanging(value);
					this.SendPropertyChanging();
					this._SourceCurrency = value;
					this.SendPropertyChanged("SourceCurrency");
					this.OnSourceCurrencyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExchangeRate", DbType="Float")]
		public System.Nullable<double> ExchangeRate
		{
			get
			{
				return this._ExchangeRate;
			}
			set
			{
				if ((this._ExchangeRate != value))
				{
					this.OnExchangeRateChanging(value);
					this.SendPropertyChanging();
					this._ExchangeRate = value;
					this.SendPropertyChanged("ExchangeRate");
					this.OnExchangeRateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Userid", DbType="BigInt")]
		public System.Nullable<long> Userid
		{
			get
			{
				return this._Userid;
			}
			set
			{
				if ((this._Userid != value))
				{
					this.OnUseridChanging(value);
					this.SendPropertyChanging();
					this._Userid = value;
					this.SendPropertyChanged("Userid");
					this.OnUseridChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UpdateDate", DbType="NVarChar(MAX)")]
		public string UpdateDate
		{
			get
			{
				return this._UpdateDate;
			}
			set
			{
				if ((this._UpdateDate != value))
				{
					this.OnUpdateDateChanging(value);
					this.SendPropertyChanging();
					this._UpdateDate = value;
					this.SendPropertyChanged("UpdateDate");
					this.OnUpdateDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UpdateBy", DbType="NVarChar(50)")]
		public string UpdateBy
		{
			get
			{
				return this._UpdateBy;
			}
			set
			{
				if ((this._UpdateBy != value))
				{
					this.OnUpdateByChanging(value);
					this.SendPropertyChanging();
					this._UpdateBy = value;
					this.SendPropertyChanged("UpdateBy");
					this.OnUpdateByChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.tbl_CurrencyCode")]
	public partial class tbl_CurrencyCode : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _ID;
		
		private string _CurrencyCode;
		
		private string _CurrencyDetails;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(long value);
    partial void OnIDChanged();
    partial void OnCurrencyCodeChanging(string value);
    partial void OnCurrencyCodeChanged();
    partial void OnCurrencyDetailsChanging(string value);
    partial void OnCurrencyDetailsChanged();
    #endregion
		
		public tbl_CurrencyCode()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CurrencyCode", DbType="NVarChar(50)")]
		public string CurrencyCode
		{
			get
			{
				return this._CurrencyCode;
			}
			set
			{
				if ((this._CurrencyCode != value))
				{
					this.OnCurrencyCodeChanging(value);
					this.SendPropertyChanging();
					this._CurrencyCode = value;
					this.SendPropertyChanged("CurrencyCode");
					this.OnCurrencyCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CurrencyDetails", DbType="NVarChar(MAX)")]
		public string CurrencyDetails
		{
			get
			{
				return this._CurrencyDetails;
			}
			set
			{
				if ((this._CurrencyDetails != value))
				{
					this.OnCurrencyDetailsChanging(value);
					this.SendPropertyChanging();
					this._CurrencyDetails = value;
					this.SendPropertyChanged("CurrencyDetails");
					this.OnCurrencyDetailsChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
