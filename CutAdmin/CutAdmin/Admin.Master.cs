﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Menucontainer.InnerHtml = CutAdmin.Models.Forms.GetFormByMenu();
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string sPage = Page.Title;
                Page.Title = objGlobalDefault.AgencyName;
                if (sPage != "")
                    Page.Title += " :: " + sPage;
            }
            else
            {
                Response.Redirect("Default.aspx?error=Session Expired.");
            }

        }
    }
}