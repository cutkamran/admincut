﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="exchangerate.aspx.cs" Inherits="CutAdmin.exchangerate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
       function AddMarkup(id) {
           if (id != "radio_Amt") {
               $("#div_MrkAmt").css("display", "none")
               $("#div_MrkPer").css("display", "")
           }
           else {
               $("#div_MrkPer").css("display", "none")
               $("#div_MrkAmt").css("display", "")
           }
       }
       $(document).ready(function () {
           $("#radio_Per").click(function () {
               AddMarkup(this.id);
           });
           $("#radio_Amt").click(function () {
               AddMarkup(this.id);
           });
           $("#dateId").datepicker({
               changeMonth: true,
               changeYear: true,
               dateFormat: "dd/mm/yy",
               maxDate: new Date(),
               //minDate: "-1D"
           });
       });
</script>
    <script src="Scripts/exchangerate.js?v=1.3"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Exchange Markup</h1>
            <%--<h2><i class="icon-plus" title="Add Exchange Rate" onclick="ShowAddDiv()"></i></h2>--%>
            <h2><a onclick="ShowAddDiv()" class="button anthracite-gradient" title="Add Rate"><span class="icon-plus" style="cursor:pointer">Add</span></a>
                 <a onclick="GetOnlineRate()" class="button anthracite-gradient" title="Update Online Rate"><span class="icon-search" style="cursor:pointer">Update Online Rate</span></a>
            </h2>
             <span> </b> Rate: ( Update Date & Time: <span class="countoverstay size18" id="SpanUdtDate"></span> )
         <%--   <h2 class="updateDate"><span class="lato size15 grey">Update Online Rate</span></h2>--%>
        </hgroup>
        <br />
       
            <div class="Exchange with-mid-padding anthracite-gradient" id="demo"  style="display:none;">
                    <div class="columns">
                    <div class="two-columns twelve-columns-mobile" id="div_Currency">
                        <label>Currency:</label><br />
                            <select id="sel_Currency" name="validation-select" class="select  full-width" onchange="GetCurrencyValue(this.value)">
                            </select>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Currency">
                                <b>* This field is required</b></label>
                    </div>
                    <div class="two-columns twelve-columns-mobile">
                        <label>Online Rate.:</label><br />
                          <input class="input full-width" readonly="readonly" id="txt_FCY" type="text" autocomplete="off">
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_FCY">
                                <b>* This field is required</b></label>
                     </div>

                    <div class="two-columns twelve-columns-mobile">
                        <input type="radio" name="radio" id="radio_Amt" value="" checked="checked" class=" mid-margin-left" onchange="AddMarkup(this.id)">
                        <label for="radio_Amt" class="label">Markup Amount</label><br />
                         <input value="" class="input full-width" type="text" id="txt_SetMarkupAmt" onkeyup="GetMarkupAmt(this.id)">
                         <input type="hidden" id="txt_MarkupAmt" value="" />
                         <input type="hidden" id="Hdn_Id" />
                         <input type="hidden" id="Hdn_FCY" />
                    </div>
                    <div class="two-columns twelve-columns-mobile" style="width:24%; margin-left:1%">
                        <input type="radio" name="radio" id="radio_Per" value="" class=" mid-margin-left" onchange="AddMarkup(this.id)">
                        <label for="radio_Per" class="label">Markup in Percent</label><br />
                          <input value="" class="input full-width" type="text" id="txt_SetMarkupPer" onkeyup="GetMarkupAmt(this.id)">
                    </div>

                    <div class="two-columns six-columns-mobile">
                        <label>Total :</label><div class="input full-width">
                            <input value="" class="input-unstyled full-width" readonly="readonly" type="text" id="txt_TotalFCY">
                        </div>
                    </div>

                    <div class="two-columns six-columns-mobile" >
                   <button type="submit" class="button anthracite-gradient" onclick="Submit()">Update</button>
                    <button type="submit" class="button anthracite-gradient" onclick="FCYHide()">Cancel</button>
                </div>
            </div>
                </div>
         <div class="clear"></div>
        <div class="with-small-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_log">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Currency</th>
                            <th scope="col" class="align-center">Exchange Rate</th>
                            <th scope="col" class="align-center">Markup</th>
                            <th scope="col" class="align-center">Total</th>
                            <%--<th scope="col" class="align-center">Update Date</th>--%>
                            <th scope="col" class="align-center">Update By</th>
                            <th scope="col" class="align-center">Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
