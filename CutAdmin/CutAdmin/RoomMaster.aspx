﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="RoomMaster.aspx.cs" Inherits="CutAdmin.RoomMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">

    <script src="Scripts/RoomMaster.js?v=1.2"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section role="main" id="main">
          <hgroup id="main-title" class="thin">
            <h1>Room Type</h1>
        </hgroup>
          <hr />
          <div class="with-padding">
            <div class="columns">
             <div class="three-columns two-columns-tablet twelve-columns-mobile">
                    <label for="block-label-1" class="label">Room Type</label>
                   
                  <input type="text" name="block-label-1" id="txt_Roomtype" class="input full-width" value="">
                <input type="hidden" id="RoomTypeID">
                  </div>
                <div class="three-columns two-columns-tablet twelve-columns-mobile" style="padding-top: 0.5%">
                    <br />
                    <button type="button" class="button anthracite-gradient" onclick="AddRoomType()" id="btn_Add">ADD</button>
                     <button type="button" class="button anthracite-gradient" onclick="UpdateRoomType()" style="display: none" id="btn_Update">Update</button>
                     </div>
                
                
                  </div>
             <table class="table responsive-table" id="tbl_RoomMaster">
                <thead>
                    <tr>
                        <th scope="col"  class="align-center">Sr No</th>
                        <th scope="col"  class="align-center">Room Type</th>
                        <th scope="col"  class="align-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          
         </div>
     </section>

</asp:Content>
