﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin
{
    public class VoucherManager
    {

        static DBHandlerDataContext db = new DBHandlerDataContext();
        public static string GenrateVoucher(string ReservationID, string Uid, string Status)
        {
            try
            {
                string sVoucher = "";
                Common.Common.BookingID = ReservationID;
                var arrResrvation = Common.Common.arrBookingDetails;
                var AgentDetails = Services.User._DetailByUserEmail(arrResrvation.Email);
                using (var db = new DBHandlerDataContext())
                {
                    var arrPaxes = (from obj in db.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                    var arrRooms = (from obj in db.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                    string RoomDetails = "",Cancellation="";
                    #region Room Details
                    long AdminID = AccountManager.GetSupplierByUser();
                    ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
                    string HotelCode = arrResrvation.HotelCode;
                    var dtHotelAdd = (from obj in cdb.tbl_CommonHotelMaster where obj.sid.ToString() == HotelCode select obj).FirstOrDefault();
                    string RoomName = "", Canceldate = "",SrNo="";
                    foreach (var arrRoom in arrRooms)
                    {
                        string PaxName = "";
                        long Adult = arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "AD").ToList().Count;
                        long Child = arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "CH").ToList().Count;
                        string ChildAge = "-";
                        if(Child!=0)
                            foreach (var arrPax in arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "CH").ToList())
                            {
                                ChildAge += arrPax.Age + ",";
                            }
                        foreach (var arrPax in arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber).ToList())
                            PaxName +=   arrPax.Name + " ";
                        TemplateGenrator.GetTemplatePath(Convert.ToInt64(AdminID), "Voucher Room");
                        TemplateGenrator.arrParms = new Dictionary<string, string>
                        {
                              {"No",(arrRooms.IndexOf(arrRoom) + 1).ToString()},
                              {"roomname",arrRoom.RoomType },
                              {"mealplan",arrRoom.BoardText},
                              {"Paxname",PaxName},
                              {"adult",Adult.ToString()},
                              {"child",Child.ToString()},
                              {"ages",ChildAge.TrimStart('-') },
                              {"remark",arrRoom.Remark},
                        };
                        if (arrRoom.EssentialInfo != "")
                            TemplateGenrator.arrParms.Add("Free Addons: addons", "<b>Free Addons: <b>" +arrRoom.EssentialInfo); 
                        else
                            TemplateGenrator.arrParms.Add("Free Addons: addons", "");
                        RoomDetails += TemplateGenrator.GetTemplate;

                        #region Cancellation Policy
                        SrNo += (arrRooms.IndexOf(arrRoom) + 1).ToString() + "<br/><br/>";
                        RoomName += arrRoom.RoomType + "<br/><br/>";
                        Canceldate += arrRoom.CutCancellationDate + "<br/><br/>";
                        #endregion
                    }
                    TemplateGenrator.GetTemplatePath(Convert.ToInt64(AdminID), "Cancellation Policy");
                    TemplateGenrator.arrParms = new Dictionary<string, string>
                        {
                              {"sr",SrNo},
                              {"roomname",RoomName},
                              {"date",Canceldate},
                        };
                    Cancellation = TemplateGenrator.GetTemplate;
                    #endregion
                    string Phone = "-", Fax = "-";
                    if(dtHotelAdd != null)
                    {
                        Phone = dtHotelAdd.MobileNo;
                        Fax = "-";
                    }
                    TemplateGenrator.GetTemplatePath(Convert.ToInt64(AdminID), "Voucher");
                    TemplateGenrator.arrParms = new Dictionary<string, string>
                        {
                              {"AgencyName",AgentDetails.Name},
                              {"address",AgentDetails.Address},
                              {"city",AgentDetails.City},
                              {"mobile",AgentDetails.Mobile},
                              {"phone",AgentDetails.phone},
                              {"PinCode",AgentDetails.PinCode},
                              {"supportMail",AgentDetails.email},
                              {"Country",AgentDetails.Countryname},
                              /*Hotel Info*/
                              {"hotelname ",arrResrvation.HotelName},
                              {"location",arrResrvation.City},
                              {"checkin",arrResrvation.checkin},
                              {"checkout",arrResrvation.checkout},
                              {"@phone",Phone},
                              {"@fax",Fax},
                              {"ROOM",RoomDetails},
                              {"Policy",Cancellation},
                              { "http://img.mailinblue.com/2096904/images/rnb/original/5ce91ecece057438f265bb01.jpg", "https://clickurhotel.com/Agencylogo/"+  AgentDetails.Code + ".jpg"}  ,
                        };
                    sVoucher = TemplateGenrator.GetTemplate;
                }
                return sVoucher;
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return "";
            }
        }

        private static bool UrlExists(string url)
        {
            try
            {
                new System.Net.WebClient().DownloadData(url);
                return true;
            }
            catch (System.Net.WebException e)
            {
                if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                else
                    throw;
            }
        }
    }
}