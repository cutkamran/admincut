﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelModule/HotelMaster.Master" AutoEventWireup="true" CodeBehind="availability_pricing.aspx.cs" Inherits="CutAdmin.HotelModule.availability_pricing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <%-- <script src="Scripts/Hotels/Hotels.js"></script>--%>
    <script src="../Scripts/Supplier.js"></script>
    <script src="../Scripts/Hotels/Markets.js?v=1.0"></script>
    <script src="../Scripts/availability_pricing.js?v=1.66"></script>
    <script src="../Scripts/availability_Inventory.js?v=1.96"></script>
    <link rel="stylesheet" href="../js/libs/glDatePicker/developr.fixed.css?v=1" />
    <script src="../js/developr.modal.js?v=1.0"></script>
    <style>
        label span {
            margin: 0px;
            float: left
        }
        #ui-datepicker-div {
            z-index:10000000,!important;
        }
        .dataTables_scrollBody{

        }::-webkit-scrollbar
{
  width: 8px;  /* for vertical scrollbars */
  height: 8px; /* for horizontal scrollbars */
  z-index: 999000;
  border-radius: 3px;
}

::-webkit-scrollbar-track
{
  background: transparent;
}

::-webkit-scrollbar-thumb
{
position: absolute;
    background: url(../img/old-browsers/grey50.png);
    background:rgba(243, 237, 237, 0.69);
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    cursor: pointer;
    -webkit-box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);
    -moz-box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);
    box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);

}
        @media only screen and (max-width: 767px) {

            span {
                font-size: .8em;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
            <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24 "><b>Availability Calander & Rates</b>
                <p class="button-group compact float-right">
                    <label for="Freesale_Id" class="button green-active" >
                        <input type="radio" name="InvType" id="Freesale_Id" onchange="GenrateDatesRoom()" value="Freesale" checked>
                        Freesale
                    </label>
                    <label for="Allocation_Id" class="button green-active " >
                        <input type="radio" name="InvType" id="Allocation_Id" onchange="GenrateDatesRoom()" value="Allocation">
                        Allocation
                    </label>
                    <label for="Allotmnet_Id" class="button green-active">
                        <input type="radio" name="InvType" id="Allotmnet_Id" onchange="GenrateDatesRoom()" value="Allotment">
                       Alotment  
                    </label>
                    
                </p>

            </h3>
            <hr />
        </hgroup>
        <div class="with-padding ">
            <div class="columns">
                <div class="three-columns twelve-columns-mobile  button-height">
                    <span class="input">
                        	<label for="pseudo-input-2" class="button grey-gradient">
								<i class="icon-home glossy"></i>
							</label>
                        <select id="sel_Hotel" name="validation-select" class="select  compact"  style="width:200px" >
                   </select>
                    </span>
                </div>
                <div class="three-columns twelve-columns-mobile button-height">
                    <span class="input">
                        <label for="" class="button grey-gradient glossy icon-chevron-thin-left" onclick="Previous();" title="Previous"></label>
                        <input type="text" id="datepicker_start" onchange="GenrateDatesRoom()" class="input-unstyled dt1" name="datepicker_From" style="width: 70px" value="">
                    </span>
                    <span class="input">
                        <input type="text" id="datepicker_end" onchange="GenrateDatesRoom()" class="input-unstyled dt2" name="datepicker_From" style="width: 70px" value="">
                        <label for="" class="button grey-gradient glossy icon-chevron-thin-right" onclick="Next();" title="Next"></label>
                    </span>
                </div>
                <div class="two-columns six-columns-mobile button-height">
              <select id="SelMealPlan" name="validation-select" class="select  full-width" >
              </select>
        </div>
                <div class="two-columns six-columns-mobile button-height">
              <select id="Sel_Supplier" name="validation-select" class="select  full-width" >
              <option selected value="0">Direct Hotel</option>
              </select>
        </div>
                <div class="two-columns six-columns-mobile button-height">
        
                    <span class="button-group compact">
                     
                        <label for="rdb_Selling" class="button green-active" >
                            <input type="radio" name="radio-buttons" id="rdb_Selling" value="2"  onclick="CheckByPrice()" checked>
                            Selling
                        </label>
                           <label for="rdb_Cost" class="button green-active" >
                            <input type="radio" name="radio-buttons" id="rdb_Cost" value="1"  onclick="CheckByPrice()">
                            Cost
                        </label>
                    </span>
                </div>
            </div>
              <div class="columns" >
                  <div class="three-columns six-columns-mobile button-height">
                        <span class="input">
                        	<label for="pseudo-input-2" class="button grey-gradient with-tooltip" title="Rate Type">
								<i class="icon-paperclip"></i>
							</label>
                        <select id="sel_RateType" name="validation-select" class="select  compact expandable-list" style="width:200px" >
                   </select>
                    </span>
                     
                  </div>
              <div class="three-columns hidden six-columns-mobile button-height" id="divofferType">
                    <span class="input">
                        	<label for="pseudo-input-2" class="button orange-gradient with-tooltip" title="Valid Offers">
								<i class="icon-rocket"></i>
							</label>
                         <select name="pseudo-input-select" id="selOfferType"  class="select compact expandable-list" style="width:200px"  >
                   </select>
                    </span>
              </div>
            </div>
            <div id="table-scroll" class="table-scroll">
                <div class="respTable" id="div_tbl_InvList">
                </div>
            </div>
        </div>

     <%-- Inventory Filter--%>
       <div id="Marketfilter" class="<%--green-gradient--%>"  style="display: none;max-height:200px; overflow-y:scroll">
           <form id="frm_Inventory">
           <input type="hidden" id="txt_RoomID" />
           <input type="hidden" id="txt_RoomName" />
          
          <p class="button-height"  style="">
           <select class="select easy-multiple-selection anthracite-gradient check-list" id="sel_Market" multiple>
           </select>
           </p>
           </form>
        
       </div>
       <script>
        function openModal(content, buttons) {
            $.modal({
                title: 'Modal window',
                content: content,
                buttons: buttons,
                beforeContent: '<div class="carbon">',
                afterContent: '</div>',
                buttonsAlign: 'center',
                resizable: false
            });
        }
    </script>

    <%-- Cancel Filter--%>
       <div id="Cancelfilter" class="<%--green-gradient--%>" style="width:500px;display: none;">
           <form id="frm_Cancel">
           
           </form>
        
       </div>

     <%-- Offer Filter--%>
       <div id="Offerfilter" class="<%--green-gradient--%>" style="width:500px;display: none;">
           <form id="frm_Offer">
           
           </form>
        
       </div>

     <%-- Inventory Filter--%>
       <div id="OfferfilterType" class="<%--green-gradient--%>" style="width:200px;display: none;">
           <form id="">
           <select class="select  auto-open  full-width validate[required]" id="sel_OfferType" onchange="AddInventory()">
               <option value="" disabled>-Select Offer Type-</option>
           </select>
           </form>
        
       </div>
</asp:Content>
