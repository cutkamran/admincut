﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelModule/HotelMaster.Master" AutoEventWireup="true" CodeBehind="BookingList.aspx.cs" Inherits="CutAdmin.HotelModule.BookingList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../Scripts/Invoice.js?v=1.3"></script>
    <script src="Scripts/BookingList.js?v=3.4"></script>
    <script src="../Scripts/Booking.js?v=2.1"></script>
    <style>
        #ConfirmDate {
            z-index: 99999999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <hgroup id="main-title" class="thin">
            <h1>Hotel Booking Report</h1>
            <hr />
            <h2><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <div class="with-small-padding">
                <div class="standard-tabs margin-bottom" id="filter" style="display: none;">
                    <ul class="tabs">
                        <li class="Hotel"><a href="#BookingReport">Booking Report</a></li>
                        <li class="Hotel"><a href="#ArrivalReport">Arrival Report</a></li>
                        <li class="Hotel"><a href="#ClientReservation">Client Reservation</a></li>
                    </ul>
                    <div class="tabs-content">
                        <div class="with-mid-padding anthracite-gradient" id="BookingReport">
                            <form class="form-horizontal">
                                <div class="columns">
                                    <div class="three-columns  twelve-columns-mobile">
                                        <label>Agencies</label>
                                        <select id="sel_Agency" class="select">
                                        </select>
                                    </div>
                                    <div class="two-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-In</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" autocomplete="off" id="Check-In" class="input full-width" value="">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-Out</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="Check-Out" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile inputWithIcon">
                                        <label>Booking Date</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="Bookingdate" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Passenger Name </label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reference No.</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Reference" autocomplete="off" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Hotel Name</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_HotelSearch" autocomplete="off" class="input-unstyled  ui-autocomplete-input full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Location</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Location" autocomplete="off" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reservation Status</label>
                                        <div class="full-width button-height typeboth">
                                            <select id="selReservation" class="select">
                                                <option selected="selected">All</option>
                                                <option>Vouchered</option>
                                                <option>Cancelled</option>
                                                <option>OnRequest</option>
                                                <option>Reconfirmed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                        <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                                        <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                                    </div>
                                    <div class="two-columns twelve-columns-mobile bold text-alignright">
                                        <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                            <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                        </span>
                                        <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                            <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="with-mid-padding anthracite-gradient" id="ArrivalReport">
                            <form class="form-horizontal">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-In</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="CheckIn" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-Out</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="CheckOut" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="four-columns twelve-columns-mobile bold">
                                        <label>Hotel Name</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_HotelName" autocomplete="off" class="input-unstyled  ui-autocomplete-input full-width">
                                        </div>
                                    </div>
                                    <div class="one-column twelve-columns-mobile bold text-alignright">
                                        <span class="icon-pdf right" onclick="ArrivalReport()">
                                            <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer; margin-top: 15px" title="Export To Pdf" height="35" width="35">
                                        </span>
                                    </div>
                                </div>
                                <br />
                            </form>
                        </div>
                        <div class="with-mid-padding anthracite-gradient" id="ClientReservation">
                            <form class="form-horizontal">
                                <div class="columns">
                                    <div class="two-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-In</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="Check_In" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile inputWithIcon">
                                        <label>Check-Out</label>
                                        <i class="icon-calendar"></i>
                                        <input type="text" name="datepicker" id="Check_Out" autocomplete="off" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Hotel Name</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt-HotelName" autocomplete="off" class="input-unstyled  ui-autocomplete-input full-width">
                                        </div>
                                    </div>
                                    <div class="four-columns  twelve-columns-mobile">
                                        <label>Agencies</label>
                                        <div class="full-width button-height">
                                            <select id="Agency_List" class="select">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="one-column twelve-columns-mobile bold text-alignright">
                                        <span class="icon-pdf right" onclick="ArrivalReportForClient()">
                                            <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer; margin-top: 15px" title="Export To Pdf" height="35" width="35">
                                        </span>
                                    </div>
                                </div>
                                <br />
                            </form>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        </hgroup>
        <div class="with-small-padding">
            <div class="respTable">
                <table class="table responsive-table font10" id="tbl_BookingList">
                    <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Ref No.</th>
                           <%-- <th scope="col" class="align-center">Agency</th>--%>
                            <th scope="col" class="align-center">Passenger</th>
                            <th scope="col" class="align-center">Hotel &amp; Location</th>
                            <th scope="col" class="align-center">Stay</th>
                            <th scope="col" class="align-center">Room</th>
                            <th scope="col" class="align-center">Status</th>
                          <%--  <th scope="col" class="align-center">Amount</th>
                            <th scope="col" class="align-center">Actions</th>--%>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
  
    <script>
        $(function () {
            $("#txt_Hotel").autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../handler/SearchHandler.asmx/GetHotels",
                        data: "{'name':'" + document.getElementById('txt_Hotel').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    jQuery('#hdnDCode').val(ui.item.id);
                }
            });

        })
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
