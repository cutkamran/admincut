﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelModule/HotelMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="CutAdmin.HotelModule.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <!-- Additional styles -->
    <script src="Scripts/Dashboard.js?v=1.99"></script>
    <script src="Scripts/BookingList.js?v=1.4"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="../js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Main content -->
 
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <div class="standard-tabs margin-bottom">
            <!-- Tabs -->
            <ul class="tabs">
                <li class="Hotel"><a href="#hotel-tab">Hotel</a></li>
               <%-- <li><a href="#Airlines">Another tab</a></li>
                <li><a href="#tab-3">Another tab</a></li>
                <li class="disabled"><a href="#tab-4">Disabled tab</a></li>
                <li>Non-active</li>--%>
            </ul>
            <div class="tabs-content">
                <!-- Hotel Tab -->
                <div id="hotel-tab" class="">
                    <!--Black Strip -->
                    
                    <div class="with-padding">
                    
                    <div class="columns datepicker" id="">
                    </div>
                </div>
                </div>
           </div>
        </div>

        <script>
            var UserType = '<%=Session["UserType"]%>';
            if (UserType != "Hotel") {
                $("#divlist").show();
            }
        </script>
    
</asp:Content>
