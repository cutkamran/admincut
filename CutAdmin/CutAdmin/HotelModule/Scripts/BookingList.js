﻿$(document).ready(function () {
    var Status = getParameterByName('Status');
    var type = getParameterByName('Type');
    BookingListAll();
    if (type != "") {
        BookingListFilter(Status, type);
    }

    $("#Check-In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Check-Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#CheckIn").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#CheckOut").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#Check_In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    $("#Check_Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var BookingList;
var arrAgency = new Array();
function BookingListAll() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/BookingList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAgency = result.arrAgents;
                GenrateAgency();
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function BookingListFilter(status, type) {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var data = {
        status: status,
        type: type
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/BookingListFilter",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAgency = result.arrAgents;
                GenrateAgency()
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}


function ArrivalReport() {
    var CheckIn = $("#CheckIn").val();
    var CheckOut = $("#CheckOut").val();
    var HotelName = $("#txt_HotelName").val();
    window.location.href = "../HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=ArrivalReportHotlier&CheckIn=" + CheckIn + "&CheckOut=" + CheckOut + "&HotelName=" + HotelName;
}


function ArrivalReportForClient() {
    var CheckIn = $("#Check_In").val();
    var CheckOut = $("#Check_Out").val();
    var HotelName = $("#txt-HotelName").val();
    var Agency = $("#Agency_List option:selected").val();
    if (Agency == "-") {
        Agency = "";
    }
    window.location.href = "../HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=HotlierReservationStatement&CheckIn=" + CheckIn + "&CheckOut=" + CheckOut + "&HotelName=" + HotelName + "&Agency=" + Agency;
}

//function ArrivalReport() {
//    $.ajax({
//        type: "POST",
//        url: "../handler/BookingHandler.asmx/ArrivelReport",
//        data: JSON.stringify({}),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//            }
//            else {
//            }
//        }
//    })
//}

/*function htmlGenrator() {
    // $("#tbl_BookingList").empty();
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++) {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;
        //if (BookingList[i].IsConfirm == true)
        //    trow += '<tr style="background:#6ead12;color:white">';
        //else
            trow += '<tr>';

        trow += '<td style="width:3%">' + (i + 1) + '</td>';

        trow += '<td style="width:12%">' + BookingList[i].ReservationDate + ' </td>';

        if (BookingList[i].IsConfirm && BookingList[i].Status != 'Cancelled') {
            trow += '<td style="width:7%">' + BookingList[i].ReservationID + '<br/><small class="tag green-bg">Confirmed</small></td>';
        }
        else if (BookingList[i].Status == 'Vouchered' && BookingList[i].IsConfirm != true) {
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Re-Confirm" onclick="REConfirmBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a></td>';
        }
        else if(BookingList[i].Status == 'Cancelled'){
            trow += '<td style="width:7%"><a>' + BookingList[i].ReservationID + ' </a><br/><small class="tag red-bg">Cancelled</small></td>';
        }
        else if (BookingList[i].Status == 'OnRequest') {
            trow += '<td style="width:7%"><a>' + BookingList[i].ReservationID + ' </a><br/><small class="tag blue-bg">Onrequest</small></td>';
        }
        else
            trow += '<td style="width:7%"><a>' + BookingList[i].ReservationID + ' </a></td>';
        trow += '<td>' + BookingList[i].AgencyName + ' </td>';
        trow += '<td style="width:10%">' + BookingList[i].bookingname + '</td>';
        trow += '<td style="width:20%">' + BookingList[i].HotelName + ', ' + BookingList[i].City + ' </td>';
        trow += '<td style="width:11%">' + BookingList[i].CheckIn + ' </td>';
        trow += '<td style="width:11%">' + BookingList[i].CheckOut + ' </td>';
        trow += '<td style="width:4%">' + BookingList[i].TotalRooms + ' </td>';

        if (BookingList[i].Status == 'Vouchered') {
            trow += '<td style="width:6%">Definite</td>';
        }
        else {
            //  trow += '<td style="width:6%" onclick="ConfirmHoldBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].Status + ' </td>';
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Confirm" onclick="ConfirmBookingModal(\'' + BookingList[i].ReservationID + '\')">' + BookingList[i].Status + ' </a></td>';
        }

        if (BookingList[i].Status != "GroupRequest")
            trow += '<td><span style="width:10%"><i class="' + GetCurrencyIcon(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        else
            trow += '<td><span style="width:10%">n/a</span></td>';

        trow += '<td  class="align-center">'
        if (BookingList[i].Status != "GroupRequest") {
            trow += '<a style="cursor:pointer;padding-left: 5px;" title="Invoice"   onclick="GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-cc-share tracked"></span></a>';
            if (BookingList[i].Status == 'OnRequest' || BookingList[i].Status == 'Cancelled')
                trow += '<span style="cursor:pointer;padding-left: 5px;" class="icon-page-list-inverted disabled"></span> ';
            else
                trow += '<a style="cursor:pointer;padding-left: 5px;" title="Voucher"   onclick="GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-page-list-inverted"></span> </a>';
        }
        else {
            trow += '<span style="width:10%">-</span><';
        }
       

        if (BookingList[i].Status == "GroupRequest" || BookingList[i].Status == "OnRequest") {
            trow += '<span style="width:10%">-</span>';
        }
        else {
            trow += '<a style="cursor:pointer;padding-left: 5px;" title="Amendment"   onclick="GetAmendment(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-cc-share tracked"></span></a>';
        }
        trow += ' </td>';
        trow += '</tr>';
    }
    trow += '</tbody>'
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()
    $("#tbl_BookingList").dataTable({
        bSort: true, sPaginationType: 'full_numbers',
    });
    $("#tbl_BookingList").removeAttr("style");
}*/

function htmlGenrator() {
    // $("#tbl_BookingList").empty();
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++)
    {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;

        if (BookingList[i].IsConfirm == "True")
            BookingList[i].IsConfirm = true;

        if (BookingList[i].IsConfirm == "False")
            BookingList[i].IsConfirm = false;

        trow += '<tr>';
        trow += '<td style="max-width:25px; vertical-align: middle;" class="center">' + (i + 1) + '</td>';
        trow += '<td style="min-width:60px; vertical-align: middle;" class="center">' + BookingList[i].ReservationDate + ' </td>';
        trow += '<td style="max-width: 115px; vertical-align: middle;" class="center">'
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<a style="cursor:pointer" title="Booking Cancelled">' + BookingList[i].ReservationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
        }
        else if (BookingList[i].Status == 'OnRequest' && BookingList[i].IsConfirm == false) {
            trow += '<a style="cursor:pointer" title="Booking On Request">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
        }
        else if (BookingList[i].Status == 'Rejected' && BookingList[i].IsConfirm == false) {
            trow += '<a style="cursor:pointer" title="Booking Rejected" >' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
        }
        else if (BookingList[i].Status == 'OnHold' && BookingList[i].IsConfirm == false) {
            trow += '<a style="cursor:pointer" title="Booking On Hold">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
        }
        else if (BookingList[i].IsConfirm) {
            trow += '<a style="cursor:pointer" title="Confirmed" >' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-reconfirmed">Confirmed</span>';
        }
        else {
            trow += '<a style="cursor:pointer" title="Reconfirmation Pending">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
        }
        trow += '</td>'
        //trow += '<td style="vertical-align: middle; width: 100px;">' + BookingList[i].AgencyName + ' </td>';
        trow += '<td style="vertical-align: middle;width: 100px;">' + BookingList[i].bookingname + '</td>';
        trow += '<td style="min-width: 100px; vertical-align: middle;">' + BookingList[i].HotelName + '</br><b>' + BookingList[i].City + '</b></td>';
        trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckIn + '<br> to </br>' + BookingList[i].CheckOut + ' </td>';
        /* trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckOut + ' </td>';*/
        trow += '<td style="max-width: 20px; vertical-align: middle;" class="center">' + BookingList[i].TotalRooms + ' </td>';
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-cancelled">Cancelled</span></td>';
        } else if (BookingList[i].Status == 'Vouchered') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-vouchered">Vouchered</span></td>';
        } else if (BookingList[i].Status == 'OnRequest') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onrequest">On Request</span></td>';
        } else if (BookingList[i].Status == 'OnHold') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onhold">On Hold</span></td>';
        } else {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-rejected">Rejected</span></td>';
        }

       // trow += '<td style="vertical-align: middle; min-width: 60px;" class="align-right"><span class="strong"><i class="' + GetCurrency(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        /* trow += '<td style="width:3%" class="align-center"><a style="cursor:pointer" title="Invoice" class="button"  onclick="GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-cc-share tracked"></span></a></td>';*/
        //trow += '<td style="max-width: 55px; vertical-align: middle;" class="align-center action-button">';
        //trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Voucher</a >';
        //trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Invoice</a >';

        //if (BookingList[i].Status != 'Cancelled') {
        //    trow += '<a title = "Click to Modify Booking" class="modify-btn"  onclick="GetAmendment(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">Modify</a >';
        //    trow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="OpenCancellationModel(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Status + '\')">Cancel</a> ';
        //}
        //trow += ' </td>';
        trow += '</tr>';
    }
    trow += '</tbody>'
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()
    $("#tbl_BookingList").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $("#tbl_BookingList").removeAttr("style");
}

function Search() {

    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var AgencyName = $("#sel_Agency option:selected").text();

    AgencyName = AgencyName.split("(");
    AgencyName = AgencyName[0];
    if (AgencyName == "Select Agency") {
        AgencyName = ";"
    }
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_HotelSearch").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();

    var data = {
        AgencyName: AgencyName,
        CheckIn: CheckIn,
        CheckOut: CheckOut,
        Passenger: Passenger,
        BookingDate: BookingDate,
        Reference: Reference,
        HotelName: HotelName,
        Location: Location,
        ReservationStatus: ReservationStatus
    }

    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_BookingList").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });

}

function Reset() {
    $("#sel_Agency").val('');
    $("#Check-In").val('');
    $("#Check-Out").val('');
    $("#txt_Passenger").val('');
    $("#Bookingdate").val('');
    $("#txt_Reference").val('');
    $("#txt_HotelSearch").val('');
    $("#txt_Location").val('');
    $("#selReservation").val('All');
}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}

function ExportBookingDetailsToExcel(Document) {
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_HotelSearch").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();
    var Type = "All";
    if (CheckIn == "" && CheckOut == "" && Passenger == "" && BookingDate == "" && Reference == "" && HotelName == "" && Location == "" && ReservationStatus == "All") {
        window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=SupplierBookingDetails&Type=" + Type + "&Document=" + Document;
    }
    else {
        Type = "Search";
        window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=SupplierBookingDetails&Type=" + Type + "&Document=" + Document;
    }
}

////  Confirm On Request booking //////
function ConfirmBookingModal(ReservationID) {
    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                var Detail = result.Detail;

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].bookingname + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                   '<br/><input id="btn_ConfirmBooking" type="button" value="Confirm Booking" class="button anthracite-gradient" style="width: 20%; float:right" onclick="ConfirmOnRequestBooking(\'' + ReservationID + '\');" />' +

                  '</div>',
                    title: 'Confirm OnRequest Booking',
                    width: 700,
                    height: 280,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true

                });
            }

            else if (result.retCode == 0) {
                Success('Something Went Wrong');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            Success("Getting Error");
            setTimeout(function () {
                window.location.href = "bookinglist.aspx";
            }, 2000);
        }
    });

}

function ConfirmOnRequestBooking(ReservationID) {
    var data =
        {
            ReservationId: ReservationID
        }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/ConfirmOnRequestBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                Success('Booking Confirm Sucessfully.');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
            else if (result.retCode == 0) {
                Success('Something Went Wrong');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }

            else if (result.retCode == 2) {
                Success('Inventory is not available please update inventory and try again.');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            Success("Getting Error");
            setTimeout(function () {
                window.location.href = "bookinglist.aspx";
            }, 2000);
        }
    });

}


///// For Reconfirmation /////////////////
function REConfirmBooking(ReservationID, Uid, Status, Source) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].bookingname + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" style="width: 100%">' +
                   '<tr>' +
                  '<h4>Re-Confirmation Detail</h4>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="float:right" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input style="float:right" type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel Staff Name :&nbsp;&nbsp; <input style="float:right" type="text" id="StaffName" class="input" > </span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select style="float:right" id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +

                  '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                  '<tr>' +
                    '<td style="border-bottom: none;" >' +
                  '<span class="text-left">Comment :  <input  type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                   '' +
                  '</td>' +
                  '</tr>' +
                   '</table>' +

                   '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                  '</div>',
                    title: 'Re-confirm Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true

                });

                //$("#ConfirmDate").datepicker({
                //    changeMonth: true,
                //    changeYear: true,
                //    dateFormat: "dd-mm-yy",
                //    //onSelect: insertDepartureDate,
                //    //minDate: "dateToday",
                //    //maxDate: "+3M +10D"
                //});
                $('#ConfirmDate').glDatePicker({
                    zIndex: 999300,
                    allowMonthSelect: true,
                    allowYearSelect: true,
                    todayDate: new Date(),
                    position: 'inherit',
                    monthNames: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    onClick: (function (el, cell, date, data) {
                        var armco = date.toLocaleDateString();
                        armco = toDate(armco);
                        el.val(armco);
                    })
                });

            }
            else if (result.retCode == 0) {
                Success('Something Went Wrong');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            Success("Getting Error");
            setTimeout(function () {
                window.location.href = "bookinglist.aspx";
            }, 2000);
        }
    });
}

////function REConfirmBooking(ReservationID, Uid, Status, Source) {
////    //OpenCancellationPopup(ReservationID, Status);
////    $("#hdn_Supplier").val(Source);
////    //$("#hdn_AffiliateCode").val(AffilateCode);
////    OpenHoldPopup(ReservationID, Status);

////    //$("#hdn_HoldDate").val(HoldDate);
////    //$("#hdn_DeadLineDate").val(DeadLineDate);
////    //$('#ConfirmAlertForOnHoldModel').modal('show');
////}

function SaveConfirmDetail(ReservationID, status, HotelName) {
    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").val();
    if (ReconfirmThrough == "-") {
        Success('Please Select Reconfirm Through.');
        return false;
    }

    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,
    }

    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/SaveConfirmDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Confirm Detail Save.");
                $("#ConfirmDate").val('');
                $("#HotelConfirmationNo").val('');
                $("#StaffName").val('');
                $("#ReconfirmThrough option:selected").val('-');
                $("#Comment").val('');
                setTimeout(function () {
                    window.location.href = "BookingList.aspx";
                }, 2000);
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });


}


//// For Hold Booking////////
function ConfirmHoldBooking(ReservationID, status, HotelName) {
    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
    }

    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/ConfirmHoldBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Booking Confirm Successfully.");
                setTimeout(function () {
                    window.location.href = "BookingList.aspx";
                }, 2000);
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });
}

function ConfirmHoldBookingModal(ReservationID, Uid, Status, Source) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                $.modal({
                    content:

                        '<div class="modal-body">' +
                        '' +
                        '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                        '<tr>' +
                        '<h4>Booking Detail</h4>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' +
                        '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' +
                        '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].bookingname + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' +
                        '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td>' +
                        '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '</table>' +

                        '<br/><input id="btn_ConfirmHoldBooking" type="button" value="Confirm Booking" class="button anthracite-gradient" style="width: 20%; float:right" onclick="ConfirmHoldBooking(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                        '</div>',
                    title: 'Confirm Hold Booking',
                    width: 700,
                    height: 200,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
            else if (result.retCode == 0) {
                Success('Something Went Wrong');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            Success("Getting Error");
            setTimeout(function () {
                window.location.href = "bookinglist.aspx";
            }, 2000);
        }
    });
}

///////////////////////////////////////

function GetAmendment(ReservationID, Uid, Status, Source) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetAmendmentDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            var html = "";

            if (result.retCode == 1) {
                var Detail = result.Detail;
                var count = 0;
                for (var i = 0; i < Detail.length; i++) {
                    count = Detail[i].RoomNumber;
                }

                for (var i = 0; i < count; i++) {
                    var arrRoom = $.grep(Detail, function (h) { return h.RoomNumber == (i + 1) })
                                    .map(function (h) { return h; });



                    html += '<div class="columns Rooms" >';
                    html += ' <hgroup id="main-title" class="thin"><h4>Room No ' + arrRoom[0].RoomNumber + '  </h4><hr></hgroup>';
                    for (var r = 0; r < arrRoom.length; r++) {

                        var Name = arrRoom[r].Name;
                        var LastName = arrRoom[r].LastName;

                        var Type = "";
                        if (Name.split(" ")[0] == "AD")
                            Type = "Mr"
                        else
                            Type = "Marster"

                        html += '<div class="columns Room' + i + '">';
                        html += '<div class="new-row  three-columns twelve-columns-mobile custumer">';
                        //html += '<select class="select full-width sel_Gender" selected="' + Name.split(" ")[0] + '">';
                        //html += '<option value="Mr">Mr</option>';
                        //html += '<option value="Mrs">Mrs</option>';
                        //html += '<option value="Master">Master</option>';
                        //html += '</select>';
                        html += '<input type="text" class="input full-width sel_Gender"  value="' + Name.split(" ")[0] + '" readonly="readonly">';
                        html += '<input type="hidden" class="Age" value="' + arrRoom[r].Age + '">'
                        html += '</div>';
                        html += '<div class="four-columns twelve-columns-mobile">';
                        html += '<input type="text" class="input full-width txtFirstName" placeholder="First Name" value="' + Name.split(" ")[1] + '" id="">';
                        html += '</div>';
                        html += '<div class="four-columns twelve-columns-mobile">';
                        html += '<input type="text" class="input full-width txtLastName" placeholder="Last Name" value="' + LastName + '" id="">';
                        html += '</div>';
                        html += '</div>';
                        //$("#sel_Gender" + arrRoom[r].RoomNumber + "").val(Name.split(" ")[0]);
                        //$("#txtFirstName" + arrRoom[r].RoomNumber + "").val(Name.split(" ")[1]);
                        //$("#txtLastName" + arrRoom[r].RoomNumber + "").val(LastName);

                    }
                    html += '</div>';



                }

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +

                   html +

                  //'<br><br><table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                  //'<tr>' +
                  //  '<td style="border-bottom: none;" >' +
                  //'<span class="text-left">Remark :  <input type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                  // '' +
                  //'</td>' +
                  //'</tr>' +
                  // '</table>' +

                  '<br/><input id="btn_SaveAmendemnt" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveAmendemnt(\'' + ReservationID + '\');" />' +

                  '</div>',

                    title: 'Amendment Detail',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true

                });



            }
            else if (result.retCode == 0) {
                Success('Something Went Wrong');
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            Success("Getting Error");
            setTimeout(function () {
                window.location.href = "bookinglist.aspx";
            }, 2000);
        }
    });
}

function SaveAmendemnt(ReservationID) {
    var Custumer = new Array();
    var arrRooms = $(".Rooms");
    for (var i = 0; i < arrRooms.length; i++) {
        var ndCustumers = $(arrRooms[i]).find(".Room" + i)
        for (var c = 0; c < ndCustumers.length; c++) {
            var Type = "";
            if ($(ndCustumers[c]).find(".sel_Gender").val() == "Mr" || $(ndCustumers[c]).find(".sel_Gender").val() == "Mrs")
                Type = "AD"
            else if ($(ndCustumers[c]).find(".sel_Gender").val() == "Master")
                Type = "CH"
            var Leading;
            if (c == 0)
                Leading = true;
            else
                Leading = false;

            var arrCustumer =
                {
                    ReservationID: ReservationID,
                    PassengerType: Type,
                    LastName: $(ndCustumers[c]).find(".txtLastName").val(),
                    // $("#txtLastName" + (c)).val(),
                    Name: $(ndCustumers[c]).find(".sel_Gender").val() + " " + $(ndCustumers[c]).find(".txtFirstName").val(),
                    //$("#txtFirstName" + (c)).val(),
                    RoomNumber: (i + 1),
                    Age: $(ndCustumers[c]).find(".Age").val(),
                    IsLeading: Leading
                }


            Custumer.push(arrCustumer);
        }

    }

    var data = {
        ReservationID: ReservationID,
        ListPax: Custumer
    }

    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/SaveAmendemnt",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Amendment Detail Save.");
                setTimeout(function () {
                    window.location.href = "bookinglist.aspx";
                }, 2000);
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });
}

function GenrateAgency() {
    var opt = '';
    try {
        opt += '<option value="-" selected="selected">Select Agency</option>'
        for (var i = 0; i < arrAgency.length; i++) {
            opt += '<option value="' + arrAgency[i].sid + '">' + arrAgency[i].Name + '</option>'
        }
        $("#sel_Agency").append(opt);
        $("#Agency_List").append(opt);
    }
    catch (ex) {

    }
}
