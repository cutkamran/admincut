﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CutAdmin.EntityModal;
using System.Globalization;
using Elmah;
using CommonLib.Response;
using CutAdmin.Common;
namespace CutAdmin.DataLayer.Datalayer
{
    public class OfferType
    {
        public long? ID { get; set; }
        public string Name { get; set; }
    }
    public class OfferManager
    {
        #region Offer Save
        public static void SaveOffer(List<tbl_CommonHotelOffer> arrOffers, List<tbl_RoomOfferRate> NewRates, long HotelCode, List<long> arrRoomID)
        {
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    Random generator = new Random();
                    String OfferId = generator.Next(0, 999).ToString("D3");
                    arrOffers.ForEach(d => d.OfferID = Convert.ToInt32(OfferId));
                    db.tbl_CommonHotelOffer.AddRange(arrOffers);
                    db.SaveChanges();

                    /*Genrate Offer Dates*/
                    List<DateTime> arrOfferDate = new List<DateTime>();
                    List<string> arrsFrom = arrOffers.Where(d => d.DateType == "S").ToList().Select(F => F.ValidFrom).ToList();
                    List<string> sTo = arrOffers.Where(d => d.DateType == "S").ToList().Select(F => F.ValidTo).ToList();
                    foreach (var sFrom in arrsFrom)
                    {
                        DateTime dtFrom = DateTime.ParseExact(sFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.StartDate = dtFrom;
                        DateTime dtToDate = DateTime.ParseExact(sTo[arrsFrom.IndexOf(sFrom)], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.EndDate = dtToDate;
                        List<DateTime> arrDates = Common.Common.Calander;
                        foreach (var Date in arrDates)
                        {
                            if (!arrOfferDate.Contains(Date))
                            {
                                arrOfferDate.Add(Date);
                            }
                        }
                    }
                    /*Assign Offer to Rate*/
                    RoomRateManager.AssignOffer(OfferId, arrOfferDate, HotelCode, arrRoomID);
                    List<tbl_RoomOfferRate> arrRates = new List<tbl_RoomOfferRate>(); ;
                    foreach (var offer in arrOffers.Where(d => d.SeasonName != "Block Date"))
                    {
                        foreach (var Rate in NewRates)
                        {
                            Rate.OfferID = offer.OfferID;
                            arrRates.Add(Rate);
                        }
                    }
                    db.tbl_RoomOfferRate.AddRange(arrRates);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        #endregion

        #region Get Offer By Day
        public static List<tbl_CommonHotelOffer> GetOfferByDay(long? OfferID, DateTime sRateDate, string sOfferType)
        {
            List<tbl_CommonHotelOffer> arrOffer = new List<tbl_CommonHotelOffer>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {

                    string Day = sRateDate.DayOfWeek.ToString();
                    IQueryable<tbl_CommonHotelOffer> IQueryableOffer =
                             (from obj in db.tbl_CommonHotelOffer
                              where obj.OfferID == OfferID
                              && (obj.BlockDay == "" || !obj.BlockDay.Contains(Day))
                              select obj);
                    foreach (var offer in IQueryableOffer.ToList())
                    {

                        DateTime dtFrom = DateTime.ParseExact(offer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.StartDate = dtFrom;
                        DateTime dtToDate = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.EndDate = dtToDate;
                        List<DateTime> arrDates = Common.Common.Calander;
                        if (arrDates.Contains(sRateDate))
                        {
                            bool BlockDate = false;
                            IQueryable<tbl_CommonHotelOffer> IQueryableBlockOffer = (from obj in db.tbl_CommonHotelOffer where obj.OfferID == offer.OfferID && obj.DateType == "D" select obj);
                            if (IQueryableBlockOffer.Count() != 0)
                            {
                                dtFrom = DateTime.ParseExact(IQueryableBlockOffer.FirstOrDefault().ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                Common.Common.StartDate = dtFrom;
                                dtToDate = DateTime.ParseExact(IQueryableBlockOffer.FirstOrDefault().ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                Common.Common.EndDate = dtToDate;
                                arrDates = Common.Common.Calander;
                                if (arrDates.Contains(sRateDate))
                                    BlockDate = true;
                            }
                            if (BlockDate == false)
                                arrOffer.Add(offer);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return arrOffer.Distinct().ToList();
        }

        #endregion

        #region Get Offer By Tarrif
        public static List<OfferType> GetOfferType(long HotelCode, string RateType, long Supplier, string From, string To)
        {
            List<OfferType> ListOfferType = new List<OfferType>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    List<long?> RoomCode = new List<long?>();
                    IQueryable<long?> arrRoomCode = (from obj in db.tbl_commonRoomDetails where obj.HotelId == HotelCode select obj.RoomTypeId);
                    if (arrRoomCode.Count() != 0)
                    {
                        RoomCode = arrRoomCode.ToList();
                        IQueryable<string> arrData = (from obj in db.tbl_TarrifPlan
                                                      join
                                            objPlan in db.tbl_RatePlan on obj.nPlanID equals objPlan.PlanID
                                                      where RoomCode.Contains(obj.RoomTypeID) && obj.nHotelID == HotelCode && (obj.sOffer != null && obj.sOffer != "")
                                                      && objPlan.RateType == RateType && objPlan.SupplierID == Supplier
                                                      select obj.sOffer);
                        if (arrData.Count() != 0)
                        {
                            foreach (var sOfferID in arrData.ToList())
                            {
                                foreach (var OfferID in sOfferID.Split(','))
                                {
                                    long? offerID = Convert.ToInt64(OfferID);
                                    IQueryable<tbl_CommonHotelOffer> arrOffer = (from obj in db.tbl_CommonHotelOffer where obj.OfferID == offerID select obj);
                                    foreach (var obj in arrOffer.ToList())
                                    {
                                        Common.Common.StartDate = DateTime.ParseExact(From, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Common.Common.EndDate = DateTime.ParseExact(To, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        List<DateTime> arrRateDate = Common.Common.Calander;
                                        Common.Common.StartDate = DateTime.ParseExact(obj.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Common.Common.EndDate = DateTime.ParseExact(obj.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        List<DateTime> arrOfferDate = Common.Common.Calander;
                                        var match = arrRateDate.Intersect(arrOfferDate).Count();
                                        if (!ListOfferType.Any(d => d.ID == obj.OfferID) && match != 0)
                                        {
                                            string OfferName = obj.Category + "(" + obj.SeasonName + ")";
                                            ListOfferType.Add( new OfferType
                                            {
                                                ID = obj.OfferID,
                                                Name = OfferName
                                            });
                                        }
                                       
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
            }
            return ListOfferType;
        }
        #endregion

        #region Get Offer Rate
        public static float GetOfferRate(tbl_CommonHotelOffer arrOffer, float Rate, string PaxType)
        {
            float OfferRate = Rate;
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    if (arrOffer.Category == "Early Booking" || arrOffer.Category == "Last Minute")
                    {
                        if (arrOffer.OfferType == "Discount")/*Check Discount Rate*/
                        {
                            IQueryable<tbl_RoomOfferRate> arrData = (from obj in DB.tbl_RoomOfferRate where obj.OfferID == arrOffer.OfferID select obj);
                            if (arrData.Count() != 0)
                            {
                                var Discount = Convert.ToSingle(arrData.FirstOrDefault().Rate);
                                OfferRate = Rate - (Rate * Discount / 100);
                            }
                        }
                        else /*Amount Offer*/
                        {
                            IQueryable<tbl_RoomOfferRate> arrData = (from obj in DB.tbl_RoomOfferRate
                                                                     where obj.OfferID == arrOffer.OfferID
                                                                     && obj.sPaxType == PaxType
                                                                     select obj);
                            if (arrData.Count() != 0)
                                OfferRate = Convert.ToSingle(arrData.FirstOrDefault().Rate);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return OfferRate;
        }
        #endregion

        #region Offer Creation 
        public static List<OfferSeason> GetOfferByRate(date arrRate, string Nationality, DateTime sCheckIn, long noNight)
        {
            List<OfferSeason> arrOffer = new List<OfferSeason>();
            List<string> arrOfferID = new List<string>();
            try
            {
                if (arrRate.offerID != null && arrRate.offerID != "")
                {
                    arrOfferID = arrRate.offerID.Split(',').ToList();
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        IQueryable<tbl_CommonHotelOffer> arrData = (from obj in db.tbl_CommonHotelOffer
                                                                    where arrOfferID.Contains(obj.OfferID.ToString()) && obj.DateType == "S"
                                                                    && obj.OfferNationality.Contains(Nationality)
                                                                    select obj);
                        foreach (var offer in arrData.ToList())
                        {
                            DateTime ValidTill = new DateTime();
                            DateTime offerDate = DateTime.ParseExact(arrRate.datetime, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            if (ValidOffer(offer, offerDate))
                            {
                                /*Early Booking*/
                                #region Early Booking
                                if (offer.Category == "Early Booking")
                                {
                                    if (offer.DaysPrior != null)
                                        ValidTill = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(offer.DaysPrior));
                                    else
                                        ValidTill = DateTime.ParseExact(offer.BookBefore, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(offer.DaysPrior));
                                }
                                #endregion

                                /*Last Minute Booking*/
                                #region Last Minute Booking
                                if (offer.Category == "Last Minute")
                                {
                                    if (offer.DaysPrior != null)
                                    {
                                        if (offer.DaysPrior.Contains("Day"))
                                            ValidTill = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(offer.DaysPrior.Replace("Day", "")));
                                        else
                                            ValidTill = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddHours(-Convert.ToInt16(offer.DaysPrior.Replace("Hr", "")));
                                    }
                                }
                                #endregion

                                if (offer.Category != "Free Nights")
                                {
                                    if (ValidTill >= DateTime.Now)
                                    {
                                        arrOffer.Add(new OfferSeason
                                        {
                                            offerName = offer.Category +"("+offer.SeasonName+")",
                                            offerNote = offer.OfferNote,
                                            OfferTerms = offer.OfferTerm,
                                            Dates = offerDate,
                                            DisountAmount = Convert.ToDecimal(offer.DiscountAmount),
                                            DisountPercent = Convert.ToSingle(offer.DiscountPercent),
                                            OfferType = offer.OfferType,
                                            arrRate = new List<PaxRate>(),
                                        });
                                    }
                                    bool isPercent = false;
                                    if (arrOffer.LastOrDefault().DisountPercent != 0)
                                        isPercent = true;
                                    arrOffer.LastOrDefault().arrRate = GetOfferRate(offer, isPercent, arrOffer.LastOrDefault().DisountPercent, arrRate);
                                }

                                #region Free Night
                                if (offer.Category == "Free Nights")
                                {
                                    Common.Common.StartDate = DateTime.ParseExact(offer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    Common.Common.EndDate = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    List<DateTime> arrDate = Common.Common.Calander;
                                    if (arrDate.Contains(offerDate) && noNight >= offer.MinNight)
                                    {
                                        arrOffer.Add(new OfferSeason
                                        {
                                            offerName = offer.Category + "(" + offer.SeasonName + ")",
                                            offerNote = offer.OfferNote,
                                            OfferTerms = offer.OfferTerm,
                                            Dates = offerDate,
                                            DisountAmount = Convert.ToDecimal(offer.DiscountAmount),
                                            DisountPercent = Convert.ToSingle(offer.DiscountPercent),
                                            OfferType = offer.OfferType,
                                            arrRate = new List<PaxRate>(),
                                            FreeNight = offer.FreeNight,
                                            MinNight = offer.MinNight,
                                        });
                                    }
                                }
                                #endregion

                                #region Free Addons
                                if (offer.Category == "Free AddOns")
                                {
                                    Common.Common.StartDate = DateTime.ParseExact(offer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    Common.Common.EndDate = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    List<DateTime> arrDate = Common.Common.Calander;
                                    if (arrDate.Contains(offerDate))
                                    {
                                        arrOffer.Add(new OfferSeason
                                        {
                                            offerName = offer.Category + "(" + offer.SeasonName + ")",
                                            offerNote = offer.OfferNote,
                                            OfferTerms = offer.OfferTerm,
                                            Dates = offerDate,
                                            FreeItem = offer.FreeItemName,
                                            DisountAmount = Convert.ToDecimal(offer.DiscountAmount),
                                            DisountPercent = Convert.ToSingle(offer.DiscountPercent),
                                            OfferType = offer.OfferType,
                                            arrRate = new List<PaxRate>(),
                                            FreeNight = offer.FreeNight,
                                            MinNight = offer.MinNight,
                                        });
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return arrOffer;
        }

        public static bool ValidOffer(tbl_CommonHotelOffer arrOffer, DateTime offerDate)
        {
            bool Valid = false;
            try
            {

                #region Check Valid Date
                List<string> Days = arrOffer.BlockDay.Split(',').ToList();
                if (arrOffer.BlockDay == "" || Days.Count == 0)
                    Valid = true; /* If no block day selected */
                foreach (var Day in Days)
                {
                    if (Day != "")
                    {
                        if (Day == "Sunday" && offerDate.DayOfWeek == DayOfWeek.Sunday)
                            Valid = true;
                        if (Day == "Monday" && offerDate.DayOfWeek == DayOfWeek.Monday)
                            Valid = true;
                        if (Day == "Tuesday" && offerDate.DayOfWeek == DayOfWeek.Tuesday)
                            Valid = true;
                        if (Day == "Wednesday" && offerDate.DayOfWeek == DayOfWeek.Wednesday)
                            Valid = true;
                        if (Day == "Thursday" && offerDate.DayOfWeek == DayOfWeek.Thursday)
                            Valid = true;
                        if (Day == "Friday" && offerDate.DayOfWeek == DayOfWeek.Friday)
                            Valid = true;
                        if (Day == "Saterday" && offerDate.DayOfWeek == DayOfWeek.Saturday)
                            Valid = true;
                    }
                }
                #endregion
                if (Valid)
                {
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        long offerID = arrOffer.OfferID;
                        IQueryable<tbl_CommonHotelOffer> arrData = (from obj in db.tbl_CommonHotelOffer
                                                                    where obj.OfferID == offerID && obj.DateType == "D"
                                                                    select obj);

                        if (arrData.Count() == 0)
                            Valid = true; /*If No Bolck Date selected*/
                        foreach (var offer in arrData.ToList())
                        {

                            Common.Common.StartDate = DateTime.ParseExact(offer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Common.Common.EndDate = DateTime.ParseExact(offer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            List<DateTime> arrBlockDate = Common.Common.Calander;
                            if (arrBlockDate.Contains(offerDate))
                                Valid = false;
                            else
                                Valid = true;
                        }
                    }

                    if (Valid)
                    {
                        Common.Common.StartDate = DateTime.ParseExact(arrOffer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.EndDate = DateTime.ParseExact(arrOffer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        List<DateTime> arrDate = Common.Common.Calander;
                        if (arrDate.Contains(offerDate))
                            Valid = true;
                        else
                            Valid = false;
                    }

                }
            }
            catch (Exception)
            {
                Valid = false;
            }
            return Valid;
        }

        public static List<PaxRate> GetOfferRate(tbl_CommonHotelOffer arrOffer, bool IsPercent, Double? Percent, date arrRate)
        {
            List<PaxRate> arrPaxRate = new List<PaxRate>();
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                Int64 AvailBility = 0;
                float ExchangeRate = ExchangeRateManager.Exchange(arrRate.Currency, false) /
                    ExchangeRateManager.Exchange(objGlobalDefault.Currency, true);
                string UserCurrency = objGlobalDefault.Currency;
                if (IsPercent != true)
                {
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        IQueryable<PaxRate> arrData = (from obj in db.tbl_RoomOfferRate
                                                       where obj.OfferID == arrOffer.OfferID
                                                       select new PaxRate
                                                       {
                                                           Rate = obj.Rate,
                                                           Type = obj.sPaxType,
                                                       });
                        if (arrData.ToList().Count != 0)
                            arrPaxRate = arrData.ToList();
                        foreach (var sRate in arrPaxRate)
                        {
                            if (sRate.Type == "RR" && arrRate.RoomRate != 0)
                                arrRate.RoomRate = Convert.ToSingle(sRate.Rate) * ExchangeRate;
                            if (sRate.Type == "EB" && arrRate.EBRate != 0)
                                arrRate.EBRate = Convert.ToSingle(sRate.Rate) * ExchangeRate;
                            if (sRate.Type == "CW" && arrRate.ChidWBedRate != 0)
                                arrRate.ChidWBedRate = Convert.ToSingle(sRate.Rate) * ExchangeRate;
                            if (sRate.Type == "CN" && arrRate.CNBRate != 0)
                                arrRate.CNBRate = Convert.ToSingle(sRate.Rate) * ExchangeRate;
                        }
                    }
                }
                else
                {
                    arrPaxRate.Add(new PaxRate
                    {
                        Type = "RR",
                        Rate = Convert.ToDecimal(arrRate.RoomRate - (arrRate.RoomRate * Percent / 100))
                    });
                    arrRate.RoomRate = Convert.ToSingle(arrPaxRate.LastOrDefault().Rate);
                    if (arrRate.EBRate != 0)
                    {
                        arrPaxRate.Add(new PaxRate
                        {
                            Type = "EB",
                            Rate = Convert.ToDecimal(arrRate.EBRate - (arrRate.EBRate * Percent / 100))
                        });
                        arrRate.EBRate = Convert.ToSingle(arrPaxRate.LastOrDefault().Rate);
                    }

                    if (arrRate.ChidWBedRate != 0)
                    {
                        arrPaxRate.Add(new PaxRate
                        {
                            Type = "CW",
                            Rate = Convert.ToDecimal(arrRate.ChidWBedRate - (arrRate.ChidWBedRate * Percent / 100))
                        });
                        arrRate.ChidWBedRate = Convert.ToSingle(arrPaxRate.LastOrDefault().Rate);
                    }

                    if (arrRate.CNBRate != 0)
                    {
                        arrPaxRate.Add(new PaxRate
                        {
                            Type = "CW",
                            Rate = Convert.ToDecimal(arrRate.CNBRate - (arrRate.CNBRate * Percent / 100))
                        });
                        arrRate.CNBRate = Convert.ToSingle(arrPaxRate.LastOrDefault().Rate);
                    }
                }
                arrRate.Total = (arrRate.RoomRate + arrRate.EBRate + arrRate.CNBRate + arrRate.ChidWBedRate);
            }
            catch (Exception ex)
            {
            }
            return arrPaxRate;
        }
        #endregion
    }
}