﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CutAdmin.BL;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class RoomManager
    {

        #region Images
        public static DataLayer.DataManager.DBReturnCode RoomImages(string ImagePath, string RoomId, string HotelId)
        {
            string[] Path = ImagePath.Split('^');
            string MainImage = ImagePath.Split('^')[0];
            int i = 0; ImagePath = "";
            foreach (string Image in Path)
            {


                if (Image != "")
                {
                    if ((Path.Length - 1) != i)
                    {
                        string Img = Image.Split('.')[0];

                        ImagePath += Img + ".jpg" + "^";
                    }

                    else
                    {
                        string Imge = Image.Split('.')[0];
                        ImagePath += Imge + ".jpg";
                    }

                }
                i++;
            }
            
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            if (ImagePath != "")
            {
                // sSQL.Append("Update tbl_commonRoomDetails set RoomImage ='" + MainImage + "', SubImages='" + ImagePath + "' where RoomId='" + RoomId + "' and  HotelId='" + HotelId + "'");
                ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                using (var Img = db.Database.BeginTransaction())
                {
                    tbl_commonRoomDetails Update = db.tbl_commonRoomDetails.Single(x => x.RoomId == Convert.ToInt32(RoomId)&& x.HotelId == Convert.ToInt32(HotelId));
                    Update.RoomImage += MainImage;
                    Update.SubImages += ImagePath;
                    db.SaveChanges();
                    Img.Commit();


                }
            }
            return DataManager.DBReturnCode.SUCCESS;
            //  return retCopde = DataManager.ExecuteNonQuery(sSQL.ToString());
        }

        #endregion

        #region SaveRooms

        public static tbl_commonRoomType SaveRoomType(tbl_commonRoomType arrRoomType)
        {
            tbl_commonRoomType RoomType = new tbl_commonRoomType();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    if (!db.tbl_commonRoomType.ToList().Exists(d => d.RoomType == arrRoomType.RoomType))
                    {
                        db.tbl_commonRoomType.Add(arrRoomType);
                        db.SaveChanges();
                        RoomType = arrRoomType;
                    }

                    else
                    {
                        // RoomType = db.tbl_commonRoomType.Where(d => d.RoomType == arrRoomType.RoomType).FirstOrDefault();
                        tbl_commonRoomType Update = db.tbl_commonRoomType.Single(d => d.RoomType == arrRoomType.RoomType);
                        Update.RoomType = arrRoomType.RoomType;
                        db.SaveChanges();
                        RoomType = arrRoomType;
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return RoomType;
        }

        public static tbl_commonRoomType updateRoomType(tbl_commonRoomType arrBeddingType)
        {
            tbl_commonRoomType RoomType = new tbl_commonRoomType();

            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {

                    tbl_commonRoomType Update = db.tbl_commonRoomType.Single(x => x.RoomTypeID == arrBeddingType.RoomTypeID);
                    Update.RoomType = arrBeddingType.RoomType;
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {

                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return RoomType;
        }

        public static void SaveRooms(tbl_commonRoomType arrRoomType, tbl_commonRoomDetails arrRoom ,long HotelCode)
        {
            try
            {
                arrRoomType = SaveRoomType(arrRoomType);
                using (var db = new ClickUrHotel_DBEntities())
                {
                  
                    if (arrRoom.RoomId == 0)
                    {
                        var roomtype = (from obj in db.tbl_commonRoomDetails where obj.HotelId == HotelCode && obj.RoomTypeId == arrRoom.RoomTypeId select obj).FirstOrDefault();
                        if(roomtype != null)
                        {
                            db.tbl_commonRoomDetails.Remove(roomtype);
                            arrRoom.RoomTypeId = arrRoomType.RoomTypeID;
                            db.tbl_commonRoomDetails.Add(arrRoom);
                        }
                        else
                        {
                            arrRoom.RoomTypeId = arrRoomType.RoomTypeID;
                            db.tbl_commonRoomDetails.Add(arrRoom);
                        }

                       
                    }
                    else
                    {
                        var arrData = (db.tbl_commonRoomDetails.Where(d => d.RoomId.ToString() == arrRoom.RoomId.ToString()).FirstOrDefault());
                        arrData.RoomAmenitiesId = arrRoom.RoomAmenitiesId;
                        arrData.RoomDescription = arrRoom.RoomDescription;
                        arrData.RoomNote = arrRoom.RoomNote;
                        arrData.RoomImage = arrRoom.RoomImage;
                        arrData.SubImages = arrRoom.SubImages;
                        arrData.SmokingAllowed = arrRoom.SmokingAllowed;
                        arrData.BeddingType = arrRoom.BeddingType;
                        arrData.RoomOccupancy = arrRoom.RoomOccupancy;
                        arrData.MaxExtrabedAllowed = arrRoom.MaxExtrabedAllowed;
                        arrData.NoOfChildWithoutBed = arrRoom.NoOfChildWithoutBed;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
               

            }
        }

        #endregion

        #region Bedding Type of Rooms
        public static tbl_commonBeddingType SaveBeddingType(tbl_commonBeddingType arrBeddingType)
        {
            tbl_commonBeddingType BeddingType = new tbl_commonBeddingType();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                  
                    if (!db.tbl_commonBeddingType.ToList().Exists(d=>d.BeddingType==arrBeddingType.BeddingType))
                    {
                        db.tbl_commonBeddingType.Add(arrBeddingType);
                        db.SaveChanges();
                        BeddingType = arrBeddingType;

                    }
                    else
                    {
                        BeddingType = db.tbl_commonBeddingType.Where(d => d.BeddingType == arrBeddingType.BeddingType ).FirstOrDefault();
                    }


                }

            }
            catch (Exception ex)
            {

                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return BeddingType;

        } 

        public static tbl_commonBeddingType updateBeddingType(tbl_commonBeddingType arrBeddingType)
        {
            tbl_commonBeddingType BeddingType = new tbl_commonBeddingType();

            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {

                    tbl_commonBeddingType Update = db.tbl_commonBeddingType.Single(x => x.BeddingTypeID == arrBeddingType.BeddingTypeID);
                    Update.BeddingType = arrBeddingType.BeddingType;
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {

                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return BeddingType;
        }
        #endregion
    }
}