﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using System.Globalization;
using CutAdmin.EntityModal;
using Elmah;
using CutAdmin.DataLayer.Datalayer;

namespace CutAdmin.DataLayer
{
    public class RoomRateManager
    {
        public static ClickUrHotel_DBEntities db { get; set; }
        #region Save Rates
        public static void SaveRates(tbl_RatePlan arrRatePlan, List<tbl_TarrifPlan> arrTarrif, List<Rates> arrRates)
        {
            if (AccountManager.CheckSession)
            {
                db = new ClickUrHotel_DBEntities();
                Int64 AdminID = AccountManager.GetSupplierByUser();
                using (var Transaction = db.Database.BeginTransaction())
                {
                    arrRatePlan.ParentID = AdminID;
                    try
                    {
                        /*To avoid duplicate RatePlan */
                        var arrPlan = (from obj in db.tbl_RatePlan
                                       where obj.HotelID == arrRatePlan.HotelID && obj.Market == arrRatePlan.Market
                                       && obj.RateType == arrRatePlan.RateType && obj.RoomTypeID == arrRatePlan.RoomTypeID
                                       && obj.SupplierID == arrRatePlan.SupplierID && obj.sCurrency == arrRatePlan.sCurrency
                                       select obj).FirstOrDefault();
                        if (arrPlan == null)
                            db.tbl_RatePlan.Add(arrRatePlan);
                        else
                            arrRatePlan = arrPlan;
                        db.SaveChanges();

                        #region /*Add Tarrif Plan*/
                        arrTarrif.ForEach(d => d.nPlanID = arrRatePlan.PlanID);
                        arrTarrif.ForEach(d => d.ParentID = AdminID);
                        foreach (var objTarrif in arrTarrif)
                        {
                            var Tarrif = db.tbl_TarrifPlan.Where(d => d.sFromDate == objTarrif.sFromDate && d.sToDate == objTarrif.sToDate && d.nPlanID == arrRatePlan.PlanID).FirstOrDefault();
                            if (Tarrif == null)
                                db.tbl_TarrifPlan.Add(objTarrif);
                            else
                            {
                                Tarrif.nCancellation = objTarrif.nCancellation;
                                Tarrif.nonShow = objTarrif.nonShow;
                                Tarrif.nonRefundable = Tarrif.nonRefundable;
                            }

                        }
                        db.SaveChanges();
                        #endregion

                        List<tbl_DatesRate> arrDate = new List<tbl_DatesRate>();
                        List<tbl_PurchaseRate> arrPurchaseRate = new List<tbl_PurchaseRate>();
                        tbl_DatesRate DateRate = new tbl_DatesRate();
                        foreach (var objTarrif in arrTarrif)
                        {
                            List<DayRate> arrSellDay = arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.arrDays;
                            List<DayRate> arrPurchaseDay = arrRates[arrTarrif.IndexOf(objTarrif)].arrDays;
                            #region Dates Rates
                            var dFromDate = DateTime.ParseExact(objTarrif.sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            var dToDate = DateTime.ParseExact(objTarrif.sToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Double noDays = (dToDate - dFromDate).TotalDays;
                            List<DateTime> arrChargeDates = new List<DateTime>();
                            for (int d = 0; d < noDays; d++)
                            {
                                arrChargeDates.Add(dFromDate.AddDays(d));
                            }
                            foreach (var month in arrChargeDates.Select(d => d.Month).Distinct().ToList())
                            {
                                List<DateTime> arrRatedate = arrChargeDates.Where(d => d.Month == month).ToList();
                                foreach (var ChargeDate in arrRatedate)
                                {
                                    DateTime sDate = ChargeDate;
                                    float Rate = RatesByDay(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.arrDays, ChargeDate.DayOfWeek.ToString());
                                    float PurchaseRate = RatesByDay(arrRates[arrTarrif.IndexOf(objTarrif)].arrDays, ChargeDate.DayOfWeek.ToString());
                                    string sYear = sDate.Year.ToString();
                                    string sMonth = sDate.Month.ToString();
                                    string RoomTypID = objTarrif.RoomTypeID.ToString();
                                    IQueryable<tbl_DatesRate> arrDateRates = (from obj in db.tbl_DatesRate
                                                                              where obj.sMonth == sMonth && obj.sYear == sYear
                                                                              && obj.nHotelID == arrRatePlan.HotelID
                                                                              && obj.RoomTypeID == arrRatePlan.RoomTypeID
                                                                              && obj.nSupplierID == arrRatePlan.SupplierID
                                                                              && obj.nMealPlanID == objTarrif.nMeal_PlanID
                                                                              && obj.nPlanID == arrRatePlan.PlanID
                                                                              select obj);

                                    IQueryable<tbl_PurchaseRate> arrPurchaseRates = (from obj in db.tbl_PurchaseRate
                                                                                     where obj.sMonth == sMonth && obj.sYear == sYear
                                                                                     && obj.nHotelID == arrRatePlan.HotelID
                                                                                     && obj.RoomTypeID == arrRatePlan.RoomTypeID
                                                                                     && obj.nSupplierID == arrRatePlan.SupplierID
                                                                                     && obj.nMealPlanID == objTarrif.nMeal_PlanID
                                                                                     && obj.nPlanID == arrRatePlan.PlanID
                                                                                     select obj);
                                    if (arrDateRates.ToList().Count == 0)
                                    {
                                        if (!arrDate.Exists(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID))
                                        {
                                            tbl_PurchaseRate arrPurchase = new tbl_PurchaseRate();
                                            DateRate = new tbl_DatesRate();
                                            SetRates("RR", Convert.ToDecimal(Rate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(PurchaseRate), DateRate, arrPurchase);
                                            arrDate.Add(DateRate);
                                            arrPurchaseRate.Add(arrPurchase);

                                            DateRate = new tbl_DatesRate();
                                            arrPurchase = new tbl_PurchaseRate();
                                            SetRates("EB", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.EBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].EBRate), DateRate, arrPurchase);
                                            arrDate.Add(DateRate);
                                            arrPurchaseRate.Add(arrPurchase);

                                            DateRate = new tbl_DatesRate();
                                            arrPurchase = new tbl_PurchaseRate();
                                            SetRates("CW", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CWBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CWBRate), DateRate, arrPurchase);
                                            arrDate.Add(DateRate);
                                            arrPurchaseRate.Add(arrPurchase);

                                            DateRate = new tbl_DatesRate();
                                            arrPurchase = new tbl_PurchaseRate();
                                            SetRates("CN", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CNBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CNBRate), DateRate, arrPurchase);
                                            arrDate.Add(DateRate);
                                            arrPurchaseRate.Add(arrPurchase);
                                        }
                                        else
                                        {
                                            DateRate = arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "RR").FirstOrDefault();
                                            tbl_PurchaseRate arrPurchase = arrPurchaseRate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.sPaxType == "RR").FirstOrDefault();
                                            SetRates("RR", Convert.ToDecimal(Rate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(PurchaseRate), DateRate, arrPurchase);
                                            arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "RR").ToList().ForEach(d => d = DateRate);
                                            arrPurchaseRate.Where(d => d.sPaxType == "RR").ToList().ForEach(r => r = arrPurchase);

                                            arrPurchase = arrPurchaseRate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.sPaxType == "EB").FirstOrDefault();
                                            DateRate = arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "EB").FirstOrDefault();
                                            SetRates("EB", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.EBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].EBRate), DateRate, arrPurchase);
                                            arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "EB").ToList().ForEach(d => d = DateRate);
                                            arrPurchaseRate.Where(d => d.sPaxType == "EB").ToList().ForEach(r => r = arrPurchase);


                                            arrPurchase = arrPurchaseRate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.sPaxType == "CW").FirstOrDefault();
                                            DateRate = arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "CW").FirstOrDefault();
                                            SetRates("CW", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CWBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CWBRate), DateRate, arrPurchase);
                                            arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "CW").ToList().ForEach(d => d = DateRate);
                                            arrPurchaseRate.Where(d => d.sPaxType == "CW").ToList().ForEach(r => r = arrPurchase);


                                            arrPurchase = arrPurchaseRate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.sPaxType == "CN").FirstOrDefault();
                                            DateRate = arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "CN").FirstOrDefault();
                                            SetRates("CN", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CNBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CNBRate), DateRate, arrPurchase);
                                            arrDate.Where(r => r.sMonth == sMonth && r.sYear == sYear && r.nMealPlanID == objTarrif.nMeal_PlanID && r.PaxType == "CN").ToList().ForEach(d => d = DateRate);
                                            arrPurchaseRate.Where(d => d.sPaxType == "CN").ToList().ForEach(r => r = arrPurchase);
                                        }
                                    }
                                    else
                                    {
                                        List<tbl_DatesRate> OlRates = arrDateRates.ToList();
                                        List<tbl_PurchaseRate> oldPurchaseRates = arrPurchaseRate.ToList();
                                        tbl_PurchaseRate arrPurchase = new tbl_PurchaseRate();
                                        if (OlRates.Where(d => d.PaxType == "RR" && d.sMonth.TrimEnd(' ') == sMonth && d.sYear == sYear).FirstOrDefault() != null)
                                        {
                                            DateRate = GetRateByRateType(OlRates, "RR", sMonth, sYear);
                                            SetRates("RR", Convert.ToDecimal(Rate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(PurchaseRate), DateRate, arrPurchase);
                                            GetRateListByRateType(OlRates, "RR", sMonth, sYear).ForEach(d => d = DateRate);
                                            oldPurchaseRates.Where(d => d.sPaxType == "RR").ToList().ForEach(d => d = arrPurchase);
                                        }
                                        if (OlRates.Where(d => d.PaxType == "EB" && d.sMonth.TrimEnd(' ') == sMonth && d.sYear == sYear).FirstOrDefault() != null)
                                        {
                                            arrPurchase = new tbl_PurchaseRate();
                                            DateRate = GetRateByRateType(OlRates, "EB", sMonth, sYear);
                                            SetRates("EB", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.EBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].EBRate), DateRate, arrPurchase);
                                            OlRates.Where(d => d.PaxType == "EB").ToList().ForEach(d => d = DateRate);
                                            oldPurchaseRates.Where(d => d.sPaxType == "EB").ToList().ForEach(d => d = arrPurchase);
                                        }
                                        if (OlRates.Where(d => d.PaxType == "CW" && d.sMonth.TrimEnd(' ') == sMonth && d.sYear == sYear).FirstOrDefault() != null)
                                        {
                                            arrPurchase = new tbl_PurchaseRate();
                                            DateRate = GetRateByRateType(OlRates, "CW", sMonth, sYear);
                                            SetRates("CW", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CWBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CWBRate), DateRate, arrPurchase);
                                            OlRates.Where(d => d.PaxType == "CW").ToList().ForEach(d => d = DateRate);
                                            oldPurchaseRates.Where(d => d.sPaxType == "CW").ToList().ForEach(d => d = arrPurchase);
                                        }
                                        if (OlRates.Where(d => d.PaxType == "CN").FirstOrDefault() != null)
                                        {
                                            arrPurchase = new tbl_PurchaseRate();
                                            DateRate = GetRateByRateType(OlRates, "CN", sMonth, sYear);
                                            SetRates("CN", Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].MinSelling.CNBRate), objTarrif.nMeal_PlanID, sMonth, sYear, (ChargeDate.Day).ToString(), Convert.ToDecimal(arrRates[arrTarrif.IndexOf(objTarrif)].CNBRate), DateRate, arrPurchase);
                                            OlRates.Where(d => d.PaxType == "CN").ToList().ForEach(d => d = DateRate);
                                            oldPurchaseRates.Where(d => d.sPaxType == "CN").ToList().ForEach(d => d = arrPurchase);
                                        }
                                    }
                                }
                            }
                        }
                        arrDate.ForEach(d => d.nPlanID = arrRatePlan.PlanID);
                        arrDate.ForEach(d => d.nSupplierID = arrRatePlan.SupplierID);
                        arrDate.ForEach(d => d.RoomTypeID = arrRatePlan.RoomTypeID);
                        arrDate.ForEach(d => d.nHotelID = arrRatePlan.HotelID);
                        arrPurchaseRate.ForEach(d => d.nPlanID = arrRatePlan.PlanID);
                        arrPurchaseRate.ForEach(d => d.nSupplierID = arrRatePlan.SupplierID);
                        arrPurchaseRate.ForEach(d => d.RoomTypeID = arrRatePlan.RoomTypeID);
                        arrPurchaseRate.ForEach(d => d.nHotelID = arrRatePlan.HotelID);
                        db.tbl_DatesRate.AddRange(arrDate);
                        db.tbl_PurchaseRate.AddRange(arrPurchaseRate);
                        db.SaveChanges();
                        #endregion
                        Transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Transaction.Dispose();
                        ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        public static tbl_DatesRate GetRateByRateType(List<tbl_DatesRate> arrRate, string sPaxType, string sMonth, string sYear)
        {
            tbl_DatesRate Rate = new tbl_DatesRate();
            try
            {
                Rate = arrRate.Where(d => d.PaxType == sPaxType && d.sMonth.TrimEnd(' ') == sMonth && d.sYear == sYear).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Rate;
        }
        public static List<tbl_DatesRate> GetRateListByRateType(List<tbl_DatesRate> arrRate, string sPaxType, string sMonth, string sYear)
        {
            List<tbl_DatesRate> Rate = new List<tbl_DatesRate>();
            try
            {
                Rate = arrRate.Where(d => d.PaxType == sPaxType && d.sMonth == sMonth && d.sYear == sYear).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Rate;
        }



        public static void SetRates(string PaxType, Decimal Rate, long nMeal_PlanID, string sMonth, string sYear, string Day, Decimal PurchaseRate, tbl_DatesRate arrRate, tbl_PurchaseRate arrPurchase)
        {
            try
            {
                arrRate.sMonth = sMonth;
                arrRate.sYear = sYear;
                arrRate.PaxType = PaxType;
                arrRate.nMealPlanID = nMeal_PlanID;
                GetDateRate(Day, Convert.ToDecimal(Rate), arrRate); /*Sell Rate*/

                arrPurchase.sMonth = sMonth;
                arrPurchase.sYear = sYear;
                arrPurchase.sPaxType = PaxType;
                arrPurchase.nMealPlanID = nMeal_PlanID;
                GetPurchaseRate(Day, PurchaseRate, arrPurchase);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }


        public static float RatesByDay(List<DayRate> arrDayRate, string Day)
        {
            float Rate = 0;
            try
            {
                Rate = arrDayRate.Where(d => Day.ToLower().Contains(d.Day)).FirstOrDefault().Rate;

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Rate;
        }
        #endregion

        #region Get Rates
        public static tbl_DatesRate GetDateRate(string Day, decimal Rate, tbl_DatesRate DateRates)
        {
            try
            {
                switch (Day)
                {
                    case "1":
                        DateRates.dDate_1 = Rate;
                        break;
                    case "2":
                        DateRates.dDate_2 = Rate;
                        break;
                    case "3":
                        DateRates.dDate_3 = Rate;
                        break;
                    case "4":
                        DateRates.dDate_4 = Rate;
                        break;
                    case "5":
                        DateRates.dDate_5 = Rate;
                        break;
                    case "6":
                        DateRates.dDate_6 = Rate;
                        break;
                    case "7":
                        DateRates.dDate_7 = Rate;
                        break;
                    case "8":
                        DateRates.dDate_8 = Rate;
                        break;
                    case "9":
                        DateRates.dDate_9 = Rate;
                        break;
                    case "10":
                        DateRates.dDate_10 = Rate;
                        break;
                    case "11":
                        DateRates.dDate_11 = Rate;
                        break;
                    case "12":
                        DateRates.dDate_12 = Rate;
                        break;
                    case "13":
                        DateRates.dDate_13 = Rate;
                        break;
                    case "14":
                        DateRates.dDate_14 = Rate;
                        break;
                    case "15":
                        DateRates.dDate_15 = Rate;
                        break;
                    case "16":
                        DateRates.dDate_16 = Rate;
                        break;
                    case "17":
                        DateRates.dDate_17 = Rate;
                        break;
                    case "18":
                        DateRates.dDate_18 = Rate;
                        break;
                    case "19":
                        DateRates.dDate_19 = Rate;
                        break;
                    case "20":
                        DateRates.dDate_20 = Rate;
                        break;
                    case "21":
                        DateRates.dDate_21 = Rate;
                        break;
                    case "22":
                        DateRates.dDate_22 = Rate;
                        break;
                    case "23":
                        DateRates.dDate_23 = Rate;
                        break;
                    case "24":
                        DateRates.dDate_24 = Rate;
                        break;
                    case "25":
                        DateRates.dDate_25 = Rate;
                        break;
                    case "26":
                        DateRates.dDate_26 = Rate;
                        break;
                    case "27":
                        DateRates.dDate_27 = Rate;
                        break;
                    case "28":
                        DateRates.dDate_28 = Rate;
                        break;
                    case "29":
                        DateRates.dDate_29 = Rate;
                        break;
                    case "30":
                        DateRates.dDate_30 = Rate;
                        break;
                    case "31":
                        DateRates.dDate_31 = Rate;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return DateRates;
        }

        public static tbl_PurchaseRate SetPurchaseRate()
        {
            tbl_PurchaseRate arrPurchaseRate = new tbl_PurchaseRate();
            try
            {

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrPurchaseRate;
        }

        public static tbl_PurchaseRate GetPurchaseRate(string Day, decimal Rate, tbl_PurchaseRate DateRates)
        {
            try
            {
                switch (Day)
                {
                    case "1":
                        DateRates.dDate_1 = Rate;
                        break;
                    case "2":
                        DateRates.dDate_2 = Rate;
                        break;
                    case "3":
                        DateRates.dDate_3 = Rate;
                        break;
                    case "4":
                        DateRates.dDate_4 = Rate;
                        break;
                    case "5":
                        DateRates.dDate_5 = Rate;
                        break;
                    case "6":
                        DateRates.dDate_6 = Rate;
                        break;
                    case "7":
                        DateRates.dDate_7 = Rate;
                        break;
                    case "8":
                        DateRates.dDate_8 = Rate;
                        break;
                    case "9":
                        DateRates.dDate_9 = Rate;
                        break;
                    case "10":
                        DateRates.dDate_10 = Rate;
                        break;
                    case "11":
                        DateRates.dDate_11 = Rate;
                        break;
                    case "12":
                        DateRates.dDate_12 = Rate;
                        break;
                    case "13":
                        DateRates.dDate_13 = Rate;
                        break;
                    case "14":
                        DateRates.dDate_14 = Rate;
                        break;
                    case "15":
                        DateRates.dDate_15 = Rate;
                        break;
                    case "16":
                        DateRates.dDate_16 = Rate;
                        break;
                    case "17":
                        DateRates.dDate_17 = Rate;
                        break;
                    case "18":
                        DateRates.dDate_18 = Rate;
                        break;
                    case "19":
                        DateRates.dDate_19 = Rate;
                        break;
                    case "20":
                        DateRates.dDate_20 = Rate;
                        break;
                    case "21":
                        DateRates.dDate_21 = Rate;
                        break;
                    case "22":
                        DateRates.dDate_22 = Rate;
                        break;
                    case "23":
                        DateRates.dDate_23 = Rate;
                        break;
                    case "24":
                        DateRates.dDate_24 = Rate;
                        break;
                    case "25":
                        DateRates.dDate_25 = Rate;
                        break;
                    case "26":
                        DateRates.dDate_26 = Rate;
                        break;
                    case "27":
                        DateRates.dDate_27 = Rate;
                        break;
                    case "28":
                        DateRates.dDate_28 = Rate;
                        break;
                    case "29":
                        DateRates.dDate_29 = Rate;
                        break;
                    case "30":
                        DateRates.dDate_30 = Rate;
                        break;
                    case "31":
                        DateRates.dDate_31 = Rate;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return DateRates;
        }

        public static decimal GetRateByDate(string Day, tbl_DatesRate DateRates)
        {
            decimal? Rate = 0;
            try
            {
                switch (Day)
                {
                    case "1":
                        Rate = DateRates.dDate_1;
                        break;
                    case "2":
                        Rate = DateRates.dDate_2;
                        break;
                    case "3":
                        Rate = DateRates.dDate_3;
                        break;
                    case "4":
                        Rate = DateRates.dDate_4;
                        break;
                    case "5":
                        Rate = DateRates.dDate_5;
                        break;
                    case "6":
                        Rate = DateRates.dDate_6;
                        break;
                    case "7":
                        Rate = DateRates.dDate_7;
                        break;
                    case "8":
                        Rate = DateRates.dDate_8;
                        break;
                    case "9":
                        Rate = DateRates.dDate_9;
                        break;
                    case "10":
                        Rate = DateRates.dDate_10;
                        break;
                    case "11":
                        Rate = DateRates.dDate_11;
                        break;
                    case "12":
                        Rate = DateRates.dDate_12;
                        break;
                    case "13":
                        Rate = DateRates.dDate_13;
                        break;
                    case "14":
                        Rate = DateRates.dDate_14;
                        break;
                    case "15":
                        Rate = DateRates.dDate_15;
                        break;
                    case "16":
                        Rate = DateRates.dDate_17;
                        break;
                    case "17":
                        Rate = DateRates.dDate_17;
                        break;
                    case "18":
                        Rate = DateRates.dDate_18;
                        break;
                    case "19":
                        Rate = DateRates.dDate_19;
                        break;
                    case "20":
                        Rate = DateRates.dDate_20;
                        break;
                    case "21":
                        Rate = DateRates.dDate_21;
                        break;
                    case "22":
                        Rate = DateRates.dDate_22;
                        break;
                    case "23":
                        Rate = DateRates.dDate_23;
                        break;
                    case "24":
                        Rate = DateRates.dDate_24;
                        break;
                    case "25":
                        Rate = DateRates.dDate_25;
                        break;
                    case "26":
                        Rate = DateRates.dDate_26;
                        break;
                    case "27":
                        Rate = DateRates.dDate_27;
                        break;
                    case "28":
                        Rate = DateRates.dDate_28;
                        break;
                    case "29":
                        Rate = DateRates.dDate_29;
                        break;
                    case "30":
                        Rate = DateRates.dDate_30;
                        break;
                    case "31":
                        Rate = DateRates.dDate_31;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Convert.ToDecimal(Rate);
        }

        public static decimal GetPurchaseRateByDate(string Day, tbl_PurchaseRate DateRates)
        {
            decimal? Rate = 0;
            try
            {
                switch (Day)
                {
                    case "1":
                        Rate = DateRates.dDate_1;
                        break;
                    case "2":
                        Rate = DateRates.dDate_2;
                        break;
                    case "3":
                        Rate = DateRates.dDate_3;
                        break;
                    case "4":
                        Rate = DateRates.dDate_4;
                        break;
                    case "5":
                        Rate = DateRates.dDate_5;
                        break;
                    case "6":
                        Rate = DateRates.dDate_6;
                        break;
                    case "7":
                        Rate = DateRates.dDate_7;
                        break;
                    case "8":
                        Rate = DateRates.dDate_8;
                        break;
                    case "9":
                        Rate = DateRates.dDate_9;
                        break;
                    case "10":
                        Rate = DateRates.dDate_10;
                        break;
                    case "11":
                        Rate = DateRates.dDate_11;
                        break;
                    case "12":
                        Rate = DateRates.dDate_12;
                        break;
                    case "13":
                        Rate = DateRates.dDate_13;
                        break;
                    case "14":
                        Rate = DateRates.dDate_14;
                        break;
                    case "15":
                        Rate = DateRates.dDate_15;
                        break;
                    case "16":
                        Rate = DateRates.dDate_17;
                        break;
                    case "17":
                        Rate = DateRates.dDate_17;
                        break;
                    case "18":
                        Rate = DateRates.dDate_18;
                        break;
                    case "19":
                        Rate = DateRates.dDate_19;
                        break;
                    case "20":
                        Rate = DateRates.dDate_20;
                        break;
                    case "21":
                        Rate = DateRates.dDate_21;
                        break;
                    case "22":
                        Rate = DateRates.dDate_22;
                        break;
                    case "23":
                        Rate = DateRates.dDate_23;
                        break;
                    case "24":
                        Rate = DateRates.dDate_24;
                        break;
                    case "25":
                        Rate = DateRates.dDate_25;
                        break;
                    case "26":
                        Rate = DateRates.dDate_26;
                        break;
                    case "27":
                        Rate = DateRates.dDate_27;
                        break;
                    case "28":
                        Rate = DateRates.dDate_28;
                        break;
                    case "29":
                        Rate = DateRates.dDate_29;
                        break;
                    case "30":
                        Rate = DateRates.dDate_30;
                        break;
                    case "31":
                        Rate = DateRates.dDate_31;
                        break;
                }
            }
            catch (Exception)
            {

            }
            return Convert.ToDecimal(Rate);
        }

        public static List<DayRate> GetDaysRate(long TarrifID)
        {
            List<DayRate> arrDayRate = new List<DayRate> {
              new DayRate {Day ="Mon"},
              new DayRate {Day ="Tue"},
              new DayRate {Day ="Wed"},
              new DayRate {Day ="Thu"},
              new DayRate {Day ="Fri"},
              new DayRate {Day ="Sat"},
              new DayRate {Day ="Sun"},
            };
            try
            {
                using (db = new ClickUrHotel_DBEntities())
                {

                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrDayRate;
        }

        public static decimal GetTarrifRate(DateTime sFromDate, DateTime dToDate, long PlanID, long RoomID, long Meal_PlanID, out decimal EBRate, out decimal CWBRate, out decimal CNBRate)
        {
            decimal Rate = 0;
            EBRate = 0; CWBRate = 0; CNBRate = 0;
            try
            {
                List<decimal> arrRates = new List<decimal>();
                var calendar = new List<DateTime>();
                while (sFromDate <= dToDate)
                {
                    sFromDate = sFromDate.AddDays(1);
                    string Month = sFromDate.Month.ToString();
                    string Year = sFromDate.Year.ToString();
                    using (db = new ClickUrHotel_DBEntities())
                    {
                        var arrRate = (from obj in db.tbl_DatesRate
                                       where obj.nPlanID == PlanID &&
                                       obj.nMealPlanID == Meal_PlanID
                                       select obj).ToList();

                        decimal RR = GetRateByDate(sFromDate.Day.ToString(), arrRate.Where(R => R.PaxType == "RR").FirstOrDefault());
                        if (RR != 0)
                            arrRates.Add(RR);
                        EBRate = GetRateByDate(sFromDate.Day.ToString(), arrRate.Where(R => R.PaxType == "EB").FirstOrDefault());
                        CWBRate = GetRateByDate(sFromDate.Day.ToString(), arrRate.Where(R => R.PaxType == "CW").FirstOrDefault());
                        CNBRate = GetRateByDate(sFromDate.Day.ToString(), arrRate.Where(R => R.PaxType == "CN").FirstOrDefault());
                    }


                }
                Rate = arrRates.Min();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Rate;
        }

        #endregion

        #region Update Rates
        public static List<Rates> GetRoomRate(string HotelCode, string MealID, List<string> RoomID, string SupplierID, List<DateTime> DateRange, string RateType, string sTarrifType, string sOfferType,bool bShowAll)
        {
            List<Rates> arrRates = new List<Rates>();
            try
            {
                foreach (var Month in DateRange.Select(d => d.Month).Distinct().ToList())
                {
                    string sMonth = Month.ToString();
                    string sYear = DateRange.Where(d => d.Month == Month).FirstOrDefault().Year.ToString();
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        if (RateType == "S")
                        {
                            IQueryable<tbl_DatesRate> arrData;  
                            if (bShowAll==false)
                             arrData = (from obj in db.tbl_DatesRate
                                                                 join objRatePlan in db.tbl_RatePlan on obj.nPlanID equals objRatePlan.PlanID
                                                                 where RoomID.Contains(obj.RoomTypeID.ToString()) &&
                                                            obj.nMealPlanID.ToString() == MealID
                                                            && obj.nHotelID.ToString() == HotelCode &&
                                                            objRatePlan.RateType == sTarrifType &&
                                                            obj.nSupplierID.ToString() == SupplierID && obj.sMonth == sMonth && obj.sYear == sYear && obj.PaxType == "RR"
                                                                 select
obj);
                            else
                                arrData = (from obj in db.tbl_DatesRate
                                           join objRatePlan in db.tbl_RatePlan on obj.nPlanID equals objRatePlan.PlanID
                                           where RoomID.Contains(obj.RoomTypeID.ToString()) &&
                                      obj.nMealPlanID.ToString() == MealID
                                      && obj.nHotelID.ToString() == HotelCode &&
                                      objRatePlan.RateType == sTarrifType &&
                                      obj.nSupplierID.ToString() == SupplierID && obj.sMonth == sMonth && obj.sYear == sYear && obj.PaxType != "RR"
                                           select
obj);
                            if (arrData.ToList().Count != 0)
                            {
                                List<tbl_DatesRate> arrRoomRates = arrData.ToList().ToList();
                                foreach (var Date in DateRange.Where(d => d.Month == Month))
                                {
                                    foreach (var Rate in arrRoomRates)
                                {
                                   
                                        float fRate = Convert.ToSingle(GetRateByDate(Date.Day.ToString(), Rate));
                                        arrRates.Add(new Rates
                                        {
                                            PlanID = Rate.nPlanID,
                                            sPaxType = Rate.PaxType,
                                            RoomID = Convert.ToInt64(Rate.RoomTypeID),
                                            RRate = fRate,
                                            EBRate = fRate,
                                            CNBRate = fRate,
                                            CWBRate = fRate,
                                            arrOffer = new List<tbl_CommonHotelOffer>(),
                                            Date = Date.ToString("dd-MM-yyyy"),
                                        });
                                        if (sOfferType != "")
                                        {
                                            arrRates.LastOrDefault().arrOffer = OfferManager.GetOfferByDay(Convert.ToInt64(sOfferType), Date, sOfferType);
                                            if (arrRates.LastOrDefault().arrOffer.Count != 0)
                                                arrRates.LastOrDefault().RRate = OfferManager.GetOfferRate(arrRates.LastOrDefault().arrOffer.FirstOrDefault(), arrRates.LastOrDefault().RRate, Rate.PaxType);
                                        }

                                    }
                                }

                            }
                        }
                        else
                        {
                            IQueryable<tbl_PurchaseRate> arrData = (from obj in db.tbl_PurchaseRate
                                                                    join objRatePlan in db.tbl_RatePlan on obj.nPlanID equals objRatePlan.PlanID
                                                                    where RoomID.Contains(obj.RoomTypeID.ToString()) &&
                                                           obj.nMealPlanID.ToString() == MealID
                                                           && obj.nHotelID.ToString() == HotelCode &&
                                                           objRatePlan.RateType == sTarrifType &&
                                                           obj.nSupplierID.ToString() == SupplierID && obj.sMonth == sMonth && obj.sYear == sYear && obj.sPaxType == "RR"
                                                                    select
obj);
                            if (arrData.ToList().Count != 0)
                            {
                                List<tbl_PurchaseRate> arrRoomRates = arrData.ToList();
                                foreach (var Date in DateRange.Where(d => d.Month == Month))
                                {
                                    foreach (var Rate in arrRoomRates)
                                {
                                    
                                        float fRate = Convert.ToSingle(GetPurchaseRateByDate(Date.Day.ToString(), Rate));
                                        arrRates.Add(new Rates
                                        {
                                            PlanID = Rate.nPlanID,
                                            sPaxType = Rate.sPaxType,
                                            RoomID = Convert.ToInt64(Rate.RoomTypeID),
                                            RRate = fRate,
                                            EBRate = fRate,
                                            CNBRate = fRate,
                                            CWBRate = fRate,
                                            arrOffer = new List<tbl_CommonHotelOffer>(),
                                            Date = Date.ToString("dd-MM-yyyy"),
                                        });
                                    }
                                }

                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
            }
            return arrRates;
        }



        public static void UpdateRate(string HotelCode, string MealID, string RoomID, string SupplierID, DateTime Date, decimal Rate, string PaxType, string RateType)
        {
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {

                    string sMonth = Date.Month.ToString();
                    string sYear = Date.Year.ToString();
                    if (RateType == "S")
                    {
                        IQueryable<tbl_DatesRate> arrRate = (from obj in db.tbl_DatesRate
                                                             where
                                RoomID == obj.RoomTypeID.ToString() && obj.nHotelID.ToString() == HotelCode &&
                                obj.nSupplierID.ToString() == SupplierID && obj.sMonth == sMonth
                                 && obj.sYear == sYear && obj.PaxType == PaxType
                                                             select obj);
                        if (arrRate.ToList().Count != 0)
                        {
                            var RRate = GetDateRate(Date.Day.ToString(), Rate, arrRate.ToList().FirstOrDefault());
                            db.SaveChanges();
                        }
                        else
                            throw new Exception("Unable to Update Rate");
                    }
                    else
                    {
                        IQueryable<tbl_PurchaseRate> arrRate = (from obj in db.tbl_PurchaseRate
                                                                where
                                RoomID == obj.RoomTypeID.ToString() && obj.nHotelID.ToString() == HotelCode &&
                                obj.nSupplierID.ToString() == SupplierID && obj.sMonth == sMonth
                                 && obj.sYear == sYear && obj.sPaxType == PaxType
                                                                select obj);
                        if (arrRate.ToList().Count != 0)
                        {
                            var RRate = GetPurchaseRate(Date.Day.ToString(), Rate, arrRate.ToList().FirstOrDefault());
                            db.SaveChanges();
                        }
                        else
                            throw new Exception("Unable to Update Rate");
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
        }

        public static void UpdatePaxRate(string PaxType, float Rate, List<long> arrPlanID, string SellType, string sFrom, string sTo, long MealPlanID)
        {
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    CutAdmin.Common.Common.EndDate = DateTime.ParseExact(sTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    CutAdmin.Common.Common.StartDate = DateTime.ParseExact(sFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    List<DateTime> arrChargeDates = CutAdmin.Common.Common.Calander;
                    foreach (var nPlanID in arrPlanID)
                    {
                        foreach (var month in arrChargeDates.Select(d => d.Month).Distinct().ToList())
                        {
                            List<DateTime> arrRatedate = arrChargeDates.Where(d => d.Month == month).ToList();
                            foreach (var ChargeDate in arrRatedate)
                            {
                                DateTime sDate = ChargeDate;
                                string sYear = sDate.Year.ToString();
                                string sMonth = sDate.Month.ToString();
                                IQueryable<tbl_DatesRate> arrDateRates = (from obj in db.tbl_DatesRate
                                                                          where obj.sMonth == sMonth && obj.sYear == sYear
                                                                          && obj.nPlanID == nPlanID && obj.PaxType == PaxType
                                                                          && obj.nMealPlanID == MealPlanID
                                                                          select obj);

                                IQueryable<tbl_PurchaseRate> arrPurchaseRates = (from obj in db.tbl_PurchaseRate
                                                                                 where obj.sMonth == sMonth && obj.sYear == sYear
                                                                                 && obj.nPlanID == nPlanID && obj.sPaxType == PaxType
                                                                                 && obj.nMealPlanID == MealPlanID
                                                                                 select obj);

                                if (SellType == "S")
                                {
                                    if (arrDateRates.Count() == 0)
                                        continue;
                                    long count = arrDateRates.Count();
                                    var DateRate = arrDateRates.FirstOrDefault();
                                    GetDateRate(ChargeDate.Day.ToString(), Convert.ToDecimal(Rate), DateRate); /*Sell Rate*/
                                }
                                else
                                {
                                    if (arrPurchaseRates.Count() == 0)
                                        continue;
                                    var arrPurchase = arrPurchaseRates.FirstOrDefault();
                                    GetPurchaseRate(ChargeDate.Day.ToString(), Convert.ToDecimal(Rate), arrPurchase);
                                }
                            }
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get Valid Markets & Cancellation
        public class Market
        {
            public string Markets { get; set; }
        }
        public static List<string> GetMarket(string FromDate, string ToDate, string HotelCode, string RoomID, string MealPlan, string Supplier)
        {
            List<string> arrMarket = new List<string>();
            try
            {
                Common.Common.StartDate = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Common.Common.EndDate = DateTime.ParseExact(ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> arrDates = Common.Common.Calander;
                foreach (var Month in arrDates.Select(d => d.Month).Distinct().ToList())
                {
                    string sYear = arrDates.Where(d => d.Month == Month).FirstOrDefault().Year.ToString();
                    string sMonth = Month.ToString();
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        IQueryable<Market> arrData = (from obj in db.tbl_DatesRate
                                                      join objPlan in db.tbl_RatePlan on obj.nPlanID.ToString() equals objPlan.PlanID.ToString()
                                                      where obj.sMonth == sMonth && obj.sYear == sYear && obj.nHotelID.ToString() == HotelCode &&
                                                      objPlan.RoomTypeID.ToString() == RoomID && obj.nMealPlanID.ToString() == MealPlan &&
                                                      obj.nSupplierID.ToString() == Supplier
                                                      select new Market
                                                      {
                                                          Markets = objPlan.Market
                                                      });
                        if (arrData.ToList().Count != 0)
                        {
                            var arrMarkets = arrData.ToList();
                            foreach (var obj in arrData.ToList())
                            {
                                if (!arrMarket.Contains(obj.Markets))
                                {
                                    foreach (var Market in obj.Markets.Split(','))
                                    {
                                        if (!arrMarket.Contains(obj.Markets) && Market != "")
                                            arrMarket.Add(Market);
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception("No Rates Found.");
            }
            return arrMarket.Distinct().ToList();

        }

        public static List<arrCancellation> GetCancellation(string FromDate, string ToDate, string HotelCode, string RoomID, string MealPlan, string Supplier, string sTarrifType)
        {
            List<arrCancellation> arrCancel = new List<arrCancellation>();
            DateTime dFrom = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Common.Common.StartDate = dFrom;
            DateTime dToDate = DateTime.ParseExact(ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Common.Common.EndDate = dToDate;
            List<DateTime> arrChargeDate = Common.Common.Calander;
            try
            {
                bool? nonRafundable = false;
                using (var db = new ClickUrHotel_DBEntities())
                {
                    List<arrTarrif> arrData = (from obj in db.tbl_TarrifPlan
                                               join objPlan in db.tbl_RatePlan on obj.nPlanID equals objPlan.PlanID
                                               where obj.nHotelID.ToString() == HotelCode && obj.nMeal_PlanID.ToString() == MealPlan &&
                                               obj.RoomTypeID.ToString() == RoomID && objPlan.SupplierID.ToString() == Supplier
                                               && objPlan.RateType == sTarrifType
                                               select new arrTarrif
                                               {
                                                   TarrifID = obj.nRateID,
                                                   FromDate = obj.sFromDate,
                                                   ToDate = obj.sToDate,
                                                   nCancellation = obj.nCancellation,
                                                   NoShow = obj.nonShow,
                                                   nonRafundable = obj.nonRefundable,

                                               }).ToList();

                    if (arrData.ToList().Count != 0)
                    {
                        List<arrTarrif> arrTarrif = arrData.ToList();
                        foreach (var obj in arrTarrif)
                        {
                            Common.Common.StartDate = DateTime.ParseExact(obj.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Common.Common.EndDate = DateTime.ParseExact(obj.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            var match = Common.Common.Calander.Intersect(arrChargeDate);
                            if (match.Count() !=0)
                            {

                                foreach (DateTime objDate in arrChargeDate)
                                {
                                    if(match.ToList().Contains(objDate))
                                    {
                                        if(!arrCancel.Exists(d=>d.Date == objDate.ToString("dd-MM-yyyy")))
                                        {
                                            if (obj.nonRafundable == true)
                                            {
                                                arrCancel.Add(new arrCancellation
                                                {
                                                    TarrifID = obj.TarrifID,
                                                    RefundType = "Ammount",
                                                    Type = "non-Refundable",
                                                    Date = objDate.ToString("dd-MM-yyyy"),
                                                });
                                            }
                                            else
                                            {
                                                List<string> CancellID = new List<string> { obj.nCancellation.ToString(), obj.NoShow.ToString() };
                                                List<arrCancellation> arrCancelDetails =
                                                 (from objCancel in db.tbl_CommonCancelationPolicy
                                                  where CancellID.Contains(objCancel.CancelationID.ToString())
                                                  select new arrCancellation
                                                  {
                                                      TarrifID = obj.TarrifID,
                                                      RefundType = objCancel.RefundType,
                                                      Type = objCancel.ChargesType,
                                                      Amount = objCancel.AmountToCharge,
                                                      noNights = objCancel.NightsToCharge,
                                                      Percentage = objCancel.PercentageToCharge,
                                                      DayTill = objCancel.DaysPrior,

                                                  }).ToList();
                                                foreach (var Cancel in arrCancelDetails)
                                                {
                                                    Cancel.Date = objDate.ToString("dd-MM-yyyy");
                                                    arrCancel.Add(Cancel);
                                                }
                                              
                                            }
                                        }
                                    }
                                      
                                }
                            }
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            if (arrCancel.All(d => d.Type == "non-Refundable"))
            {
                arrCancel = arrCancel.OrderByDescending(d => d.TarrifID).ToList();
            }
            arrCancel = arrCancel.Distinct().ToList();
            return arrCancel;
        }
        #endregion

        #region Change Tarrif Policy && Cancellation
        public static void ChangeTarrifByPolicy(long TarrifID, string sFrom, List<tbl_CommonCancelationPolicy> arrPolicy)
        {
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    List<tbl_TarrifPlan> ListTarrif = new List<tbl_TarrifPlan>();
                    tbl_TarrifPlan arrTarrif = (from obj in DB.tbl_TarrifPlan where obj.nRateID == TarrifID select obj).FirstOrDefault();
                    if (arrTarrif == null)
                        throw new Exception("Not a Valid Rates");
                    DB.tbl_TarrifPlan.Remove(arrTarrif);
                    DB.SaveChanges();
                    DateTime dtFrom = DateTime.ParseExact(arrTarrif.sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Common.Common.StartDate = dtFrom;
                    DateTime dtToDate = DateTime.ParseExact(arrTarrif.sToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Common.Common.EndDate = dtToDate;
                    List<DateTime> arrDates = Common.Common.Calander;
                    DateTime dtFromDate = DateTime.ParseExact(sFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    /*Update Previous Plan */
                    if (dtFrom <= (dtFromDate.AddDays(-1)))
                    {
                        tbl_TarrifPlan arrNewTarrif = arrTarrif;
                        arrTarrif.sToDate = dtFromDate.AddDays(-1).ToString("dd-MM-yyyy");
                        ListTarrif.Add(arrTarrif);
                        DB.tbl_TarrifPlan.Add(arrNewTarrif);
                        DB.SaveChanges();
                    }
                    if (dtFromDate.AddDays(1) <= dtToDate)
                    {
                        tbl_TarrifPlan arrNewTarrif = arrTarrif;
                        arrTarrif.sFromDate = dtFromDate.AddDays(1).ToString("dd-MM-yyyy");
                        arrTarrif.sToDate = dtToDate.ToString("dd-MM-yyyy");
                        DB.tbl_TarrifPlan.Add(arrNewTarrif);
                        DB.SaveChanges();
                    }
                    /*Current Plan Update*/
                    if (arrDates.Contains(dtFromDate))
                    {
                        tbl_TarrifPlan arrNewTarrif = arrTarrif;
                        arrNewTarrif.sFromDate = sFrom;
                        arrNewTarrif.sToDate = sFrom;
                        if (arrPolicy.Count != 0)
                        {
                            arrNewTarrif.nonRefundable = false;
                            /*Cancellation Policy*/
                            arrNewTarrif.nCancellation = arrPolicy.Where(d => d.RefundType == "Refundable").FirstOrDefault().CancelationID;
                            if (arrPolicy.Count == 2)
                                arrNewTarrif.nonShow = arrPolicy.Where(d => d.RefundType == "NoShow").FirstOrDefault().CancelationID;
                            else /*No Show same as Cancellation*/
                                arrNewTarrif.nonShow = arrPolicy.Where(d => d.RefundType == "Refundable").FirstOrDefault().CancelationID;
                        }
                        else
                        {
                            arrNewTarrif.nonRefundable = true;
                            arrNewTarrif.nCancellation = 0;
                            arrNewTarrif.nonShow = 0;
                        }
                        DB.tbl_TarrifPlan.Add(arrNewTarrif);
                        DB.SaveChanges();
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Assign Offers 
        public static void AssignOffer(string OfferID, List<DateTime> arrOfferDates, long nHotelID, List<long> arrRoomID)
        {
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    foreach (var nRoomID in arrRoomID)
                    {
                        var arrTarrif = DB.tbl_TarrifPlan.Where(d => d.nHotelID == nHotelID && d.RoomTypeID == nRoomID).ToList();
                        foreach (var tarrif in arrTarrif)
                        {
                            DateTime dtFrom = DateTime.ParseExact(tarrif.sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Common.Common.StartDate = dtFrom;
                            DateTime dtToDate = DateTime.ParseExact(tarrif.sToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Common.Common.EndDate = dtToDate;
                            List<DateTime> arrDates = Common.Common.Calander;
                            if (arrOfferDates.Intersect(arrDates).Count() != 0)
                            {
                                if (tarrif.sOffer != null && tarrif.sOffer != "")
                                    tarrif.sOffer += "," + OfferID;
                                else
                                    tarrif.sOffer += OfferID;
                            }
                        }
                        DB.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion
    }
    public class Rates
    {
        public long? PlanID { get; set; }
        public string sPaxType { get; set; }
        public string sRateType { get; set; }
        public long RoomID { get; set; }
        public float RRate { get; set; }
        public float EBRate { get; set; }
        public float CWBRate { get; set; }
        public float CNBRate { get; set; }
        public List<DayRate> arrDays { get; set; }
        public Rates MinSelling { get; set; }
        public List<tbl_CommonHotelOffer> arrOffer { get; set; }

        public string Date { get; set; }
    }

    public class RateOffer
    {
        public List<tbl_CommonHotelOffer> arrOffer { get; set; }
    }
    public class DayRate
    {
        public string Day { get; set; }
        public float Rate { get; set; }
        public Int64 PlanID { get; set; }
    }

    /*Room Rate*/
    public class RoomRate
    {
        public tbl_PurchaseRate arrPurchase { get; set; }
        public tbl_DatesRate arrRate { get; set; }
        public string RateType { get; set; }

    }


    /* Tarrif Policy*/
    public class arrTarrif
    {
        public long TarrifID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long? nCancellation { get; set; }
        public long? NoShow { get; set; }

        public bool? nonRafundable { get; set; }
    }


    public class arrCancellation
    {
        public long TarrifID { get; set; }
        public string RefundType { get; set; }
        public string Type { get; set; }
        public string Condition { get; set; }

        public int? DayTill { get; set; }
        public string Amount { get; set; }
        public string Percentage { get; set; }
        public string noNights { get; set; }

        public string Date { get; set; }
    }
}