﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class MailManager
    {
        #region 

        #endregion

        #region Send Invoice and Vouchers

        public static bool Sent( string Activity, string MailBody, string Subject, string Attachments, string To)
        {
            List<string> arrFrom = new List<string>();
            List<string> arrAttachments = new List<string>();
            Dictionary<string, string> arrTo = new Dictionary<string, string>();
            Dictionary<string, string> BccMails = new Dictionary<string, string>(), CcMails = new Dictionary<string, string>();
            string AccessKey = string.Empty;
            AccessKey = string.Empty;
            if(To !="")
                    arrTo.Add(To, To);
            else
                arrTo.Add(AccountManager.GetSupplierEmail(), AccountManager.GetSupplierEmail());
            try
            {
                long ID = AccountManager.GetSupplierByUser();
                using (var db = new DBHandlerDataContext())
                {
                    var arrMails = (from obj in db.tbl_ActivityMails where obj.Activity == Activity && obj.ParentID == ID select obj).FirstOrDefault();
                    if (arrMails != null)
                    {
                         if(arrMails.Email != "")
                                arrFrom.Add(arrMails.Email);
                         else
                            arrFrom.Add(AccountManager.GetSupplierEmail());
                        if (arrMails.BCcMail != null && arrMails.BCcMail != "")
                        {
                            foreach (var Mail in arrMails.BCcMail.Split(';'))
                            {
                                if (!BccMails.ToList().Exists(d => d.Key == Mail))
                                    BccMails.Add(Mail, Mail);
                            }
                        }

                        if (arrMails.CcMail != null && arrMails.CcMail != "")
                        {
                            foreach (var Mail in arrMails.CcMail.Split(';'))
                            {
                                if (!CcMails.ToList().Exists(d => d.Key == Mail))
                                    CcMails.Add(Mail, Mail);
                            }
                        }
                    }
                    else
                    {
                        arrFrom.Add(AccountManager.GetSupplierEmail());
                    }
                    foreach (var objAttachment in Attachments.Split(','))
                    {
                        if (objAttachment != "")
                            arrAttachments.Add(objAttachment);
                    }
                }
            }
            catch (Exception ex)
            {
                arrFrom.Add(AccountManager.GetSupplierEmail());
            }
            return MailSent(arrTo, BccMails, CcMails, Subject, MailBody, arrFrom, arrAttachments);
        }

        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            try
            {
                List<string> from = new List<string>();
                List<string> DocPathList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                Dictionary<string, string> BccList = new Dictionary<string, string>();
                from.Add(AccountManager.GetSupplierEmail());
                if (sTo == "")
                    sTo =(AccountManager.GetSupplierEmail());
                foreach (string mail in sTo.Split(',').Distinct().ToList())
                {
                    if (mail != "")
                    {
                        if (!Email1List.ToList().Exists(d => d.Key == mail))
                            Email1List.Add(mail, mail);
                    }
                }
                foreach (string mail in Cc.Split(';'))
                {
                    if (mail != "")
                    {
                        if(!BccList.ToList().Exists(d=>d.Key == mail))
                            BccList.Add(mail, mail);
                    }
                }
                foreach (string link in DocPath.Split(';'))
                {
                    if (link != "")
                    {
                        DocPathList.Add(link);
                    }
                }

                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
           
        }


        public static void SendInvoice(string ReservationID,Int64 Uid,string sTo,params string[] BookType)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    //DBHandlerDataContext DB = new DBHandlerDataContext();
                    /*Tausif Work*/
                    #region Send Mail
                    string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                    string From = ConfigurationManager.AppSettings["HotelMail"];
                    Int64 Supplier = AccountManager.GetSupplierByUser();
                    var sData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();
                    var dtHotelReservation = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                    ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                    var dtHotelContact = (from obj in db.Comm_HotelContacts where obj.HotelCode.ToString() == dtHotelReservation.HotelCode && obj.SupplierCode == Supplier && obj.Type != "HotelContact" select obj).ToList();
                    string HResEmail = "";
                    if (dtHotelContact.Count !=0)
                    {
                        foreach (var obj in dtHotelContact)
                        {
                            HResEmail += obj.email +";";
                        }
                        HResEmail = HResEmail.TrimEnd(';');
                    }

                    string title = "";
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    Int64 AgentID = AccountManager.GetUserByLogin();
                    string Invoice = InvoiceManager.GetInvoice(ReservationID, AgentID, out  title);
                    CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, ReservationID, "Invoice");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    string Voucher = VoucherManager.GenrateVoucher(ReservationID, AgentID.ToString(), "Vouchered");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    if (Voucher != "")
                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(Voucher, ReservationID, "Voucher");
                    string sMail = "";
                    if (dtHotelReservation.Status == "Vouchered")
                        sMail = CutAdmin.DataLayer.EmailManager.BookingMail4Agent(ReservationID);
                    else
                        sMail = CutAdmin.DataLayer.EmailManager.RequestMail2Agent(ReservationID, AccountManager.GetUserByLogin());
                    string DocPath = "";
                    if (dtHotelReservation.Status == "Vouchered")
                        DocPath = URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";
                    if (Voucher != "" && dtHotelReservation.Status =="Vouchered")
                        DocPath += ";" + URL + "InvoicePdf/" + ReservationID + "_Voucher.pdf";
                    if(dtHotelReservation.Status=="OnRequest")
                        DocPath = ";" + URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";

                    // Booking Mail To Agent 
                    //if (dtHotelReservation.Status == "Vouchered")
                    SendMail(sTo, title, sMail, DocPath, HResEmail);
                   // else

                    //Booking mail to hotel
                    if(dtHotelReservation.Status =="Vouchered")
                        CutAdmin.DataLayer.EmailManager.BookingMail2Hotel(ReservationID, AgentID.ToString(), "Vouchered");

                    // Booking Confirm Mail To Admin
                    string Admin_Bc, Admin_Cc;
                    string sAdminMail = EmailManager.BookingMail2CUT(ReservationID, Uid, out Admin_Bc, out Admin_Cc);
                    SendMail(Admin_Bc, title, sAdminMail, DocPath, Admin_Cc);
                    #endregion
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        public static void SendReconfrmInvoice(string ReservationID, Int64 Uid, string sTo)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    //DBHandlerDataContext DB = new DBHandlerDataContext();
                    /*Tausif Work*/
                    #region Send Mail
                    string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                    string From = ConfigurationManager.AppSettings["HotelMail"];

                    var sData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();

                    string title = "";
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    Int64 AgentID = AccountManager.GetUserByLogin();
                    string Invoice = InvoiceManager.GetInvoice(ReservationID, AgentID, out  title);
                    CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, ReservationID, "Invoice");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    string Voucher = VoucherManager.GenrateVoucher(ReservationID, AgentID.ToString(), "Vouchered");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    if (Voucher != "")
                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(Voucher, ReservationID, "Voucher");
                    string sMail = CutAdmin.DataLayer.EmailManager.BookingMail4Agent(ReservationID);
                    string DocPath = URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";
                    if (Voucher != "")
                        DocPath += ";" + URL + "InvoicePdf/" + ReservationID + "_Voucher.pdf";

                    // Booking Mail To Agent 
                    SendMail(sTo, title, sMail, DocPath, From);

                    // Booking Confirm Mail To Admin
                    string Admin_Bc, Admin_Cc;
                    string sAdminMail = EmailManager.BookingMail2CUT(ReservationID, Uid, out Admin_Bc, out Admin_Cc);
                    SendMail(Admin_Bc, title, sAdminMail, DocPath, Admin_Cc);
                    #endregion
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion



        public static bool SendMail(string Key,

                            Dictionary<string, string> to,
                            string subject,
                            string MailBody,
                            List<string> from_name,
                            List<string> attachment)
        {
            try
            {
                API test = new API(Key);
                from_name = new List<string>();
                using (var db = new DBHandlerDataContext())
                {
                    Int64 Uid = AccountManager.GetSupplierByUser();
                    from_name.Add(db.tbl_AdminLogins.Where(d => d.sid == Uid).FirstOrDefault().uid.ToString());
                }
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                //data.Add("bcc", to);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }

        }

        public static bool SendMail(string Key,

                             Dictionary<string, string> to,
                             Dictionary<string, string> Bcc,
                             string subject,
                             string MailBody,
                             List<string> from_name,
                             List<string> attachment)
        {
            try
            {
                API test = new API(ConfigurationManager.AppSettings["AccessKey"]);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                from_name = new List<string>();
                using (var db = new DBHandlerDataContext())
                {
                    Int64 Uid = AccountManager.GetSupplierByUser();
                    from_name.Add(db.tbl_AdminLogins.Where(d => d.sid == Uid).FirstOrDefault().uid);
                }
                data.Add("to", to);
                data.Add("bcc", Bcc);
               // data.Add("cc", Bcc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }



        }


        public static bool SendMailByAdmin(
                            Dictionary<string, string> to,
                            Dictionary<string, string> Bcc,
                            string subject,
                            string MailBody,
                            List<string> from_name,
                            List<string> attachment)
        {
            try
            {
                API test = new API(ConfigurationManager.AppSettings["AccessKey"]);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                data.Add("bcc", Bcc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                return true;
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }



        }

        #region forgetpassword
        public static bool SendForgetMail(string Key,

                    Dictionary<string, string> to,
                    string subject,
                    string MailBody,
                    List<string> from_name,
                    List<string> attachment)
        {
            try
            {
                API test = new API(Key);
                from_name = new List<string>();
                using (var db = new DBHandlerDataContext())
                {
                   // string Uid = ConfigurationManager.AppSettings["supportMail"];
                   from_name.Add(ConfigurationManager.AppSettings["supportMail"]);
                }
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                data.Add("bcc", to);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                return true;
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }

        }


        #endregion

        public static bool MailSent(Dictionary<string, string> to,
                             Dictionary<string, string> Bcc,
                              Dictionary<string, string> Cc,
                             string subject,
                             string MailBody,
                             List<string> from_name,
                             List<string> attachment)
        {
            try
            {
                string accessKey = ConfigurationManager.AppSettings["AccessKey"];

                API test = new API(accessKey);
                // API test = new API(Key);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                data.Add("bcc", Bcc);
                data.Add("cc", Cc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }

    }
}