﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using CommonLib.Response;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class TaxManager
    {
        //static dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        //static DBHandlerDataContext db = new DBHandlerDataContext();
        public static bool SaveTaxDetails(List<Comm_TaxMapping> ListTax)
        {
            using (var dbTax = new dbTaxHandlerDataContext())
            {
                bool retCode = false;
                var oldRates = (from obj in dbTax.Comm_TaxMappings
                                where obj.HotelID == ListTax.First().HotelID
                                    && obj.ServiceID == ListTax.First().ServiceID
                                    && obj.IsAddOns == ListTax.First().IsAddOns
                                select obj).ToList();
                if (oldRates.Count != 0)
                {
                    dbTax.Comm_TaxMappings.DeleteAllOnSubmit(oldRates);
                    dbTax.SubmitChanges();
                }
                dbTax.Comm_TaxMappings.InsertAllOnSubmit(ListTax);
                dbTax.SubmitChanges();
                return retCode;
            }
        }

        public static bool SaveTaxDetails(List<Comm_AddOnsRate> ListAddOns)
        {
            using (var dbTax = new dbTaxHandlerDataContext())
            {
                bool retCode = false;
                var oldAddOns = (from obj in dbTax.Comm_AddOnsRates
                                 where obj.HotelID == ListAddOns.First().HotelID
                                     && obj.RateID == ListAddOns.First().RateID
                                 select obj).ToList();
                if (oldAddOns.Count != 0)
                {
                    dbTax.Comm_AddOnsRates.DeleteAllOnSubmit(oldAddOns);
                    dbTax.SubmitChanges();
                }
                dbTax.Comm_AddOnsRates.InsertAllOnSubmit(ListAddOns);
                dbTax.SubmitChanges();
                return retCode;
            }
        }


        public static List<TaxRate> GetTaxRates(List<TaxRate> ListRates, float Total)
        {
            List<TaxRate> arrRate = new List<TaxRate>();
            try
            {
                foreach (var objRate in ListRates)
                {
                    List<Tax> arrTax = new List<Tax>();
                    float TotalRate = 0;
                    float BaseRate = Total;
                    foreach (var obj in objRate.TaxOn)
                    {
                        Tax sTax = new Tax();

                        if (objRate.TaxOn.Count == 1)
                        {
                            sTax.TaxName = obj.TaxName;
                            sTax.TaxRate = objRate.Per * Total / 100;
                        }
                        else
                        {
                            if (obj.ID == 0)
                            {
                                 sTax.TaxRate = Total;
                                 sTax.TaxName = obj.TaxName;
                            }
                               
                            else
                            {
                                sTax.ID = obj.ID;
                                sTax.TaxName = obj.TaxName;
                                BaseRate = (Total + arrRate.FirstOrDefault().TotalRate);
                                sTax.TaxRate = objRate.Per * BaseRate / 100;
                            }

                        }
                        arrTax.Add(sTax);
                    }
                    if (objRate.TaxOn.Count == 1)
                    {
                        TotalRate = arrTax.Select(d => d.TaxRate).ToList().Sum();
                    }
                    else
                        TotalRate = arrTax.Where(d => d.ID != 0).Select(r => r.TaxRate).ToList().Sum();

                    arrRate.Add(new TaxRate { 
                        BaseRate = BaseRate, 
                        ID = objRate.ID, 
                        Per = objRate.Per, 
                        RateName = objRate.RateName, 
                        TaxOn = arrTax,
                        TotalRate = TotalRate,
                        Type = objRate.Type
                    });
                }
            }
            catch
            {

            }
            return arrRate;

        }

        public static List<TaxRate> GetTaxRates(List<TaxRate> ListRates, float Total, float Markup)
        {
            List<TaxRate> arrRate = new List<TaxRate>();
            try
            {
                foreach (var objRate in ListRates)
                {
                    arrRate.Add(new TaxRate
                    {
                        ID = objRate.ID,
                        BaseRate = Total,
                        Per = Convert.ToSingle(ListRates.Where(d => d.ID == objRate.ID).FirstOrDefault().Per),
                        RateName = ListRates.Where(d => d.ID == objRate.ID).FirstOrDefault().RateName,
                        TotalRate = 0,
                        TaxOn = new List<Tax>(),
                    });
                    List<Tax> arrTax = new List<Tax>();
                    float TotalRate = 0;
                    float BaseRate = Total;
                    foreach (var obj in objRate.TaxOn)
                    {
                        Tax sTax = new Tax();
                         string TaxName = ""; float TaxPer = 0;
                            if(obj.ID ==0)
                            {
                                    TaxName = "Base Rate";
                                    TaxPer = Convert.ToSingle(ListRates.Where(d => d.ID == objRate.ID).FirstOrDefault().Per);
                                    TotalRate = Total * TaxPer / 100;
                            }
                            else if(obj.ID  ==-1)
                            {
                                     TaxName = "Markup Rate";
                                     TaxPer =  Convert.ToSingle(ListRates.Where(d => d.ID == objRate.ID).FirstOrDefault().Per);
                                     TotalRate = Markup * TaxPer / 100;
                            }
                            arrTax.Add(new Tax { ID = Convert.ToInt64(obj.ID), TaxName = TaxName, TaxPer = TaxPer,TaxRate=TotalRate });
                        }
                         arrRate.LastOrDefault().TaxOn = arrTax;
                         if (objRate.TaxOn.Count == 1)
                         {
                             TotalRate = arrTax.Select(d => d.TaxRate).ToList().Sum();
                         }
                         else
                             TotalRate = arrTax.Select(r => r.TaxRate).ToList().Sum();
                         arrRate.LastOrDefault().TotalRate = arrTax.Select(d => d.TaxRate).Sum();
                    }
                    
               
            }
            catch
            {

            }
            return arrRate;

        }


        public static ServiceCharge GetCharge(ServiceCharge arrHotelCharge)
        {
            ServiceCharge arrCharge = new ServiceCharge();
            Int64 Uid = AccountManager.GetSupplierByUser();
            try
            {
                List<TaxRate> ListTaxes = new List<TaxRate>();
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    arrCharge.RoomRate = arrHotelCharge.RoomRate+arrHotelCharge.AdminMarkup ;
                    arrCharge.AdminMarkup = arrHotelCharge.AdminMarkup;
                    var arrTaxes = GetRates(Uid);
                    foreach (Int64 objTax in arrTaxes.Select(d => d.TaxID).Distinct())
                    {
                        ListTaxes.Add(new TaxRate
                        {
                            ID = objTax,
                            BaseRate = arrCharge.RoomRate,
                            Per = Convert.ToSingle(arrTaxes.Where(d => d.TaxID == objTax).FirstOrDefault().Value),
                            RateName = dbTax.Comm_Taxes.Where(d=>d.ID == objTax).FirstOrDefault().Name,
                            TotalRate = 0,
                            TaxOn = new List<Tax>(),
                        });
                        List<Tax> arrTax = new List<Tax>();
                        var ListTax = arrTaxes.Where(d => d.TaxID == objTax).ToList();
                        foreach (var compTax in ListTax)
                        {
                            string TaxName = ""; float TaxPer = 0,TotalRate=0;;
                            if(compTax.TaxOn ==0)
                            {
                                    TaxName = "Base Rate";
                                    TotalRate = arrCharge.RoomRate;
                            }
                            if(compTax.TaxOn ==-1)
                            {
                                     TaxName = "Markup Rate";
                                     TaxPer =  Convert.ToSingle(ListTax.Where(d => d.TaxID == objTax).FirstOrDefault().Value);
                                     TotalRate = arrCharge.AdminMarkup * TaxPer/100;
                            }
                            else
                            {
                                TaxName = dbTax.Comm_Taxes.Where(d => d.ID == compTax.TaxOn).FirstOrDefault().Name;
                                TaxPer = Convert.ToSingle(dbTax.Comm_Taxes.Where(d => d.ID == compTax.TaxOn).FirstOrDefault().Value);
                                TotalRate = TaxPer * Convert.ToSingle(arrTaxes.Where(d => d.TaxID == compTax.TaxOn).FirstOrDefault().Value);
                            }
                            arrTax.Add(new Tax { ID = Convert.ToInt64(compTax.TaxOn), TaxName = TaxName, TaxPer = TaxPer,TaxRate=TotalRate });
                        }
                        ListTaxes.LastOrDefault().TaxOn = arrTax;
                        ListTaxes.LastOrDefault().TotalRate = arrTax.Select(d => d.TaxRate).Sum();
                    }
                    arrCharge.HotelTaxes = new List<TaxRate>();
                    foreach (var objCharge in arrHotelCharge.HotelTaxes)
                    {
                        var arrSupCharge = ListTaxes.Where(d => d.RateName == objCharge.RateName).FirstOrDefault();
                        float Rate = 0;
                        if (arrSupCharge != null)
                            Rate = arrSupCharge.TotalRate;
                        arrCharge.HotelTaxes.Add(new TaxRate
                        {
                            BaseRate = arrCharge.RoomRate ,
                            ID = objCharge.ID,
                            Per = objCharge.Per,
                            RateName = objCharge.RateName,
                            TotalRate = objCharge.TotalRate + Rate,
                        });
                    }
                    arrCharge.TotalPrice = arrCharge.RoomRate + arrCharge.HotelTaxes.Select(d => d.TotalRate).ToList().Sum();

                }  
               
            }
            catch (Exception ex)
            {
              
            }
            return arrCharge;

        }

        public static List<Comm_SupplierTax> GetRates(Int64 uid)
        {
            List<Comm_SupplierTax> arrRates = new List<Comm_SupplierTax>();
            try
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    arrRates = dbTax.Comm_SupplierTaxes.Where(d => d.SupplierID == uid).ToList();
                }
               
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrRates;
        }

        public static bool DeleteTaxDetails(Int64 HotelCode)
        {
            bool retCode = false;
            try
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    var oldRates = (from obj in dbTax.Comm_TaxMappings
                                    where obj.HotelID == HotelCode
                                    select obj).ToList();
                    if (oldRates.Count != 0)
                    {
                        dbTax.Comm_TaxMappings.DeleteAllOnSubmit(oldRates);
                        dbTax.SubmitChanges();
                    }
                    return retCode;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return retCode;
            }

        }

        public static bool DeleteTaxDetailsAddOns(Int64 HotelCode)
        {
            bool retCode = false;
            try
            {      
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    var oldRates = (from obj in dbTax.Comm_AddOnsRates
                                    where obj.HotelID == HotelCode
                                    select obj).ToList();
                    if (oldRates.Count != 0)
                    {
                        dbTax.Comm_AddOnsRates.DeleteAllOnSubmit(oldRates);
                        dbTax.SubmitChanges();
                    }
                    return retCode;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return retCode;
            }
        }
    }
}