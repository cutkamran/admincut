﻿using CommonLib.Response;
using CutAdmin.BL;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CutAdmin.DataLayer
{
    public class TransactionManager
    {
        public static DBHelper.DBReturnCode GetTransactions(out DataTable dtResult)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            //if (objGlobalDefault.UserType != "Supplier")
            // Uid = objGlobalDefault.ParentId;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            sqlParams[1] = new SqlParameter("@usertype", "Admin");
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_TransactionLoadAll", out dtResult, sqlParams);
            return retCode;
        }

        public static ReservationDetails objReservation { get; set; }
        public static void GenrateBooking(string ReservationID)
        {
            try
            {
                string OldResrvationID = ReservationID;
                string ConfirmationNo = ReservationID;
                string RefrenceNumbers = ReservationID;
                using (var db = new DBHandlerDataContext())
                {
                    BookingManager.objReservation = objReservation;
                    if (objReservation.SupplierID != AccountManager.GetSupplierByUser())
                    {

                        /*Supplier*/
                        ConfirmationNo = GenrateBookingBySupplier(ReservationID, "CUH", 232, objReservation.SupplierID, objReservation.SupplierID, objReservation.objCharges.HotelRate, objReservation.objCharges.SupplierTotal);

                        /*ClickUrHotel Reservation*/
                        var arrLastReservationID = (from obj in db.tbl_CommonHotelReservations select obj).ToList().Count;
                        ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                        objReservation.ReservationID = ReservationID;
                        BookingManager.objReservation = objReservation;
                        ConfirmationNo = GenrateBookingBySupplier(ReservationID, "Supplier", AccountManager.GetSupplierByUser(), 232, 232, objReservation.objCharges.SupplierTotal, objReservation.objCharges.CUHTotal);

                        RefrenceNumbers += "|" + ReservationID;

                        arrLastReservationID = (from obj in db.tbl_CommonHotelReservations select obj).ToList().Count;
                        ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                        objReservation.ReservationID = ReservationID;
                        BookingManager.objReservation = objReservation;
                        ConfirmationNo = GenrateBookingBySupplier(ReservationID, "AG", AccountManager.GetUserByLogin(), AccountManager.GetSupplierByUser(), AccountManager.GetSupplierByUser(), objReservation.objCharges.TotalPrice, objReservation.objCharges.TotalPrice);
                        RefrenceNumbers += "|" + ReservationID;
                    }
                    else
                    {
                        ConfirmationNo = GenrateBookingBySupplier(ReservationID, "AG", AccountManager.GetUserByLogin(), AccountManager.GetSupplierByUser(), AccountManager.GetSupplierByUser(), objReservation.objCharges.TotalPrice, objReservation.objCharges.TotalPrice);

                    }

                }
                using (var DB = new DBHandlerDataContext())
                {
                    foreach (var objRefrenceNo in RefrenceNumbers.Split('|'))
                    {
                        var arrReservation = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == objRefrenceNo select obj).FirstOrDefault();
                        arrReservation.SupplierConfirmNo = RefrenceNumbers;
                        DB.SubmitChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        public static string GenrateBookingBySupplier(string RervationID, string UserType, Int64 Uid, Int64 SupplierID, Int64 ParentID, float BaseFare, float TotalFare)
        {
            string SupplerConfirmNo = "";
            using (var DB = new DBHandlerDataContext())
            {
                try
                {
                    string Status = "";
                    
                    if (objReservation.OnVouchered==true && objReservation.BookingStatus == "")
                    {
                        Status = "Vouchered";
                    }
                    else if (objReservation.OnVouchered == false && objReservation.BookingStatus == "")
                    {
                        Status = "OnRequest";
                    }
                    else
                    {
                        Status = "OnHold";
                    }
                    

                    #region Hotel Booking Details
                    tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation
                    {
                        ReservationID = Convert.ToString(RervationID),
                        HotelCode = objReservation.objHotelDetails.sid.ToString(),
                        HotelName = objReservation.objHotelDetails.HotelName,
                        mealplan = objReservation.ListRoom[0].RoomDescription,
                        TotalRooms = Convert.ToInt16(objReservation.ListRoom.Count),
                        RoomCode = "",
                        sightseeing = 0,
                        Source = objReservation.SupplierID.ToString(),
                        Status = Status,
                        SupplierCurrency = "SAR",
                        terms = objReservation.objHotelDetails.HotelNote,
                        ParentID = ParentID,
                        Uid = Uid,
                        Updatedate = DateTime.Now.ToString("dd-MM-yyyy"),
                        Updateid = DataLayer.AccountManager.GetUserByLogin().ToString(),
                        VoucherID = "VCH-" + RervationID,
                        DeadLine = "",
                        AgencyName = objReservation.CompanyName,
                        AgentContactNumber = objReservation.Mobile,
                        AgentEmail = objReservation.email,
                        AgentMarkUp_Per = 0,
                        AgentRef = "",
                        AgentRemark = "",
                        AutoCancelDate = "",
                        AutoCancelTime = "",
                        bookingname = objReservation.LisCustumer[0].Title + ". " + objReservation.LisCustumer[0].Name + " " + objReservation.LisCustumer[0].LastName,
                        BookingStatus = Status,
                        CancelDate = "",
                        CancelFlag = false,
                        CheckIn = objReservation.sCheckIn,
                        CheckOut = objReservation.sCheckOut,
                        ChildAges = "",
                        Children = Convert.ToInt16(objReservation.RateGroup.RoomOccupancy.Select(d => d.ChildCount).ToList().Sum()),
                        City = objReservation.objHotelDetails.CityId,
                        deadlineemail = false,
                        Discount = 0,
                        ExchangeValue = 0,
                        ExtraBed = 0,
                        GTAId = "",
                        holdbooking = 0,
                        HoldTime = "",
                        HotelBookingData = "",
                        HotelDetails = "",
                        Infants = 0,
                        InvoiceID = "-",
                        IsAutoCancel = false,
                        IsConfirm = false,
                        LatitudeMGH = objReservation.objHotelDetails.HotelLatitude,
                        LongitudeMGH = objReservation.objHotelDetails.HotelLatitude,
                        LuxuryTax = 0,
                        mealplan_Amt = 0,
                        NoOfAdults = Convert.ToInt16(objReservation.RateGroup.RoomOccupancy.Select(d => d.AdultCount).ToList().Sum()),
                        NoOfDays = Convert.ToInt16(objReservation.noNights),
                        OfferAmount = 0,
                        OfferId = 0,
                        RefAgency = "",
                        ReferenceCode = "",
                        Remarks = objReservation.Remarks,
                        ReservationDate = DateTime.Now.ToString("dd-MM-yyyy"),
                        ReservationTime = "",
                        RoomRate = 0,
                        SalesTax = 0,
                        Servicecharge = 0,
                        ComparedFare = Convert.ToDecimal(BaseFare) + Convert.ToDecimal(objReservation.AddOnsCharge),
                        ComparedCurrency = "",
                        TotalFare = Convert.ToDecimal(TotalFare), //- objReservation.AdminCommission);
                    };
                    DB.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
                    DB.SubmitChanges();
                    #endregion
                    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
                    if (objReservation.OnVouchered && objReservation.BookingStatus!="OnHold")
                        retCode = CutAdmin.DataLayer.BookingManager.BookingTransact(Uid, SupplierID, ParentID, BaseFare, TotalFare);
                    int b = 0;
                    #region Booked Room
                    List<float> arrRates = new List<float>();
                    List<float> arrBaseRate = new List<float>();
                    List<TaxRate> ListTaxces = new List<TaxRate>();
                    if (UserType == "CUH")
                    {
                        arrRates = objReservation.ListRoom.Select(d => d.objCharges.SupplierTotal).ToList();
                        arrBaseRate = objReservation.ListRoom.Select(d => d.objCharges.HotelRate).ToList();
                    }
                    else if (UserType == "Supplier")
                    {
                        arrRates = objReservation.ListRoom.Select(d => d.objCharges.CUHTotal).ToList();
                        arrBaseRate = objReservation.ListRoom.Select(d => d.objCharges.CUHTotal).ToList();
                    }
                    else
                    {
                        arrRates = objReservation.ListRoom.Select(d => d.objCharges.TotalPrice).ToList();
                        arrBaseRate = objReservation.ListRoom.Select(d => d.objCharges.TotalPrice).ToList();
                    }
                    DBHandlerDataContext dbRoom = new DBHandlerDataContext();
                    List<tbl_CommonBookedRoom> arrBookedRoom = new List<tbl_CommonBookedRoom>();
                    List<tbl_CommonBookedPassenger> arrPax = new List<tbl_CommonBookedPassenger>();
                    foreach (var objRoom in objReservation.ListRoom)
                    {
                        //For Free Adons
                        string FreeItem = "";
                        if(objRoom.arrOffers.Count != 0)
                        {
                            foreach (var RoomOffer in objRoom.arrOffers)
                            {
                                if (RoomOffer.offerName.Contains("Free AddOns"))
                                    FreeItem = RoomOffer.FreeItem;
                            }
                        }
                        

                        string Ages = "";
                        string CancellationAmountMultiple = "";
                        string CancellationDateTime = "";
                        Ages = objRoom.ChildAges;
                        tbl_CommonBookedRoom BookedRoom = new tbl_CommonBookedRoom();
                        BookedRoom.Adults = objRoom.AdultCount;
                        BookedRoom.BoardText = Convert.ToString(objRoom.RoomDescription);
                        BookedRoom.CanAmtWithTax = "";
                        BookedRoom.RoomAmount = Convert.ToDecimal(arrRates[objReservation.ListRoom.IndexOf(objRoom)]);
                        foreach (var Canclellation in objRoom.CancellationPolicy)
                        {
                            if (UserType == "CUH")
                                CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
                            else if (UserType == "Supplier")
                                CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
                            else
                                CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";

                            CancellationDateTime += Canclellation.CancellationDateTime + "|";
                        }
                        BookedRoom.CancellationAmount = CancellationAmountMultiple;
                        BookedRoom.Child = objRoom.ChildCount;
                        BookedRoom.ChildAge = Ages;
                        BookedRoom.Commision = 0;
                        BookedRoom.CutCancellationDate = CancellationDateTime;
                        BookedRoom.Discount = 0;
                        BookedRoom.EssentialInfo = FreeItem;    //Saving Free Adons
                        BookedRoom.ExchangeRate = 0;
                        BookedRoom.FranchiseeMarkup = 0;
                        BookedRoom.FranchiseeTaxDetails = "";
                        BookedRoom.LeadingGuest = "";
                        BookedRoom.MealPlanID = Convert.ToString(objRoom.RoomTypeId);
                        BookedRoom.Remark = objReservation.Remarks;
                        BookedRoom.ReservationID = Convert.ToString(RervationID);
                        BookedRoom.TaxDetails = "";
                        //if (objRoom.RoomRateType == null)
                        //    objRoom.RoomRateType = "Contracted";
                        BookedRoom.RateType = objRoom.dates[0].Type;
                        if (objRoom.objCharges.HotelTaxes != null)
                        {
                            foreach (var objCharge in objRoom.objCharges.HotelTaxes)
                                BookedRoom.TaxDetails += objCharge.RateName + ":" + objCharge.TotalRate;
                        }
                        BookedRoom.RoomAmtWithTax = 0;
                        BookedRoom.RoomCode = Convert.ToString(objRoom.RoomDescriptionId);
                        BookedRoom.RoomNumber = Convert.ToString(b + 1);
                        BookedRoom.RoomServiceTax = 0;
                        BookedRoom.RoomType = objRoom.RoomTypeName;
                        BookedRoom.SupplierAmount = Convert.ToDecimal(arrBaseRate[objReservation.ListRoom.IndexOf(objRoom)]);
                        BookedRoom.SupplierCurrency = objRoom.dates[0].Currency;
                        BookedRoom.SupplierNoChargeDate = "";
                        BookedRoom.TDS = 0;
                        BookedRoom.TotalRooms = Convert.ToInt16(objReservation.ListRoom.Where(d => d.RoomTypeId == objRoom.RoomTypeId && d.RoomDescription == objRoom.RoomDescription).ToList().Count);
                        arrBookedRoom.Add(BookedRoom);
                        var arrRoomPax = objReservation.LisCustumer.Where(d => d.RoomNo == b + 1).ToList();
                        foreach (var objPax in arrRoomPax)
                        {
                            tbl_CommonBookedPassenger BookedPassenger = new tbl_CommonBookedPassenger();
                            BookedPassenger.Age = objPax.Age;
                            if (objReservation.ListRoom.IndexOf(objRoom) == 0)
                                BookedPassenger.IsLeading = true;
                            else
                                BookedPassenger.IsLeading = false;
                            BookedPassenger.LastName = objPax.LastName;
                            BookedPassenger.Name = objPax.Title + " " + objPax.Name;
                            BookedPassenger.PassengerType = objPax.type;
                            BookedPassenger.ReservationID = RervationID;
                            BookedPassenger.RoomCode = "";
                            BookedPassenger.RoomNumber = Convert.ToString(b + 1);
                            arrPax.Add(BookedPassenger);
                        }
                        b++;
                    }
                    dbRoom.tbl_CommonBookedRooms.InsertAllOnSubmit(arrBookedRoom);
                    dbRoom.tbl_CommonBookedPassengers.InsertAllOnSubmit(arrPax);
                    dbRoom.SubmitChanges();
                    #endregion
                    SupplerConfirmNo = HotelReserv.Sid.ToString();
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                }
            }
            return SupplerConfirmNo;
        }
    }
}