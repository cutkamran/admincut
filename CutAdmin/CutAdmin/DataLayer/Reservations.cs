﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using System.Globalization;
namespace CutAdmin.DataLayer
{
    public class Reservations
    {
        public int Sid { get; set; }
        public string ReservationID { get; set; }
        public string ReservationDate { get; set; }
        public string AgencyName { get; set; }
        public string Status { get; set; }
        public string bookingname { get; set; }
        public decimal? TotalFare { get; set; }
        public int TotalRooms { get; set; }
        public string HotelName { get; set; }
        public string sCheckIn { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string City { get; set; }
        public int? Children { get; set; }
        public string BookingStatus { get; set; }
        public int? NoOfAdults { get; set; }
        public string Source { get; set; }
        public Int64 Uid { get; set; }
        public string LatitudeMGH { get; set; }
        public string LongitudeMGH { get; set; }
        public string CurrencyCode { get; set; }
        public bool? IsConfirm { get; set; }

        public string ConfNo { get; set; }

        public int Night { get; set; }

        public string Meals { get; set; }

        public string Roomtype { get; set; }

        public long noNights { get; set; }

        public decimal? PricePerRoom { get; set; }

        public decimal? MealPerPax { get; set; }

        public string Deadline { get; set; }

        //static   DBHandlerDataContext DB = new DBHandlerDataContext();
        public static List<Reservations> BookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
          
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    if (objGlobalDefault.UserType != "Hotel")
                    {
                        IQueryable<Reservations> arrResult = from obj in DB.tbl_CommonHotelReservations
                                                             join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                                             join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                                             where obj.Uid == Uid
                                                             select new Reservations
                                                             {
                                                                 AgencyName = AgName.AgencyName,
                                                                 bookingname = obj.bookingname,
                                                                 BookingStatus = obj.BookingStatus,
                                                                 CurrencyCode = AgName.CurrencyCode,
                                                                 IsConfirm = obj.IsConfirm,
                                                                 CheckIn = obj.CheckIn,
                                                                 sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                                 CheckOut = obj.CheckOut,
                                                                 Children = obj.Children,
                                                                 City = obj.City,
                                                                 HotelName = obj.HotelName,
                                                                 LatitudeMGH = obj.LatitudeMGH,
                                                                 LongitudeMGH = obj.LongitudeMGH,
                                                                 NoOfAdults = obj.NoOfAdults,
                                                                 ReservationDate = DateTime.ParseExact(obj.ReservationDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("dd-MM-yy"),
                                                                 ReservationID = obj.ReservationID,
                                                                 Sid = obj.Sid,
                                                                 Source = obj.Source,
                                                                 Status = obj.Status,
                                                                 TotalFare = obj.TotalFare,
                                                                 TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                                 Uid = Convert.ToInt64(obj.Uid),
                                                                 noNights = Convert.ToInt64(obj.NoOfDays),
                                                                 Deadline = objroom.CutCancellationDate 
                                           };
                        ListReservation = arrResult.ToList().Distinct().ToList();
                        if (objGlobalDefault.UserType != "Agent")
                        {
                            ListReservation = (from obj in DB.tbl_CommonHotelReservations
                                               join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                               join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                               where AgName.UserType == "Admin" && obj.Uid == AgName.sid && obj.Uid == 232 && Convert.ToInt64(obj.Source) == AccountManager.GetSupplierByUser()
                                               select new Reservations
                                               {
                                                   AgencyName = AgName.AgencyName,
                                                   bookingname = obj.bookingname,
                                                   BookingStatus = obj.BookingStatus,
                                                   CurrencyCode = AgName.CurrencyCode,
                                                   IsConfirm = obj.IsConfirm,
                                                   CheckIn = obj.CheckIn,
                                                   sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                   CheckOut = obj.CheckOut,
                                                   Children = obj.Children,
                                                   City = obj.City,
                                                   HotelName = obj.HotelName,
                                                   LatitudeMGH = obj.LatitudeMGH,
                                                   LongitudeMGH = obj.LongitudeMGH,
                                                   NoOfAdults = obj.NoOfAdults,
                                                   ReservationDate = obj.ReservationDate,
                                                   ReservationID = obj.ReservationID,
                                                   Sid = obj.Sid,
                                                   Source = obj.Status,
                                                   Status = obj.Status,
                                                   TotalFare = obj.TotalFare,
                                                   TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                   Uid = Convert.ToInt64(obj.Uid),
                                                   noNights = Convert.ToInt64(obj.NoOfDays),
                                                   Deadline = objroom.CutCancellationDate
                                               }).Distinct().ToList();

                            if(arrResult.ToList().Count != 0)
                            {
                                foreach (var obj in arrResult.ToList().Distinct().ToList())
                                {
                                    ListReservation.Add(obj);
                                }
                            }

                            IQueryable<Reservations> arrResrvation = from obj in DB.tbl_CommonHotelReservations
                                                                     join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                                                     join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                                 where AgName.ParentID == Uid && obj.BookingStatus != "GroupRequest"
                                                 select new Reservations
                                                 {
                                                     AgencyName = AgName.AgencyName,
                                                     bookingname = obj.bookingname,
                                                     BookingStatus = obj.BookingStatus,
                                                     CurrencyCode = AgName.CurrencyCode,
                                                     CheckIn = obj.CheckIn,
                                                     CheckOut = obj.CheckOut,
                                                     sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                     Children = obj.Children,
                                                     City = obj.City,
                                                     IsConfirm = obj.IsConfirm,
                                                     HotelName = obj.HotelName,
                                                     LatitudeMGH = obj.LatitudeMGH,
                                                     LongitudeMGH = obj.LongitudeMGH,
                                                     NoOfAdults = obj.NoOfAdults,
                                                     ReservationDate = obj.ReservationDate,
                                                     ReservationID = obj.ReservationID,
                                                     Sid = obj.Sid,
                                                     Source = obj.Source,
                                                     Status = obj.Status,
                                                     TotalFare = obj.TotalFare,
                                                     TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                     Uid = Convert.ToInt64(obj.Uid),
                                                     noNights = Convert.ToInt64(obj.NoOfDays),
                                                       Deadline = objroom.CutCancellationDate
                                                 };

                            foreach (var obj in arrResrvation.ToList())
                            {
                                ListReservation.Add(obj);
                            }

                        }
                        ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                    }
                    else
                    {

                        var arrGroup = (from obj in DB.tbl_Comm_HotelGroups
                                        join objHotel in DB.tbl_Comm_HotelGroupMappings on obj.Group_id equals objHotel.Group_id
                                        where obj.UniqueCode == Uid.ToString()
                                        select new
                                        {
                                            HotelCode = objHotel.Hotel_Code,
                                            Supplierid = obj.SupplierId
                                        }).ToList();



                        var arrReservations = new List<Reservations>();
                        foreach (var arrobj in arrGroup)
                        {
                            arrReservations = (from obj in DB.tbl_CommonHotelReservations
                                               join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                               where obj.HotelCode == arrobj.HotelCode && obj.ParentID == arrobj.Supplierid
                                               select new Reservations
                                               {
                                                   AgencyName = AgName.AgencyName,
                                                   bookingname = obj.bookingname,
                                                   BookingStatus = obj.BookingStatus,
                                                   CurrencyCode = AgName.CurrencyCode,
                                                   IsConfirm = obj.IsConfirm,
                                                   CheckIn = obj.CheckIn,
                                                   sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                   CheckOut = obj.CheckOut,
                                                   Children = obj.Children,
                                                   City = obj.City,
                                                   HotelName = obj.HotelName,
                                                   LatitudeMGH = obj.LatitudeMGH,
                                                   LongitudeMGH = obj.LongitudeMGH,
                                                   NoOfAdults = obj.NoOfAdults,
                                                   ReservationDate = obj.ReservationDate,
                                                   ReservationID = obj.ReservationID,
                                                   Sid = obj.Sid,
                                                   Source = obj.Status,
                                                   Status = obj.Status,
                                                   TotalFare = obj.TotalFare,
                                                   TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                   Uid = Convert.ToInt64(obj.Uid),

                                               }).Distinct().ToList();
                            foreach (var obj in arrReservations)
                            {
                                ListReservation.Add(obj);
                            }
                        }
                        ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }

        public static List<Reservations> GroupBookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    ListReservation = (from obj in DB.tbl_CommonHotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where obj.Uid == Uid && obj.BookingStatus == "GroupRequest"
                                       select new Reservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Source,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)

                                       }).Distinct().ToList();
                    if (objGlobalDefault.UserType != "Agent")
                    {
                        var arrResrvation = (from obj in DB.tbl_CommonHotelReservations
                                             join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                             where AgName.ParentID == Uid && obj.BookingStatus == "GroupRequest"
                                             select new Reservations
                                             {
                                                 AgencyName = AgName.AgencyName,
                                                 bookingname = obj.bookingname,
                                                 BookingStatus = obj.BookingStatus,
                                                 CurrencyCode = AgName.CurrencyCode,
                                                 CheckIn = obj.CheckIn,
                                                 CheckOut = obj.CheckOut,
                                                 sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                 Children = obj.Children,
                                                 City = obj.City,
                                                 IsConfirm = obj.IsConfirm,
                                                 HotelName = obj.HotelName,
                                                 LatitudeMGH = obj.LatitudeMGH,
                                                 LongitudeMGH = obj.LongitudeMGH,
                                                 NoOfAdults = obj.NoOfAdults,
                                                 ReservationDate = obj.ReservationDate,
                                                 ReservationID = obj.ReservationID,
                                                 Sid = obj.Sid,
                                                 Source = obj.Source,
                                                 Status = obj.Status,
                                                 TotalFare = obj.TotalFare,
                                                 TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                 Uid = Convert.ToInt64(obj.Uid)

                                             }).ToList().Distinct();
                        foreach (var obj in arrResrvation)
                        {
                            ListReservation.Add(obj);
                        }
                    }

                    if (objGlobalDefault.UserType == "Hotel")
                    {
                        var arrGroup = (from obj in DB.tbl_Comm_HotelGroups
                                        join objHotel in DB.tbl_Comm_HotelGroupMappings on obj.Group_id equals objHotel.Group_id
                                        where obj.UniqueCode == Uid.ToString()
                                        select new
                                        {
                                            HotelCode = objHotel.Hotel_Code,
                                            Supplierid = obj.SupplierId
                                        }).ToList();



                        var arrReservations = new List<Reservations>();
                        foreach (var arrobj in arrGroup)
                        {
                            arrReservations = (from obj in DB.tbl_CommonHotelReservations
                                               join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                               where obj.HotelCode == arrobj.HotelCode && obj.ParentID == arrobj.Supplierid && obj.BookingStatus == "GroupRequest"
                                               select new Reservations
                                               {
                                                   AgencyName = AgName.AgencyName,
                                                   bookingname = obj.bookingname,
                                                   BookingStatus = obj.BookingStatus,
                                                   CurrencyCode = AgName.CurrencyCode,
                                                   IsConfirm = obj.IsConfirm,
                                                   CheckIn = obj.CheckIn,
                                                   sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                   CheckOut = obj.CheckOut,
                                                   Children = obj.Children,
                                                   City = obj.City,
                                                   HotelName = obj.HotelName,
                                                   LatitudeMGH = obj.LatitudeMGH,
                                                   LongitudeMGH = obj.LongitudeMGH,
                                                   NoOfAdults = obj.NoOfAdults,
                                                   ReservationDate = obj.ReservationDate,
                                                   ReservationID = obj.ReservationID,
                                                   Sid = obj.Sid,
                                                   Source = obj.Source,
                                                   Status = obj.Status,
                                                   TotalFare = obj.TotalFare,
                                                   TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                   Uid = Convert.ToInt64(obj.Uid)

                                               }).Distinct().ToList();
                            foreach (var obj in arrReservations)
                            {
                                ListReservation.Add(obj);
                            }
                        }

                        ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return ListReservation;
        }


        public static List<Reservations> AdminBookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                   IQueryable<Reservations> arrResult =from obj in DB.tbl_CommonHotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where AgName.UserType == "Supplier" && obj.Uid == AgName.sid && obj.Uid != 232
                                       select new Reservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Status,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)
                                       };
                    ListReservation = arrResult.ToList();
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }

        public static List<Reservations> AdminGroupBookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    ListReservation = (from obj in DB.tbl_CommonHotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where AgName.UserType == "Supplier" && obj.BookingStatus == "GroupRequest"
                                       select new Reservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Source,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)

                                       }).Distinct().ToList();
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }
    }


}