﻿using CutAdmin.EntityModal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Services
{
    public class HotelCurrency
    {
        public static List<string> GetCurrency(string HotelCode)
        {
            List<string> arrCurrency = new List<string>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    IQueryable<string> IQueryableCurrency = (from objPlan in db.tbl_RatePlan
                                                          join
                                                          obj in db.tbl_CommonHotelMaster on objPlan.HotelID equals obj.sid
                                                          where obj.sid.ToString() == HotelCode
                                                          select objPlan.sCurrency);
                    if (IQueryableCurrency.ToList().Count == 0)
                        throw new Exception("No Rate Found Found");
                    arrCurrency = IQueryableCurrency.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrCurrency;
        }
    }
}