﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using CommonLib.Response;
using CutAdmin.EntityModal;

namespace CutAdmin.DataLayer
{
    public class ReservationDetails
    {
        public string AffilateCode { get; set; }

        public string BookingStatus { get; set; }
        public tbl_CommonHotelMaster objHotelDetails { get; set; }
        public List<CommonLib.Request.Customer> LisCustumer { get; set; }
        public List<CutAdmin.BookingHandler.Addons> arrAddOns { get; set; }
        public string sCheckIn { get; set; }
        public string sCheckOut { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public DateTime ChargeDate { get; set; }
        public DateTime DeadLineDate { get; set; }
        public DateTime HoldingTime { get; set; }
        public string BookingDate { get; set; }
        public string CurrentStatus { get; set; }
        public int noNights { get; set; }


        //... Booking Room Details ..//
        public string ReservationID { get; set; }
        public List<tbl_CommonBookedRoom> sRooms { get; set; }
        public List<CommonLib.Response.RoomType> ListRoom { get; set; }
        //.. Amount & Currency Details ..//
        public string ComparedCurrency { get; set; }

        public float AdminCommission { get; set; }
        public CommonLib.Response.ServiceCharge objCancellationCharge { get; set; } /* Booking Amount  */
        public float ServiceTax { get; set; }
        public float CancelattionAmount { get; set; }
        public float Markup { get; set; }
        public float SupplierMarkup { get; set; }

        public float TotalAmount { get; set; }
        public float TotalPayable { get; set; }
        public float AddOnsCharge { get; set; }

        /*User Details*/
        public Int64 HotelId { get; set; }
        public Int64 SupplierID { get; set; }
        public Int64 PurcheserID { get; set; }
        public Int64 CUH_ID { get; set; }

        /*Booking Status*/
        public bool OnVouchered { get; set; }
        public string Remarks { get; set; }

        public string  CompanyName { get; set; }
        public string Mobile { get; set; }
        public string email { get; set; }


        /*Charges Details*/
        public RateGroup RateGroup { get; set; }
        public CommonLib.Response.ServiceCharge objCharges { get; set; } 

    }
}