﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
namespace CutAdmin.DataLayer
{
    public class pdfManager
    {
        public static void GeneratePDF(string HTML)
        {
            try
            {
                HttpContext context = System.Web.HttpContext.Current;
                string htmlContent = HTML;
                NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                pdfConverter.Size = NReco.PdfGenerator.PageSize.A4;
                pdfConverter.PdfToolPath = ConfigurationManager.AppSettings["rootPath"];
                var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                context.Response.ContentType = "application/pdf";
                context.Response.BinaryWrite(pdfBytes);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}