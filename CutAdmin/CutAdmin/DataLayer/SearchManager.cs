﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.Common;
using CutAdmin.BL;
using CommonLib.Response;
using System.Web.Script.Serialization;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class SearchManager : RatesManager
    {
        public static HttpContext context { get; set; }
        #region Hotel Details
        /// <summary>
        /// Get HotelDeatils By Supplier
        /// </summary>
        /// <param name="HotelCodes"></param>
        /// <param name="CheckIn"></param>
        /// <param name="CheckOut"></param>
        /// <param name="Nationality"></param>
        /// <param name="SearchValid"></param>
        /// <param name="City"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        public static List<CommonHotelDetails> GetHotelDetails(Int64[] HotelCodes, string CheckIn, string CheckOut, string[] Nationality, string SearchValid, string Search_Params)
        {
            List<CommonHotelDetails> objHotelDetails = new List<CommonHotelDetails>();
            List<tbl_commonRoomDetails> ListRoom = new List<tbl_commonRoomDetails>();
            try
            {
                string City = "", Country = "";
                long ParentID = AccountManager.GetSupplierByUser();
                Int64 noRooms = Convert.ToInt64(Search_Params.Split('_')[4]);
                City = Search_Params.Split('_')[1].Split(',')[0].TrimStart(' ');
                Country = Search_Params.Split('_')[1].Split(',')[1].TrimStart(' ');

                if (HotelCodes.Length == 0)
                {
                    List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier();
                    using (var DB = new ClickUrHotel_DBEntities())
                    {
                        var arrHotel = (from obj in DB.tbl_CommonHotelMaster
                                        join
   objRatePlan in DB.tbl_RatePlan on obj.sid equals objRatePlan.HotelID
                                        where obj.ParentID == ParentID &&
                                        obj.CityId.Contains(City) && obj.CountryId.Contains(Country)
                                        select new HotelInfo
                                        {
                                            HotelName = obj.HotelName,
                                            sid = obj.sid,
                                            SupplierName = "",
                                        }).ToList();
                        foreach (var objHotel in arrHotel.Distinct())
                        {
                            //if (objHotel.HotelName != "JACOBS GARDEN HOTEL")
                            //    continue;
                            List<RateGroup> ListRateGroup = GetRateGroup(objHotel.sid, CheckIn, CheckOut, Nationality, SearchValid, noRooms);
                            if (ListRateGroup.Count != 0)
                            {
                                var arrRates = ListRateGroup.Where(d => d.Charge.TotalPrice != 0).ToList();
                                if (arrRates.Count != 0)
                                    objHotelDetails.Add(GetHotelDetails(objHotel.sid, Search_Params, ListRateGroup));
                            }
                        }
                    }
                }
                else
                {
                    foreach (var HotelID in HotelCodes)
                    {
                        List<RateGroup> ListRateGroup = GetRateGroup(HotelID, CheckIn, CheckOut, Nationality, SearchValid, noRooms);
                        if (ListRateGroup.Count != 0)
                        {
                            var arrRates = ListRateGroup.Where(d => d.Charge.TotalPrice != 0).ToList();
                            if (arrRates.Count != 0)
                                objHotelDetails.Add(GetHotelDetails(HotelID, Search_Params, arrRates));
                        }
                    }
                }
                if (objHotelDetails.Count == 0)
                    throw new Exception("Hotels Not Available..");

                HotelFilter objFliter = new HotelFilter();
                objFliter.arrHotels = objHotelDetails;
                objFliter.arrSupplier = objHotelDetails.Select(d => d.Supplier).Distinct().ToList();
                objFliter.SearchParams = Search_Params;
                objFliter.GenrateHotelFilter();
                HttpContext.Current.Session["ModelHotel" + Search_Params] = objFliter;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return objHotelDetails;
        }

        public static HotelFilter GetSearchHotel(string Search_Params)
        {
            HotelFilter objHotel = new HotelFilter();
            try
            {
                if (HttpContext.Current.Session["ModelHotel" + Search_Params] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search_Params];
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return objHotel;
        }

        public static string Availbility(string Serach, string RoomID, string RoomDescID, int RoomNo, Int64 HotelCode, string Supplier)
        {
            JavaScriptSerializer objSerilize = new JavaScriptSerializer();
            try
            {
                HotelFilter objHotel = new HotelFilter();
                if (HttpContext.Current.Session["ModelHotel" + Serach] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Serach];
                CommonHotelDetails arrHotelDetails = objHotel.arrHotels.Where(d => d.HotelId == HotelCode.ToString()).FirstOrDefault();
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo, HotelCode.ToString(), Supplier);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), HotelCode);
                return objSerilize.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {

                return objSerilize.Serialize(new object { });
                ;
            }
        }
        #endregion

        #region ValidRates
        /// <summary>
        /// Validate Hotel have Ocuupancy &  Channels
        /// </summary>
        /// <returns></returns>
        public static List<RateGroup> GetRateGroup(Int64 HotelCode, string Checkin, string Checkout, string[] Nationality, string SearchValid, Int64 noRooms)
        {
            List<RateGroup> RateList = new List<RateGroup>();
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    var HotelDetails = (from obj in DB.tbl_CommonHotelMaster where obj.sid == HotelCode select obj).FirstOrDefault();
                    var ListRoom = (from Room in DB.tbl_commonRoomDetails
                                    join
objRatePlan in DB.tbl_RatePlan on Room.RoomTypeId equals objRatePlan.RoomTypeID
                                    where Room.HotelId == HotelCode
                                    select Room).Distinct().ToList();
                    RatesManager.context = context;
                    if (ListRoom.Count != 0)
                    {
                        RateList = GetRateList(HotelCode, Checkin, Checkout, Nationality, ListRoom, SearchValid, noRooms);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return RateList;
        }


        #endregion

        #region Get Hotel Details
        public static CommonHotelDetails GetHotelDetails(Int64 HotelCode, string Search_Params, List<RateGroup> ListRate)
        {
            var arrHotel = new CommonLib.Response.CommonHotelDetails();
            try
            {
                CutAdmin.DataLayer.GlobalDefault objGlobalDeafult = (CutAdmin.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                #region AvailRates
                var nDB = new ClickUrHotel_DBEntities();
                string[] arrSearchAttr = Search_Params.Split('_');
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    var arrHotelInfo = (from obj in DB.tbl_CommonHotelMaster
                                            //where obj.ParentID == AccountManager.GetUserByLogin()

                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.sid,
                                            obj.LocationId,
                                            obj.HotelName,
                                            obj.HotelLatitude,
                                            obj.HotelLangitude,
                                            obj.HotelAddress,
                                            obj.HotelZipCode,
                                            obj.HotelImage,
                                            obj.SubImages,
                                            obj.HotelCategory,
                                            obj.HotelDescription,
                                            obj.HotelFacilities,
                                            Supplier = "",
                                            LocationName = "",
                                        }).FirstOrDefault();
                    string Image = arrHotelInfo.HotelImage;
                    string URL =  System.Configuration.ConfigurationManager.AppSettings["URL"];
                    List<Image> arrImage = new List<Image>();
                    if (Image != null)
                    {
                        List<string> Url = Image.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    if (arrHotelInfo.SubImages != null)
                    {
                        List<string> Url = arrHotelInfo.SubImages.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    List<string> sFacilities = arrHotelInfo.HotelFacilities.Split(',').ToList();
                    if (sFacilities.Count != 0)
                    {
                        sFacilities = (from obj in DB.tbl_commonFacility
                                       where sFacilities.Contains(obj.HotelFacilityID.ToString())
                                       select obj.HotelFacilityName).ToList();
                    }
                    List<Location> arrLocation = new List<Location>();
                    string Location = "";
                    var arrLocationName = nDB.tbl_Location.Where(d => d.Lid == arrHotelInfo.LocationId).FirstOrDefault();
                    if (arrLocationName != null)
                        Location = nDB.tbl_Location.Where(d => d.Lid == arrHotelInfo.LocationId).FirstOrDefault().LocationName;
                    if (Location != null)
                        arrLocation.Add(new Location { Name = Location, HotelCode = arrHotelInfo.sid.ToString() });
                    arrHotel = new CommonHotelDetails
                    {
                        DateFrom = arrSearchAttr[2],
                        DateTo = arrSearchAttr[3],
                        HotelId = arrHotelInfo.sid.ToString(),
                        Description = arrHotelInfo.HotelDescription,
                        HotelName = arrHotelInfo.HotelName,
                        Category = arrHotelInfo.HotelCategory,
                        Address = arrHotelInfo.HotelAddress,
                        Image = arrImage,
                        RateList = ListRate,
                        List_RateGroup = ListRate,
                        Supplier = arrHotelInfo.Supplier,
                        Langitude = arrHotelInfo.HotelLangitude,
                        Latitude = arrHotelInfo.HotelLatitude,
                        Facility = CutAdmin.Common.ParseCommonResponse.GetCommonFacilities(sFacilities),
                        Location = arrLocation,
                        Currency = objGlobalDeafult.Currency,
                        Total = ListRate.Select(d => d.Charge.TotalPrice).FirstOrDefault(),
                    };
                }
                if (ListRate.Count != 0)
                {
                    arrHotel.Charge = new ServiceCharge();
                    arrHotel.MinPrice = ListRate.Select(d => d.Charge.TotalPrice).ToList().Min();
                    arrHotel.MaxPrice = ListRate.Select(d => d.Charge.TotalPrice).ToList().Max();
                    arrHotel.Charge.TotalPrice = Convert.ToSingle(arrHotel.MinPrice);
                }
                #endregion
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrHotel;
        }
        #endregion

        #region Hotel Filter
        public static HotelFilter Filters(CutAdmin.Common.HotelFilter.Filter arrFilter, string Search_Params)
        {
            HotelFilter objHotel = new HotelFilter();
            try
            {
                if (HttpContext.Current.Session["ModelHotel" + Search_Params] == null)
                    throw new Exception("No Hotels Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search_Params];
                List<CommonHotelDetails> arrHotels = objHotel.arrHotels;
                if (arrFilter.Category != null && arrFilter.Category.Count != 0)
                {
                    arrHotels = HotelFilter.Search(arrHotels, arrFilter.Category);
                }
                if (arrFilter.arrLocation != null && arrFilter.arrLocation.Count != 0)
                {
                    arrHotels = HotelFilter.Search(arrHotels, arrFilter.arrLocation);
                }
                if (arrFilter.arrFacility != null && arrFilter.arrFacility.Count != 0)
                {
                    arrHotels = HotelFilter.Search(arrHotels, arrFilter.arrLocation);
                }
                if (arrFilter.HotelName != null && arrFilter.HotelName != "")
                {
                    arrHotels = arrHotels.Where(d => d.HotelName.Contains(arrFilter.HotelName)).ToList();
                }

                if (arrFilter.MinPrice != 0 && arrFilter.MaxPrice != 0)
                {
                    arrHotels = arrHotels.Where(d => d.Charge.TotalPrice >= arrFilter.MinPrice && d.Charge.TotalPrice <= arrFilter.MinPrice).ToList();
                }
                if (arrHotels.Count == 0)
                    throw new Exception("No Hotels found.");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return objHotel;
        }
        #endregion

        #region GetRate GroupBy Search
        public static List<RateGroup> GetRateGroup(string HotelCode, string Search_Params)
        {
            List<RateGroup> arrRateGroup = new List<RateGroup>();
            try
            {
                CommonHotelDetails arrHotel = GetHotelDetailsBySearch(Search_Params).Where(d => d.HotelId == HotelCode).FirstOrDefault();
                foreach (var objRateGroup in arrHotel.RateList)
                {
                    foreach (var objOccupancy in objRateGroup.RoomOccupancy)
                    {
                        foreach (var objRoom in objOccupancy.Rooms)
                        {
                            foreach (var objDate in objRoom.dates)
                            {
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(HotelCode, Convert.ToInt64(objDate.RateTypeId), objRoom.RoomTypeId, objDate.datetime, Convert.ToInt64(objRateGroup.Name));
                            }
                            objRoom.OnRequest = objRoom.dates.Any(d => d.NoOfCount == 0);
                        }
                    }
                }
                arrRateGroup = arrHotel.RateList;

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return arrRateGroup;
        }
        #endregion

        #region Get Search Hotels
        public static List<CommonHotelDetails> GetHotelDetailsBySearch(string Search_Params)
        {
            List<CommonHotelDetails> arrHotel = new List<CommonHotelDetails>();
            try
            {
                if (HttpContext.Current.Session["ModelHotel" + Search_Params] == null)
                    throw new Exception("Your search expire , kindly re-search.");
                HotelFilter objFliter = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search_Params];
                arrHotel = objFliter.arrHotels;

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return arrHotel;
        }
        #endregion
    }
}