﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using CutAdmin.Common;
using CutAdmin.BL;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class GenralManager
    {
        public static CommonHotelDetails GetHotelInfo(string Search)
        {
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return objHotelInfo;
        }

        public static RateGroup GetRateInfo(string Search,out string HotelID)
        {
            RateGroup objRates = new RateGroup();
            HotelID = "";
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                HotelID = objHotelInfo.HotelId; 
                objRates = objHotelInfo.RateList[0];
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return objRates;
        }

        public static RateGroup GetRateInfo(string Search,string Supplier,string HotelID)
        {
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                HotelFilter objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search];
                objHotelInfo = objHotel.arrHotels.Where(d => d.HotelId == HotelID.ToString()).FirstOrDefault();
                objRates = objHotelInfo.RateList.Where(d=>d.Name==Supplier).FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return objRates;
        }

        public static List<HotelOccupancy> GetOccupancyInfo(string Search)
        {
            List<HotelOccupancy> arrOccupancy = new List<HotelOccupancy>();
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                arrOccupancy = objHotelInfo.RateList[0].RoomOccupancy;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return arrOccupancy;
        }

        #region Rate Type
        public static List<tbl_RateType> GetRateType(string HotelAdmin)
        {
            List<tbl_RateType> arrRateType = new List<tbl_RateType>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    IQueryable<tbl_RateType> arrData = (from obj in db.tbl_RateType where obj.ParentID.ToString() == HotelAdmin select obj);
                    if (arrData.ToList().Count == 0)
                        throw new Exception("No Ratetypes.");
                    arrRateType = arrData.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }

        public static void SaveRateType(tbl_RateType arrRateType)
        {
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    if (db.tbl_RateType.Where(d => d.RateType == arrRateType.RateType && d.ParentID == arrRateType.ParentID).FirstOrDefault() != null)
                    {
                        var arrType = db.tbl_RateType.Where(d => d.RateType == arrRateType.RateType && d.ParentID == arrRateType.ParentID).FirstOrDefault();
                        arrType.InventoryType = arrRateType.InventoryType.TrimEnd(',');
                    }
                    else
                        db.tbl_RateType.Add(arrRateType);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
        }



        #endregion

        #region Meals Details

        public static List<Comm_AddOn> GetMeals()
        {
            Int64 Uid = AccountManager.GetSupplierByUser();
            List<CutAdmin.BL.Comm_AddOn> arrMeals = new List<Comm_AddOn>();
            try
            {
                using (var db_Hotel = new DBHandlerDataContext())
                {
                    arrMeals = (from obj in db_Hotel.Comm_AddOns
                                where obj.UserId == Uid
                                select obj).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrMeals;
        }

        public static object GetMealPlans()
        {
            object arrMealPlan = new List<object>();
            try
            {
                Int64 Uid = AccountManager.GetSupplierByUser();
                using (var db_Hotel = new DBHandlerDataContext())
                {
                    arrMealPlan = (from obj in db_Hotel.tbl_MealPlans
                                   where obj.ParentID == Uid
                                   select new
                                   {
                                       obj.ID,
                                       obj.Meal_Plan,
                                       arrMealID = (from objMealMap in db_Hotel.tbl_MealPlanMappings
                                                    where objMealMap.PlanID == obj.ID
                                                    select objMealMap.MealID).ToList()
                                   }).ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                //throw new Exception(ex.Message);
            }
            return arrMealPlan;
        }

        public static void SaveMealPlan(tbl_MealPlan arrMealPlan, List<tbl_MealPlanMapping> arrMeals)
        {
            try
            {
                using (var db_Hotel = new DBHandlerDataContext())
                {
                    if (arrMealPlan.ID == 0)
                    {
                        arrMealPlan.ParentID = AccountManager.GetSupplierByUser();
                        db_Hotel.tbl_MealPlans.InsertOnSubmit(arrMealPlan);
                        db_Hotel.SubmitChanges();
                        arrMeals.ForEach(d => d.PlanID = arrMealPlan.ID);
                        db_Hotel.tbl_MealPlanMappings.InsertAllOnSubmit(arrMeals);
                        db_Hotel.SubmitChanges();
                    }
                    else
                    {
                        var arrPlans = db_Hotel.tbl_MealPlans.Where(d => d.ID == arrMealPlan.ID).FirstOrDefault();
                        arrPlans.Meal_Plan = arrMealPlan.Meal_Plan;
                        var arrMealsMaped = db_Hotel.tbl_MealPlanMappings.Where(d => d.PlanID == arrMealPlan.ID).ToList();
                        db_Hotel.tbl_MealPlanMappings.DeleteAllOnSubmit(arrMealsMaped);
                        db_Hotel.tbl_MealPlanMappings.InsertAllOnSubmit(arrMeals);
                        db_Hotel.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
        }

        public static void DeletePlan(Int64 PlanID)
        {
            try
            {
                using (var db_Hotel = new DBHandlerDataContext())
                {
                    var arrPlan = db_Hotel.tbl_MealPlans.Where(d => d.ID == PlanID).FirstOrDefault();
                    db_Hotel.tbl_MealPlans.DeleteOnSubmit(arrPlan);
                    db_Hotel.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion
    }
}