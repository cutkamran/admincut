﻿using CutAdmin.EntityModal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class LocationManager
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static void SaveLocation(tbl_Location arrLocation, tbl_MappedArea arrCity)
        {
            try
            {
                string Code = "";
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var arrData = db.tbl_MappedArea.Where(d => d.Name == arrCity.Name &&
                                  d.Placeid == arrCity.Placeid).FirstOrDefault();
                    if (arrData == null)
                    {
                        Code = RandomString(3);
                        while (db.tbl_MappedArea.Any(d => d.Destination_ID == Code))
                        {
                            Code = RandomString(3);
                        }
                        arrCity.Destination_ID = Code;
                        db.tbl_MappedArea.Add(arrCity);
                        db.SaveChanges();
                    }
                    else
                    {
                        if (arrData.Destination_ID == null)
                        {
                            Code = RandomString(3);
                            while (db.tbl_MappedArea.Any(d => d.Destination_ID == Code))
                            {
                                Code = RandomString(3);
                            }
                            arrData.Destination_ID = Code;
                        }
                        arrCity.Destination_ID = arrData.Destination_ID;
                    }


                    arrLocation.CityCode = arrCity.Destination_ID.ToString();
                    var arrLocat = db.tbl_Location.Where(d => d.LocationName == arrLocation.LocationName &&
                                   d.Placeid == arrLocation.Placeid).FirstOrDefault();
                    if (arrLocat == null)
                        db.tbl_Location.Add(arrLocation);
                    else
                        arrLocation.Lid = arrLocat.Lid;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static List<tbl_Location> GetLocation()
        {
            List<tbl_Location> arrLocation = new List<tbl_Location>();
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    arrLocation = DB.tbl_Location.GroupBy(x => x.LocationName).
                        Select(x => x.FirstOrDefault()).ToList().
                        OrderBy(d => d.LocationName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrLocation;
        }

        public static void Delete(long ID)
        {
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    tbl_Location arrData = DB.tbl_Location.Where(D => D.Lid == ID).FirstOrDefault();
                    if (arrData == null)
                        throw new Exception("Not valid Location");
                    DB.tbl_Location.Remove(arrData);
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        /*Hotel Location Manager*/
        public static object LoadCityList()
        {
            var sList = new List<tbl_MappedArea>();
            List<object> arrLocation = new List<object>();
            try
            {
              //  var DB = new helperDataContext();
                using (var db = new ClickUrHotel_DBEntities())
                {
                    sList = (from obj in db.tbl_MappedArea select obj).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return sList;
        }

        /* City Mapping*/
        public static void SaveMapCity(tbl_MappedArea arrCity)
        {
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var arrData = (from obj in db.tbl_MappedArea where obj.Name == arrCity.Name && obj.Placeid == arrCity.Placeid select obj).FirstOrDefault();
                    if (arrData != null)
                    {
                        arrCity.Sid = arrData.Sid;
                        db.Entry(arrData).CurrentValues.SetValues(arrCity);
                    }
                    else
                    {
                        if (db.tbl_MappedArea.Any(d => d.Destination_ID == arrCity.Destination_ID))
                            throw new Exception("City Code already exist ,Please select Other Code");
                        else if (arrCity.Destination_ID == "")
                        {
                            arrCity.Destination_ID = RandomString(3);
                            while (db.tbl_MappedArea.Any(d => d.Destination_ID == arrCity.Destination_ID))
                            {
                                arrCity.Destination_ID = RandomString(3);
                            }
                        }
                        db.tbl_MappedArea.Add(arrCity);
                    }
                    db.SaveChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}