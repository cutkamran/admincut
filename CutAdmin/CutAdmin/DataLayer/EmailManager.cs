﻿using CutAdmin.BL;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CutAdmin.EntityModal;
using CutAdmin.Services;

namespace CutAdmin.DataLayer
{
    public class EmailManager
    {

        #region  Old Work

        public static DBHelper.DBReturnCode SendInvoice(string sEmail, string ReservationId, string UID, string Night)
        {


            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            if (objGlobalDefaults.UserType == "SupplierStaff")
            {
                Uid = Convert.ToInt64(UID);
            }

            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            bool reponse = true;
            string Mail = InvoiceManager.GetInvoice(ReservationId, Uid, out invoice);
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sEmail, "");
                reponse = MailManager.SendMail(accessKey, Email1List, "Your Hotel Booking Invoice", Mail, from, attachmentList);

                return DBHelper.DBReturnCode.SUCCESS;

            }
            catch
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static DBHelper.DBReturnCode SendCommissionInvoice(string sEmail, string ReservationId, string UID, string Night)
        {


            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            using (var dbTax = new dbTaxHandlerDataContext())
            {
                var dtInvoiceDetails = (from obj in dbTax.tbl_Invoices where obj.InvoiceNo == ReservationId && obj.InvoiceType == "Hotel Commission" select obj).FirstOrDefault();

                Int64 CycleId = Convert.ToInt64(dtInvoiceDetails.CycleId);
                string invoice = "";
                bool reponse = true;
                string Mail = InvoiceManager.GetCommissionInvoice(ReservationId, Uid, CycleId, out invoice);

                try
                {
                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sEmail, "");
                    reponse = MailManager.SendMail(accessKey, Email1List, "Your Commission Invoice", Mail, from, attachmentList);

                    return DBHelper.DBReturnCode.SUCCESS;
                }
                catch
                {
                    return DBHelper.DBReturnCode.EXCEPTION;
                }
            }
        }

        public static DBHelper.DBReturnCode SendVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp, string Status)
        {
            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            if (objGlobalDefaults.UserType == "SupplierStaff")
            {
                Uid = Convert.ToInt64(UID);
            }
            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            bool reponse = true;

            string Mail = VoucherManager.GenrateVoucher(ReservationId, Uid.ToString(), Status);
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sEmail, "");
                reponse = MailManager.SendMail(accessKey, Email1List, "Your Hotel Booking Voucher", Mail, from, attachmentList);
                if (reponse)
                {
                    return DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    return DBHelper.DBReturnCode.EXCEPTION;
                }

            }
            catch
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static void GenrateAttachment(string sFormat, string Name, string Type)
        {

            string rootPath = System.Configuration.ConfigurationManager.AppSettings["rootPath"];
            HttpContext context = System.Web.HttpContext.Current;
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
            pdfConverter.PdfToolPath = rootPath + "Agent";
            var pdfBytes = pdfConverter.GeneratePdf(sFormat);
            MemoryStream ms = new MemoryStream(pdfBytes);
            //write to file
            string FilePath = HttpContext.Current.Server.MapPath("~/InvoicePdf/") + Name + "_" + Type + ".pdf";
            FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
            ms.WriteTo(file);
            file.Close();
            ms.Close();
        }

       


  

       
        public static string BookingMail2Hotel(string ReservationID, string Uid, string Status)
        {
            //DBHandlerDataContext db = new DBHandlerDataContext();
            string shtml = "";
            Common.Common.BookingID = ReservationID;
            var arrResrvation = Common.Common.arrBookingDetails;
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    using (var DB = new DBHandlerDataContext())
                    {
                        bool reponse = true;
                        try
                        {
                            var arrPaxes = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                            var arrRooms = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                            var AdminUser = Services.User._SupplierByAgent(Convert.ToInt64(Uid));
                            string RoomDetails = "";
                            #region Room Details
                            long AdminID = AccountManager.GetSupplierByUser();
                            ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
                            string HotelCode = arrResrvation.HotelCode;
                            var dtHotelAdd = (from obj in cdb.tbl_CommonHotelMaster where obj.sid.ToString() == HotelCode select obj).FirstOrDefault();
                            string RoomName = "", Canceldate = "", SrNo = "";
                            foreach (var arrRoom in arrRooms)
                            {
                                string PaxName = "";
                                long Adult = arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "AD").ToList().Count;
                                long Child = arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "CH").ToList().Count;
                                string ChildAge = "-";
                                if (Child != 0)
                                    foreach (var arrPax in arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber && d.PassengerType == "CH").ToList())
                                    {
                                        ChildAge += arrPax.Age + ",";
                                    }
                                foreach (var arrPax in arrPaxes.Where(d => d.RoomNumber == arrRoom.RoomNumber).ToList())
                                    PaxName += arrPax.Name + " ";
                                TemplateGenrator.GetTemplatePath(Convert.ToInt64(AdminID), "Hotel Request Room");
                                TemplateGenrator.arrParms = new Dictionary<string, string>
                             {
                              {"No",(arrRooms.IndexOf(arrRoom) + 1).ToString()},
                              {"roomname",arrRoom.RoomType },
                              {"mealplan",arrRoom.BoardText},
                              {"Paxname",PaxName},
                              {"adult",Adult.ToString()},
                              {"child",Child.ToString()},
                              {"ages",ChildAge.TrimStart('-') },
                              {"remark",arrRoom.Remark},
                            };
                                if (arrRoom.EssentialInfo != "")
                                    TemplateGenrator.arrParms.Add("Free Addons: addons", "<b>Free Addons: <b>" + arrRoom.EssentialInfo);
                                else
                                    TemplateGenrator.arrParms.Add("Free Addons: addons", "");
                                RoomDetails += TemplateGenrator.GetTemplate;
                            }
                            #endregion

                            #region Genral Details
                            TemplateGenrator.GetTemplatePath(Convert.ToInt64(AdminID), "Hotel Request");
                            TemplateGenrator.arrParms = new Dictionary<string, string>
                        {
                              {"CompanyName",AdminUser.Name},
                              {"address",AdminUser.Address},
                              {"city",AdminUser.City},
                              {"mobile",AdminUser.Mobile},
                              {"phone",AdminUser.phone},
                              {"supportMail",AdminUser.email},
                              {"Country",AdminUser.Countryname},
                              /*Hotel Info*/
                              {"PropertyName",arrResrvation.HotelName},
                              {"PaxName",arrResrvation.Passenger},
                              {"location",arrResrvation.City},
                              {"from",arrResrvation.checkin},
                              {"to",arrResrvation.checkout},
                             
                              {"RoomDetails",RoomDetails},
                              {"remarks",arrResrvation.Remark},
                              { "http://img.mailinblue.com/2096904/images/rnb/original/5ce91ecece057438f265bb01.jpg", "https://clickurhotel.com/Agencylogo/"+  AdminUser.Code + ".jpg"}  ,
                        };
                            if (arrResrvation.HotelConfirm == "")
                                TemplateGenrator.arrParms.Add("Conf_irm", "#");
                            else
                                TemplateGenrator.arrParms.Add("Conf_irm", arrResrvation.HotelConfirm);
                            shtml = TemplateGenrator.GetTemplate;
                            #endregion
                            var dtHotelContact = (from obj in db.Comm_HotelContacts where obj.HotelCode.ToString() == arrResrvation.HotelCode && obj.SupplierCode == AdminID select obj).FirstOrDefault();
                            List<string> from = new List<string>();
                            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                            List<string> attachmentList = new List<string>();
                            Dictionary<string, string> Email1List = new Dictionary<string, string>();
                            string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                            if (dtHotelContact != null)
                                Email1List.Add(Convert.ToString(dtHotelContact.email), "");
                            // Email1List.Add("desk@clickurhotel.com", "");
                            reponse = MailManager.SendMail(accessKey, Email1List, "Booking Request", shtml, from, attachmentList);
                        }
                        catch
                        {

                        }
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public static string GetErrorMessage(string Type)
        {
            string MMessage = "";
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = 0;
                if (objGlobalDefault.UserType == "AdminStaff")
                {
                    Uid = objGlobalDefault.ParentId;
                }
                else
                {
                    if (objGlobalDefault.UserType != "Supplier")
                        Uid = objGlobalDefault.ParentId;
                    else
                        Uid = objGlobalDefault.sid;
                }
                //DBHandlerDataContext db = new DBHandlerDataContext();

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                using (var db = new DBHandlerDataContext())
                {
                    var Message = (from obj in db.tbl_ActivityMails where obj.Activity == Type && obj.ParentID == Uid select obj.ErroMessage).FirstOrDefault();

                    MMessage = Convert.ToString(Message);
                }
                return MMessage;
            }
            catch
            {
                return MMessage;
            }

            // jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, ErrorMessage = ErrorMessage });
        }

        public static bool CancelHoldBooking(string ReservationID, Int64 ParentID, string Supplier, string AffiliateCode)
        {
            //AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
            //DBHandlerDataContext db = new DBHandlerDataContext();

            // var Data = new List<tbl_HotelReservation>();
            //var AdminData=new List<CutAdmin.HotelAdmin.tbl_AdminLogin>();
            using (var DB = new AdminDBHandlerDataContext())
            {
                using (var db = new DBHandlerDataContext())
                {
                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();
                    var AdminData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();
                    var Data = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();

                    //string ContactPerson = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj.ContactPerson).ToString();
                    //string CurrencyCode = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj.CurrencyCode).ToString();
                    //string InvoiceID = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.InvoiceID).ToString();
                    //string Status = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.Status).ToString();
                    //string ReservationDate = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.ReservationDate).ToString();
                    //string HotelName = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.HotelName).ToString();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("'<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>   ");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>    ");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto\">  ");
                    sb.Append("<div style=\"margin-left:9%;height:50px;width:auto\">");
                    sb.Append("<br />");
                    sb.Append(" <img src=\"https://www.clickurtrip.co.in/images/logosmal.png\"  style=\"padding-left:10px;\" />");
                    sb.Append("</div>");
                    sb.Append("<div style=\"margin-left:10%\">");
                    sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Dear " + AdminData.ContactPerson + ",</span><br /></p>");
                    sb.Append(" <p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p>");
                    sb.Append("<style type=\"text/css\">");
                    sb.Append(".tg  {border-collapse:collapse;border-spacing:0;} ");
                    sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
                    sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;} ");
                    sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                    sb.Append(".tg .tg-yw4l{vertical-align:top}");
                    sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>");
                    sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                    sb.Append(" <tr>");
                    sb.Append("    <td class=\"tg-9hbo\"><b>InvoiceID</b></td>");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.InvoiceID + "</td> ");
                    sb.Append(" </tr> ");
                    sb.Append("<tr> ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Status</b> </td> ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.Status + "</td>");
                    sb.Append(" </tr>   ");
                    sb.Append("<tr>  ");
                    sb.Append("   <td class=\"tg-9hbo\"><b>Reservation-ID</b></td>");
                    sb.Append("  <td class=\"tg-yw4l\">: " + ReservationID + "</td> ");
                    sb.Append("</tr>  ");
                    sb.Append(" <tr>   ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>BookingDate</b>     </td> ");
                    sb.Append(" <td class=\"tg-yw4l\">:  " + Data.ReservationDate + "</td>");
                    sb.Append("</tr>   ");
                    sb.Append(" <tr>    ");
                    sb.Append(" <td class=\"tg-9hbo\"><b>Hotel Name</b>        </td>  ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.HotelName + "</td>    ");
                    sb.Append(" </tr>    ");
                    sb.Append(" <tr>    ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Check-In</b>            </td>   ");
                    sb.Append("   <td class=\"tg-yw4l\">:  " + Data.CheckIn + "</td>   ");
                    sb.Append("</tr>    ");
                    sb.Append("<tr>    ");
                    sb.Append(" <td class=\"tg-9hbo\"><b>Check-Out </b>        </td>    ");
                    sb.Append(" <td class=\"tg-yw4l\">:  " + Data.CheckOut + "</td>");
                    sb.Append(" </tr>    ");
                    sb.Append(" <tr>    ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Total  </b>                </td>  ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + AdminData.CurrencyCode + " " + Data.TotalFare + "</td>  ");
                    sb.Append("</tr>    ");
                    sb.Append("</table></div>    ");
                    sb.Append(" <p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>  ");
                    sb.Append("  <span style=\"margin-left:2%\"> ");
                    sb.Append(" <b>Thank You,</b><br />    ");
                    sb.Append(" </span>    ");
                    sb.Append(" <span style=\"margin-left:2%\">    ");
                    sb.Append(" Administrator    ");
                    sb.Append(" </span>");
                    sb.Append(" </div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("   <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("       <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + arrSupplierDetails.website + "</div></td>");
                    sb.Append("   </tr>");
                    sb.Append("  </table>");
                    sb.Append("  </div> ");
                    sb.Append("  </body>");
                    sb.Append(" </html>' ");

                    try
                    {


                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                        List<string> DocLinksList = new List<string>();
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        string CcTeamMail = "";
                        var sMail = new tbl_ActivityMail();
                        sMail = (from obj in db.tbl_ActivityMails where obj.Activity.Contains("Hold Cancel Confirmation") && obj.ParentID == 232 select obj).FirstOrDefault();

                        if (sMail != null)
                        {
                            //BCcTeamMails = sMail.Email;
                            CcTeamMail = sMail.CcMail;
                        }
                        foreach (string mail in AdminData.uid.Split(','))
                        {
                            if (mail != "")
                            {
                                Email1List.Add(mail, mail);
                            }
                        }
                        Dictionary<string, string> BccList = new Dictionary<string, string>();
                        foreach (string mail in CcTeamMail.Split(';'))
                        {
                            if (mail != "")
                            {
                                BccList.Add(mail, mail);
                            }
                        }
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                        bool reponse = MailManager.SendMail(accessKey, Email1List, BccList, "Hold Cancel Confirmation", sb.ToString(), from, DocLinksList);

                        return reponse;


                    }
                    catch
                    {
                        return false;
                    }
                }
            }
        }

        public static string GenrateCancellationMail(string ReservationID, Int64 UID, string User)
        {
            //DBHandlerDataContext db = new DBHandlerDataContext();
            //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
            using (var db = new DBHandlerDataContext())
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    StringBuilder sMail = new StringBuilder();
                    string Url = ConfigurationManager.AppSettings["URL"];
                    string Logo = HttpContext.Current.Session["logo"].ToString();
                    var arrReservation = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                    if (arrReservation != null)
                    {
                        var arrUserDetails = (from obj in db.tbl_AdminLogins where obj.sid == UID select obj).FirstOrDefault();
                        var arrBookingTrasaction = (from obj in dbTax.tbl_BookingTransactions where obj.ReservationID == ReservationID && obj.uid == UID select obj).FirstOrDefault();
                        sMail.Append("<!DOCTYPE html>  <html lang=\"en\"  xmlns=\"http://www.w3.org/1999/xhtml\"> <head>  ");
                        sMail.Append("<meta charset=\"utf-8\" /> </head>");
                        sMail.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
                        sMail.Append("<div style=\"height:50px;width:auto\"><br /> ");

                        sMail.Append("<img src=\"" + Url + "AgencyLogo/" + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />  ");
                        sMail.Append("</div>   ");
                        sMail.Append("<div style=\"margin-left:10%\">  ");
                        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hi " + arrUserDetails.AgencyName + ",</span><br /></p> ");
                        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p> ");
                        sMail.Append("<style type=\"text/css\">  ");
                        sMail.Append(".tg  {border-collapse:collapse;border-spacing:0;}   ");
                        sMail.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                        sMail.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                        sMail.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}  ");
                        sMail.Append(".tg .tg-yw4l{vertical-align:top}   ");
                        sMail.Append(" @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>   ");
                        sMail.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                        sMail.Append(" <td class=\"tg-yw4l\">:  " + ReservationID + "</td></tr>");
                        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Reservation Status</b></td>    ");
                        sMail.Append("<td class=\"tg-yw4l\">:" + arrReservation.BookingStatus + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Reservation Date</b></td>       ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.ReservationDate + "</td></tr>");
                        sMail.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>                 ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.HotelName + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Location</b></td>               ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.City + "</td></tr>                 ");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-In</b></td>               ");
                        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckIn + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-Out </b> </td>            ");
                        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckOut + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Rate</b></td> ");
                        sMail.Append("<td class=\"tg-yw4l\">:" + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt / arrReservation.NoOfDays) + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Total  </b></td>");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt) + "</td></tr>");
                        sMail.Append("</table></div>");
                        sMail.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
                        sMail.Append("<span style=\"margin-left:2%\">");
                        sMail.Append("<b>Thank You,</b><br />                                                                                                                        ");
                        sMail.Append("</span>");
                        sMail.Append("<span style=\"margin-left:2%\">");
                        sMail.Append("Administrator");
                        sMail.Append("</span>");
                        sMail.Append("</div>");
                        sMail.Append("<div>");
                        sMail.Append(" <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                        sMail.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                        //if (User == "FR")
                        //     sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  ClickurTrip.com</div></td>");
                        //else
                        sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + ConfigurationManager.AppSettings["DomainUser"] + "</div></td>");
                        sMail.Append("</tr> </table></div>");
                        sMail.Append("</body></html>");


                        var arrEmails = new tbl_ActivityMail();
                        if (User == "SupplierStaff")
                            arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == arrUserDetails.ParentID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();
                        else
                            arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == UID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();

                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                        List<string> from = new List<string>();
                        List<string> DocPathList = new List<string>();
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Dictionary<string, string> BccList = new Dictionary<string, string>();
                        if (arrEmails.Email != "")
                            from.Add(Convert.ToString(arrEmails.Email));
                        else
                            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                        foreach (var Mails in arrEmails.CcMail.Split(','))
                        {
                            if (Mails != "")
                            {
                                BccList.Add(Mails, Mails); ;
                            }
                        }
                        if (User != "AG" && BccList.Count == 0)
                        {
                            var arrAdmin = (from obj in db.tbl_AdminLogins where obj.sid == arrUserDetails.ParentID select obj).First();
                            BccList.Add(arrAdmin.AgencyName, arrAdmin.uid);
                        }

                        Email1List.Add(arrUserDetails.AgencyName, arrUserDetails.uid);
                        string title = "Cancel Confirmation : " + ReservationID;
                        MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
                    }
                    return sMail.ToString();
                }
            }
        }
        public static bool SendEmail(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + sName + ",</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Username is : <b>" + sEmail + "</b> and Password is : <b>" + sMessage + "</b></span><br />");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            //sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            sb.ToString();


            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                //  bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Password Detail", sb.ToString(), from, DocLinksList);
                bool reponse = MailManager.SendForgetMail(accessKey, Email1List, "Your Password Detail", sb.ToString(), from, DocLinksList);

                return reponse;
            }
            catch
            {
                return false;
            }

            //try
            //{

            //    int rowsAffected = 0;
            //    SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
            //    sqlParamsGrpName[0] = new SqlParameter("@sTo", sTo);
            //    sqlParamsGrpName[1] = new SqlParameter("@sSubject", "Your Password Detail");
            //    sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
            //    DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_RegistrationMail", out rowsAffected, sqlParamsGrpName);
            //    //string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
            //    //sMailMessage = sMailMessage.Replace("#name#", sName);
            //    //sMailMessage = sMailMessage.Replace("#CompanyName#", sCompanyName);
            //    //sMailMessage = sMailMessage.Replace("#CompanyAddress#", sCompanyAddress);
            //    //sMailMessage = sMailMessage.Replace("#City#", sCity);
            //    //sMailMessage = sMailMessage.Replace("#State#", sState);
            //    //sMailMessage = sMailMessage.Replace("#Desgn#", sDesgn);
            //    //sMailMessage = sMailMessage.Replace("#email#", sEmail);
            //    //sMailMessage = sMailMessage.Replace("#phone#", sPhone);
            //    //sMailMessage = sMailMessage.Replace("#selectoption#", sSelectProducts);
            //    //sMailMessage = sMailMessage.Replace("#text#", sMessage);
            //    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
            //    //mailMsg.From = new MailAddress("support@royalsheets.asia");
            //    //mailMsg.To.Add(sTo);
            //    //mailMsg.Subject = sSubject;
            //    //mailMsg.IsBodyHtml = true;
            //    //mailMsg.Body = sb.ToString(); ;
            //    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
            //    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
            //    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //    //mailObj.EnableSsl = true;
            //    //mailObj.Send(mailMsg);
            //    //mailMsg.Dispose();
            //    return true;

            //}
            //catch
            //{
            //    return false;
            //}
        }
        #endregion

        #region New Email Work

        #region User Registeration
        public static bool RegistrationEmail(string sEmail)
        {
            long AdminID = AccountManager.GetSupplierByUser();
            var arrUser = User._DetailByUserEmail(sEmail);
            string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Account Activated");
            String sMail = "";
            TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"agencyname",arrUser.Name },
                         {"ContactName", arrUser.ContactPerson },
                         {"username", arrUser.email},
                         {"agentid",arrUser.Code}  ,
                         {"country",arrUser.Countryname}  ,
                         {"password",arrUser.PassWord}  ,
                         {"CompanyName",AccountManager.GetSupplierName()}  ,
                         {"supportMail",AccountManager.GetSupplierEmail()}  ,
                         };
            sMail += TemplateGenrator.GetTemplate;
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("Account Activated", sMail, "Your Password Detail", "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
        }


        public static bool ForgotPasswordEmail(string sEmail)
        {
            long AdminID = AccountManager.GetSupplierByUser();
            var arrUser = User._DetailByUserEmail(sEmail);

            string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "ForgotPassword");
            String sMail = "";
            TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"firstname",arrUser.Name },
                         {"lastname", arrUser.ContactPerson },
                         {"UserName", arrUser.email},
                         {"PassWord",arrUser.PassWord}  ,
                         };
            sMail += TemplateGenrator.GetTemplate;
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("ForgetPassword", sMail, "Your Password Detail", "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
        }

        public static bool ActivateNotificationMail(string sEmail, string AgentCode, string AgencyName)
        {
            long AdminID = AccountManager.GetSupplierByUser();
            var arrUser = User._DetailByUserEmail(sEmail);
            string sMail = "";
            string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Account Activated");
            TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"agencyname",arrUser.Name },
                         {"ContactName", arrUser.ContactPerson },
                         {"username", arrUser.email},
                         {"agentid",arrUser.Code}  ,
                         {"country",arrUser.Countryname}  ,
                         {"password",arrUser.PassWord}  ,
                         {"CompanyName",AccountManager.GetSupplierName()}  ,
                         {"supportMail",AccountManager.GetSupplierEmail()}  ,
                         };
            sMail += TemplateGenrator.GetTemplate;
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("Account Activated", sMail, "Dear " + arrUser.Name + " , Your Account is now activated. Your Code is " + AgentCode, "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
        }

        public static bool DeActivateNotificationMail(string sEmail, string AgentCode, string AgencyName)
        {
            long AdminID = AccountManager.GetSupplierByUser();
            var arrUser = User._DetailByUserEmail(sEmail);
            string sMail = "Dear " + arrUser.Name + ",<br/> Your Account has been Deactivated.Please Contact Admistrator.";
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("Account Deactivated", sMail, "Your  Account Deactivated", "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Account Mail
        public static bool CreditRequest(string AgentCode,string  AgencyName,string BankName,string AccountNumber,string AmountDeposit,string Remarks,string TypeOfDeposit,string TypeOfCash,string TypeOfCheque,string EmployeeName,string ReceiptNumber,string ChequeDDNumber,string ChequeDrawn,string DepositDate)
        {
            bool response = false;
            string sMail = "";
            long AdminID = AccountManager.GetSupplierByUser();
            string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Credit Request");
            string receiptNo = ReceiptNumber;
            if (ReceiptNumber == "")
                receiptNo = ChequeDDNumber;
            string givenTo = EmployeeName;
            if (EmployeeName == "")
                givenTo = ChequeDrawn;
            TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"AgencyName",AgencyName },
                         {"agencyname",AgencyName },
                         {"agentid", AgentCode},
                         {"depositedate", DepositDate},
                         {"depositetype",TypeOfDeposit}  ,
                         {"transactiontype",TypeOfCash}  ,
                         {"bank_name",BankName + "-" + AccountNumber}  ,
                         {"receiptNo",receiptNo}  ,
                         {"ammount",AmountDeposit}  ,
                         {"givenTo",givenTo}  ,
                         {"remark",Remarks}  ,
                         {"CompanyName",AccountManager.GetSupplierName()}  ,
                         {"supportMail",AccountManager.GetSupplierEmail()}  ,
                         };
            sMail += TemplateGenrator.GetTemplate;
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("Credit Request", sMail, "Payment Request", "", "");
                return Response;
            }
            catch
            {
                return false;
            }
            return response;
        }

        public static bool CreditApproved(string Details, string sEmail, bool isCredit)
        {
            bool Response = false;
            string sMail = GenrateMail(Details);
            try
            {
                if (isCredit)
                    Response = MailManager.Sent("Account Credit", sMail, "Your Account Credited", "", sEmail);
                else
                    Response = MailManager.Sent("Account Debited", sMail, "Your Account Debited", "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
            return Response;
        }

        public static bool CreditLimit(long uid , string Amount,string LimitType)
        {
            bool response = false;
            try
            {
                long AdminID = AccountManager.GetSupplierByUser();
                var arrUser = User._DetailByUserID(uid);
                TemplateGenrator.GetTemplatePath(AdminID, "Credit Limit");
                String sMail = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"AgencyName",arrUser.Name },
                         {"Amount", arrUser.Currency + " " +  Amount },
                         {"limit", LimitType},
                         {"credit_type", LimitType},
                         {"Admin",AccountManager.GetSupplierName()}  ,
                         };
                sMail += TemplateGenrator.GetTemplate;
                sMail = GenrateMail(sMail);
                response = MailManager.Sent("Credit Limit", sMail, LimitType+  " given", "", arrUser.email);
            }
            catch (Exception ex)
            {

            }
            return response;
        }
        #endregion

        #region Booking 
        #region Confirm Mail
        public static string BookingMail4Agent(string ReservationID)
        {
            string sMail = "";
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.Uid == objAgent.sid && obj.ReservationID == ReservationID
                                        select new
                                        {
                                            Currency = objAgent.CurrencyCode,
                                            customer_name = objAgent.ContactPerson,
                                            AgencyName = objAgent.AgencyName,
                                            Hotel = obj.HotelName,
                                            obj.Source,
                                            obj.ReservationID,
                                            obj.CancelDate,
                                            obj.CancelFlag,
                                            obj.ExchangeValue,
                                            obj.ComparedFare,
                                            guest_name = obj.bookingname,
                                            destination = obj.City
                                        }).FirstOrDefault();
                    long AdminID = AccountManager.GetSupplierByUser();
                    string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Booking Confirmation");
                    TemplateGenrator.arrParms = new Dictionary<string, string> {
                    {"Agent",sReservation.AgencyName },
                    {"Admin", AccountManager.GetSupplierName() },
                    {"PaxName", sReservation.guest_name},
                    {"Hotel",sReservation.Hotel + ",<b>" + sReservation.destination +"</b>"},
                    {"ReservatonID", sReservation.ReservationID},
                    {"SupportMails", AccountManager.GetSupplierEmail()},
                    };
                    sMail += TemplateGenrator.GetTemplate;
                    sMail = GenrateMail(sMail);
                }
            }
            catch
            {
                return "";
            }
            return sMail;
        }


        public static string BookingMail2CUT(string ReservationID, Int64 Uid, out string BCcTeamMails, out string CcTeamMail)
        {
            string sMail = "";
            BCcTeamMails = string.Empty;
            CcTeamMail = string.Empty;
            try
            {
                Common.Common.BookingID = ReservationID;
                var sReservation = Common.Common.arrBookingDetails;
                using (var DB = new DBHandlerDataContext())
                {
                    long AdminID = AccountManager.GetSupplierByUser();
                    string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Booking Request");
                    TemplateGenrator.arrParms = new Dictionary<string, string> {
                    {"Agent",sReservation.ContactPerson },
                    {"Admin", AccountManager.GetSupplierName() },
                    {"PaxName", sReservation.Passenger},
                    {"hotel",sReservation.HotelName + ",<b>" + sReservation.City +"</b>"},
                    {"CheckIn", sReservation.checkin},
                    {"CheckOut", sReservation.checkout },
                    {"status", sReservation.Status.Replace("Vouchered","Definate") },
                    {"Fare", sReservation.Ammount },
                    {"ResrvationID", sReservation.InvoiceID },
                    {"SupportMails", AccountManager.GetSupplierEmail()},
                    };
                    sMail += TemplateGenrator.GetTemplate;
                    /*Click Ur Hotel Mail*/
                    //long[] arrAdmin = new long[1];
                    //arrAdmin[0] = 232;
                    sMail = GenrateMail(sMail);
                    var Mail = new tbl_ActivityMail();
                    if (sReservation.Status == "Vouchered")
                        Mail = (from obj in DB.tbl_ActivityMails where obj.Activity.Contains("Booking Confirm") && obj.ParentID == Uid select obj).FirstOrDefault();
                    else
                        Mail = (from obj in DB.tbl_ActivityMails where obj.Activity.Contains("Booking OnRequest") && obj.ParentID == Uid select obj).FirstOrDefault();
                    if (sMail != null)
                    {
                        BCcTeamMails = Mail.Email;
                        CcTeamMail = Mail.CcMail;
                    }
                }
            }
            catch(Exception ex)
            {
            }
            return sMail;
        }

        #endregion


        #region Booking Request to Agent
        public static string RequestMail2Agent(string ReservationID, Int64 Uid)
        {
            StringBuilder sb = new StringBuilder();
            string sMail = "";
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        from objRoom in DB.tbl_CommonBookedRooms
                                        where obj.Uid == objAgent.sid && obj.ReservationID == ReservationID && objRoom.ReservationID == ReservationID
                                        select new
                                        {
                                            customer_name = objAgent.ContactPerson,
                                            obj.Status,
                                            AgencyName = objAgent.AgencyName,
                                            Currency = objAgent.CurrencyCode,
                                            obj.Source,
                                            obj.ReservationID,
                                            //obj.CancelDate,
                                            Hotel = obj.HotelName,
                                            CancelDate = objRoom.CutCancellationDate,
                                            obj.CancelFlag,
                                            obj.ExchangeValue,
                                            obj.ComparedFare,
                                            guest_name = obj.bookingname,
                                            destination = obj.City,
                                            bookingStatus = obj.Status,
                                            BookedOn = obj.ReservationDate,
                                            obj.CheckIn,
                                            obj.CheckOut,
                                        }).FirstOrDefault();
                    List<decimal> SupplierAmt = new List<decimal>();
                    foreach (var objRoom in DB.tbl_CommonBookedRooms.Where(d => d.ReservationID == ReservationID))
                    {
                        SupplierAmt.Add(Convert.ToDecimal(objRoom.RoomAmount));
                    }
                    long AdminID = AccountManager.GetSupplierByUser();
                    string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Booking Request");
                    TemplateGenrator.arrParms = new Dictionary<string, string> {
                    {"Agent",sReservation.AgencyName },
                    {"Admin", AccountManager.GetSupplierName() },
                    {"ResrvationID", sReservation.ReservationID},
                    {"BookingOn", sReservation.BookedOn},
                    {"PaxName", sReservation.guest_name},
                    {"hotel",sReservation.Hotel + ",<b>" + sReservation.destination +"</b>"},
                    {"CheckOut ", sReservation.CheckOut},
                    {"CheckIn ", sReservation.CheckIn},
                    {"status", sReservation.Status},
                    {"Fare", sReservation.Currency + " " + SupplierAmt.ToList().Sum()},
                    {"SupportMails", AccountManager.GetSupplierEmail()},
                    };
                    sMail += TemplateGenrator.GetTemplate;
                    sMail = GenrateMail(sMail);
                    return sMail;
                }
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region Cancellation 
        public static bool CancellationMail(string BookingID , decimal CancelAmount)
        {
            bool Response = false;
            Common.Common.BookingID = BookingID;
            var sReservation = Common.Common.arrBookingDetails;
            try
            {
                string Title = "Hotel " + sReservation.HotelName + " - " + sReservation.Status + " on " + DateTime.Now.ToString("dd/MM/yyyy") + " By " + sReservation.Passenger;
                string sMail = "";
                GlobalDefault obj = AccountManager.GetAdmin(sReservation.Uid);
                long AdminID = obj.sid;
                long[] arrAdminID = new long[1];
                arrAdminID[0] = AdminID;
                string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Cancel Booking");
                TemplateGenrator.arrParms = new Dictionary<string, string>
                {
                    {"Agent",sReservation.ContactPerson },
                    {"BookingOn",sReservation.Bookingdate },
                    {"hotel",sReservation.HotelName + " ,<b>" + sReservation.City + "<b>" },
                    {"CheckIn",sReservation.checkin },
                    {"CheckOut",sReservation.checkout },
                    {"status",sReservation.Status },
                    {"deadlineDate",sReservation.CancelDate },
                    {"Charge", obj.Currency + " " + CancelAmount },
                    {"PaxName", sReservation.Passenger},
                    {"Fare", sReservation.Ammount},
                    {"ReservatonID", BookingID},
                    {"Admin", obj.AgencyName},
                    {"SupportMails", AccountManager.GetSupplierEmail()},
                    };
                sMail += TemplateGenrator.GetTemplate;
                sMail = GenrateMail(sMail, arrAdminID);

                Response = MailManager.Sent("Booking Cancelled", sMail, Title, "", sReservation.Email);
                return Response;
            }
            catch (Exception ex)
            {

            }
            return Response;
        }
        #endregion

        
        #endregion

        #region Admin Template
        public static string GenrateMail(string Details,params long[] id)
        {
            string sMail = string.Empty;
            try
            {
                long AdminID = 0;
                if(id != null && id.Length !=0)
                    AdminID = id[0];
                else
                    AdminID = AccountManager.GetSupplierByUser();
                var arrAdmin = User._DetailsBySupplier(AdminID);
                string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Profile");
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                {"Details",Details},
                {"CompanyName",arrAdmin.Name },
                {"Address", arrAdmin.Address },
                {"Email", arrAdmin.email},
                {"http://img.mailinblue.com/2096904/images/rnb/original/5ce91ecece057438f265bb01.jpg", "https://clickurhotel.com/Agencylogo/"+  arrAdmin.Code + ".jpg"}  ,
                };
                sMail += TemplateGenrator.GetTemplate;
            }
            catch (Exception)
            {
                throw;
            }
            return sMail;
        }
        #endregion

        #region Hotel Account Email
        public static bool HotelAccountEmail(string sEmail)
        {
            long AdminID = AccountManager.GetSupplierByUser();
            var arrUser = User._DetailByUserEmail(sEmail);
            string sProfile = TemplateGenrator.GetTemplatePath(AdminID, "Hotel Account");
            String sMail = "";
            TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"agencyname",arrUser.Name },
                         {"ContactName", arrUser.ContactPerson },
                         {"username", arrUser.email},
                         {"agentid",arrUser.Code}  ,
                         {"country",arrUser.Countryname}  ,
                         {"password",arrUser.PassWord}  ,
                         {"CompanyName",AccountManager.GetSupplierName()}  ,
                         {"supportMail",AccountManager.GetSupplierEmail()}  ,
                         };
            sMail += TemplateGenrator.GetTemplate;
            sMail = GenrateMail(sMail);
            try
            {
                bool Response = MailManager.Sent("Account Activated", sMail, "Your Password Detail", "", sEmail);
                return Response;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #endregion



    }
}