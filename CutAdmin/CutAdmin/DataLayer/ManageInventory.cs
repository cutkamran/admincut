﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using CutAdmin.BL;
using System.Globalization;
using System.Data;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class ManageInventory : InventoryManager
    {
        #region Inventoy Old
        public class Room
        {
            public string RoomName { get; set; }
            public Int64 RoomTypeID { get; set; }
        }
        public class Inventory
        {
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string Address { get; set; }
            public List<date> Dates { get; set; }
            public List<RoomType> ListRoom { get; set; }
        }
       public static List<DateTime> RateDate{ get; set; }
       public static List<Inventory> GetInventory(string CheckIn, string CheckOut, Int64 HotelCode, string InventoryTypee, Int64 SupplierID)
       {
           List<Inventory> ListInventory = new List<Inventory>();
           try
           {
               using(var db  = new ClickUrHotel_DBEntities())
               {
                   DateTime From = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   DateTime To = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                 
                   RateDate = new List<DateTime>();
                   double noGDays = (To - From).TotalDays;
                   List<string> ListDates = new List<string>();
                   for (int i = 0; i <= noGDays; i++)
                   {
                       ListDates.Add(From.AddDays(i).ToString("dd-MM-yyyy"));
                   }
                   DateTime targetDate = From;
                   while (targetDate <= To)
                   {
                       RateDate.Add(targetDate);
                       targetDate = targetDate.AddDays(1);
                   }
                   var arrHotels = (from objHotel in db.tbl_CommonHotelMaster
                                    join objInvent in db.tbl_CommonHotelInventory on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString()
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   if (HotelCode !=0)
                   {
                       arrHotels = (from objHotel in db.tbl_CommonHotelMaster
                                    join objInvent in db.tbl_CommonHotelInventory on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString() && objInvent.HotelCode == HotelCode.ToString()
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   }
                   foreach (var objHotel in arrHotels)
                   {
                       try
                       {
                           var arrRooms = (from objRoom in db.tbl_commonRoomDetails
                                           join objRoomType in db.tbl_commonRoomType on objRoom.RoomTypeId equals objRoomType.RoomTypeID
                                           //join objInvent in db.tbl_CommonHotelInventories on objRoomType.RoomTypeID equals Convert.ToInt64(objInvent.RoomType)
                                           where objRoom.HotelId == Convert.ToInt64(objHotel.HotelCode)
                                           select new Room
                                           {
                                               RoomTypeID = objRoomType.RoomTypeID,
                                               RoomName = objRoomType.RoomType,
                                           }).ToList().Distinct();
                           List<RoomType> ListRooms = new List<RoomType>();
                           foreach (var objRoom in arrRooms)
                           {
                               List<date> ListDate = new List<date>();
                               for (int i = 0; i <= noGDays; i++)
                               {
                                   string Type = "";
                                    string InventoryType = "";
                                    string Date = From.AddDays(i).ToString("dd-MM-yyyy");
                                   var listInventory = (from obj in db.tbl_CommonHotelInventory
                                                        where obj.HotelCode == objHotel.HotelCode.ToString() && obj.Month == Date.Split('-')[1] &&
                                                        obj.Year == Date.Split('-')[2] && obj.RoomType == objRoom.RoomTypeID.ToString() &&
                                                        obj.SupplierId == SupplierID.ToString() && obj.InventoryType == InventoryTypee
                                                        select obj).ToList();
                                   Int64 Count = 0;
                                   Int64 Booked = 0;
                                   string Saletype = "";

                                    if (listInventory.Count != 0)
                                   {
                                       ListtoDataTable lsttodt = new ListtoDataTable();
                                       DataTable dt = lsttodt.ToDataTable(listInventory);

                                       string date = "";
                                       if (From.AddDays(i).ToString("dd").StartsWith("0"))
                                           date = "Date_" + From.AddDays(i).ToString("dd").TrimStart('0');
                                       else
                                           date = "Date_" + From.AddDays(i).ToString("dd");
                                       string noRooms = dt.Rows[0][date].ToString();
                                       if (noRooms != "")
                                       {
                                           string noTotalRooms = noRooms.Split('_')[1];
                                           if (noTotalRooms != "")
                                           {
                                               Count = Count + Convert.ToInt64(noTotalRooms);
                                               Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                               Saletype = noRooms.Split('_')[0];
                                            }
                                           else
                                           {
                                               Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                                Saletype = noRooms.Split('_')[0];
                                            }
                                       }
                                   }
                                   Int64 Sid = 0;
                                   if (listInventory.Count != 0)
                                   {
                                       Sid = listInventory.FirstOrDefault().Sid;
                                       Type = listInventory.FirstOrDefault().RateType;
                                       InventoryType = listInventory.FirstOrDefault().InventoryType;
                                    }
                                   else
                                       InventoryType = "0";

                                   ListDate.Add(new date
                                   {
                                       RateTypeId = Convert.ToInt16(Sid),
                                       datetime = From.AddDays(i).ToString("dd-MM-yyyy"),
                                       day = From.AddDays(0).Day.ToString(),
                                       NoOfCount = Count,
                                       Type = Booked.ToString(),
                                       discount = Saletype,
                                       InventoryType = InventoryType,
                                   });
                               }
                               ListRooms.Add(new RoomType
                               {
                                   RoomTypeName = objRoom.RoomName,
                                   RoomTypeId = objRoom.RoomTypeID.ToString(),
                                   dates = ListDate
                               });
                           }

                           if (ListRooms.Count != 0)
                           {
                               List<date> Listdate = new List<date>();
                               foreach (var objDates in ListDates)
                               {
                                   Int64 Count = 0;
                                   Int64 NoBooked = 0;
                                   var arrRoomDates = ListRooms.Select(d => d.dates.Where(r => r.datetime == objDates)).ToList();
                                   foreach (var item in arrRoomDates)
                                   {
                                       Count += item.Select(d => d.NoOfCount).Sum();
                                       NoBooked += item.Select(d => Convert.ToInt64(d.Type)).Sum();
                                   }
                                   if (Listdate.Where(d => d.datetime == objDates).ToList().Count == 0)
                                   {
                                       Listdate.Add(new date
                                       {
                                           datetime = objDates,
                                           NoOfCount = Count,
                                           Type = NoBooked.ToString(),
                                       });
                                   }
                                   else
                                   {
                                       Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().NoOfCount = Listdate.Where(d => d.datetime == objDates).ToList().Count + Count;
                                       Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().Type = (Convert.ToInt64(Listdate.Where(d => d.datetime == objDates).ToList().Count) + NoBooked).ToString();
                                   }
                               }
                               ListInventory.Add(new Inventory
                               {
                                   HotelCode = objHotel.HotelCode,
                                   HotelName = objHotel.HotelName,
                                   Address = objHotel.Address,
                                   ListRoom = ListRooms,
                                   Dates = Listdate,
                               });
                           }
                       }
                       catch (Exception ex)
                       {
                       }
                   }
               }
           }
           catch (Exception ex)
           {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                // throw new Exception(ex.Message);
            }
           return ListInventory;
       }


       public static List<Inventory> SearchInventory(string CheckIn, string CheckOut, string HotelCode, string SupplierId)
       {
           List<Inventory> ListInventory = new List<Inventory>();
           try
           {
               using (var db = new ClickUrHotel_DBEntities())
               {
                   DateTime From = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   DateTime To = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   Int64 SupplierID = AccountManager.GetSupplierByUser();
                   RateDate = new List<DateTime>();
                   double noGDays = (To - From).TotalDays;
                   List<string> ListDates = new List<string>();
                   for (int i = 0; i <= noGDays; i++)
                   {
                       ListDates.Add(From.AddDays(i).ToString("dd-MM-yyyy"));
                   }
                   DateTime targetDate = From;
                   while (targetDate <= To)
                   {
                       RateDate.Add(targetDate);
                       targetDate = targetDate.AddDays(1);
                   }
                   var arrHotels = (from objHotel in db.tbl_CommonHotelMaster
                                    join objInvent in db.tbl_CommonHotelInventory on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString() && objHotel.sid == Convert.ToInt64(HotelCode)
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   foreach (var objHotel in arrHotels)
                   {

                       var arrRooms = (from objRoom in db.tbl_commonRoomDetails
                                       join objRoomType in db.tbl_commonRoomType on objRoom.RoomTypeId equals objRoomType.RoomTypeID
                                       where objRoom.HotelId == Convert.ToInt64(objHotel.HotelCode)
                                       select new Room
                                       {
                                           RoomTypeID = objRoomType.RoomTypeID,
                                           RoomName = objRoomType.RoomType,
                                       }).ToList();
                       List<RoomType> ListRooms = new List<RoomType>();
                       foreach (var objRoom in arrRooms)
                       {
                           List<date> ListDate = new List<date>();
                           for (int i = 0; i <= noGDays; i++)
                           {
                               string Type = "";
                               string Date = From.AddDays(i).ToString("dd-MM-yyyy");
                               var listInventory = (from obj in db.tbl_CommonHotelInventory where obj.HotelCode == objHotel.HotelCode.ToString() && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RoomType == objRoom.RoomTypeID.ToString() && obj.SupplierId == SupplierID.ToString() select obj).ToList();
                               Int64 Count = 0;
                               Int64 Booked = 0;
                               string InventoryType = "";
                               if (listInventory.Count != 0)
                               {
                                   ListtoDataTable lsttodt = new ListtoDataTable();
                                   DataTable dt = lsttodt.ToDataTable(listInventory);

                                   string date = "";
                                   if (From.AddDays(i).ToString("dd").StartsWith("0"))
                                       date = "Date_" + From.AddDays(i).ToString("dd").TrimStart('0');
                                   else
                                       date = "Date_" + From.AddDays(i).ToString("dd");
                                   string noRooms = dt.Rows[0][date].ToString();
                                   if (noRooms != "")
                                   {
                                       string noTotalRooms = noRooms.Split('_')[1];
                                       if (noTotalRooms != "")
                                       {
                                           Count = Count + Convert.ToInt64(noTotalRooms);
                                           Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                       }
                                       else
                                       {
                                           Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                       }
                                   }
                               }
                               if (listInventory.Count != 0)
                               {
                                   Type = listInventory.FirstOrDefault().RateType;
                                   InventoryType = listInventory.FirstOrDefault().InventoryType;
                               }

                               ListDate.Add(new date
                               {
                                   datetime = From.AddDays(i).ToString("dd-MM-yyyy"),
                                   day = From.AddDays(0).Day.ToString(),
                                   NoOfCount = Count,
                                   Type = Booked.ToString(),
                                   InventoryType = InventoryType,
                               });
                           }
                           ListRooms.Add(new RoomType
                           {
                               RoomTypeName = objRoom.RoomName,
                               RoomTypeId = objRoom.RoomTypeID.ToString(),
                               Dates = ListDate
                           });
                       }

                       if (ListRooms.Count != 0)
                       {
                           List<date> Listdate = new List<date>();
                           foreach (var objDates in ListDates)
                           {
                               Int64 Count = 0;
                               Int64 NoBooked = 0;
                               var arrRoomDates = ListRooms.Select(d => d.Dates.Where(r => r.datetime == objDates)).ToList();
                               foreach (var item in arrRoomDates)
                               {
                                   Count += item.Select(d => d.NoOfCount).Sum();
                                   NoBooked += item.Select(d => Convert.ToInt64(d.Type)).Sum();
                               }
                               if (Listdate.Where(d => d.datetime == objDates).ToList().Count == 0)
                               {
                                   Listdate.Add(new date
                                   {
                                       datetime = objDates,
                                       NoOfCount = Count,
                                       Type = NoBooked.ToString(),
                                   });
                               }
                               else
                               {
                                   Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().NoOfCount = Listdate.Where(d => d.datetime == objDates).ToList().Count + Count;
                                   Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().Type = (Convert.ToInt64(Listdate.Where(d => d.datetime == objDates).ToList().Count) + NoBooked).ToString();
                               }
                           }
                           ListInventory.Add(new Inventory
                           {
                               HotelCode = objHotel.HotelCode,
                               HotelName = objHotel.HotelName,
                               Address = objHotel.Address,
                               ListRoom = ListRooms,
                               Dates = Listdate,
                           });
                       }
                   }
               }
           }
           catch (Exception ex)
           {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
           }
           return ListInventory;
       }
        #endregion

        #region Inventory Updatees
        public static List<date> GetRoomInventory(string HotelCode, List<string> arrRoomID, string InventoryType, string SupplierID, List<DateTime> arrDates)
        {
            List<date> arrInventory = new List<date>();
            var InType = InventoryType;
            string InvName = "";
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    IQueryable<tbl_CommonHotelInventory> listInventory = (from obj in db.tbl_CommonHotelInventory
                                                                          where obj.HotelCode == HotelCode && arrRoomID.Contains(obj.RoomType) &&
                                                                          obj.SupplierId == SupplierID.ToString() && obj.InventoryType == InventoryType
                                                                          select obj);
                    if (listInventory.ToList().Count != 0)
                    {
                        var ListInventory = listInventory.ToList();
                        foreach (var RoomID in arrRoomID)
                        {
                            foreach (var Date in arrDates)
                            {
                                string Type = "";
                                string dateMonth = Date.Month.ToString();
                                string dateYear = Date.Year.ToString();
                                var arrRoomInventory = ListInventory.Where(d => d.RoomType == RoomID && d.Year == dateYear && d.Month == dateMonth).ToList();
                                Int64 Count = 0;
                                Int64 Booked = 0;
                                string Saletype = "";
                                if (arrRoomInventory.Count != 0)
                                {
                                    ListtoDataTable lsttodt = new ListtoDataTable();
                                    DataTable dt = lsttodt.ToDataTable(arrRoomInventory);

                                    string date = "";
                                    if (Date.ToString("dd").StartsWith("0"))
                                        date = "Date_" + Date.ToString("dd").TrimStart('0');
                                    else
                                        date = "Date_" + Date.ToString("dd");
                                    string noRooms = dt.Rows[0][date].ToString();
                                    if (noRooms != "")
                                    {
                                        string noTotalRooms = noRooms.Split('_')[1];
                                        if (noTotalRooms != "")
                                        {
                                            Count = Count + Convert.ToInt64(noTotalRooms);
                                            Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                            Saletype = noRooms.Split('_')[0];
                                        }
                                        else
                                        {
                                            Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                            Saletype = noRooms.Split('_')[0];
                                        }
                                    }
                                }
                                Int64 Sid = 0;
                                if (arrRoomInventory.Count != 0)
                                {
                                    Sid = arrRoomInventory.FirstOrDefault().Sid;
                                    Type = Booked + "_" + arrRoomInventory.FirstOrDefault().RateType;
                                    InventoryType = arrRoomInventory.FirstOrDefault().InventoryType;
                                }
                                else
                                {
                                    InvName = InType;
                                    InventoryType = "0";
                                }


                                arrInventory.Add(new date
                                {
                                    RoomTypeId = Convert.ToInt64(RoomID),
                                    RateTypeId = Convert.ToInt64(Sid),
                                    datetime = Date.ToString("dd-MM-yyyy"),
                                    day = Date.Day.ToString(),
                                    NoOfCount = Count,
                                    Type = Type,
                                    discount = Saletype,
                                    InventoryType = InventoryType,
                                    InvName = InvName
                                });
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrInventory;
        }

        public static void SaveInventory(string HotelCode, Int64[] arrRoom, string MaxRoom, string DtTill, string[] DateInvFr, string[] DateInvTo, List<string> arrRateType, string OptRoomperDate, string InvLiveOrReq, string InType, string InventoryState, List<string> arrDays)
        {
            try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    string SupplierID = AccountManager.GetSupplierByUser().ToString();
                    List<tbl_CommonHotelInventory> arrInventory = new List<tbl_CommonHotelInventory>();
                    tbl_CommonHotelInventory Inventory = new tbl_CommonHotelInventory();
                    foreach (var sRateType in arrRateType)/*Room Rate Type*/
                    {
                        string InvSoldType = InvLiveOrReq;
                        foreach (var sFromDate in DateInvFr.ToList())
                        {
                            CutAdmin.Common.Common.arrDays = new List<string>();
                            CutAdmin.Common.Common.StartDate = DateTime.ParseExact(sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            CutAdmin.Common.Common.EndDate = DateTime.ParseExact(DateInvTo[DateInvFr.ToList().IndexOf(sFromDate)], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            foreach (var sRoom in arrRoom)  /*Rooms*/
                            {
                                List<DateTime> ListDates = CutAdmin.Common.Common.Calander;
                                foreach (var nMonth in ListDates.Select(m => m.Month).Distinct().ToList())
                                {
                                    string sMonth = ListDates.Where(m => m.Month == nMonth).ToList().FirstOrDefault().Month.ToString();
                                    string sYear = ListDates.Where(m => m.Month == nMonth).ToList().FirstOrDefault().Year.ToString();
                                    IQueryable<tbl_CommonHotelInventory> arrOldInv = from obj in DB.tbl_CommonHotelInventory
                                                                                     where obj.HotelCode == HotelCode
                                                                                     && obj.RoomType == sRoom.ToString() &&
                                                                                     obj.Year == sYear && obj.Month == sMonth && obj.RateType == sRateType
                                                                                     && obj.SupplierId == SupplierID
                                                                                     && obj.InventoryType == InType
                                                                                     select obj;/* Check Old Inventory */
                                    foreach (var sDate in ListDates)
                                    {
                                        if (arrOldInv.ToList().Count == 0)
                                        {
                                            if (!arrInventory.Exists(d => d.HotelCode == HotelCode && d.RoomType == sRoom.ToString() && d.RateType == sRateType && d.Month == sMonth && d.Year == sYear))
                                            {
                                                arrInventory.Add(new tbl_CommonHotelInventory
                                                {
                                                    HotelCode = HotelCode,
                                                    SupplierId = AccountManager.GetSupplierByUser().ToString(),
                                                    RoomType = sRoom.ToString(),
                                                    RateType = sRateType,
                                                    Month = sMonth,
                                                    InventoryType = InType,
                                                    Year = sYear,
                                                    FreeSaleTill = DtTill,
                                                });
                                                string sInventory = InType + "_" + MaxRoom + "_" + "0";
                                                if (InventoryState == "ss")
                                                    sInventory = InventoryState + "_" + MaxRoom + "_" + "0";

                                                string OptRoom = OptRoomperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                                SetInventory(arrInventory.LastOrDefault(), sDate.Day.ToString(), sInventory, OptRoom);
                                            }
                                            else
                                            {
                                                Inventory = arrInventory.Where(d => d.HotelCode == HotelCode && d.RoomType == sRoom.ToString() && d.RateType == sRateType && d.Month == sMonth && d.Year == sYear).FirstOrDefault();
                                                string sInventory = InType + "_" + MaxRoom + "_" + "0";
                                                string OptRoom = OptRoomperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                                SetInventory(arrInventory.LastOrDefault(), sDate.Day.ToString(), sInventory, OptRoom);
                                            }
                                        }
                                        else
                                        {
                                            string OptRoom = "";
                                            Inventory = arrOldInv.ToList().Where(d => d.HotelCode == HotelCode && d.RoomType == sRoom.ToString() && d.RateType == sRateType && d.Month == sMonth && d.Year == sYear).FirstOrDefault();
                                            string sInventory = GettInventory(Inventory, sDate.Day.ToString(), out OptRoom);
                                            
                                            if (sInventory != null && sInventory.Split('_').ToList().Count != 0)
                                            {
                                                sInventory = InType + "_" + MaxRoom + "_" + sInventory.Split('_').ToList()[2];
                                                if (InventoryState == "ss")
                                                    sInventory = InventoryState + "_" + MaxRoom + "_" + sInventory.Split('_').ToList()[2];
                                            }
                                            else
                                            {
                                                sInventory = InType + "_" + MaxRoom + "_" + "0";
                                                if (InventoryState == "ss")
                                                    sInventory = InventoryState + "_" + MaxRoom + "_" + "0";
                                            }
                                            if (OptRoom != null && OptRoom.Split('_').ToList().Count != 0 && OptRoom.Split('_').ToList().Count == 2)
                                            {
                                                OptRoom = DtTill + "_" + InvSoldType;
                                            }
                                            else
                                            {
                                                OptRoom = OptRoomperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                            }
                                            SetInventory(Inventory, sDate.Day.ToString(), sInventory, OptRoom);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    DB.tbl_CommonHotelInventory.AddRange(arrInventory);
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }


        #region Get Set Inventory
        public string UpdateDate(string OldDate, string InventType)
        {
            string Update = "";
            try
            {
                Update += InventType;
                if (OldDate.Split('_').Length > 1)
                {
                    Update += "_" + OldDate.Split('_')[1];
                }
                if (OldDate.Split('_').Length == 3)
                {
                    Update += "_" + OldDate.Split('_')[2];
                }

            }
            catch(Exception ex) {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Update;
        }
        public string UpdateRoom(string OldDate, string noOfRoom)
        {
            string Update = "";
            try
            {
                Update = noOfRoom;
                var date = OldDate.Split('_');
                if (date.Length > 1)
                {
                    date[1] = Update;
                }
                if (OldDate.Split('_').Length == 3)
                {
                    Update = date[0] + "_" + date[1] + '_' + date[2];
                }

            }
            catch(Exception ex) {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Update;
        }




        public static tbl_CommonHotelInventory SetInventory(tbl_CommonHotelInventory Update, string Day, string Inventory, string Option_Date)
        {
            try
            {
                switch (Day)
                {
                    case "1":
                        Update.Date_1 = Inventory;
                        Update.Option_Date_1 = Option_Date;
                        break;
                    case "2":
                        Update.Date_2 = Inventory;
                        Update.Option_Date_2 = Option_Date;
                        break;
                    case "3":
                        Update.Date_3 = Inventory;
                        Update.Option_Date_3 = Option_Date;
                        break;
                    case "4":
                        Update.Date_4 = Inventory;
                        Update.Option_Date_4 = Option_Date;
                        break;
                    case "5":
                        Update.Date_5 = Inventory;
                        Update.Option_Date_5 = Option_Date;
                        break;
                    case "6":
                        Update.Date_6 = Inventory;
                        Update.Option_Date_6 = Option_Date;
                        break;
                    case "7":
                        Update.Date_7 = Inventory;
                        Update.Option_Date_7 = Option_Date;
                        break;
                    case "8":
                        Update.Date_8 = Inventory;
                        Update.Option_Date_8 = Option_Date;
                        break;
                    case "9":
                        Update.Date_9 = Inventory;
                        Update.Option_Date_9 = Option_Date;
                        break;
                    case "10":
                        Update.Date_10 = Inventory;
                        Update.Option_Date_10 = Option_Date;
                        break;
                    case "11":
                        Update.Date_11 = Inventory;
                        Update.Option_Date_11 = Option_Date;
                        break;
                    case "12":
                        Update.Date_12 = Inventory;
                        Update.Option_Date_12 = Option_Date;
                        break;
                    case "13":
                        Update.Date_13 = Inventory;
                        Update.Option_Date_13 = Option_Date;
                        break;
                    case "14":
                        Update.Date_14 = Inventory;
                        Update.Option_Date_14 = Option_Date;
                        break;
                    case "15":
                        Update.Date_15 = Inventory;
                        Update.Option_Date_15 = Option_Date;
                        break;
                    case "16":
                        Update.Date_16 = Inventory;
                        Update.Option_Date_16 = Option_Date;
                        break;
                    case "17":
                        Update.Date_17 = Inventory;
                        Update.Option_Date_17 = Option_Date;
                        break;
                    case "18":
                        Update.Date_18 = Inventory;
                        Update.Option_Date_18 = Option_Date;
                        break;
                    case "19":
                        Update.Date_19 = Inventory;
                        Update.Option_Date_19 = Option_Date;
                        break;
                    case "20":
                        Update.Date_20 = Inventory;
                        Update.Option_Date_1 = Option_Date;
                        break;
                    case "21":
                        Update.Date_21 = Inventory;
                        Update.Option_Date_21 = Option_Date;
                        break;
                    case "22":
                        Update.Date_22 = Inventory;
                        Update.Option_Date_22 = Option_Date;
                        break;
                    case "23":
                        Update.Date_23 = Inventory;
                        Update.Option_Date_23 = Option_Date;
                        break;
                    case "24":
                        Update.Date_24 = Inventory;
                        Update.Option_Date_24 = Option_Date;
                        break;
                    case "25":
                        Update.Date_25 = Inventory;
                        Update.Option_Date_25 = Option_Date;
                        break;
                    case "26":
                        Update.Date_26 = Inventory;
                        Update.Option_Date_26 = Option_Date;
                        break;
                    case "27":
                        Update.Date_27 = Inventory;
                        Update.Option_Date_27 = Option_Date;
                        break;
                    case "28":
                        Update.Date_28 = Inventory;
                        Update.Option_Date_28 = Option_Date;
                        break;
                    case "29":
                        Update.Date_29 = Inventory;
                        Update.Option_Date_29 = Option_Date;
                        break;
                    case "30":
                        Update.Date_30 = Inventory;
                        Update.Option_Date_30 = Option_Date;
                        break;
                    case "31":
                        Update.Date_31 = Inventory;
                        Update.Option_Date_31 = Option_Date;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Update;
        } /*Set Inventory*/


        public static tbl_CommonHotelInventory GetInventory(tbl_CommonHotelInventory Update, string Day)
        {
            string Inventory, Option_Date;
            try
            {
                switch (Day)
                {
                    case "1":
                        Inventory = Update.Date_1;
                        break;
                    case "2":
                        Inventory = Update.Date_2;
                        break;
                    case "3":
                        Inventory = Update.Date_3;
                        break;
                    case "4":
                        Inventory = Update.Date_4;
                        break;
                    case "5":
                        Inventory = Update.Date_5;
                        break;
                    case "6":
                        Inventory = Update.Date_6;
                        break;
                    case "7":
                        Inventory = Update.Date_7;
                        break;
                    case "8":
                        Inventory = Update.Date_8;
                        break;
                    case "9":
                        Inventory = Update.Date_9;
                        break;
                    case "10":
                        Inventory = Update.Date_10;
                        break;
                    case "11":
                        Inventory = Update.Date_11;
                        break;
                    case "12":
                        Inventory = Update.Date_12;
                        break;
                    case "13":
                        Inventory = Update.Date_13;
                        break;
                    case "14":
                        Inventory = Update.Date_14;
                        break;
                    case "15":
                        Inventory = Update.Date_15;
                        break;
                    case "16":
                        Inventory = Update.Date_16;
                        break;
                    case "17":
                        Inventory = Update.Date_17;
                        break;
                    case "18":
                        Inventory = Update.Date_18;
                        break;
                    case "19":
                        Inventory = Update.Date_19;
                        break;
                    case "20":
                        Inventory = Update.Date_20;
                        break;
                    case "21":
                        Inventory = Update.Date_21;
                        break;
                    case "22":
                        Inventory = Update.Date_22;
                        break;
                    case "23":
                        Inventory = Update.Date_23;
                        break;
                    case "24":
                        Inventory = Update.Date_24;
                        break;
                    case "25":
                        Inventory = Update.Date_25;
                        break;
                    case "26":
                        Inventory = Update.Date_26;
                        break;
                    case "27":
                        Inventory = Update.Date_27;
                        break;
                    case "28":
                        Inventory = Update.Date_28;
                        break;
                    case "29":
                        Inventory = Update.Date_29;
                        break;
                    case "30":
                        Inventory = Update.Date_30;
                        break;
                    case "31":
                        Inventory = Update.Date_31;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Update;
        } /*Set Inventory*/


        public static string GettInventory(tbl_CommonHotelInventory Update, string Day, out string OptRoom)
        {
            string Inventory = string.Empty;
            OptRoom = string.Empty;
            try
            {
                switch (Day)
                {
                    case "1":
                        Inventory = Update.Date_1;
                        OptRoom = Update.Option_Date_1;
                        break;
                    case "2":
                        Inventory = Update.Date_2;
                        OptRoom = Update.Option_Date_2;
                        break;
                    case "3":
                        Inventory = Update.Date_3;
                        OptRoom = Update.Option_Date_3;
                        break;
                    case "4":
                        Inventory = Update.Date_4;
                        OptRoom = Update.Option_Date_4;
                        break;
                    case "5":
                        Inventory = Update.Date_5;
                        OptRoom = Update.Option_Date_5;
                        break;
                    case "6":
                        Inventory = Update.Date_6;
                        OptRoom = Update.Option_Date_6;
                        break;
                    case "7":
                        Inventory = Update.Date_7;
                        OptRoom = Update.Option_Date_7;
                        break;
                    case "8":
                        Inventory = Update.Date_8;
                        OptRoom = Update.Option_Date_5;
                        break;
                    case "9":
                        Inventory = Update.Date_9;
                        OptRoom = Update.Option_Date_9;
                        break;
                    case "10":
                        Inventory = Update.Date_10;
                        OptRoom = Update.Option_Date_10;
                        break;
                    case "11":
                        Inventory = Update.Date_11;
                        OptRoom = Update.Option_Date_11;
                        break;
                    case "12":
                        Inventory = Update.Date_12;
                        OptRoom = Update.Option_Date_12;
                        break;
                    case "13":
                        Inventory = Update.Date_13;
                        OptRoom = Update.Option_Date_13;
                        break;
                    case "14":
                        Inventory = Update.Date_14;
                        OptRoom = Update.Option_Date_14;
                        break;
                    case "15":
                        Inventory = Update.Date_15;
                        OptRoom = Update.Option_Date_14;
                        break;
                    case "16":
                        Inventory = Update.Date_16;
                        OptRoom = Update.Option_Date_16;
                        break;
                    case "17":
                        Inventory = Update.Date_17;
                        OptRoom = Update.Option_Date_17;
                        break;
                    case "18":
                        Inventory = Update.Date_18;
                        OptRoom = Update.Option_Date_18;
                        break;
                    case "19":
                        Inventory = Update.Date_19;
                        OptRoom = Update.Option_Date_19;
                        break;
                    case "20":
                        Inventory = Update.Date_20;
                        OptRoom = Update.Option_Date_20;
                        break;
                    case "21":
                        Inventory = Update.Date_21;
                        OptRoom = Update.Option_Date_21;
                        break;
                    case "22":
                        Inventory = Update.Date_22;
                        OptRoom = Update.Option_Date_22;
                        break;
                    case "23":
                        Inventory = Update.Date_23;
                        OptRoom = Update.Option_Date_23;
                        break;
                    case "24":
                        Inventory = Update.Date_24;
                        OptRoom = Update.Option_Date_24;
                        break;
                    case "25":
                        Inventory = Update.Date_25;
                        OptRoom = Update.Option_Date_25;
                        break;
                    case "26":
                        Inventory = Update.Date_26;
                        OptRoom = Update.Option_Date_26;
                        break;
                    case "27":
                        Inventory = Update.Date_27;
                        OptRoom = Update.Option_Date_27;
                        break;
                    case "28":
                        Inventory = Update.Date_28;
                        OptRoom = Update.Option_Date_28;
                        break;
                    case "29":
                        Inventory = Update.Date_29;
                        OptRoom = Update.Option_Date_29;
                        break;
                    case "30":
                        Inventory = Update.Date_30;
                        OptRoom = Update.Option_Date_30;
                        break;
                    case "31":
                        Inventory = Update.Date_31;
                        OptRoom = Update.Option_Date_31;
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Inventory;
        }
        #endregion
        #endregion
    }
}