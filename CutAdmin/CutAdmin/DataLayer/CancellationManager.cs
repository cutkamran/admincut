﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using System.Text;
using CutAdmin.EntityModal;
namespace CutAdmin.DataLayer
{
    public class CancellationManager
    {
        public static tbl_CommonHotelReservation Resrvation { get; set; }
        public static List<string> arrBookingID { get; set; }

        public static bool CancelBooking(string ReservationID)
        {
            var dbInvoice = new dbTaxHandlerDataContext();
            bool IsCancelled = false;
            try
            {
                string SupplierConfirmNo = GetSupplierConfimation(ReservationID);/*Set Resrvation & Get ConfirmNo*/
                if (SupplierConfirmNo == "")
                    throw new Exception("Invalid Reservation");
                arrBookingID = new List<string>();
                arrBookingID = SupplierConfirmNo.Split('|').ToList();
                bool response = InventoryManager.UpdateInvOnCancellation(arrBookingID.FirstOrDefault());
                if (response || (Resrvation.Status == "Vouchered" || Resrvation.Status == "On Request"))
                {
                    ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                    DBHandlerDataContext DB = new DBHandlerDataContext();
                    using (var cancle = db.Database.BeginTransaction())
                    {
                        if (response)
                        {
                            List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                            List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                            for (int i = 0; i < Record.Count; i++)
                            {
                                tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                                Recordnew.BookingId = Record[i].BookingId;
                                Recordnew.InvSid = Record[i].InvSid;
                                Recordnew.SupplierId = Record[i].SupplierId;
                                Recordnew.HotelCode = Record[i].HotelCode;
                                Recordnew.RoomType = Record[i].RoomType;
                                Recordnew.RateType = Record[i].RateType;
                                Recordnew.InvType = Record[i].InvType;
                                Recordnew.Date = Record[i].Date;
                                Recordnew.Month = Record[i].Month;
                                Recordnew.Year = Record[i].Year;
                                Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                                Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                                Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                                Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                                Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                                Recordnew.UpdateOn = Record[i].UpdateOn;
                                AddRecord.Add(Recordnew);
                            }
                            db.tbl_CommonInventoryRecord.AddRange(AddRecord);
                            db.SaveChanges();
                            cancle.Commit();
                        }
                        foreach (var BookingID in arrBookingID)
                        {
                            tbl_CommonHotelReservation Update = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == BookingID);
                            var CancleDetail = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == BookingID select obj).ToList();
                            var BookingDetail = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == BookingID select obj).FirstOrDefault();
                          
                            
                            List<DateTime> CDate = new List<DateTime>();
                            DateTime CancleDate = DateTime.Now;
                            long Parent = Convert.ToInt64(Update.ParentID);
                            var InvoiceDetails = (from obj in dbInvoice.tbl_Invoices where obj.InvoiceNo == BookingDetail.InvoiceID && obj.AgentID == Update.Uid && obj.ParentID == Parent select obj).ToList();
                            var Commission = "0";

                            decimal CancelAmnt = 0;
                            foreach (var objRoom in CancleDetail)
                            {
                                decimal CanclAmnt = 0;
                                decimal Total = 0;
                                var Cancle = objRoom.CutCancellationDate.Split('|');
                                var Cancleamnt = objRoom.CancellationAmount.Split('|');
                                for (int i = 0; i < Cancle.Length - 1; i++)
                                {
                                    if (CancleDate >= DateTime.ParseExact(Cancle[i], "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture))
                                    {
                                        CanclAmnt = Convert.ToDecimal(Cancleamnt[i]);
                                        //Total = Convert.ToDecimal(CancleDetail[i].RoomAmount);
                                        CancelAmnt +=  CanclAmnt;
                                        break;
                                    }
                                }
                            }
                           
                            DBHelper.DBReturnCode retCode = HotelManager.CancelBooking(BookingID, CancelAmnt.ToString(), Update.Status, "", BookingDetail.TotalFare.ToString(), Commission.ToString(), "");
                            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            {
                                IsCancelled = true;
                                /*Booking Status Mail*/
                                // BookingStatusMail(BookingID);
                                EmailManager.CancellationMail(BookingID, CancelAmnt);
                            }

                        }
                        
                    }
                }
                else
                    throw new Exception("Booking aleready Cancelled.");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return IsCancelled;
        }

        public static string  GetSupplierConfimation(string ReservationID)
        {
            try
            {
                Resrvation = new tbl_CommonHotelReservation();
                using (var db = new DBHandlerDataContext())
                {
                    IQueryable<tbl_CommonHotelReservation> arrrReservation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj);
                    Resrvation = arrrReservation.ToList().FirstOrDefault();
                    return Resrvation.SupplierConfirmNo;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string BookingStatusMail(string BookingID) {
            StringBuilder sb = new StringBuilder();
            try
            {
                Common.Common.BookingID = BookingID;
                using (var db = new DBHandlerDataContext())
                {
                    var sReservation = Common.Common.arrBookingDetails;
                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>    ");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">    ");
                    sb.Append("<div style=\"height:50px;width:auto\">    ");
                    sb.Append("<br />");
                    sb.Append("<img src=" + sReservation.httplogo + "\" alt=\"\" style=\"padding-left:10px;\" />    ");
                    sb.Append("</div>");
                    sb.Append("<br>");
                    sb.Append("<div style=\"margin-left:10%\">");
                    sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">Dear " + sReservation.ContactPerson + ",</span><br /></p>");
                    sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">We have received your request, your "+ sReservation.Status.Replace("Cancelled", "Cancellation") + " Details.</span><br /></p>       ");
                    sb.Append("<style type=\"text/css\">");
                    sb.Append(".tg  {border-collapse:collapse;border-spacing:0;}");
                    sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                    sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                    sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                    sb.Append(".tg .tg-yw4l{vertical-align:top}");
                    sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>    ");
                    sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                    sb.Append("<tr>");
                    sb.Append("<td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.InvoiceID + "</td>  ");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td class=\"tg-9hbo\"><b>Reservation Status</b></td>  ");
                    sb.Append("<td class=\"tg-yw4l\">:" + sReservation.Status + "</td>  ");
                    sb.Append("</tr>  ");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Reservation Date</b></td>  ");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.Bookingdate + "</td>");
                    sb.Append("</tr>  ");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>  ");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.HotelName + "</td>");
                    sb.Append("</tr>  ");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Location</b></td>  ");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.City + "</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Check-In</b></td>  ");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkin + "</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Check-Out </b></td>");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkout + "</td>");
                    sb.Append("</tr>  ");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Rate</b></td>");
                    sb.Append("<td class=\"tg-yw4l\">:" + sReservation.CurrencyCode + " " + Convert.ToInt32(sReservation.Ammount) / Convert.ToInt32(sReservation.Nights) + "</td>");
                    sb.Append("</tr>  ");
                    sb.Append("<tr>  ");
                    sb.Append("<td class=\"tg-9hbo\"><b>Total</b></td>");
                    sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.CurrencyCode + "" + sReservation.Ammount + "</td>  ");
                    sb.Append("</tr>");
                    sb.Append("</table></div>");
                    sb.Append("<p><span style=\\\"margin-left:2%;font-weight:400\\\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
                    sb.Append("<span style=\\\"margin-left:2%\\\">");
                    sb.Append("<b>Thank You,</b><br />");
                    sb.Append("</span>    ");
                    sb.Append("<span style=\\\"margin-left:2%\\\">");
                    sb.Append("Administrator");
                    sb.Append("</span>");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">    ");
                    sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">" + sReservation.SupplierMail + "</div></td>    ");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");
                    string Title = "Hotel "+ sReservation.HotelName + " - "+ sReservation.Status + " on "+ DateTime.Now.ToString("dd/MM/yyyy")+" By "+ sReservation.Passenger;
                    #region Mail Configuration
                    List<string> from = new List<string>();
                    List<string> DocPathList = new List<string>();
                    Dictionary<string, string> to = new Dictionary<string, string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> BccList = new Dictionary<string, string>();
                    string title = "", Voucher="";
                    #endregion
                    to.Add(sReservation.Email, sReservation.ContactPerson);
                    BccList.Add(sReservation.SupplierMail, sReservation.SupplierMail);
                    from.Add(sReservation.SupplierMail);
                    if (!sReservation.Status.Contains("Cancel"))
                    {
                        string Invoice = InvoiceManager.GetInvoice(BookingID, sReservation.Uid, out title);
                        Voucher = VoucherManager.GenrateVoucher(BookingID, sReservation.Uid.ToString() ,sReservation.Status);
                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, BookingID, "Invoice");
                       if (sReservation.Status.Contains("Request"))
                       {
                            if (Invoice != "")
                                DocPathList.Add(Common.Common.url + "/InvoicePdf" + BookingID + "_Invoice.pdf");
                       }
                       else if (sReservation.Status.Contains("Vouchered"))
                        {
                            if (Invoice != "")
                                DocPathList.Add(Common.Common.url + "/InvoicePdf" + BookingID + "_Invoice.pdf");
                            if (Voucher != "")
                                DocPathList.Add(Common.Common.url + "InvoicePdf/" + BookingID + "_Voucher.pdf");
                        }
                    }
                    MailManager.SendMailByAdmin(to, BccList, Title, sb.ToString(), from, DocPathList);
                }
            }
            catch (Exception)
            {
              
            }
            return sb.ToString();
        }

        public static void SaveCancellation(List<tbl_CommonCancelationPolicy> arrCancellation)
        {
           var db = new ClickUrHotel_DBEntities();
            using (var trasction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var objCancellation in arrCancellation)
                    {
                        var arrData = db.tbl_CommonCancelationPolicy.Where(d => d.CancelationPolicy == objCancellation.CancelationPolicy && d.SupplierID == objCancellation.SupplierID).FirstOrDefault();
                        if (arrData == null)
                        {
                            db.tbl_CommonCancelationPolicy.AddRange(arrCancellation);
                            db.SaveChanges();
                        }
                        else
                            objCancellation.CancelationID = arrData.CancelationID;

                    }
                    trasction.Commit();
                }
                catch (Exception)
                {
                    trasction.Dispose();
                }
            }
        }
    }
}