﻿using CommonLib.Response;
using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Globalization;
using CutAdmin.EntityModal;
namespace CutAdmin.DataLayer
{
    public class BookingManager
    {
        public static ReservationDetails objReservation { get; set; }
        #region Trasaction  Validate
        public static float TransactionAmount(Int64 UserID)
        {
            //DBHandlerDataContext db = new DBHandlerDataContext();
            float Amount = 0;
            if (UserID ==232)
            {
                objReservation.TotalPayable = objReservation.objCharges.SupplierTotal;
            }
            else  if (UserID != AccountManager.GetSupplierByUser())
            {
                objReservation.TotalPayable = objReservation.objCharges.CUHTotal;
            }

            else if (UserID != AccountManager.GetUserByLogin())
            {
                objReservation.TotalPayable = objReservation.objCharges.CUHTotal;
            }
            return Amount;
        }

        public static void SaveBookinTax()
        {
            //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
            try
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    List<Comm_BookingTax> ListBookingTax = new List<Comm_BookingTax>();
                    foreach (var objCharge in objReservation.objCharges.HotelTaxes)
                    {
                        string TaxOnDetails = "";
                        if (objCharge.TaxOn != null)
                        foreach (var objTax in objCharge.TaxOn)
                        {
                            TaxOnDetails += objTax.TaxName + ":" + objTax.TaxRate + "^";
                        }
                        ListBookingTax.Add(new Comm_BookingTax
                        {
                            ReservationID = objReservation.ReservationID,
                            RateName = objCharge.RateName,
                            Amount = Convert.ToDecimal(objCharge.TotalRate),
                            RateBreckups = TaxOnDetails
                        });

                    }
                    dbTax.Comm_BookingTaxes.InsertAllOnSubmit(ListBookingTax);
                    dbTax.SubmitChanges();
                }
            }
            catch 
            {

            }
        }

        public static string GetInvoice(string Service, Int64 ParentID)
        {
            using (var db = new dbTaxHandlerDataContext())
            {
                string InvoiceNo = "0";
                string Prefix = "MQ-";
                if (Service == "Visa")
                    Prefix = "VQ-";
                if (Service == "Package")
                    Prefix = "PQ-";
                if (Service == "Hotel")
                    Prefix = "HQ-";
                if (Service == "Slightseeing")
                    Prefix = "SQ-";
                if (Service == "SQ")
                    Prefix = "VQ-";
                if (Service == "Airline")
                    Prefix = "AQ-";
                var arrLastInvoice = 0;
                arrLastInvoice = (from obj in db.tbl_Invoices where obj.ServiceType == Service && obj.ParentID == ParentID select obj).ToList().Count;
                InvoiceNo = Prefix + (arrLastInvoice + 1).ToString("D" + 6);
                return InvoiceNo;
            }
        }

        public static bool CheckMinBalance(Int64 uid, float Purchase)
        {
            //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
            using (var db = new dbTaxHandlerDataContext())
            {
                bool valid = true;
                tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                #region User Balance Checking
                objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
                float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
                float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
                bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
                bool UserOTC = (bool)objUserCredit.OTC;
                float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
                {
                    valid = true;
                }
                else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
                {
                    valid = true;
                }
                else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
                {
                    valid = true;
                }
                else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
                {

                    valid = true;
                }
                else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
                {
                    valid = true;
                }
                else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
                {
                    valid = true;
                }

                else
                {
                    valid = false;
                }
                #endregion
                return valid;
            }
        }

        public static bool DebitAccount(Int64 uid, float Purchase)
        {
            //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
            bool valid = false;
            try
            {
                using (var db = new dbTaxHandlerDataContext())
                {
                    tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                    #region User Balance Checking
                    objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                    float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
                    float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
                    float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
                    bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
                    bool UserOTC = (bool)objUserCredit.OTC;
                    float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                    float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                    if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        valid = true;
                    }
                    else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        float OtcReturn = 0;
                        if (UserAvailableCredit < 0)
                        {
                            OtcReturn = @UserOTClimit + UserAvailableCredit;
                        }
                        if (@UserCreditlimit >= OtcReturn)
                            objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit + OtcReturn);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.@OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.@OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        if (objUserCredit.AvailableCredit < 0)
                            objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit) + Convert.ToDecimal(objUserCredit.AvailableCredit);
                        valid = true;
                    }
                    else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit - Purchase);
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }

                    if (valid)
                        db.SubmitChanges();

                    #endregion
                }
            }
            catch
            {

            }
            return valid;
        }

        public static bool CreditAccount(Int64 uid, float Purchase)
        {
            //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
            bool valid = false;
            try
            {
                using (var db = new dbTaxHandlerDataContext())
                {
                    tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                    #region User Balance Checking
                    objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
                    float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
                    float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
                    float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
                    bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
                    bool UserOTC = (bool)objUserCredit.OTC;
                    float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                    float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
                    if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        valid = true;
                    }
                    else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
                        float OtcReturn = 0;
                        if (UserAvailableCredit < 0)
                        {
                            OtcReturn = @UserOTClimit + UserAvailableCredit;
                        }
                        if (@UserCreditlimit >= OtcReturn)
                            objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit + OtcReturn);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.@OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        objUserCredit.MaxCreditLimit = 0;
                        objUserCredit.MaxCreditLimit_Cmp = 0;
                        objUserCredit.@OTC = false;
                        valid = true;
                    }
                    else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        if (objUserCredit.AvailableCredit < 0)
                            objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit) + Convert.ToDecimal(objUserCredit.AvailableCredit);
                        valid = true;
                    }
                    else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
                    {
                        objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        objUserCredit.CreditAmount = Convert.ToDecimal(@UserAvailableCredit - Purchase);
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }

                    if (valid)
                        db.SubmitChanges();

                    #endregion
                }
            }
            catch
            {

            }
            return valid;
        }
        #endregion

        #region Make Transactions
        public static bool GetnrateTransaction(Int64 uid, Int64 SupplierID, Int64 ParentID,float BaseFare, float TotalFare, string Type, out string InvoiceNo)
        {
            bool Valid = false;
            InvoiceNo = "";
            float BookingAmt = 0;
            try
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    using (var db = new DBHandlerDataContext())
                    {
                    #region Genrate Invoices
                    var arrTransaction = new tbl_Invoice();
                    arrTransaction.AgentID = uid;
                    BookingAmt = TotalFare;
                    string sInvoiceNo = GetInvoice(Type, ParentID);
                    if (!sInvoiceNo.Contains("Test")) /* Invoice Test  */
                    {
                        int Cycle = Convert.ToInt16((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().CommssionsDays);
                        bool RefundCancel = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().RefundCancel);
                        bool onCheckIn = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().CheckInCommission);
                        arrTransaction.Commission = Convert.ToDecimal(objReservation.AdminCommission);
                        arrTransaction.Currency = (from obj in db.tbl_AdminLogins where obj.sid == uid select obj.CurrencyCode).FirstOrDefault();
                        arrTransaction.Deleted = false;
                            if (objReservation.objCharges != null && objReservation.objCharges.HotelTaxes != null)
                                arrTransaction.GstDetails = Convert.ToDecimal(objReservation.objCharges.HotelTaxes.Select(d => d.TotalRate).ToList().Sum());
                            else
                                arrTransaction.GstDetails =0;
                        arrTransaction.InvoiceAmount = Convert.ToDecimal(BookingAmt);
                        arrTransaction.Commission = Convert.ToDecimal(objReservation.AdminCommission);
                        arrTransaction.InvoiceDate = DateTime.Now.ToString("dd/MM/yyyy");
                        arrTransaction.InvoiceNo = sInvoiceNo;
                        arrTransaction.InvoiceType = "Contarcted";
                        arrTransaction.ParentID =  ParentID;
                        arrTransaction.ServiceFee = Convert.ToDecimal(objReservation.AddOnsCharge);
                        arrTransaction.ServiceType = Type;
                        arrTransaction.Status = 1;
                        arrTransaction.TaxCharges = Convert.ToDecimal(0);
                        arrTransaction.TDS = Convert.ToDecimal(0);
                        DateTime CommDate = DateTime.Now;
                        if (onCheckIn)
                            CommDate = DateTime.ParseExact(objReservation.sCheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        else
                        {
                                if((from obj in dbTax.Comm_CommissionReports where obj.SupplierID == SupplierID select obj).ToList()!= null)
                                {
                                    var arrLastReport = (from obj in dbTax.Comm_CommissionReports where obj.SupplierID == SupplierID select obj).ToList().OrderByDescending(d => d.ID).FirstOrDefault();
                                    if (arrLastReport != null)
                                        CommDate = DateTime.ParseExact(arrLastReport.InsertDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddDays(Cycle);
                                }
                         
                        }
                        arrTransaction.CommissionDate = CommDate.ToString("dd-MM-yyyy");
                        arrTransaction.OnCheckIn = onCheckIn;
                        arrTransaction.RefundCancel = RefundCancel;
                        dbTax.tbl_Invoices.InsertOnSubmit(arrTransaction);
                        dbTax.SubmitChanges();
                        InvoiceNo = arrTransaction.InvoiceNo;
                    }
                    else
                        InvoiceNo = sInvoiceNo;

                    #endregion

                    #region Genrate Booking Statments
                    var arrBookingTrasaction = new tbl_BookingTransaction();
                    arrBookingTrasaction.uid = uid;
                    arrBookingTrasaction.ReservationID = objReservation.ReservationID;
                    arrBookingTrasaction.TransactionDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                    arrBookingTrasaction.BookingAmt = Convert.ToDecimal(BookingAmt);
                    arrBookingTrasaction.SeviceTax = Convert.ToDecimal(objReservation.ServiceTax);
                    arrBookingTrasaction.BookingAmtWithTax = Convert.ToDecimal(objReservation.TotalPayable);
                    arrBookingTrasaction.CanAmt = Convert.ToDecimal(objReservation.CancelattionAmount);
                    arrBookingTrasaction.CanServiceTax = Convert.ToDecimal(0);
                    arrBookingTrasaction.CanAmtWithTax = Convert.ToDecimal(objReservation.CancelattionAmount);
                    arrBookingTrasaction.Balance = Convert.ToDecimal((from obj in dbTax.tbl_AdminCreditLimits where obj.uid == uid select obj.AvailableCredit).FirstOrDefault());
                    arrBookingTrasaction.SalesTax = 0;
                    arrBookingTrasaction.LuxuryTax = 0;
                    arrBookingTrasaction.DiscountPer = 0;
                    arrBookingTrasaction.BookingCreationDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                    arrBookingTrasaction.BookingRemarks = objReservation.Remarks;
                    arrBookingTrasaction.BookingCancelFlag = false;
                    arrBookingTrasaction.Supplier = SupplierID.ToString();
                    arrBookingTrasaction.SupplierBasicAmt = Convert.ToDecimal(BaseFare);
                    arrBookingTrasaction.AgentComm = Convert.ToDecimal(objReservation.AdminCommission);
                    arrBookingTrasaction.CUTComm = Convert.ToDecimal(objReservation.AdminCommission);
                    arrBookingTrasaction.BookedBy = AccountManager.GetUserByLogin().ToString();
                    arrBookingTrasaction.AffilateCode = objReservation.AffilateCode;
                    arrBookingTrasaction.Status = "Booking Success";
                    dbTax.tbl_BookingTransactions.InsertOnSubmit(arrBookingTrasaction);
                    dbTax.SubmitChanges();
                    var arrParticulars = new tbl_Transaction();
                    arrParticulars.uid = uid;
                    arrParticulars.TransactionID = arrBookingTrasaction.sid;
                    arrParticulars.Particulars = "On Booking " + Type + " : " + objReservation.ReservationID;
                    arrParticulars.UpdatedBy = AccountManager.GetUserByLogin().ToString();
                    arrParticulars.CreditedAmount = 0;
                    arrParticulars.DebitedAmount = Convert.ToDecimal(BookingAmt);
                    arrParticulars.Balance = Convert.ToDecimal((from obj in dbTax.tbl_AdminCreditLimits where obj.uid == uid select obj.AvailableCredit).FirstOrDefault());
                    arrParticulars.ReservationID = objReservation.ReservationID;
                    arrParticulars.ReferenceNo = InvoiceNo;
                    arrParticulars.TransactionDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                    dbTax.tbl_Transactions.InsertOnSubmit(arrParticulars);
                    db.SubmitChanges();
                    dbTax.SubmitChanges();
                    Valid = true;
                    }
                    #endregion
                }
            }
            catch
            {

            }
            return Valid;
        }

        public static DBHelper.DBReturnCode BookingTransact(Int64 UserID, Int64 SupplierID,Int64 ParentID,float BaseFare, float TotalFare)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            DBHelper.DBReturnCode Transaction = DBHelper.DBReturnCode.EXCEPTION;
            bool transaction = false;
            try
            {
                string InvoiceNo = "-";
                #region Genrate Transactions
                Int64 uid = Convert.ToInt64(UserID);
                SaveBookinTax(); /*Tax Breckups*/
                transaction = DebitAccount(uid, TotalFare);
                if (transaction)
                {
                    GetnrateTransaction(uid, SupplierID, ParentID, BaseFare, TotalFare, "Hotel", out InvoiceNo); /* To Agent  Statments */
                }
                else
                {
                    transaction = false;
                }
              
                if (transaction)
                {
                    var arrResrvation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == objReservation.ReservationID select obj).FirstOrDefault();
                    arrResrvation.InvoiceID = InvoiceNo;
                    db.SubmitChanges();
                    Transaction = DBHelper.DBReturnCode.SUCCESS;
                }
               
                else
                {
                    Transaction = DBHelper.DBReturnCode.EXCEPTION;
                }
                #endregion
            }
            catch
            {

            }
            return Transaction;
        }
        #endregion

        #region Cancel Confirm Booking
        public static string GenrateCancellationMail(string ReservationID, Int64 UID, string User)
        {
            dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
            DBHandlerDataContext db = new DBHandlerDataContext();
            StringBuilder sMail = new StringBuilder();
            string Url = ConfigurationManager.AppSettings["Domain"];
            var arrReservation = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
            if (arrReservation != null)
            {

                var arrUserDetails = (from obj in db.tbl_AdminLogins where obj.sid == UID select obj).FirstOrDefault();
                var arrBookingTrasaction = (from obj in dbTax.tbl_BookingTransactions where obj.ReservationID == ReservationID && obj.uid == UID select obj).FirstOrDefault();
                sMail.Append("<!DOCTYPE html>  <html lang=\"en\"  xmlns=\"http://www.w3.org/1999/xhtml\"> <head>  ");
                sMail.Append("<meta charset=\"utf-8\" /> </head>");
                sMail.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
                sMail.Append("<div style=\"height:50px;width:auto\"><br /> ");
                if (User == "FR")
                    sMail.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
                else
                    sMail.Append("<img src=\"" + Url + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
                sMail.Append("</div>   ");
                sMail.Append("<div style=\"margin-left:10%\">  ");
                sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hi " + arrUserDetails.AgencyName + ",</span><br /></p> ");
                sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p> ");
                sMail.Append("<style type=\"text/css\">  ");
                sMail.Append(".tg  {border-collapse:collapse;border-spacing:0;}   ");
                sMail.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                sMail.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                sMail.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}  ");
                sMail.Append(".tg .tg-yw4l{vertical-align:top}   ");
                sMail.Append(" @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>   ");
                sMail.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                sMail.Append(" <td class=\"tg-yw4l\">:  " + ReservationID + "</td></tr>");
                sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Reservation Status</b></td>    ");
                sMail.Append("<td class=\"tg-yw4l\">:" + arrReservation.BookingStatus + "</td></tr>");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Reservation Date</b></td>       ");
                sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.ReservationDate + "</td></tr>");
                sMail.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>                 ");
                sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.HotelName + "</td></tr>");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Location</b></td>               ");
                sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.City + "</td></tr>                 ");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-In</b></td>               ");
                sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckIn + "</td></tr>");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-Out </b> </td>            ");
                sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckOut + "</td></tr>");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Rate</b></td> ");
                sMail.Append("<td class=\"tg-yw4l\">:" + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt / arrReservation.NoOfDays) + "</td></tr>");
                sMail.Append("<tr><td class=\"tg-9hbo\"><b>Total  </b></td>");
                sMail.Append("<td class=\"tg-yw4l\">:  " + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt) + "</td></tr>");
                sMail.Append("</table></div>");
                sMail.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
                sMail.Append("<span style=\"margin-left:2%\">");
                sMail.Append("<b>Thank You,</b><br />                                                                                                                        ");
                sMail.Append("</span>");
                sMail.Append("<span style=\"margin-left:2%\">");
                sMail.Append("Administrator");
                sMail.Append("</span>");
                sMail.Append("</div>");
                sMail.Append("<div>");
                sMail.Append(" <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sMail.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                //if (User == "FR")
                //     sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  ClickurTrip.com</div></td>");
                //else
                sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + ConfigurationManager.AppSettings["DomainUser"] + "</div></td>");
                sMail.Append("</tr> </table></div>");
                sMail.Append("</body></html>");


                var arrEmails = new tbl_ActivityMail();
                if (User != "FR")
                    arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == arrUserDetails.ParentID && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();
                else
                    arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == 232 && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();

                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                List<string> from = new List<string>();
                List<string> DocPathList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                Dictionary<string, string> BccList = new Dictionary<string, string>();
                if (arrEmails.Email != "")
                    from.Add(Convert.ToString(arrEmails.Email));
                else
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                foreach (var Mails in arrEmails.CcMail.Split(','))
                {
                    if (Mails != "")
                    {
                        BccList.Add(Mails, Mails); ;
                    }
                }
                if (User != "AG" && BccList.Count == 0)
                {
                    var arrAdmin = (from obj in db.tbl_AdminLogins where obj.sid == arrUserDetails.ParentID select obj).First();
                    BccList.Add(arrAdmin.AgencyName, arrAdmin.uid);
                }

                Email1List.Add(arrUserDetails.AgencyName, arrUserDetails.uid);
                string title = "Cancel Confirmatio : " + ReservationID;
                MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
            }

            return sMail.ToString();
        }

        #endregion

        #region AgentDetails  On Admin
        public static bool GetAgentDetails(Int64 uid)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            var list = (from obj in db.tbl_AdminLogins where obj.sid == uid select obj).FirstOrDefault();

            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            objAgentDetailsOnAdmin.sid = list.sid;
            objAgentDetailsOnAdmin.Currency = list.CurrencyCode;
            objAgentDetailsOnAdmin.UserType = list.UserType;
            objAgentDetailsOnAdmin.AgencyName = list.AgencyName;
            objAgentDetailsOnAdmin.Agentuniquecode = list.Agentuniquecode;

            HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
            return true;

        }
        #endregion

        #region Commission Calculations
        public static float AdminCommission(Int64 HotelCode,Int64 Supplier, Int64 noNights)
        {
            float AdminCommission = 0;
            try
            {
                Int64 Uid=0;
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Login Failed.");
                Uid = AccountManager.GetSupplierByUser();
                DBHandlerDataContext db = new DBHandlerDataContext();
                ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
                dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                string Category = (from obj in cdb.tbl_CommonHotelMaster where obj.sid == HotelCode select obj).FirstOrDefault().HotelCategory;
                var arrCommission = (from obj in dbTax.Comm_Commissions where obj.GroupID == Convert.ToInt64(Category) && obj.SupplierID == Supplier select obj).FirstOrDefault();
                if (arrCommission != null)
                {
                    if (arrCommission.PerNight == true)
                    {
                        AdminCommission = Convert.ToSingle(arrCommission.Commission) * noNights;
                    }
                    else
                    {
                        AdminCommission = Convert.ToSingle(arrCommission.Commission);
                    }
                }
                else
                    AdminCommission = 0;

            }
            catch(Exception ex)
            {
                 
            }
            return AdminCommission;
        }
        #endregion

        #region Confirm OnRequest Booking
        public static DBHelper.DBReturnCode ConfirmOnRequestBooking(string ReservationID)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
            DBHelper.DBReturnCode confirmed = DBHelper.DBReturnCode.EXCEPTION;
            bool response = true;

            var ListHotelReservation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();

            var ListBookedRoom = (from obj in db.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();

            var RoomType = ListBookedRoom.FirstOrDefault().RoomType;

            var RoomCode = (from obj in cdb.tbl_commonRoomType where obj.RoomType == RoomType select obj.RoomTypeID).FirstOrDefault();

            response = InventoryManager.CheckInventoryConFirm(ListHotelReservation.HotelCode, RoomCode.ToString(), ListHotelReservation.ParentID.ToString(), ListHotelReservation.CheckIn, ListHotelReservation.CheckOut, ListBookedRoom.FirstOrDefault().RateType, ListHotelReservation.Source, ListHotelReservation.TotalRooms.ToString());

            if (response)
                response = InventoryManager.UpdateInventory(ReservationID, ListHotelReservation.HotelCode, RoomCode.ToString(), ListHotelReservation.ParentID.ToString(), ListHotelReservation.CheckIn, ListHotelReservation.CheckOut, ListBookedRoom.FirstOrDefault().RateType, ListHotelReservation.Source, ListHotelReservation.TotalRooms.ToString());
          
            if(response)
            {
                //List<RoomType> ListRate = new List<RoomType>();
                //List<date> Dates = new List<date>();
                //var calendar = new List<DateTime>();
                //DateTime RateCheckin = DateTime.ParseExact(ListHotelReservation.CheckIn, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //DateTime RateCheckOut = DateTime.ParseExact(ListHotelReservation.CheckOut, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //while (RateCheckin < RateCheckOut)
                //{
                //    Dates.Add(new date { datetime = RateCheckin.AddDays(1).ToString("dd-MM-yyyy"), Type="Contracted" });
                //    RateCheckin = RateCheckin.AddDays(1);
                //}
                //foreach (var objRoom in ListBookedRoom)
                //{
                //    ListRate.Add(new RoomType
                //    {
                //        RoomTypeId = objRoom.MealPlanID,
                //        Dates = Dates,
                //    });
                //}
                //response = InventoryManager.UpdateInventory(ListRate, ReservationID, ListHotelReservation.HotelCode, Convert.ToInt64(ListHotelReservation.Source));
                List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                using (var Booking = cdb.Database.BeginTransaction())
                {
                    List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                    for (int i = 0; i < Record.Count; i++)
                    {
                        tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                        Recordnew.BookingId = Record[i].BookingId;
                        Recordnew.InvSid = Record[i].InvSid;
                        Recordnew.SupplierId = Record[i].SupplierId;
                        Recordnew.HotelCode = Record[i].HotelCode;
                        Recordnew.RoomType = Record[i].RoomType;
                        Recordnew.RateType = Record[i].RateType;
                        Recordnew.InvType = Record[i].InvType;
                        Recordnew.Date = Record[i].Date;
                        Recordnew.Month = Record[i].Month;
                        Recordnew.Year = Record[i].Year;
                        Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                        Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                        Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                        Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                        Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                        Recordnew.UpdateOn = Record[i].UpdateOn;
                        AddRecord.Add(Recordnew);
                    }
                    cdb.tbl_CommonInventoryRecord.AddRange(AddRecord);
                    cdb.SaveChanges();
                    Booking.Commit();
                }
              
            }
         
            if (response == false)
                return confirmed = DBHelper.DBReturnCode.SUCCESSNORESULT;


            #region Booking Transactions
            CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
            objReservation.objCharges = new ServiceCharge();
            objReservation.objCharges.TotalPrice = (float)ListHotelReservation.TotalFare;
            objReservation.AdminCommission = 0;
            Int64 noNights = Convert.ToInt64(ListHotelReservation.NoOfDays);
            objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(ListHotelReservation.HotelCode), Convert.ToInt64(ListHotelReservation.Source), noNights);
            //objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
            CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
            objReservation.TotalPayable = objReservation.objCharges.TotalPrice;
            #region Hotel Booking Details
            var arrHotel = (from obj in cdb.tbl_CommonHotelMaster where obj.sid.ToString() == ListHotelReservation.HotelCode select obj).FirstOrDefault();
            tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
            HotelReserv.ReservationID = Convert.ToString(ReservationID);
            HotelReserv.HotelCode = ListHotelReservation.HotelCode;
            HotelReserv.HotelName = arrHotel.HotelName;
            HotelReserv.mealplan = ListHotelReservation.mealplan;
            HotelReserv.TotalRooms = Convert.ToInt16(ListHotelReservation.TotalRooms);
            //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
            HotelReserv.RoomCode = "";
            HotelReserv.sightseeing = 0;
            HotelReserv.Source = ListHotelReservation.Source;
            if (response)
                HotelReserv.Status = "Vouchered";

            HotelReserv.SupplierCurrency = "SAR";
            HotelReserv.terms = arrHotel.HotelNote;
            HotelReserv.Uid = ListHotelReservation.Uid;
            HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
            HotelReserv.Updateid = ListHotelReservation.Uid.ToString();
            HotelReserv.VoucherID = "VCH-" + ReservationID;
            HotelReserv.DeadLine = "";
            HotelReserv.AgencyName = "";
            HotelReserv.AgentContactNumber = "";
            HotelReserv.AgentEmail = "";
            HotelReserv.AgentMarkUp_Per = 0;
            HotelReserv.AgentRef = "";
            HotelReserv.AgentRemark = "";
            HotelReserv.AutoCancelDate = "";
            HotelReserv.AutoCancelTime = "";
            HotelReserv.bookingname = "";
            HotelReserv.BookingStatus = "";
            HotelReserv.CancelDate = "";
            HotelReserv.CancelFlag = false;
            HotelReserv.CheckIn = Convert.ToString(ListHotelReservation.CheckIn);
            HotelReserv.CheckOut = Convert.ToString(ListHotelReservation.CheckOut);
            HotelReserv.ChildAges = "";
            HotelReserv.Children = ListHotelReservation.Children;
            HotelReserv.City = arrHotel.CityId;
            HotelReserv.deadlineemail = false;
            HotelReserv.Discount = 0;
            HotelReserv.ExchangeValue = 0;
            HotelReserv.ExtraBed = 0;
            HotelReserv.GTAId = "";
            HotelReserv.holdbooking = 0;
            HotelReserv.HoldTime = "";
            HotelReserv.HotelBookingData = "";
            HotelReserv.HotelDetails = "";
            HotelReserv.Infants = 0;
            HotelReserv.InvoiceID = "-";
            HotelReserv.IsAutoCancel = false;
            HotelReserv.IsConfirm = false;
            HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
            HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
            HotelReserv.LuxuryTax = 0;
            HotelReserv.mealplan_Amt = 0;
            HotelReserv.NoOfAdults = ListHotelReservation.NoOfAdults;
            Int32 night = Convert.ToInt16(ListHotelReservation.NoOfDays);
            //night = night - 1;
            // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
            HotelReserv.NoOfDays = Convert.ToInt16(night);
            HotelReserv.OfferAmount = 0;
            HotelReserv.OfferId = 0;
            HotelReserv.RefAgency = "";
            HotelReserv.ReferenceCode = "";
            HotelReserv.Remarks = "";
            HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
            HotelReserv.ReservationTime = "";
            HotelReserv.RoomRate = 0;
            HotelReserv.SalesTax = 0;
            HotelReserv.Servicecharge = 0;
            // HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
            HotelReserv.ComparedCurrency = "";
            // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
            // HotelReserv.TotalFare = Total;
            HotelReserv.TotalFare = Convert.ToDecimal(ListHotelReservation.TotalFare);

            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if (objGlobalDefault.UserType != "Agent")
            {
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                objAgentDetailsOnAdmin.sid = Convert.ToInt64(ListHotelReservation.Uid);
                HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
            }
            #endregion
            objReservation.objHotelDetails = new tbl_CommonHotelMaster();
            objReservation.objHotelDetails = arrHotel;
            objReservation.ReservationID = ReservationID;
            //objReservation.objCharges.HotelTaxes=
            BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelMaster();
            BookingManager.objReservation.objHotelDetails = arrHotel;

            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            if (HotelReserv.Status == "Vouchered")
            {
                retCode = CutAdmin.DataLayer.BookingManager.BookingTransact(Convert.ToInt64(ListHotelReservation.Uid), Convert.ToInt64(ListHotelReservation.Source), Convert.ToInt64(ListHotelReservation.ParentID), Convert.ToSingle(ListHotelReservation.ComparedFare), Convert.ToSingle(ListHotelReservation.TotalFare));
            }
            else
            {
                retCode = DBHelper.DBReturnCode.SUCCESS;
            }
            #endregion

            return confirmed = retCode;

        }
        #endregion


        #region Confirm Hold Booking

        public static DBHelper.DBReturnCode ConfirmHoldBooking(string ReservationID)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
            DBHelper.DBReturnCode confirmed = DBHelper.DBReturnCode.EXCEPTION;
            bool response = true;

            var ListHotelReservation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();

            var ListBookedRoom = (from obj in db.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).FirstOrDefault();

            var RoomCode = (from obj in cdb.tbl_commonRoomType where obj.RoomType == ListBookedRoom.RoomType select obj.RoomTypeID).FirstOrDefault();

            //response = InventoryManager.CheckInventoryConFirm(ListHotelReservation.HotelCode, RoomCode.ToString(), ListHotelReservation.ParentID.ToString(), ListHotelReservation.CheckIn, ListHotelReservation.CheckOut, ListBookedRoom.RateType, ListHotelReservation.Source, ListHotelReservation.TotalRooms.ToString());

            //if (response)
            //    response = InventoryManager.UpdateInventory(ReservationID, ListHotelReservation.HotelCode, RoomCode.ToString(), ListHotelReservation.ParentID.ToString(), ListHotelReservation.CheckIn, ListHotelReservation.CheckOut, ListBookedRoom.RateType, ListHotelReservation.Source, ListHotelReservation.TotalRooms.ToString());

            //if (response == false)
            //    return confirmed = DBHelper.DBReturnCode.SUCCESS;


            #region Booking Transactions
            CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
            objReservation.objCharges = new ServiceCharge();
            objReservation.objCharges.TotalPrice = (float)ListHotelReservation.TotalFare;
            objReservation.AdminCommission = 0;
            Int64 noNights = Convert.ToInt64(ListHotelReservation.NoOfDays);
            objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(ListHotelReservation.HotelCode),Convert.ToInt64(ListHotelReservation.Source), noNights);
            //objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
            CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
            objReservation.TotalPayable = objReservation.objCharges.TotalPrice;
            #region Hotel Booking Details
            var arrHotel = (from obj in cdb.tbl_CommonHotelMaster where obj.sid == Convert.ToInt64(ListHotelReservation.HotelCode) select obj).FirstOrDefault();
            tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
            HotelReserv.ReservationID = Convert.ToString(ReservationID);
            HotelReserv.HotelCode = ListHotelReservation.HotelCode;
            HotelReserv.HotelName = arrHotel.HotelName;
            HotelReserv.mealplan = ListHotelReservation.mealplan;
            HotelReserv.TotalRooms = Convert.ToInt16(ListHotelReservation.TotalRooms);
            //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
            HotelReserv.RoomCode = "";
            HotelReserv.sightseeing = 0;
            HotelReserv.Source = ListHotelReservation.Source;
            if (response)
                HotelReserv.Status = "Vouchered";

            HotelReserv.SupplierCurrency = "SAR";
            HotelReserv.terms = arrHotel.HotelNote;
            HotelReserv.Uid = ListHotelReservation.Uid;
            HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
            HotelReserv.Updateid = ListHotelReservation.Uid.ToString();
            HotelReserv.VoucherID = "VCH-" + ReservationID;
            HotelReserv.DeadLine = "";
            HotelReserv.AgencyName = "";
            HotelReserv.AgentContactNumber = "";
            HotelReserv.AgentEmail = "";
            HotelReserv.AgentMarkUp_Per = 0;
            HotelReserv.AgentRef = "";
            HotelReserv.AgentRemark = "";
            HotelReserv.AutoCancelDate = "";
            HotelReserv.AutoCancelTime = "";
            HotelReserv.bookingname = "";
            HotelReserv.BookingStatus = "";
            HotelReserv.CancelDate = "";
            HotelReserv.CancelFlag = false;
            HotelReserv.CheckIn = Convert.ToString(ListHotelReservation.CheckIn);
            HotelReserv.CheckOut = Convert.ToString(ListHotelReservation.CheckOut);
            HotelReserv.ChildAges = "";
            HotelReserv.Children = ListHotelReservation.Children;
            HotelReserv.City = arrHotel.CityId;
            HotelReserv.deadlineemail = false;
            HotelReserv.Discount = 0;
            HotelReserv.ExchangeValue = 0;
            HotelReserv.ExtraBed = 0;
            HotelReserv.GTAId = "";
            HotelReserv.holdbooking = 0;
            HotelReserv.HoldTime = "";
            HotelReserv.HotelBookingData = "";
            HotelReserv.HotelDetails = "";
            HotelReserv.Infants = 0;
            HotelReserv.InvoiceID = "-";
            HotelReserv.IsAutoCancel = false;
            HotelReserv.IsConfirm = false;
            HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
            HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
            HotelReserv.LuxuryTax = 0;
            HotelReserv.mealplan_Amt = 0;
            HotelReserv.NoOfAdults = ListHotelReservation.NoOfAdults;
            Int32 night = Convert.ToInt16(ListHotelReservation.NoOfDays);
            night = night - 1;
            // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
            HotelReserv.NoOfDays = Convert.ToInt16(night);
            HotelReserv.OfferAmount = 0;
            HotelReserv.OfferId = 0;
            HotelReserv.RefAgency = "";
            HotelReserv.ReferenceCode = "";
            HotelReserv.Remarks = "";
            HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
            HotelReserv.ReservationTime = "";
            HotelReserv.RoomRate = 0;
            HotelReserv.SalesTax = 0;
            HotelReserv.Servicecharge = 0;
           // HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
            HotelReserv.ComparedCurrency = "";
            // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
            // HotelReserv.TotalFare = Total;
            HotelReserv.TotalFare = Convert.ToDecimal(ListHotelReservation.TotalFare);

            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if(objGlobalDefault.UserType != "Agent")
            {
             AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
             objAgentDetailsOnAdmin.sid =Convert.ToInt64(ListHotelReservation.Uid);
             HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
            }
            #endregion
            objReservation.objHotelDetails = new tbl_CommonHotelMaster();
            objReservation.objHotelDetails = arrHotel;
            objReservation.ReservationID = ReservationID;
           //objReservation.objCharges.HotelTaxes=
            BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelMaster();
            BookingManager.objReservation.objHotelDetails = arrHotel;

            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            if (HotelReserv.Status == "Vouchered")
            {
                retCode = CutAdmin.DataLayer.BookingManager.BookingTransact(Convert.ToInt64(ListHotelReservation.Uid), Convert.ToInt64(ListHotelReservation.Source), Convert.ToInt64(ListHotelReservation.ParentID), Convert.ToSingle(ListHotelReservation.ComparedFare), Convert.ToSingle(ListHotelReservation.TotalFare));
            }
            else
            {
                retCode = DBHelper.DBReturnCode.SUCCESS;
            }
            #endregion

            return confirmed = retCode;

        }

        #endregion        

    }
}