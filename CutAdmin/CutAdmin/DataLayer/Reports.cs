﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class Reports
    {
        public int Sid { get; set; }
        public string ReservationID { get; set; }
        public string ReservationDate { get; set; }
        public string AgencyName { get; set; }
        public string Status { get; set; }
        public string bookingname { get; set; }
        public decimal? TotalFare { get; set; }
        public int TotalRooms { get; set; }
        public string HotelName { get; set; }
        public string sCheckIn { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string Nationality { get; set; }
        public string City { get; set; }
        public int? Children { get; set; }
        public string BookingStatus { get; set; }
        public int? NoOfAdults { get; set; }
        public Int64 Uid { get; set; }
        public string CurrencyCode { get; set; }
        public bool? IsConfirm { get; set; }
        public string ConfNo { get; set; }
        public string Roomtype { get; set; }
        public string View { get; set; }
        public int Night { get; set; }
        public string Meals { get; set; }
        public decimal? PricePerRoom { get; set; }
        public decimal? MealPerPax { get; set; }

        public static List<Reports> ReportList()
        {
            List<Reports> ListReport = new List<Reports>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = 0;
            if (objGlobalDefault.UserType != "Hotel")
               Uid = AccountManager.GetUserByLogin();
            else
                Uid= AccountManager.GetSupplierByUser();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    IQueryable<tbl_CommonHotelReservation> arrResult = DB.tbl_CommonHotelReservations.Where(x => x.ParentID == Uid && x.Status == "Vouchered");
                    arrResult.ToList();
                    foreach (tbl_CommonHotelReservation Resrvation in arrResult.ToList())
                    {
                        IQueryable<tbl_CommonBookedRoom> arrRoom = from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == Resrvation.ReservationID select obj;
                        foreach (tbl_CommonBookedRoom objRoom in arrRoom.ToList().GroupBy(d => d.RoomType).Select(r => r.FirstOrDefault()).ToList())
                        {
                            ListReport.Add(new Reports
                            {
                                ReservationID = Resrvation.ReservationID,
                                bookingname = Resrvation.bookingname,
                                NoOfAdults = Resrvation.NoOfAdults,
                                Children = Resrvation.Children,
                                ConfNo = Resrvation.HotelConfirmationNo,
                                Roomtype = objRoom.RoomType,
                                TotalRooms = Convert.ToInt32(objRoom.TotalRooms),
                                CheckIn = Resrvation.CheckIn,
                                CheckOut = Resrvation.CheckOut,
                                Night = Convert.ToInt16(Resrvation.NoOfDays),
                                Meals = objRoom.BoardText,
                                Status = Resrvation.Status,
                                PricePerRoom = objRoom.RoomAmount,
                                MealPerPax = 0,
                                TotalFare = Resrvation.TotalFare,
                                HotelName = Resrvation.HotelName,
                                BookingStatus = Resrvation.BookingStatus,
                                CurrencyCode = Resrvation.SupplierCurrency,
                                IsConfirm = Resrvation.IsConfirm,
                                Uid = Convert.ToInt64(Resrvation.Uid),
                            });
                        }

                    }
                    ListReport.GroupBy(d => d.HotelName).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return ListReport;
        }

        public static List<Reservations> ReportListHotlier()
        {
            List<Reservations> ListReservation = new List<Reservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var arrGroup = (from obj in DB.tbl_Comm_HotelGroups
                                    join objHotel in DB.tbl_Comm_HotelGroupMappings on obj.Group_id equals objHotel.Group_id
                                    where obj.UniqueCode == Uid.ToString()
                                    select new
                                    {
                                        HotelCode = objHotel.Hotel_Code,
                                        Supplierid = obj.SupplierId
                                    }).ToList();


                    var arrReservations = new List<Reservations>();
                    foreach (var arrobj in arrGroup)
                    {
                        arrReservations = (from obj in DB.tbl_CommonHotelReservations
                                           join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                           join objRoom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objRoom.ReservationID
                                           where obj.HotelCode == arrobj.HotelCode && obj.ParentID == arrobj.Supplierid
                                           select new Reservations
                                           {
                                               AgencyName = AgName.AgencyName,
                                               bookingname = obj.bookingname,
                                               BookingStatus = obj.BookingStatus,
                                               CurrencyCode = AgName.CurrencyCode,
                                               IsConfirm = obj.IsConfirm,
                                               CheckIn = obj.CheckIn,
                                               sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                               CheckOut = obj.CheckOut,
                                               Children = obj.Children,
                                               ConfNo = obj.HotelConfirmationNo,
                                               Night = Convert.ToInt16(obj.NoOfDays),
                                               Meals = objRoom.BoardText,
                                               City = obj.City,
                                               HotelName = obj.HotelName,
                                               LatitudeMGH = obj.LatitudeMGH,
                                               LongitudeMGH = obj.LongitudeMGH,
                                               NoOfAdults = obj.NoOfAdults,
                                               PricePerRoom = objRoom.RoomAmount,
                                               MealPerPax = 0,
                                               ReservationDate = obj.ReservationDate,
                                               ReservationID = obj.ReservationID,
                                               Sid = obj.Sid,
                                               Roomtype = objRoom.RoomType,
                                               Source = obj.Status,
                                               Status = obj.Status,
                                               TotalFare = obj.TotalFare,
                                               TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                               Uid = Convert.ToInt64(obj.Uid),

                                           }).Distinct().ToList();
                        foreach (var obj in arrReservations)
                        {
                            ListReservation.Add(obj);
                        }
                    }
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();


                    //foreach (tbl_CommonHotelReservation Resrvation in ListReservation)
                    //{
                    //    IQueryable<tbl_CommonBookedRoom> arrRoom = from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == Resrvation.ReservationID select obj;
                    //    foreach (tbl_CommonBookedRoom objRoom in arrRoom.ToList().GroupBy(d => d.RoomType).Select(r => r.FirstOrDefault()).ToList())
                    //    {
                    //        ListReport.Add(new Reports
                    //        {
                    //            ReservationID = Resrvation.ReservationID,
                    //            bookingname = Resrvation.bookingname,
                    //            NoOfAdults = Resrvation.NoOfAdults,
                    //            Children = Resrvation.Children,
                    //            ConfNo = Resrvation.HotelConfirmationNo,
                    //            Roomtype = objRoom.RoomType,
                    //            TotalRooms = Convert.ToInt32(objRoom.TotalRooms),
                    //            CheckIn = Resrvation.CheckIn,
                    //            CheckOut = Resrvation.CheckOut,
                    //            Night = Convert.ToInt16(Resrvation.NoOfDays),
                    //            Meals = objRoom.BoardText,
                    //            Status = Resrvation.Status,
                    //            PricePerRoom = objRoom.RoomAmount,
                    //            MealPerPax = 0,
                    //            TotalFare = Resrvation.TotalFare,
                    //            HotelName = Resrvation.HotelName,
                    //            BookingStatus = Resrvation.BookingStatus,
                    //            CurrencyCode = Resrvation.SupplierCurrency,
                    //            IsConfirm = Resrvation.IsConfirm,
                    //            Uid = Convert.ToInt64(Resrvation.Uid),
                    //        });
                    //    }

                    //}
                    ListReservation.GroupBy(d => d.HotelName).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return ListReservation;
        }


        public static List<Reports> ReportForAdmin()
        {
            List<Reports> ListReport = new List<Reports>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var arrResult = (from obj in DB.tbl_CommonHotelReservations
                                     join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                     where AgName.UserType == "Supplier" && obj.Uid == AgName.sid && obj.Uid != 232 && obj.Status == "Vouchered"
                                     select new
                                     {
                                         obj.ReservationID,
                                         obj.bookingname,
                                         obj.NoOfAdults,
                                         obj.Children,
                                         obj.HotelConfirmationNo,
                                         obj.TotalRooms,
                                         obj.CheckIn,
                                         obj.CheckOut,
                                         obj.NoOfDays,
                                         obj.Status,
                                         obj.TotalFare,
                                         obj.HotelName,
                                         obj.BookingStatus,
                                         obj.SupplierCurrency,
                                         obj.IsConfirm,
                                         obj.Uid,
                                     }).ToList();
                    foreach (var Resrvation in arrResult)
                    {
                        IQueryable<tbl_CommonBookedRoom> arrRoom = from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == Resrvation.ReservationID select obj;
                        foreach (tbl_CommonBookedRoom objRoom in arrRoom.ToList().GroupBy(d => d.RoomType).Select(r => r.FirstOrDefault()).ToList())
                        {
                            ListReport.Add(new Reports
                            {
                                ReservationID = Resrvation.ReservationID,
                                bookingname = Resrvation.bookingname,
                                NoOfAdults = Resrvation.NoOfAdults,
                                Children = Resrvation.Children,
                                ConfNo = Resrvation.HotelConfirmationNo,
                                Roomtype = objRoom.RoomType,
                                TotalRooms = Convert.ToInt32(objRoom.TotalRooms),
                                CheckIn = Resrvation.CheckIn,
                                CheckOut = Resrvation.CheckOut,
                                Night = Convert.ToInt16(Resrvation.NoOfDays),
                                Meals = objRoom.BoardText,
                                Status = Resrvation.Status,
                                PricePerRoom = objRoom.RoomAmount,
                                MealPerPax = 0,
                                TotalFare = Resrvation.TotalFare,
                                HotelName = Resrvation.HotelName,
                                BookingStatus = Resrvation.BookingStatus,
                                CurrencyCode = Resrvation.SupplierCurrency,
                                IsConfirm = Resrvation.IsConfirm,
                                Uid = Convert.ToInt64(Resrvation.Uid),
                            });
                        }

                    }
                    ListReport.GroupBy(d => d.HotelName).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return ListReport;
        }
    }
}