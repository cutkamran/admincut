﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelGroup.aspx.cs" Inherits="CutAdmin.HotelGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <link href="css/dynamicDiv.css" rel="stylesheet" />
     <script src="Scripts/Alert.js?v=1.1"></script>
    <script src="Scripts/CountryCityCode.js"></script>
    <script src="Scripts/HotelGroup.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
           <h1>Hotel Group </h1>
            <h2><a target="_blank" onclick="SaveHotelModal()" class="addnew" style=""><i class="fa fa-user-plus"></i></a></h2>
            <hr />

        </hgroup>

        <div class="with-small-padding">
                <table class="table responsive-table font10" id="tbl_HotelGroup">
                    <thead>
                        <tr>
                            <th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
                            <th scope="col" class="align-center hide-on-mobile">Group Name</th>
                             <th scope="col" class="align-center hide-on-mobile">Email</th>                       
                            <th scope="col" class="align-center hide-on-mobile">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
     
        </div>
    </section>
</asp:Content>
