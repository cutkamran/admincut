﻿
using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for HotelGroupHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelGroupHandler : System.Web.Services.WebService
    {
        dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        DBHandlerDataContext db = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();



        [WebMethod(EnableSession = true)]
        public string SaveHotelGroup(string Name, string Hotels, string Email, string Phone, string Country, string City)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                Int64 Grupid = 0;
                Int64 ContactID = 0;
                string UniqCode = "";
                Int64 Sidd = 0;
                using (var DB = new DBHandlerDataContext())
                {   
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again");

                     GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Uid = AccountManager.GetSupplierByUser();

                    if (DB.tbl_Comm_HotelGroups.Where(d => d.Group_Name == Name && d.SupplierId == Uid).ToList().Count != 0)
                    {

                        return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add this." });
                    }

                    else
                    {
                        tbl_Comm_HotelGroup add = new tbl_Comm_HotelGroup();
                        add.Group_Name = Name;
                        add.SupplierId = Uid;
                        DB.tbl_Comm_HotelGroups.InsertOnSubmit(add);
                        DB.SubmitChanges();
                        Grupid = add.Group_id;                       
                    }
                

                string[] Hotel = Hotels.Split(',');
                List<tbl_Comm_HotelGroupMapping> list = new List<tbl_Comm_HotelGroupMapping>();
              
                    foreach (var item in Hotel)
                    {
                        tbl_Comm_HotelGroupMapping Add = new tbl_Comm_HotelGroupMapping();
                        Add.Group_id = Grupid;
                        Add.Hotel_Code = item.ToString();
                        list.Add(Add);
                    }
                    DB.tbl_Comm_HotelGroupMappings.InsertAllOnSubmit(list);
                    DB.SubmitChanges();
              

               
                    tbl_Contact Con = new tbl_Contact(); ;
                    Con.email = Email;
                    Con.Mobile = Phone;
                    Con.phone = Phone;
                    Con.sCountry = Country;
                    Con.Code = City;
                    DB.tbl_Contacts.InsertOnSubmit(Con);
                    DB.SubmitChanges();

                    ContactID = Con.ContactID;
                    string HotelUniqueCode = "HTL-" + GenerateRandomString(4);
                    string sPassword = GenerateRandomString(8);
                    string sEncryptedPassword = Common.Cryptography.EncryptText(sPassword);

                    tbl_StaffLogin Staff = new tbl_StaffLogin();
                    Staff.ContactID = ContactID;
                    Staff.uid = Email;
                    Staff.password = sEncryptedPassword;
                    Staff.UserType = "Hotel";
                    Staff.ContactPerson = Name;
                    Staff.ParentId = AccountManager.GetSupplierByUser();
                    Staff.PANNo = "";
                    Staff.LoginFlag = true;
                    Staff.StaffUniqueCode = HotelUniqueCode;
                    Staff.StaffCategory = Grupid.ToString();
                    DB.tbl_StaffLogins.InsertOnSubmit(Staff);
                    DB.SubmitChanges();

                    UniqCode = Staff.sid.ToString();
                    Sidd = Staff.sid;

                    tbl_Comm_HotelGroup update = db.tbl_Comm_HotelGroups.Single(x => x.Group_id == Grupid);
                    update.UniqueCode = UniqCode;
                    db.SubmitChanges();



                    tblAgentRoleManager Ad = new tblAgentRoleManager();
                    Ad.nUid = Sidd;
                    Ad.nFormId = 15;
                    DB.tblAgentRoleManagers.InsertOnSubmit(Ad);
                    DB.SubmitChanges();

                    EmailManager.HotelAccountEmail(Email);

                  
                }

                return jsSerializer.Serialize(new { retCode = 1 });

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string UpdateHotelGroup(string Name, string Hotels, Int64 Groupid)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new DBHandlerDataContext())
                {

                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again");

                    if (db.tbl_Comm_HotelGroups.Where(d => d.Group_id == Groupid).ToList().Count != 0)
                    {
                        tbl_Comm_HotelGroup update = db.tbl_Comm_HotelGroups.Where(d => d.Group_id == Groupid).FirstOrDefault();
                        update.Group_Name = Name;
                        db.SubmitChanges();
                        //  return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Update Successfully" });
                    }
                    string[] Hotel = Hotels.Split(',');
                    List<tbl_Comm_HotelGroupMapping> list = new List<tbl_Comm_HotelGroupMapping>();
                    using (var DB = new DBHandlerDataContext())
                    {
                        var Count = (from obj in DB.tbl_Comm_HotelGroupMappings where obj.Group_id == Groupid select obj).ToList();
                        DB.tbl_Comm_HotelGroupMappings.DeleteAllOnSubmit(Count);
                        DB.SubmitChanges();

                        foreach (var item in Hotel)
                        {
                            tbl_Comm_HotelGroupMapping Add = new tbl_Comm_HotelGroupMapping();
                            Add.Group_id = Groupid;
                            Add.Hotel_Code = item.ToString();
                            list.Add(Add);
                        }
                        DB.tbl_Comm_HotelGroupMappings.InsertAllOnSubmit(list);
                        DB.SubmitChanges();
                    }


                    return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Update Successfully" });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetGroup()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = AccountManager.GetUserByLogin();
                using (var DB = new DBHandlerDataContext())
                {
                    var arrGroupList = (from obj in db.tbl_Comm_HotelGroups
                                        where obj.SupplierId == Uid
                                        select obj).ToList();

                    var arrGroup = (from obj in DB.tbl_StaffLogins
                                 join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                    where obj.UserType == "Hotel" && obj.ParentId == Uid
                                 select new
                                 {
                                     obj.ContactPerson,
                                     obj.Last_Name,
                                     obj.Designation,
                                     obj.dtLastAccess,
                                     obj.Gender,
                                     obj.uid,
                                     obj.sid,
                                     obj.Department,
                                     obj.StaffUniqueCode,
                                     obj.password,
                                     obj.LoginFlag,
                                     obj.PANNo,
                                     obj.Updatedate,
                                     objc.Address,
                                     objc.email,
                                     objc.Mobile,
                                     objc.phone

                                 }).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrGroup = arrGroup, arrGroupList = arrGroupList });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetGroupHotels(string GroupId)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var Group = DB.tbl_Comm_HotelGroupMappings.AsEnumerable();
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var ListHotels = (from obj in Group
                                      join objc in db.tbl_CommonHotelMaster on obj.Hotel_Code equals objc.sid.ToString()
                                      where obj.Group_id == Convert.ToInt64(GroupId)
                                      select new
                                      {
                                          obj.Hotel_Code,
                                          objc.HotelName,
                                      }
                                      ).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, ListHotels = ListHotels });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetDetails(string Name,string sid)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                using (var db = new DBHandlerDataContext())
                {
                    var ListHotels = (from obj in DB.tbl_CommonHotelMaster where obj.Active == "True" orderby obj.sid ascending select obj).ToList();
                    var Details = (from obj in db.tbl_Comm_HotelGroups
                                   join objs in db.tbl_StaffLogins on obj.UniqueCode equals objs.sid.ToString()
                                   join objc in db.tbl_Contacts on objs.ContactID equals objc.ContactID
                                   join objcity in db.tbl_HCities on objc.Code equals objcity.Code
                                   join obgrp in db.tbl_Comm_HotelGroupMappings on obj.Group_id equals obgrp.Group_id
                                   where obj.UniqueCode == objs.sid.ToString() && obj.Group_Name == Name
                                   select new
                                   {
                                       obj.Group_Name,
                                       objs.uid,
                                       objc.Mobile,
                                       objc.phone,
                                       objc.sCountry,
                                       objcity.Description,
                                       obj.Group_id,
                                   }).Distinct().ToList();
                    return jsSerializer.Serialize(new { retCode = 1, ListHotels = ListHotels, Details = Details });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetUpdateHotels(Int64 id)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                using (var db = new DBHandlerDataContext())
                {
                    var ListHotels = (from obj in db.tbl_Comm_HotelGroupMappings where obj.Group_id == id select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, ListHotels = ListHotels });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotels()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var ListHotels = (from obj in db.tbl_CommonHotelMaster where obj.Active == "True" orderby obj.sid ascending select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, ListHotels = ListHotels });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetGroupName()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                using (var db = new DBHandlerDataContext())
                {
                    var GroupName = (from obj in db.tbl_Comm_HotelGroups where obj.Group_Name == objGlobalDefault.ContactPerson select obj.Group_Name).FirstOrDefault();
                    return jsSerializer.Serialize(new { retCode = 1, arrGroup = GroupName });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
    }
}
