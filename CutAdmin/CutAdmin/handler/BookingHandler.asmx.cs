﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin.Handler;
using Elmah;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using static CutAdmin.Common.BoookingManger;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }

            public Int64 Id { get; set; }
            public Int64 AddOnsID { get; set; }
            public bool IsCumpulsary { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 RateID { get; set; }
            public string RateType { get; set; }
            public Decimal Rate { get; set; }
            public Int64 UserID { get; set; }


        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }

        public class RateGroup
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), Convert.ToInt64(arrHotelDetails.HotelId));
                return jsSerializer.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, errMsg = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GenrateBookingDetails(List<Group> ListRates, string Serach)
        {
            try
            {

                Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                if (arrHotelDetails == null)
                    throw new Exception("Please Search again ,Rates are Changged.");
                //List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                arrHotelDetails.RateList[0].Charge = new ServiceCharge();
                float RoomTotal = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                float Markup = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate[0].RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        arrRate.HotelTaxRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100) * arrRate.dates.Count; }); //  Take Other Rate
                        Other += arrRate.HotelTaxRates.Select(d => d.TotalRate).ToList().Sum();
                        foreach (var objOther in arrRate.HotelTaxRates)
                        {
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                                    other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                            }
                        }
                        Markup += arrRate.dates.Select(d => d.AdminMarkup).Sum() + arrRate.dates.Select(d => d.S2SMarkup).Sum();
                        RoomTotal += arrRate.dates.Select(d => d.RoomRate).Sum();//+ arrRate.Dates.Select(d => d.AdminMarkup).Sum() + arrRate.Dates.Select(d => d.S2SMarkup).Sum();
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        #endregion
                        #region Check Inventory
                        foreach (var objDate in arrRate.dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, Convert.ToDecimal(objDate.RateTypeId), objDate.RoomTypeId.ToString(), objDate.datetime, AccountManager.GetSupplierByUser());
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }

                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                Session["RateList" + Serach] = Rooms;
                var arrHotelCharge = new ServiceCharge { RoomRate = RoomTotal, HotelTaxes = other, TotalPrice = RoomTotal + other.Select(d => d.TotalRate).ToList().Sum(), AdminMarkup = Markup };
                arrHotelDetails.RateList[0].Charge = arrHotelCharge;
                Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetRateGroup(string HotelCode, string Search_Params)
        {
            try
            {
                var arrRateGroup = SearchManager.GetRateGroup(HotelCode, Search_Params);
                return jsSerializer.Serialize(new { retCode = 1, arrRateGroup = arrRateGroup });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetBookingDetails(string Serach)
        {
            try
            {
                List<RoomType> Rooms = (List<RoomType>)Session["RateList" + Serach];
                List<Group> ListRates = (List<Group>)Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, arrHotelDetails.RateList[0].Charge });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string ValidateTransaction(List<Addons> arrAdons, string Serach, Int64 SupplierID)
        {
            jsSerializer = new JavaScriptSerializer();
            string JSON = "";
            bool IsValid = false;
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (Session["AvailDetails" + Serach] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                float BaseRate = objAvailRate.Where(d => d.Name == SupplierID.ToString()).FirstOrDefault().Charge.TotalPrice + AddOnsPrice;

                if (SupplierID != AccountManager.GetSupplierByUser() && CutAdmin.Common.BoookingManger.ValidTransact(AccountManager.GetSupplierByUser(), BaseRate) == false)
                    throw new Exception("Please Contact Administration to Make this booking");
                Int64 UserID = AccountManager.GetUserByLogin();
                if (SupplierID != UserID || UserID != AccountManager.GetSupplierByUser())
                {
                    if (CutAdmin.Common.BoookingManger.ValidTransact(UserID, BaseRate))

                        JSON = jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                    else
                        throw new Exception("Your Balance is insufficient to Make this booking");
                }
                else
                {
                    JSON = jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                }
                return JSON;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookHotel(List<Addons> arrAddOns, string Serach, List<CommonLib.Request.Customer> LisCustumer, string Supplier, string Remark, string UmrahCompany, string Email, string Mobile)
        {
            try
            {
                string OnHold = "false";
                string json = "";
                return json = CutAdmin.Common.BoookingManger.BookHotel(arrAddOns, Serach, LisCustumer, Supplier, OnHold, Remark, UmrahCompany, Mobile, Email);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { });
            }
        }
        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingList"] = null;
                List<Reservations> BookingList = Reservations.BookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                var arrAgents = AccountManager.GetAgentsBySupplier();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList(), arrAgents = arrAgents });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GroupBookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingListGroup"] = null;
                List<Reservations> BookingList = Reservations.GroupBookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingListGroup"] = dtResult;
                dtResult.Dispose();
                var arrAgents = AccountManager.GetAgentsBySupplier();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList(), arrAgents = arrAgents });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            try
            {
                List<Reservations> BookingList = Reservations.BookingList();
                List<Reservations> GroupBookingList = Reservations.GroupBookingList();
                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingPending });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingConfirmed });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = BookingList.Where(d => d.Status == "Cancelled").ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingRejected });
                }
                else if (status == "OnRequest" && type == "BookingPending")
                {
                    var OnHoldBookings = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookings });
                }
                else if (status == "OnRequest" && type == "Requested")
                {
                    var OnHoldRequested = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }
                else if (status == "OnRequest" && type == "Confirmed")
                {
                    var OnHoldConfirmed = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldConfirmed });
                }
                else if (status == "OnHold" && type == "BookingPending")
                {
                    var OnHoldBookingPending = BookingList.Where(d => d.Status == "OnHold" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookingPending });
                }
                else if (status == "OnHold" && type == "BookingConfirmed")
                {
                    var OnHoldBookingConfirmed = BookingList.Where(d => d.Status == "OnHold" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookingConfirmed });
                }
                else if (status == "OnHold" && type == "Requested")
                {
                    var OnHoldRequested = BookingList.Where(d => d.Status == "OnHold" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }


                else if (status == "Vouchered" && type == "ReconfirmPending")
                {
                    var ReconfirmPending = BookingList.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmPending });
                }
                else if (status == "Vouchered" && type == "ReconfirmRequested")
                {
                    var ReconfirmRequested = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmRequested });
                }
                else if (status == "Vouchered" && type == "ReconfirmReject")
                {
                    var ReconfirmReject = BookingList.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmReject });
                }

                else if (status == "GroupRequest" && type == "GroupRequestPending")
                {
                    var GroupRequestPending = GroupBookingList.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestPending });
                }
                else if (status == "GroupRequest" && type == "GroupRequestRequested")
                {
                    var GroupRequestRequested = GroupBookingList.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestRequested });
                }
                else if (status == "GroupRequest" && type == "GroupRequestReject")
                {
                    var GroupRequestReject = GroupBookingList.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestReject });
                }

            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Search(string AgencyName, string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SupplierBookingList"];
            DataRow[] row = null;
            try
            {
                if (AgencyName != "")
                {
                    row = BookingList.Select("AgencyName like '%" + AgencyName + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("bookingname like '%" + Passenger + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }

                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                Session["SearchSupplierBookingList"] = BookingList;
                //List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                //BookingList.Dispose();
                //json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string SearchGroupBooking(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SupplierBookingListGroup"];
            DataRow[] row = null;
            try
            {
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("bookingname like '%" + Passenger + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }


                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                Session["SearchSupplierBookingList"] = BookingList;
                //List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                //BookingList.Dispose();
                //json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string BookingCancle(string ReservationID, string Status)
        {
            try
            {
               bool isCancelled=  CancellationManager.CancelBooking(ReservationID);
               if (isCancelled == false)
                    throw new Exception("");
                    string Message = EmailManager.GetErrorMessage("Booking Cancelled");
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Message = Message });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                string Message = EmailManager.GetErrorMessage("Booking Cancelled Error");
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var Detail = (from obj in DB.tbl_CommonHotelReservations
                                      //  join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                                  join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                  where obj.ReservationID == ReservationID
                                  select new
                                  {
                                      obj.Sid,
                                      obj.ReservationID,
                                      obj.ReservationDate,
                                      obj.Status,
                                      obj.TotalFare,
                                      obj.TotalRooms,
                                      obj.HotelName,
                                      obj.CheckIn,
                                      obj.CheckOut,
                                      obj.City,
                                      obj.Children,
                                      obj.BookingStatus,
                                      obj.NoOfAdults,
                                      obj.Source,
                                      obj.Uid,
                                      obj.NoOfDays,
                                      obj.bookingname,
                                      obj.SupplierCurrency,
                                      //  objPass.Name,
                                      //  objPass.LastName,
                                      //  objPass.RoomNumber,
                                      objroom.CancellationAmount,
                                      objroom.CutCancellationDate
                                  }).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAmendmentDetail(string ReservationID)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var Detail = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string ConfirmOnRequestBooking(string ReservationId)
        {
            try
            {
                DBHelper.DBReturnCode retcode = BookingManager.ConfirmOnRequestBooking(ReservationId);

                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;

                    var ListHotelReservation = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationId select obj).FirstOrDefault();


                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
                        Bit.Status = "Vouchered";
                        DB.SubmitChanges();

                        //Inventory Update On Reconfirmation//


                        //

                        //New Work///

                        Int64 Uid = 0; string sTo = "";
                        Uid = AccountManager.GetSupplierByUser();
                        sTo = objGlobalDefault.uid;
                        sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                        // sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                        //MailManager.SendReconfrmInvoice(ReservationId, Uid, sTo);
                        MailManager.SendInvoice(ReservationId, Uid, sTo);
                    }

                    else if(ListHotelReservation.Uid == Convert.ToInt64(UserId) && retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
                        if(Bit.Status != "Vouchered")
                        {
                            Bit.Status = "Vouchered";
                            DB.SubmitChanges();
                            Int64 Uid = 0; string sTo = "";
                            Uid = AccountManager.GetSupplierByUser();
                            sTo = objGlobalDefault.uid;
                            sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                            // sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                            //MailManager.SendReconfrmInvoice(ReservationId, Uid, sTo);
                            MailManager.SendInvoice(ReservationId, Uid, sTo);
                        }
                    }
                    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                    {
                        return jsSerializer.Serialize(new { retCode = 2, Session = 1 });
                    }

                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {

                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;

                    var sReservations = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationId select obj).FirstOrDefault().SupplierConfirmNo;

                    if (sReservations == null)
                        sReservations = ReservationId;
                    foreach (var ReservationID in sReservations.Split('|'))
                    {
                        tbl_CommonHotelReconfirmationDetail Confirm = new tbl_CommonHotelReconfirmationDetail();
                        Confirm.UserID = UserId;
                        Confirm.ReservationId = ReservationID;
                        Confirm.ReconfirmDate = ConfirmDate;
                        Confirm.Staff_Name = StaffName;
                        Confirm.ReconfirmThrough = ReconfirmThrough;
                        Confirm.HotelConfirmationNo = HotelConfirmationNo;
                        Confirm.Comment = Comment;
                        DB.tbl_CommonHotelReconfirmationDetails.InsertOnSubmit(Confirm);
                        DB.SubmitChanges();

                        //DBHelper.DBReturnCode retcode = BookingManager.ConfirmHoldBooking(ReservationId);

                        //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                        //{
                        tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID);
                        Bit.IsConfirm = true;
                        Bit.Status = "Vouchered";
                        Bit.HotelConfirmationNo = HotelConfirmationNo;
                        DB.SubmitChanges();
                        ReconfirmationMail(Convert.ToInt64(Bit.Uid), Confirm.sid, HotelName, ReservationID, StaffName, Comment);

                        //Inventory Update On Reconfirmation//
                        Int64 Uid = 0; string sTo = "";
                        Uid = AccountManager.GetSupplierByUser();
                        sTo = objGlobalDefault.uid;
                        sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                        MailManager.SendReconfrmInvoice(ReservationID, Uid, sTo);
                    }


                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        [WebMethod(EnableSession = true)]
        public string ConfirmHoldBooking(string HotelName, string ReservationId)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;

                    var sReservations = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationId select obj).FirstOrDefault().SupplierConfirmNo;

                    if (sReservations == null)
                        sReservations = ReservationId;
                    foreach (var ReservationID in sReservations.Split('|'))
                    {
                        DBHelper.DBReturnCode retcode = BookingManager.ConfirmHoldBooking(ReservationId);

                        if (retcode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID);
                            Bit.IsConfirm = false;
                            Bit.Status = "Vouchered";
                            Bit.HotelConfirmationNo = "";
                            DB.SubmitChanges();

                            ConfirmationMail(Convert.ToInt64(Bit.Uid), HotelName, ReservationID);

                            Int64 Uid = 0; string sTo = "";
                            Uid = AccountManager.GetSupplierByUser();
                            sTo = objGlobalDefault.uid;
                            sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                            MailManager.SendReconfrmInvoice(ReservationID, Uid, sTo);
                        }
                    }
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        public static bool ConfirmationMail(Int64 Uid, string HotelName, string ReservationId)
        {

            try
            {
                //DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var DB = new DBHandlerDataContext())
                {
                    var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                    string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking Confirm" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = sMail.BCcMail;
                    string CcTeamMail = sMail.CcMail;


                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" / Font Definitions /");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" / Style Definitions /");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + sReservation.customer_name + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + sReservation.CompanyName + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                    sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                    sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    sb.Append(" <tr>");
                    sb.Append(" <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    sb.Append(" No.</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append(" <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    sb.Append(" Name</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append(" <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    sb.Append(" ID</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append(" <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append(" normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    sb.Append(" </td>");
                    sb.Append(" </tr>");

                    sb.Append("</table>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Hotel Confirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sReservation.customer_Email, sReservation.customer_Email);
                    if (BCcTeamMails != "")
                        Email1List.Add(BCcTeamMails, BCcTeamMails);
                    if (CcTeamMail != "")
                        Email2List.Add(CcTeamMail, CcTeamMail);

                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }
        }
        [WebMethod(EnableSession = true)]
        public string AcceptGroupReq(Int64 Id, string Status, string Remark)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
                    Accept.Status = "Vouchered";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string RejectGroupReq(Int64 Id, string Status, string Remark)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
                    Accept.Status = "Cancelled";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        public static bool ReconfirmationMail(Int64 Uid, Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                //DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var DB = new DBHandlerDataContext())
                {
                    var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                    string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity.Contains("Booking Reconfirm") && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = ""; string CcTeamMail = "";
                    if (sMail!=null)
                    {
                         BCcTeamMails = sMail.BCcMail;
                         CcTeamMail = sMail.CcMail;
                    }
                    


                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + sReservation.customer_name + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + sReservation.CompanyName + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                    sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                    sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    sb.Append("  No.</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    sb.Append("  Name</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    sb.Append("  ID</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                    sb.Append("  by</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                    sb.Append("  / Note</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append("</table>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sReservation.customer_Email, sReservation.customer_Email);
                    if (BCcTeamMails != "")
                        Email1List.Add(BCcTeamMails, BCcTeamMails);
                    if (CcTeamMail != "")
                        Email2List.Add(CcTeamMail, CcTeamMail);

                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }
        }


        public static bool OnRequestMail(Int64 Uid, string HotelName, string ReservationId)
        {

            try
            {
                //DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var DB = new DBHandlerDataContext())
                {
                    var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                    string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();

                    var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking OnRequest" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = sMail.BCcMail;
                    string CcTeamMail = sMail.CcMail;



                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + arrSupplierDetails.CompanyName + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + sReservation.customer_name + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>");
                    sb.Append("thanks for choosing our service,booking is subject to room availiblity and The confirmation of whether the room is available or not</span></p>");
                    sb.Append("will be communicated to you within within 24 business hours");
                    // sb.Append("we will confirm your booking when we have availability</span></p>");
                    // sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    //sb.Append("  No.</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    //sb.Append("  Name</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    //sb.Append("  ID</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Onrequest");
                    //sb.Append("  by</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                    //sb.Append("  / Note</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    sb.Append("</table>");
                    //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    //sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    //sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Booking On-Request - " + HotelName;

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    //Email1List.Add(sReservation.customer_Email, "");
                    Email1List.Add(arrSupplierDetails.Uid, "");
                    Email1List.Add(BCcTeamMails, "");
                    Email2List.Add(CcTeamMail, "");

                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveAmendemnt(string ReservationID, List<tbl_CommonBookedPassenger> ListPax)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {

                    var oldDetails = (from obj in DB.tbl_CommonBookedPassengers
                                      where obj.ReservationID == ReservationID
                                      select obj).ToList();
                    if (oldDetails.Count != 0)
                    {
                        DB.tbl_CommonBookedPassengers.DeleteAllOnSubmit(oldDetails);
                        DB.SubmitChanges();
                    }
                    DB.tbl_CommonBookedPassengers.InsertAllOnSubmit(ListPax);
                    DB.SubmitChanges();

                    var Name = ListPax[0].Name + " " + ListPax[0].LastName;


                    tbl_CommonHotelReservation tbl = DB.tbl_CommonHotelReservations.Where(d => d.ReservationID == ReservationID).FirstOrDefault();
                    tbl.bookingname = Name;
                    DB.SubmitChanges();


                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
    }
}

