﻿using CutAdmin.BL;
using CutAdmin.EntityModal;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using static CutAdmin.DataLayer.RoomRateManager;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for RateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RateHandler : System.Web.Services.WebService
    {

        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
        [WebMethod(EnableSession = true)]
        public string SaveRates(List<tbl_commonRoomRate> arrTarrif, List<Rates> arrRates, List<tbl_CommonCancelationPolicy> arrCancellationPolicy, List<List<Comm_AddOnsRate>> arrMealsRate)
        {
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
               // RoomRateManager.SaveRates(arrTarrif, arrRates, arrCancellationPolicy, arrMealsRate);
                return objSerlizer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
    }
}
