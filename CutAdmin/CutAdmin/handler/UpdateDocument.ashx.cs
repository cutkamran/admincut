﻿using CutAdmin.bin.BL;
using CutAdmin.EntityModal;
using Elmah;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for UpdateDocument
    /// </summary>
    public class UpdateDocument : IHttpHandler
    {
      //  public static CUT_LIVE_UATSTEntities db { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string path = System.Convert.ToString(context.Request.QueryString["path"]);
                string oldFile = System.Convert.ToString(context.Request.QueryString["OldFile"]);
                string OldImage = System.Convert.ToString(context.Request.QueryString["OldImage"]);
                string Type = System.Convert.ToString(context.Request.QueryString["Type"]);
                string Id = System.Convert.ToString(context.Request.QueryString["Id"]);
                string NewImage = path.Split('/')[2];
                string ext = Path.GetExtension(NewImage);
                if (ext == "")
                {
                    NewImage += ".jpg";
                }
                //if (Type == "Activity")
                //{
                //    using (var db = new CUT_LIVE_UATSTEntities())
                //    {
                //        var arrList = (from obj in db.tbl_SightseeingMaster where obj.Activity_Id.ToString() == Id select obj).FirstOrDefault();
                //        if (arrList != null)
                //        {
                //            string[] images = arrList.Act_Images.Split(',');
                //            string NewImgpath = SetNewPath(images, OldImage, NewImage);
                //            arrList.Act_Images = NewImgpath;
                //            db.SaveChanges();
                //        }
                //    }
                //}
                 if (Type == "Hotel")
                {
                    using (var db = new ClickUrHotel_DBEntities())
                    {
                        var arrList = (from obj in db.tbl_CommonHotelMaster where obj.sid.ToString() == Id select obj).FirstOrDefault();
                        if (arrList != null)
                        {
                            string[] images = arrList.HotelImage.Split(',');
                            string NewImgpath = SetNewPath(images, OldImage, NewImage);
                            arrList.HotelImage = NewImgpath;
                            arrList.SubImages = NewImgpath;
                            db.SaveChanges();
                        }
                    }
                }
                //else if (Type == "Package")
                //{
                //    using (var db = new CUT_LIVE_UATSTEntities())
                //    {
                //        var arrList = (from obj in db.tbl_PackageImages where obj.nPackageID.ToString() == Id select obj).FirstOrDefault();
                //        if (arrList != null)
                //        {
                //            //string[] images = arrList.ImageArray.Split(@'/^_^');
                //            string[] images = arrList.ImageArray.Split(new string[] { "^_^" }, StringSplitOptions.None);
                //            string NewImgpath = SetNewPath(images, OldImage, NewImage);
                //            NewImgpath = NewImgpath.Replace(@",", @"^_^");
                //            arrList.ImageArray = NewImgpath;
                //            db.SaveChanges();
                //        }
                //    }
                //}
                string oldFilePath = Path.Combine(context.Server.MapPath("/" + oldFile+".jpg"));
                string newFilePath = Path.Combine(context.Server.MapPath("/" + path+".jpg"));
                File.Move(oldFilePath, newFilePath);
                context.Response.Write("1");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

        }

        public string SetNewPath(string[] images, string OldImage, string NewImage)
        {
            string Newpath = "";
            try
            {
                string Replimg = "";
                foreach (var img in images)
                {
                    if (img != "")
                    {
                        if (img == OldImage)
                        {
                            Replimg = img.Replace(OldImage, NewImage);
                        }
                        else
                        {
                            Replimg = img;
                        }
                        Newpath += Replimg + ",";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Newpath;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}