﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CommonLib.Response;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        //DBHandlerDataContext db = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region SAVE TAXES
        [WebMethod(EnableSession = true)]
        public string SaveAddOn(Comm_AddOn objAddOn)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new DBHandlerDataContext())
                {
                    if (objAddOn.ID == 0)
                    {
                        if (HttpContext.Current.Session["LoginUser"] == null)
                            throw new Exception("Session Expired ,Please Login and try Again");
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        Int64 Uid = objGlobalDefault.sid;
                        if (objGlobalDefault.UserType != "Supplier")
                            Uid = objGlobalDefault.ParentId;
                        if (db.Comm_AddOns.Where(d => d.AddOnName == objAddOn.AddOnName && d.UserId == Uid).ToList().Count != 0)
                            return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add this." });
                        objAddOn.UserId = Uid;
                        db.Comm_AddOns.InsertOnSubmit(objAddOn);
                        db.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                    else
                    {
                        var arrTax = db.Comm_AddOns.Where(d => d.ID == objAddOn.ID).FirstOrDefault();
                        arrTax.AddOnName = objAddOn.AddOnName;
                        arrTax.Details = objAddOn.Details;
                        arrTax.Type = objAddOn.Type;
                        arrTax.IsMeal = objAddOn.IsMeal;
                        arrTax.Activate = objAddOn.Activate;
                        db.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string DeleteAddOns(Int64 ID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new DBHandlerDataContext())
                {
                    var arrAddOns = db.Comm_AddOns.Where(d => d.ID == ID).FirstOrDefault();
                    db.Comm_AddOns.DeleteOnSubmit(arrAddOns);
                    db.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadAddOns()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                if (objGlobalDefault.UserType != "Supplier")
                    Uid = objGlobalDefault.ParentId;
                using (var db = new DBHandlerDataContext())
                {
                    var arrAddOns = (from obj in db.Comm_AddOns
                                     where obj.UserId == Uid
                                     select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrAddOns = arrAddOns });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        [WebMethod(EnableSession = true)]
        public string GetMealPlans()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    return jsSerializer.Serialize(new { retCode = 1, arrMealPlan = GenralManager.GetMealPlans(), arrMeals = GenralManager.GetMeals() });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string SaveMealPlans(tbl_MealPlan arrMealPlan, List<tbl_MealPlanMapping> arrMeals)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    GenralManager.SaveMealPlan(arrMealPlan, arrMeals);
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string DeletePlan(Int64 PlanID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    GenralManager.DeletePlan(PlanID);
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Supplier Staff

        [WebMethod(EnableSession = true)]
        public string GetStaffDetails()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 ParentId = objGlobalDefault.sid;
                if(objGlobalDefault.UserType == "SupplierStaff")
                    ParentId = objGlobalDefault.ParentId;

                var Stafflist = (from obj in DB.tbl_StaffLogins
                                 join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                 where obj.ParentId == ParentId && obj.UserType == "SupplierStaff"
                                 select new
                                 {
                                     obj.ContactPerson,
                                     obj.Last_Name,
                                     obj.Designation,
                                     obj.dtLastAccess,
                                     obj.Gender,
                                     obj.uid,
                                     obj.sid,
                                     obj.Department,
                                     obj.StaffUniqueCode,
                                     obj.password,
                                     obj.LoginFlag,
                                     obj.PANNo,
                                     obj.Updatedate,
                                     objc.Address,
                                     objc.email,
                                     objc.Mobile,
                                     objc.phone

                                 }).ToList();
                Session["StafflistSession"] = ConvertToDatatable(Stafflist);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, Staff = Stafflist });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        #endregion

        #region ConvertToDatatable
        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        #endregion


        #region Hotel Info
        [WebMethod(EnableSession = true)]
        public string GetHotelInfo(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMaster
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelLatitude,
                                            obj.HotelLangitude,
                                            obj.HotelAddress,
                                            obj.HotelZipCode,
                                            obj.HotelImage,
                                            obj.HotelCategory,
                                            obj.HotelDescription
                                        }).FirstOrDefault();
                    return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

         [WebMethod(EnableSession = true)]
         public string GetHotelImages(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMaster
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelImage,
                                            obj.SubImages

                                        }).FirstOrDefault();
                    string Image = arrHotelInfo.HotelImage;
                    List<Image> arrImage = new List<Image>();
                    if (Image != null)
                    {
                        List<string> Url = Image.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    foreach (var sImage in arrHotelInfo.SubImages.Split('^'))
                    {
                        arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + sImage, Count = arrHotelInfo.SubImages.Split('^').ToList().Count });
                    }
                    return jsSerializer.Serialize(new { retCode = 1, arrImage = arrImage });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

         [WebMethod(EnableSession = true)]
         public string GetHotelInfoBySearch(string Search)
         {
             jsSerializer = new JavaScriptSerializer();
             try
             {
                 var arrHotelInfo = GenralManager.GetHotelInfo(Search);
                 return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
             }
             catch
             {
                 return jsSerializer.Serialize(new { retCode = 0, });
             }
         }
        #endregion


        [WebMethod(EnableSession = true)]
        public string GetCurrencyByHotel(string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrCurrency = CutAdmin.Services.HotelCurrency.GetCurrency(HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }



        [WebMethod(EnableSession = true)]
        public string GetHotels(string AdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrHotels = CutAdmin.Services.Hotels.GetHotels(AdminID) });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #region Get Rate Type
        [WebMethod(EnableSession = true)]
        public string _ByRateType(string HotelAdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRateType(HotelAdminID) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string _ByRate(string HotelAdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRate(HotelAdminID) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }


        [WebMethod(EnableSession = true)]
        public string _ByRateInventory(string Inventory, string HotelCode, string RoomCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRateInventory(Inventory, HotelCode, RoomCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string _ByHotelInventory(string Inventory, string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByHotelInventory(Inventory, HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string _ByHotel(string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByHotel(HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetInventoryRateType(string AdminID, string RateType)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType.GetInventoryRateType(AdminID, RateType) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #endregion
    }
}
