﻿
using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.EntityModal;
using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        #region Save Hotel
        [WebMethod(true)]
        public string SaveHotel(tbl_CommonHotelMaster arrHotels, tbl_MappedArea arrCity, tbl_Location arrLocation, List<Comm_HotelContacts> arrContacts, List<Comm_TaxMapping> arrTax, List<Comm_TaxMapping> arrMeal)
        {
            try
            {
                bool AvailGroup = false;
                LocationManager.SaveLocation(arrLocation, arrCity);
                arrHotels.LocationId = arrLocation.Lid;
                if(arrHotels.HotelGroup == "")
                {
                    AvailGroup = true;
                    arrHotels.HotelGroup = arrHotels.HotelName;      
                }
                HotelManager.SaveHotel(arrHotels, arrContacts);
                if(AvailGroup)
                {
                    CreateHotelGroup(arrHotels, arrContacts, arrLocation);
                }
                else
                {
                    MapHotelGroup(arrHotels);
                }

                if (arrTax.Count != 0)
                {
                    arrTax.ForEach(c => c.HotelID = arrHotels.sid);
                    TaxManager.SaveTaxDetails(arrTax);
                }
                else
                {
                    TaxManager.DeleteTaxDetails(arrHotels.sid);
                }
                if(arrMeal.Count != 0)
                {
                    arrMeal.ForEach(c => c.HotelID = arrHotels.sid);
                    TaxManager.SaveTaxDetails(arrMeal);
                }
                else
                {
                    TaxManager.DeleteTaxDetailsAddOns(arrHotels.sid);
                }
                return jsSerializer.Serialize(new { retCode = 1, HotelCode = arrHotels.sid });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Create Hotel Group
        public string CreateHotelGroup(tbl_CommonHotelMaster arrHotels,List<Comm_HotelContacts> arrContacts, tbl_Location arrLocation)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var CityCode = (from objc in DB.tbl_HCities where objc.Description == arrLocation.City select objc).FirstOrDefault();
                string Name = arrHotels.HotelName;
                string Hotels = arrHotels.sid.ToString();
                string Email = arrContacts.Where(x => x.Type == "HotelContacts").Select(d=>d.email).FirstOrDefault();
                string Phone = arrContacts.Where(x => x.Type == "HotelContacts").Select(d => d.MobileNo).FirstOrDefault();
                string Country = CityCode.Country;
                string City = CityCode.Code;
                CutAdmin.handler.HotelGroupHandler obj = new handler.HotelGroupHandler();
                obj.SaveHotelGroup(Name, Hotels, Email, Phone, Country, City);
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        #endregion


        #region Map Hotel Group
        public string MapHotelGroup(tbl_CommonHotelMaster arrHotels)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var Groupid = (from objc in DB.tbl_Comm_HotelGroups where objc.Group_Name == arrHotels.HotelGroup select objc.Group_id).FirstOrDefault();

                var Availmap = (from obj in DB.tbl_Comm_HotelGroupMappings where obj.Group_id == Groupid && obj.Hotel_Code == arrHotels.sid.ToString() select obj).FirstOrDefault();

                if(Availmap == null)
                {
                    tbl_Comm_HotelGroupMapping Add = new tbl_Comm_HotelGroupMapping();
                    Add.Group_id = Groupid;
                    Add.Hotel_Code = arrHotels.sid.ToString();
                    DB.tbl_Comm_HotelGroupMappings.InsertOnSubmit(Add);
                    DB.SubmitChanges();
                }
               
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        #endregion
    }
}
