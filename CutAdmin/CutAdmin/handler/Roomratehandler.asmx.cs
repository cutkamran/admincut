﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using System.Threading.Tasks;
using CutAdmin.DataLayer;
using System.Globalization;
using System.Threading;
using Elmah;
using CutAdmin.DataLayer.Datalayer;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for Roomratehandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Roomratehandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
        string json = string.Empty;
     
        #region Save Rates
        [WebMethod(EnableSession = true)]
        //public string SaveRates(tbl_RatePlan arrRatePlan, List<tbl_TarrifPlan> arrTarrif, List<Rates> arrRates, List<tbl_CommonCancelationPolicy> arrCancellationPolicy)
        //{
        //    objSerlizer = new JavaScriptSerializer();
        //    objSerlizer.MaxJsonLength = Int32.MaxValue;
        //    try
        //    {
        //        CancellationManager.SaveCancellation(arrCancellationPolicy);
        //        if (arrCancellationPolicy.Count != 0)
        //        {
        //            if (arrTarrif.Select(d => d.sRate_Type).ToList().All(d => d == "GN"))
        //            {
        //                arrTarrif.ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
        //                arrTarrif.ForEach(d => d.nonShow = arrCancellationPolicy.LastOrDefault().CancelationID);
        //            }
        //            else
        //            {
        //                arrTarrif.Where(d => d.sRate_Type == "GN").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
        //                arrTarrif.Where(d => d.sRate_Type == "GN").ToList().ForEach(d => d.nonShow = arrCancellationPolicy[1].CancelationID);
        //                if (arrCancellationPolicy.Count==2)
        //                {
        //                    arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
        //                    arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nonShow = arrCancellationPolicy.LastOrDefault().CancelationID);
        //                }
        //                else
        //                {
        //                    arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy[2].CancelationID);
        //                    arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nonShow = arrCancellationPolicy[3].CancelationID);
        //                }
        //            }

        //        }
        //        if (arrCancellationPolicy.Count == 0)
        //            arrTarrif.ForEach(d => d.nonRefundable = true);
        //        RoomRateManager.SaveRates(arrRatePlan, arrTarrif, arrRates);
        //        return objSerlizer.Serialize(new { retCode = 1, arrTarrifID = arrTarrif.Select(d => d.nRateID).ToList(), });
        //    }
        //    catch (Exception ex)
        //    {
        //        return objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
        //    }
        //}

        #endregion


        public string SaveRates(tbl_RatePlan arrRatePlan, List<tbl_TarrifPlan> arrTarrif, List<Rates> arrRates, List<tbl_CommonCancelationPolicy> arrCancellationPolicy)
        {
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                CancellationManager.SaveCancellation(arrCancellationPolicy);
                if (arrCancellationPolicy.Count != 0)
                {
                    if (arrTarrif.Select(d => d.sRate_Type).ToList().All(d => d == "GN"))
                    {
                        arrTarrif.ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
                        arrTarrif.ForEach(d => d.nonShow = arrCancellationPolicy.LastOrDefault().CancelationID);
                    }
                    else
                    {
                        arrTarrif.Where(d => d.sRate_Type == "GN").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
                        arrTarrif.Where(d => d.sRate_Type == "GN").ToList().ForEach(d => d.nonShow = arrCancellationPolicy[1].CancelationID);
                        if (arrCancellationPolicy.Count == 2)
                        {
                            arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy.FirstOrDefault().CancelationID);
                            arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nonShow = arrCancellationPolicy.LastOrDefault().CancelationID);
                        }
                        else
                        {
                            arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nCancellation = arrCancellationPolicy[2].CancelationID);
                            arrTarrif.Where(d => d.sRate_Type == "SP").ToList().ForEach(d => d.nonShow = arrCancellationPolicy[3].CancelationID);
                        }
                    }

                }
                if (arrCancellationPolicy.Count == 0)
                    arrTarrif.ForEach(d => d.nonRefundable = true);
                RoomRateManager.SaveRates(arrRatePlan, arrTarrif, arrRates);
                return objSerlizer.Serialize(new { retCode = 1, arrTarrifID = arrTarrif.Select(d => d.nRateID).ToList(), });
            }
            catch (Exception ex)
            {
                return objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }


        #region Get Offer Type By Hotel
        [WebMethod(EnableSession = true)]
        public async Task<string> GetOfferType(long HotelCode, string RateType, long Supplier, string From, string To)
        {
            string json = "";
            try
            {
                var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                await System.Threading.Tasks.Task.Run(() =>
                {

                    Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                    json = objSerlizer.Serialize(new { retCode = 1, arrOfferType = OfferManager.GetOfferType(HotelCode, RateType, Supplier, From, To) });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                json = objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return json;
        }
        #endregion

        #region Get Rates By Room
        [WebMethod(EnableSession = true)]
        public async Task<string> GetRatesByRoom(string Startdt, string Enddt, string MealPlan, string Supplier, string InventoryType, string HotelCode, dynamic data, string RateType, string sTarrifType, string sOfferType)
        {
            string json = "";
            try
            {
                DateTime dtFrom = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dtTo = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> calendar = new List<DateTime>();
                while (dtFrom <= dtTo)
                {
                    calendar.Add(dtFrom);
                    dtFrom = dtFrom.AddDays(1);
                }
                HttpContext context = HttpContext.Current;
                var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                objSerlizer.MaxJsonLength = Int32.MaxValue;
                List<string> RoomCode = new List<string>();
                foreach (var RoomData in data)
                {
                    RoomCode.Add(Convert.ToString(RoomData["RoomID"]));
                }
                await System.Threading.Tasks.Task.Run(() =>
                {

                    Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                    json = objSerlizer.Serialize(new { retCode = 1, arrRates = RoomRateManager.GetRoomRate(HotelCode, MealPlan, RoomCode, Supplier, calendar, RateType, sTarrifType, sOfferType,false) });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                json = objSerlizer.Serialize(new { retCode = 2, ex = ex.Message }); ;
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public async Task<string> GetRatesByRoomID(string Startdt, string Enddt, string MealPlan, string Supplier, string InventoryType, string HotelCode, string RoomID, string RateType, string sTarrifType, string sOfferType)
        {
            string json = "";
            try
            {
                DateTime dtFrom = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dtTo = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> calendar = new List<DateTime>();
                while (dtFrom <= dtTo)
                {
                    calendar.Add(dtFrom);
                    dtFrom = dtFrom.AddDays(1);
                }
                HttpContext context = HttpContext.Current;
                var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                objSerlizer.MaxJsonLength = Int32.MaxValue;
                List<string> RoomCode = new List<string>();
                RoomCode.Add(RoomID);
                await System.Threading.Tasks.Task.Run(() =>
                {

                    Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                    json = objSerlizer.Serialize(new { retCode = 1, arrRates = RoomRateManager.GetRoomRate(HotelCode, MealPlan, RoomCode, Supplier, calendar, RateType, sTarrifType, sOfferType,true) });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                json = objSerlizer.Serialize(new { retCode = 2, ex = ex.Message }); ;
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdatePaxRate(string PaxType, float Rate, List<long> arrPlanID, string SellType, string sFrom, string sTo, long MealPlanID)
        {
            try
            {
                RoomRateManager.UpdatePaxRate(PaxType, Rate, arrPlanID, SellType, sFrom, sTo, MealPlanID);
                return objSerlizer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion


        #region Update Rates
        [WebMethod(EnableSession = true)]
        public async Task<string> UpdateRate(string Startdt, string Enddt, string HotelCode, string MealPlan, string RoomCode, string Supplier, int DateIndex, decimal Rate, string PaxType, string RateType)
        {
            string json = "";
            try
            {
                DateTime dtFrom = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dtTo = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> calendar = new List<DateTime>();
                while (dtFrom <= dtTo)
                {
                    calendar.Add(dtFrom);
                    dtFrom = dtFrom.AddDays(1);
                }
                DateTime ChangeDate = calendar[DateIndex];
                await System.Threading.Tasks.Task.Run(() =>
                {
                    RoomRateManager.UpdateRate(HotelCode, MealPlan, RoomCode, Supplier, ChangeDate, Rate, PaxType, RateType);
                    json = objSerlizer.Serialize(new { retCode = 1 });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                json = objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return json;
        }

        #endregion

        #region Get Market By Rates
        [WebMethod(EnableSession = true)]
        public async Task<string> GetValidMarket(string FromDate, string ToDate, string HotelCode, string MealPlan, string RoomCode, string Supplier)
        {
            try
            {
                await System.Threading.Tasks.Task.Run(() =>
                {
                    json = objSerlizer.Serialize(new
                    {
                        retCode = 1,
                        arrMarket = RoomRateManager.GetMarket(FromDate, ToDate, HotelCode, RoomCode, MealPlan, Supplier)
                    });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return json;
        }
        #endregion

        #region Get Cancellation By Rates
        [WebMethod(EnableSession = true)]
        public async Task<string> GetCancellation(string FromDate,string ToDate, string HotelCode, string MealPlan, string RoomCode, string Supplier, string sTarrifType)
        {
            try
            {

                await System.Threading.Tasks.Task.Run(() =>
                {
                    json = objSerlizer.Serialize(new
                    {
                        retCode = 1,
                        arrCancellation = RoomRateManager.GetCancellation( FromDate,  ToDate, HotelCode, RoomCode, MealPlan, Supplier, sTarrifType)
                    });

                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {

                json = objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveCancellation(List<tbl_CommonCancelationPolicy> arrCancellationPolicy, long TarrifID, string sFrom)
        {
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                if (arrCancellationPolicy.Count != 0)
                    CancellationManager.SaveCancellation(arrCancellationPolicy);
                RoomRateManager.ChangeTarrifByPolicy(TarrifID, sFrom, arrCancellationPolicy);
                return objSerlizer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objSerlizer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #endregion
    }
}
