﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ratetypes.aspx.cs" Inherits="CutAdmin.ratetypes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="Scripts/ratetypes.js?v=1.5"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
             <hgroup id="main-title" class="thin">
           <h1>Rate Types </h1>
              <h2><a style="cursor:pointer" onclick="AddRateType()"  class="addnew with-tooltip" title="Add rate types"><i class="fa fa-plus-circle"></i></a></h2>
            <hr />
        </hgroup>
        <div class="with-padding">
               <table class="table responsive-table font11" id="tbl_RateType">
                    <thead>
                        <tr>
                            <th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
                            <th scope="col" class="align-center hide-on-mobile">Rate Types</th>
                            <th scope="col" class="align-center hide-on-mobile">Valid Inventory</th>
                            <th scope="col" class="align-center hide-on-mobile" style="width:50px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
        </div>
    </section>






</asp:Content>
