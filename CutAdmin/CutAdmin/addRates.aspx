﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="addRates.aspx.cs" Inherits="CutAdmin.addRates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/InputIcon.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        
        <hgroup id="main-title" class="thin">
			<h1>Add Rate Details</h1>
            <h2><b class="grey"><strong>HOTEL</strong>: <strong><label id="lblHotel"></label></strong></b></h2>
            <hr/>			
		</hgroup>


                <div class="with-padding">
                    <div class="columns"> 
                    <div class="three-columns twelve-columns-mobile">                        
                            <label class="input-info">Supplier</label>
                            <select id="sel_Supplier" name="validation-select" class="select full-width">
                                <option value="25" selected="selected">From Hotel</option>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile" id="drp_Nationality">
                            <label >Nationality<span class="red">*</span></label>
                                <select id="sel_Nationality" onchange="CheckNationality(this.value);" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width" multiple>
                                    <option value="All">All&nbsp;/&nbsp;Unselect</option>     
                                </select>                               
                        </div>

                        <div class="three-columns twelve-columns-mobile SelBox" id="Currency">
                            <label >Currency<span class="red">*</span></label>
                            <select id="sel_CurrencyCode" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile SelBox">
                            <label >Meal Plan<span class="red">*</span></label>
                            <select id="sel_MealPlan" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                    </div>
                    
                  

                      <div class="columns">

                   
                    <div id="idRateValidity" class="four-columns twelve-columns-mobile cRateValidity">
                         <h4>Rate Validity</h4>
                     <hr/>
                         <label id="clickRV" style="display:none" none">1</label>
                        <div class="columns columnsRV" id="divRV0">
                            <div class="five-columns twelve-columns-mobile four-columns-tablet bold inputWithIcon">
                                <label class="input-info">From<span class="red">*</span></label>
                                   <input type="text" id="txtFromRV0" class="input full-width cRateValidFrom" style="cursor: pointer" value="">
                                  <i class="icon-calendar"></i>

                            </div>
                            <div class="five-columns ten-columns-mobile four-columns-tablet bold inputWithIcon">
                                 <label class="input-info">To<span class="red">*</span></label><br />
                                  <input type="text" id="txtToRV0"  class="input full-width cRateValidTo" value="" />
                                  <i class="icon-calendar"></i>
                            </div>
                          
                            <div class="two-columns plusminus">
                                <a id="lnkAddRV0" onclick="AddRateValidity(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>
                            </div>
                        </div>
                         <br />
                    </div>

                   

                    <div id="idSpecialDate" class="four-columns twelve-columns-mobile cSpecialDate" >
                           <h4   id="spldtheading">Special Dates</h4>  <hr/>
                          <label id="clickSD" style="display:none" none">1</label>
                        <div class="columns" id="divSD0">

                            <div class="five-columns twelve-columns-mobile four-columns-tablet bold inputWithIcon">
                                <label>From</label>
                                    <input type="text" id="txtFromSD0" class="input full-width ctxtfromSD" >
                                    <i class="icon-calendar"></i>
                            </div>
                            <div class="five-columns ten-columns-mobile four-columns-tablet bold inputWithIcon">
                                <label >To</label>
                                <input type="text" id="txtToSD0" class="input full-width ctxttoSD" />
                                   <i class="icon-calendar"></i>
                            </div>
                          
                            <div class="three-column plusminus">
                                
                                <a id="lnkAddSD0" onclick="AddSpecialDates(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>

                            </div>
                        </div>
                        <br />
                    </div>

                          <div class="four-columns">   <br />
                             <hr />
                              <div class="twelve-columns">
                                  <br />
                                    <a href="#" class="button anthracite-gradient" style="cursor:pointer" onClick="AddDates();">Add Dates</a>
                              </div>
                             
                          </div>

                           </div>
                    <div style="display:none" id="DivDates">
                        <h4 >Rooms</h4>
                    <br />
                    <div class="side-tabs same-height">
						<ul  class="tabs" id="TabRooms" >
						</ul>
						<div class="tabs-content NestesDiv" id="TabContent">
						</div>
					</div>

                    <div class="standard-tabs margin-bottom" id="hdndiv1" style="display:none"></div>
                     <br />
                     <br />
                     <br />

                 
                    <div class="side-tabs same-height">
						<ul class="tabs" id="TabRoomsSpl">
						</ul>
						<div class="tabs-content NestesDivSD" id="TabContentSpl">
						</div>
					</div>

                    <div class="standard-tabs margin-bottom" id="" style="display:none"></div>
                     <br />
                     <br />
                     <br />
                    </div>
                    

                
                <br />
                  
                       <h4 class="txt-detail">Tax Details</h4>
                     <hr/> 
                     <span class="labeltxt">(If Tax Already Included in above Rates)</span>
                      <label>Tax Included:</label>
                              <span class="button-group">
                                  <label for="chkTaxIncluded1" class="button grey-active">
                                      <input type="radio" checked name="button-radio chkTaxIncluded" id="chkTaxIncluded1" value="YES"  onclick="checkTaxApllied(this.value);" >
                                      YES
                                  </label>
                                  <label for="chkTaxIncluded2" class="button grey-active">
                                      <input type="radio"  name="button-radio chkTaxIncluded" id="chkTaxIncluded2" value="NO" onclick="checkTaxApllied(this.value);">
                                      NO
                                  </label>
                                 
                              </span>
                              
                               <input id="txtIncluded" style="display:none"none;"/>
                         
                      <div class="" id="div_TaxDetails">
                          
                     
                          
                       </div>
                      <div class="" id="div_UpdateTaxDetails" style="display:none">
                          
                     
                          
                       </div>
                      <br />
                       <br />
                     <h4 class="txt-detail">Tariff Policy</h4>
 <hr/>
                      <div class="columns">
                        
                          <div class="three-columns twelve-columns-mobile addinput ClassDynamic">
                              <label>Inclusion:</label>
                               <ul id="myUL" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV" class="header">
                                  <input type="text" id="myInput" placeholder="Title...">
                                  <span onclick="newElement()" class="addBtn">Add</span>
                              </div>

                             
                          </div>
                          <div class="three-columns twelve-columns-mobile ClassDynamic">
                              <label>Exclusion:</label>
                               <ul id="myUL1" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV1" class="header">
                                  <input type="text" id="myInput1" placeholder="Title...">
                                  <span onclick="newElement1()" class="addBtn">Add</span>
                              </div>
                          </div>
                           <div class="three-columns twelve-columns-mobile">
                              <label>Min Stay:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtminStay" class="input full-width">
                          </div>
                            <div class="three-columns twelve-columns-mobile SelBox">
                              <label>Upload Contract:</label>
                              <input type="file" id="uploadContract" value="" class="file">
                          </div>
                      </div>

         <p class="text-textright"><button type="button" onclick="SaveRates()" class="button anthracite-gradient ">Submit</button></p>   
                 
 </div> 
           

    </section>
</asp:Content>
