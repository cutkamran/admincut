﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="hotel.aspx.cs" Inherits="CutAdmin.hotel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="js/libs/AutoComplete.js"></script>
    <script src="js/developr.auto-resizing.js"></script>
    <script src="Scripts/Hotels/Hotels.js?v=1.7"></script>
    <script src="Scripts/ChargesMapping.js?v=1.34"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"></script>
    <script src="Scripts/GoogleMap.js?v=1.0"></script>
    <script src="js/UploadDoc.js?v=1.3"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Hotel Details</b></h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="frmHotel">
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Hotel Details</legend>

                    <div class="field-block button-height">
                        <small class="input-info">What is the name of this Hotel / appartement</small>
                        <label for="supplier" class="label"><b>Name</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile">
                                <input class="input validate[required]" type="text" id="txt_name" />
                                <input type="hidden" id="OldHotelID" />
                                <input type="hidden" id="HotelID" value="0" />
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <select id="sel_Ratings" class="full-width  validate[required]" multiple="multiple" data-placeholder="attraction...">
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <select id="txtHotelGroup" class="" data-placeholder="Hotel Group...">
                                </select>
                            </div>

                        </div>

                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Give details about this hotel / apprtment</small>
                        <label for="supplier" class="label"><b>Description</b></label>
                        <textarea class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft" type="text" id="txt_description"></textarea>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Tag the Hotel / appartement  with facility</small>
                        <label for="supplier" class="label"><b>Facility</b></label>
                        <select id="sel_Facility" class="" multiple="multiple" data-placeholder="Hotel Facility...">
                        </select>
                    </div>



                    <div class="field-block button-height">
                        <small class="input-info">Where is this Hotel</small>
                        <label for="supplier" class="label"><b>Location</b></label>
                        <div class="columns">
                            <input type="hidden" id="hdnlatitude" />
                            <input type="hidden" id="hdnlongitude" />
                            <input type="hidden" id="hdnPlaceid" />
                            <input type="hidden" id="hdnName" />
                            <input type="hidden" id="hdnHotelid" />
                            <input type="hidden" id="CityPlaceID" />
                            <input type="hidden" id="City_Name" />
                            <input type="hidden" id="lat_City" />
                            <input type="hidden" id="log_City" />

                            <div class="five-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <input class="input validate[required] full-width" id="txt_location" type="text" placeholder="Location" autocomplete="off">
                            </div>
                            <!-- replace with city once location is selected-->
                            <div class="three-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <label class="small-margin-right strong green" id="lbl_city"></label>
                                <input type="hidden" id="hdCityCode" />
                            </div>
                            <!-- replace with city once location is selected-->
                            <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <label class="small-margin-right strong green" id="lbl_country"></label>
                                <input type="hidden" id="hdCountryCode" />
                            </div>
                        </div>
                        <div class="columns">
                            <div class="eight-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <textarea class="input full-width autoexpanding validate[required]" placeholder="Hotel Address" data-prompt-position="topLeft" type="text" id="txt_Address"></textarea>
                            </div>
                            <!-- replace with city once location is selected-->
                            <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <input class="input validate[required]" id="txt_PinCode" type="text" placeholder="Zipcode.." autocomplete="off" style="height: 30px">
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Policy</legend>
                    <div class="field-block button-height">
                        <small class="input-info">Is there any  restriction for Pets in this Hotel/Appartment?</small>
                        <label for="childpolicy" class="label">
                            <b>Pets</b>
                        </label>
                        <p class="button-group compact Pets">
                            <label for="rdb_PN" class="button green-active">
                                <input type="radio" name="Pet" id="rdb_PN" value="Not Specified" checked>
                                Not Specified
                            </label>
                            <label for="rdb_PA" class="button green-active ">
                                <input type="radio" name="Pet" id="rdb_PA" value="Allowed">
                                Allowed
                            </label>
                            <label for="rdb_PNA" class="button green-active">
                                <input type="radio" name="Pet" id="rdb_PNA" value="Not allowed">
                                Not allowed  
                            </label>
                            <br />

                        </p>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Is there any age restriction for Smoking in this Hotel/Appartment?</small>
                        <label for="childpolicy" class="label"><b>Smoking</b></label>
                        <p class="button-group compact Smooking">
                            <label for="rdb_SN" class="button green-active">
                                <input type="radio" name="Smooking" id="rdb_SN" value="Not Specified" checked>
                                Not Specified
                            </label>
                            <label for="rdb_SA" class="button green-active ">
                                <input type="radio" name="Smooking" id="rdb_SA" value="Allowed">
                                Allowed
                            </label>
                            <label for="rdb_SNA" class="button green-active">
                                <input type="radio" name="Smooking" id="rdb_SNA" value="Not allowed">
                                Not allowed  
                            </label>
                            <br />

                        </p>
                    </div>
                    <div class="field-block button-height">
                        <label for="childpolicy" class="label"><b>Check-in Policy</b></label>
                        <div class="columns">
                            <div class="one-columns three-columns-mobile mid-margin-bottom align-center">
                                Checkin Time
                                <input type="text" id="div_Opening_Time" style="background: #e3e5ea" />

                            </div>
                            <div class="three-columns three-columns-mobile mid-margin-bottom align-center">
                                Checkout Time 
                                <input type="text" id="div_Closing_Time" style="background: #e3e5ea" />

                            </div>
                        </div>
                    </div>
                    <br />
                    <br />

                    <div class="field-block button-height">
                        <small class="input-info">There min age restrictions to participate in this Hotel/Appartment</small>
                        <label for="childpolicy" class="label"><b>Child Policy</b></label>
                        <p>
                            <label>Is there any age restriction for children in this Hotel/Appartment?</label>
                            <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_childpolicy" onchange="SetChildPolicy(this)">
                        </p>
                    </div>


                    <div class="field-drop black-inputs button-height" id="div_childpolicy" style="display: none">
                        <label for="childagefrom" class="label"><b>Age Restrictions:</b></label>
                        <p>
                            <label>Child from</label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="txtChildAgeFrm" onchange="ChangeAge()" type="text" value="2" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label>years old to </label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="txtChildAgeTo" type="text" value="12" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label for="childageto">years old consider as Child</label>
                            <br />
                            <small class="input-info white">You can change this policy as per rates at the time of rates feeding</small>
                        </p>
                    </div>


                    <div class="field-block button-height">
                        <label for="taxpolicy" class="label"><b>Tax Policy</b></label>
                        <p>
                            <label>Do you want to define extra Tax Policy?</label>
                            <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_taxpolicy" onchange="SetTaxPolicy(this)">
                        </p>
                    </div>

                    <div class="field-drop black-inputs button-height" id="div_TaxDetails" style="display: none">
                    </div>

                    <div class="field-block button-height">
                        <label for="mealpolicy" class="label"><b>Meal Policy</b></label>
                        <p>
                            <label>Do you want to define extra Meal Policy?</label>
                            <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_mealpolicy" onchange="SetMealPolicy(this)">
                        </p>
                    </div>

                    <div class="field-drop black-inputs button-height" id="div_MealDetails" style="display: none">
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Important information which you want your customers to know</small>
                        <label for="height polich" class="label"><b>Important Note</b></label>
                        <textarea class="input full-width autoexpanding" data-prompt-position="topLeft" type="text" id="txt_impnote"></textarea>
                    </div>

                </fieldset>


                <fieldset class="wizard-fieldset no-padding">
                    <legend class="legend">Images</legend>
                    <div class="content-panel mobile-panels margin-bottom">
                        <div class="panel-content linen">
                            <div class="panel-control align-right">
                                <span class="progress thin" style="width: 75px">
                                    <span class="progress-bar green-gradient" style="width: 0%"></span>
                                </span>
                                You may add <span class="ImgCount">10</span> more images
                        <input type="file" id="images" accept="image/*" class="hidden" onchange="preview_images(this,'result');" multiple />
                                <a class="button icon-cloud-upload margin-left file withClearFunctions" onclick="($('#images').click())">Add file...</a>
                            </div>
                            <div class="panel-load-target scrollable with-padding" style="height: auto">

                                <p class="message icon-info-round white-gradient">
                                    Add can add upto 10 images / photos related to this activity
                                    <input type="file" multiple id="Imges" onchange="preview_images()" class="btn-search5 hidden" />
                                </p>
                                <ul class="blocks-list fixed-size-200" id="result">
                                </ul>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="wizard-fieldset" style="border: 1px ridge">
                    <legend class="legend">Contacts</legend>
                    <div class="field-block  button-height ContactsBox">
                        <label for="childpolicy" class="label">
                            <b>HOTEL CONTACT</b>
                            <button type="button" class="button glossy mid-margin-right " onclick="AddContacts('HotelContacts')"><span class="button-icon anthracite-gradient"><span class="icon-plus-round"></span></span>More</button>
                        </label>
                        <div id="idHotelContacts">
                            <div class="columns div_HotelContacts">
                                <div class="twelve-columns eight-columns-tablet twelve-columns-mobile contdetais">
                                    <div class="columns">

                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="text" size="9" class="input full-width Person validate[required]" data-prompt-position="topLeft" value="" placeholder="Contact Person Name*">
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="email" class="input full-width Email validate[required]" data-prompt-position="topLeft" value="" placeholder="Email Id*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" size="9" class="input full-width Phone validate[required]" value="" placeholder="Phone Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Mobile validate[required]" value="" placeholder="Mobile Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Fax validate[required]" value="" placeholder="Fax Number">
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <br />
                        </div>

                    </div>

                    <div class="field-block  button-height ContactsBox">
                        <label for="childpolicy" class="label">

                            <b>RESERVATION</b>
                            <label for="chkSameReservation" class="label" style="float: right; margin-top: -14px;">
                                <input type="checkbox" id="chkSameReservation" value="1" class="checkbox" onchange="SameAbove(this,'HotelContacts','ReservationContacts');">
                                Same as Above</label><br />
                            <button type="button" class="button glossy mid-margin-right " onclick="AddContacts('ReservationContacts')"><span class="button-icon anthracite-gradient"><span class="icon-plus-round"></span></span>More</button>
                        </label>

                        <div id="idReservationContacts">
                            <div class="columns div_ReservationContacts">
                                <div class="twelve-columns twelve-columns-mobile contdetais">
                                    <div class="columns">

                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="text" size="9" class="input full-width Person validate[required]" data-prompt-position="topLeft" value="" placeholder="Contact Person Name*">
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="email" class="input full-width Email validate[required]" data-prompt-position="topLeft" value="" placeholder="Email Id*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="9" data-prompt-position="topLeft" class="input full-width Phone validate[required]" value="" placeholder="Phone Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Mobile validate[required]" value="" placeholder="Mobile Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Fax validate[required]" value="" placeholder="Fax Number">
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <br />
                        </div>



                        <div class="clear"></div>
                    </div>

                    <div class="field-block  button-height ContactsBox">
                        <label for="childpolicy" class="label">

                            <b>Account</b>
                            <label for="chkSameAccounts" class="label" style="float: right; margin-top: -14px;">
                                <input type="checkbox" class="checkbox" id="chkSameAccounts" value="1" onchange="SameAbove(this,'ReservationContacts','AccountsContacts');">
                                Same as Above</label><br>
                            <button type="button" class="button glossy mid-margin-right " onclick="AddContacts('AccountsContacts')"><span class="button-icon anthracite-gradient"><span class="icon-plus-round"></span></span>More</button>
                        </label>
                        <div id="idAccountsContacts">
                            <div class="columns div_AccountsContacts">
                                <div class="twelve-columns twelve-columns-mobile contdetais">
                                    <div class="columns">

                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="text" size="9" class="input full-width Person" data-prompt-position="topLeft" value="" placeholder="Contact Person Name*">
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <input type="email" class="input full-width Email" data-prompt-position="topLeft" value="" placeholder="Email Id*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" size="9" class="input full-width Phone" value="" placeholder="Phone Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Mobile" value="" placeholder="Mobile Number*">
                                        </div>
                                        <div class="four-columns twelve-columns-mobile">
                                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' data-prompt-position="topLeft" class="input full-width Fax" value="" placeholder="Fax Number">
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <br />

                        </div>
                    </div>

                    <div class="field-block button-height wizard-controls align-right">
                        <button type="button" class="button anthracite-gradient float-right" onclick="SaveHotel();">Save</button>
                    </div>

                </fieldset>
            </form>
        </div>
    </section>
</asp:Content>
