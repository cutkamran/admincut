﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using CutAdmin.DataLayer;

namespace CutAdmin.Services
{
    public class Hotels
    {
        public long HotelID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public static List<Hotels> GetHotels(string sAdminKey)
        {
            List<Hotels> arrHotels = new List<Hotels>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    if(objGlobalDefault.UserType != "Hotel")
                    {
                        IQueryable<Hotels> IQueryableHotel = (from obj in db.tbl_CommonHotelMaster
                                                              where obj.ParentID.ToString() == sAdminKey
                                                              select new Hotels
                                                              {
                                                                  HotelID = obj.sid,
                                                                  Name = obj.HotelName,
                                                                  Address = obj.HotelAddress
                                                              });
                        if (IQueryableHotel.ToList().Count == 0)
                            throw new Exception("No Hotels Found");
                        arrHotels = IQueryableHotel.ToList();
                    }

                    else
                    {
                        using (var DB = new DBHandlerDataContext())
                        {
                            var Groupid = (from obj in DB.tbl_Comm_HotelGroups where obj.Group_id == Convert.ToInt64(objGlobalDefault.StaffCategory) select obj.Group_id).FirstOrDefault();

                            var HotelCodes = (from obj in DB.tbl_Comm_HotelGroupMappings where obj.Group_id == Groupid select obj.Hotel_Code).ToList();

                           
                                IQueryable<Hotels> IQueryableHotel = (from obj in db.tbl_CommonHotelMaster
                                                                      where HotelCodes.Contains(obj.sid.ToString())
                                                                      select new Hotels
                                                                      {
                                                                          HotelID = obj.sid,
                                                                          Name = obj.HotelName,
                                                                          Address = obj.HotelAddress
                                                                      });
                                if (IQueryableHotel.ToList().Count == 0)
                                    throw new Exception("No Hotels Found");
                                arrHotels = IQueryableHotel.ToList(); 
                        }
                    }
      
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrHotels;
        }

        public static List<Hotels> GetHotelsModule(string sAdminKey)
        {
            List<Hotels> arrHotels = new List<Hotels>();
            try
            {
                using (var db = new ClickUrHotel_DBEntities())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    DBHandlerDataContext DB = new DBHandlerDataContext();
                    var Groupid = (from obj in DB.tbl_Comm_HotelGroups where obj.Group_Name == objGlobalDefault.ContactPerson select obj.Group_id).FirstOrDefault();

                    var HotelCodes = (from obj in DB.tbl_Comm_HotelGroupMappings where obj.Group_id == Groupid select obj.Hotel_Code).ToList();

                    IQueryable<Hotels> IQueryableHotel = (from obj in db.tbl_CommonHotelMaster
                                                          where obj.ParentID.ToString() == sAdminKey
                                                          select new Hotels
                                                          {
                                                              HotelID = obj.sid,
                                                              Name = obj.HotelName,
                                                              Address = obj.HotelAddress
                                                          });
                    if (IQueryableHotel.ToList().Count == 0)
                        throw new Exception("No Hotels Found");
                    arrHotels = IQueryableHotel.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrHotels;
        }
    }
}