﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Services
{
    public class User
    {
        public long? sid { get; set; }
        public string Name { get; set; }
        public string  Code { get; set; }
        public string  Address { get; set; }
        public string  email { get; set; }
        public string  Mobile { get; set; }
        public string  phone { get; set; }
        public string  PinCode { get; set; }
        public string  Fax { get; set; }
        public string  sCountry { get; set; }
        public string  Website { get; set; }
        public string  StateID { get; set; }
        public string  Countryname { get; set; }
        public string  City { get; set; }

        public string ContactPerson { get; set; }
        public string PassWord { get; set; }

        public string Currency { get; set; }
        public static User _DetailsBySupplier(long ID)
        {
            User OBJ = new User();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    IQueryable<User> arrIQueryable = (from obj in DB.tbl_AdminLogins
                                             join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                             from objh in DB.tbl_HCities
                                             where  objc.Code == objh.Code &&  obj.sid == ID
                                             select new User
                                             {
                                                 Name= obj.AgencyName,
                                                 Code = obj.Agentuniquecode,
                                                 Address= objc.Address,
                                                 email = objc.email,
                                                 Mobile =   objc.Mobile,
                                                 phone= objc.phone,
                                                 PinCode= objc.PinCode,
                                                 Fax= objc.Fax,
                                                 sCountry= objc.sCountry,
                                                 Website= objc.Website,
                                                 StateID= objc.StateID,
                                                 Countryname= objh.Countryname,
                                                 City= objh.Description
                                             });
                    if (arrIQueryable.Count() != 0)
                        OBJ = arrIQueryable.FirstOrDefault();
                }
            }
            catch (Exception)
            {
            }
            return OBJ;
        }

        public static User _SupplierByAgent(long? ID)
        {
            User OBJ = new User();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    IQueryable<long?> arrUserID = (from obj in DB.tbl_AdminLogins where obj.sid == ID select obj.ParentID);
                    if(arrUserID.ToList().Count !=0)
                    {
                        ID = arrUserID.FirstOrDefault();
                        IQueryable<User> arrIQueryable = (from obj in DB.tbl_AdminLogins
                                                          join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                          from objh in DB.tbl_HCities
                                                          where objc.Code == objh.Code && obj.sid == ID
                                                          select new User
                                                          {
                                                              sid = ID,
                                                              Name = obj.AgencyName,
                                                              Code = obj.Agentuniquecode,
                                                              Address = objc.Address,
                                                              email = objc.email,
                                                              Mobile = objc.Mobile,
                                                              phone = objc.phone,
                                                              PinCode = objc.PinCode,
                                                              Fax = objc.Fax,
                                                              sCountry = objc.sCountry,
                                                              Website = objc.Website,
                                                              StateID = objc.StateID,
                                                              Countryname = objh.Countryname,
                                                              City = objh.Description
                                                          });
                        if (arrIQueryable.Count() != 0)
                            OBJ = arrIQueryable.FirstOrDefault();
                    }
                    
                }
            }
            catch (Exception)
            {
            }
            return OBJ;
        }

        public static User _DetailByUserEmail(string Email)
        {

            User OBJ = new User();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    IQueryable<User> arrIQueryable = (from obj in DB.tbl_AdminLogins
                                                          join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                          from objh in DB.tbl_HCities
                                                          where objc.Code == objh.Code && obj.uid == Email
                                                          select new User
                                                          {
                                                            
                                                              Name = obj.AgencyName,
                                                              Code = obj.Agentuniquecode,
                                                              Address = objc.Address,
                                                              email = objc.email,
                                                              Mobile = objc.Mobile,
                                                              phone = objc.phone,
                                                              PinCode = objc.PinCode,
                                                              Fax = objc.Fax,
                                                              sCountry = objc.sCountry,
                                                              Website = objc.Website,
                                                              StateID = objc.StateID,
                                                              Countryname = objh.Countryname,
                                                              City = objh.Description,
                                                              PassWord = obj.password,
                                                              ContactPerson = OBJ.ContactPerson,
                                                              Currency= obj.CurrencyCode
                                                          });
                    if (arrIQueryable.Count() != 0)
                    {
                        OBJ = arrIQueryable.FirstOrDefault();
                        OBJ.PassWord = CutAdmin.Common.Cryptography.DecryptText(OBJ.PassWord);
                    }
                    else if(arrIQueryable.Count()==0)
                    {
                        IQueryable<User> arrData = (from obj in DB.tbl_StaffLogins where obj.uid == Email
                                                    select new User {
                                                        Name = obj.ContactPerson,
                                                        Code = obj.StaffUniqueCode,
                                                        PassWord= obj.password,
                                                        ContactPerson = obj.ContactPerson,
                                                        email = obj.uid,
                                                    });
                        if (arrData.Count() != 0)
                        {
                            OBJ = arrData.FirstOrDefault();
                            OBJ.PassWord = CutAdmin.Common.Cryptography.DecryptText(OBJ.PassWord);
                            OBJ.Name = CutAdmin.DataLayer.AccountManager.GetSupplierName();
                        }
                     }
                     
                }
            }
            catch (Exception)
            {
            }
            return OBJ;
        }

        public static User _DetailByUserID(long? id)
        {

            User OBJ = new User();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    IQueryable<User> arrIQueryable = (from obj in DB.tbl_AdminLogins
                                                      join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                      from objh in DB.tbl_HCities
                                                      where objc.Code == objh.Code && obj.sid == id
                                                      select new User
                                                      {
                                                          Name = obj.AgencyName,
                                                          Code = obj.Agentuniquecode,
                                                          Address = objc.Address,
                                                          email = objc.email,
                                                          Mobile = objc.Mobile,
                                                          phone = objc.phone,
                                                          PinCode = objc.PinCode,
                                                          Fax = objc.Fax,
                                                          sCountry = objc.sCountry,
                                                          Website = objc.Website,
                                                          StateID = objc.StateID,
                                                          Countryname = objh.Countryname,
                                                          City = objh.Description,
                                                          PassWord = obj.password,
                                                          ContactPerson = OBJ.ContactPerson,
                                                          Currency = obj.CurrencyCode
                                                      });
                    if (arrIQueryable.Count() != 0)
                    {
                        OBJ = arrIQueryable.FirstOrDefault();
                        OBJ.PassWord = CutAdmin.Common.Cryptography.DecryptText(OBJ.PassWord);
                    }
                    else if (arrIQueryable.Count() == 0)
                    {
                        IQueryable<User> arrData = (from obj in DB.tbl_StaffLogins
                                                    where obj.sid == id
                                                    select new User
                                                    {
                                                        Name = obj.ContactPerson,
                                                        Code = obj.StaffUniqueCode,
                                                        PassWord = obj.password,
                                                        ContactPerson = obj.ContactPerson,
                                                        email = obj.uid,
                                                    });
                        if (arrData.Count() != 0)
                        {
                            OBJ = arrData.FirstOrDefault();
                            OBJ.PassWord = CutAdmin.Common.Cryptography.DecryptText(OBJ.PassWord);
                            OBJ.Name = CutAdmin.DataLayer.AccountManager.GetSupplierName();
                        }
                    }

                }
            }
            catch (Exception)
            {
            }
            return OBJ;
        }

    }
}