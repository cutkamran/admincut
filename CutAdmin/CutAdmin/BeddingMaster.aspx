﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BeddingMaster.aspx.cs" Inherits="CutAdmin.BeddingMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">

    <script src="Scripts/BeddingMaster.js?v=1.2"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <section role="main" id="main">
          <hgroup id="main-title" class="thin">
            <h1>Bedding Type</h1>
        </hgroup>
          <hr />
          <div class="with-padding">
            <div class="columns">
             <div class="three-columns two-columns-tablet twelve-columns-mobile">
                    <label for="block-label-1" class="label">Bedding Type</label>
                   
                  <input type="text" name="block-label-1" id="txt_Beddingtype" class="input full-width" value="">
                 <input type="hidden" id="beddingTypeId">
                </div>
                <div class="three-columns two-columns-tablet twelve-columns-mobile" style="padding-top: 0.5%">
                    <br />
                    <button type="button" class="button anthracite-gradient" onclick="AddBeddingType()" id="btn_Add">ADD</button>
                     <button type="button" class="button anthracite-gradient" onclick="UpdateBeddingType()" style="display: none" id="btn_Update">Update</button>
                     </div>
                
                
                  </div>
             <table class="table responsive-table" id="tbl_BeddingMaster">
                <thead>
                    <tr>
                        <th scope="col"  class="align-center">Sr No</th>
                        <th scope="col"  class="align-center">Bedding Type</th>
                        <th scope="col"  class="align-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          
         </div>
     </section>




</asp:Content>
