﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agent/AgentMaster.Master" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="CutAdmin.Agent.HotelBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/b2bRates.js?v=1.15"></script>
    <script src="../js/GetMap.js"></script>
    <script type="text/javascript">
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        $(document).ready(function () {

            debugger;
            var data = getParameterByName('data');
            data.replace(/ /g, '+')
            if (data) {
                GetBookingDetails(data);
              
            }
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="bold grey">
        </hgroup>
        <div class=" columns with-padding">
            <div class="eight-columns six-columns-tablet twelve-columns-mobile" id="div_Rooms">
            </div>
            <div class="four-columns six-columns-tablet twelve-columns-mobile" id="div_Charges">
            </div>

        </div>

    </section>
</asp:Content>
