﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agent/AgentMaster.Master" AutoEventWireup="true" CodeBehind="AgentDashboard.aspx.cs" Inherits="CutAdmin.Agent.AgentDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Additional styles -->
    <script src="Scripts/Dashboard.js?v=1.7"></script>
    <script src="../Scripts/BookingList.js?v=1.2"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
<div class="with-small-padding ">
                    <!--First Row -->
                    <div class="columns">
                        <!--Booking Reconfirmations -->
                        <div class="three-columns  six-columns-mobile large-box-shadow orange-bg glossy " id="BookingsReconfirm">
                        </div>
                        <!--On Request Bookings-->
                        <div class="three-columns six-columns-mobile large-box-shadow blue-bg glossy " id="BookingsRequests">
                        </div>
                        <!--Bookings On Hold-->
                        <div class="three-columns six-columns-mobile large-box-shadow green-bg glossy" id="BookingOnHold">
                        </div>
                        <!--Group Request-->
                        <div class="three-columns six-columns-mobile large-box-shadow linen" id="GroupRequest">
                        </div>
                    </div>

                    <!--Second Row -->
                    <div class="columns">
                       <%-- <div class="three-columns six-columns-mobile large-box-shadow orange-bg glossy ">
                            <div class="with-mid-padding" style="background-color: darkgoldenrod; border-bottom: 2px solid navajowhite">
                                <h4 class="icon-line-graph icon-size2">Top Buyer</h4>

                            </div>

                            <div class="with-mid-padding" id="div_Buyer">
                            </div>
                        </div>--%>
                     <%--   <div class="three-columns six-columns-mobile large-box-shadow blue-bg">
                            <div class="with-mid-padding" style="background-color: steelblue; border-bottom: 2px solid lightblue">
                                <h4 class="icon-link icon-size2">Top Supplier</h4>
                            </div>
                            <div class="with-mid-padding" id="div_Supplier">
                            </div>
                        </div>--%>

                        <div class="six-columns six-columns-mobile large-box-shadow green-bg">
                            <div class="with-mid-padding" style="background-color: darkolivegreen; border-bottom: 2px solid greenyellow">
                                <h4 class="icon-globe icon-size2">Top Destinations</h4>

                            </div>
                            <div class="with-mid-padding" id="div_Destination">
                            </div>
                        </div>
                        <div class="six-columns six-columns-mobile large-box-shadow linen">
                            <div class="with-mid-padding" style="background-color: grey; border-bottom: 2px solid darkgrey">
                                <h4 class="icon-home icon-size2">Top Hotels</h4>
                            </div>
                            <div class="with-mid-padding" id="div_Hotels">
                            </div>
                        </div>

                    </div>
                    <div class="columns datepicker" id="">
                    </div>
                </div>
        
    </section>
</asp:Content>
