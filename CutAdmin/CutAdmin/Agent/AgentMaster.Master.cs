﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin.Agent
{
    public partial class AgentMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataLayer.AccountManager.CheckSession)
            {
                DataLayer.GlobalDefault objGlobalDefault = (DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string sPage = Page.Title;
                Page.Title = objGlobalDefault.AgencyName;
                if (sPage != "")
                    Page.Title += " :: " + sPage;
            }
            else
                Response.Redirect("../Default.aspx?error=Session Expired.");
        }
    }
}