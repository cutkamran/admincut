﻿var arrFilter = new Array();
function SearchByfilter() {
    session = GetQueryStringParams("data").replace(/%20/g, ' ');
    var arrCategory = $("#sel_Category").val();
    var Category = new Array();
    for (var i = 0; i < arrCategory.length; i++) {
        Category.push({Name:arrCategory[i]})
    }
    var arrLocation = $("#sel_Location").val();
    var Location = new Array();
    for (var i = 0; i < arrLocation.length; i++) {
        Location.push({ Name: arrLocation[i] })
    }

    var arrFacility = $("#sel_Facility").val();
    var Facility = new Array();
    for (var i = 0; i < arrFacility.length; i++) {
        Facility.push({ Name: arrFacility[i] })
    }
    var ndPrice = $(".tooltip-value");
    var MinPrice = $(ndPrice[0]).text();
    var MaxPrice = $(ndPrice[1]).text();
    var HotelName = $("#txt_HotelName").val();
    arrFilter={
        Category: Category,
        arrLocation: Location,
        arrFacility: Facility,
        MinPrice: MinPrice,
        MaxPrice: MaxPrice,
        HotelName: HotelName

    };
    $.ajax({
        type: "POST",
        url: "Handler/HotelHandler.asmx/SearchByFilters",
        data: JSON.stringify({ Search_Params: session, arrFilter: arrFilter }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotels = result.arrHotel;
                $("#itemcontent").empty();
                GenrateDetails();
                //windows.create({ "url": 'http://localhost:58826/Agent/b2bhotels.aspx', "incognito": true });
            }
            else if (result.retCode == 0) {
                AlertDanger(result.ex)
            }

        }
    });
}