﻿$(document).ready(function () {

    GetAgentInfo();
    GetSupplierName();
});

function GetAgentInfo() {
    $(window).unbind();
    debugger;
    $.ajax({
        type: "POST",
        url: "Handler/AgentHandler.asmx/GetAgentInfo",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#Agent_Info").empty();
            if (result.retCode == 1) {
                var CurrencyClass = result.CurrencyClass;
                switch (CurrencyClass) {
                    case "Currency-AED":
                        CurrencyClass = "AED";
                        break;
                    case "Currency-SAR":
                        CurrencyClass = "SAR";
                        break;
                    case "fa fa-eur":
                        CurrencyClass = "EUR";
                        break;
                    case "fa fa-gbp":
                        CurrencyClass = "GBP";
                        break;
                    case "fa fa-dollar":
                        CurrencyClass = "USD";
                        break;
                    case "fa fa-inr":
                        CurrencyClass = "INR";
                        break;
                }
                var html = "";
                $("#AgencyName").append('<b>Hi, ' + result.AgentName + '</b>')
                html += '<strong class="green">Available</strong>'
                html += '<strong>' + CurrencyClass + '  ' + result.AvailableCredit + '</strong>'
                html += '<strong class="green"> Credit</strong>'
                html += '<strong>' + CurrencyClass + '  ' + result.CreditLimit + '</strong>'
                $("#Agent_Info").append(html);
            }
            else if (result.retCode == 0) {
                window.location.href = "../Default.aspx";
            }
        },
        error: function () {
           // Success("An error occured while loading agent info")
        }
    });
}

function GetSupplierName() {
    $.ajax({
        type: "POST",
        url: "../Handler/AgentHandler.asmx/GetSupplierName",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 1) {
                var SupplierName = result.SupplierName[0].AgencyName;
                $('#SupplierName').text(SupplierName);
            }
        },
        error: function () {
            Success("An error occured while loading supplier name.");
        }
    });
}

