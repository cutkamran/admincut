﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using System.Globalization;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.Common
{
    public class RatesManager
    {
        public static string CheckInDate, CheckOutDate;
        static DBHandlerDataContext db = new DBHandlerDataContext();
        static dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        static ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
        #region Lib
        public class RatePrice
        {
            public string Currency { get; set; }
            public List<DateTime> Dates { get; set; }
            public string RateType { get; set; } /* Special  Date / Genral Date*/
            public string Daywise { get; set; }
            public decimal RRRate { get; set; }
            public decimal EBRate { get; set; }
            public decimal CWBRate { get; set; }
            public decimal CNBRate { get; set; }
            public decimal MonRate { get; set; }
            public decimal TueRate { get; set; }
            public decimal WedRate { get; set; }
            public decimal ThuRate { get; set; }
            public decimal FriRate { get; set; }
            public decimal SatRate { get; set; }
            public decimal SunRate { get; set; }
            public string CancelationPolicy { get; set; }
            public string CheckinDate { get; set; }
            public string CheckoutDate { get; set; }
            public string OfferID { get; set; }
            public string RateID { get; set; }
        }

        public class OfferRate
        {
            public List<DateTime> Dates { get; set; }
            public string Daywise { get; set; }
            public string OfferID { get; set; }
        }



        public class Supplier
        {
            public string Name { get; set; }
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string Nationality { get; set; }
            public string NationalityName { get; set; }
            public int noRooms { get; set; }
            public List<Details> Details { get; set; }

        }

        public class Details
        {
            public int RoomIndex { get; set; }
            public int RateTypeID { get; set; }
            public string RateType { get; set; }
            public int MealID { get; set; }
            public string MealName { get; set; }
            public int noRooms { get; set; }
            public Rate Rate { get; set; }

        }

        public class Rate
        {
            public string Currency { get; set; }
            public int RateID { get; set; }
            public float EBRate { get; set; }
            public float CNBRate { get; set; }
            public float CWBRate { get; set; }
            public Int64 MaxOccupancy { get; set; }
            public Int64 RoomOccupancy { get; set; }
            public Int64 MaxEB { get; set; }
            public Int64 MaxCWB { get; set; }
            public List<ListDates> ListDates { get; set; }
            public float SupplierMarkup { get; set; }
            public float CutMarkup { get; set; }
            public float TotalPrice { get; set; }
            public ChargeRate TotalCharge { get; set; }
            public List<CommonLib.Response.CancellationPolicy> ListCancellation { get; set; }
            public List<string> ListCancel { get; set; }
            public List<CommonLib.Request.Customer> LisCustumer { get; set; }
            public List<CommonLib.Response.includedMeal> arrMeals { get; set; }
        }
        public class ListDates
        {

            public string RateID { get; set; }
            public string Date { get; set; }
            public ChargeRate Rate { get; set; }
            public ChargeRate EBRate { get; set; }
            public ChargeRate CWBRate { get; set; }
            public ChargeRate CNBRate { get; set; }
            public float TotalPrice { get; set; }
            public string Type { get; set; }
            public List<CommonLib.Response.includedMeal> arrMeals { get; set; }
            public List<CutAdmin.BookingHandler.Addons> ListCommAddons { get; set; }

        }
        #endregion

        #region GetRateList
        public static List<TaxRate> GetRates(Int64 HotelCode)
        {
            List<TaxRate> arrRates = new List<TaxRate>();
            try
            {
                var TaxRates = dbTax.Comm_TaxMappings.Where(d => d.HotelID == HotelCode && d.IsAddOns == false).ToList();
                foreach (var objRate in TaxRates.Select(d => d.TaxID).Distinct().ToList())
                {
                    var ListTaxes = TaxRates.Where(r => r.TaxID == objRate).ToList().Select(d => d.TaxOnID).Distinct().ToList();
                    List<Tax> arrTax = new List<Tax>();
                    foreach (var objTax in ListTaxes)
                    {
                        if (dbTax.Comm_Taxes.Where(d => d.ID == objTax).ToList().Count != 0)
                        {
                            arrTax.Add(new Tax
                            {
                                ID = Convert.ToInt64(objTax),
                                TaxName = dbTax.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Name,
                                TaxPer = Convert.ToSingle(dbTax.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Value),
                                TaxRate = 0,
                            });
                        }
                        else if (objTax == 0)
                        {
                            arrTax.Add(new Tax
                            {
                                TaxName = "Base Rate",
                                TaxPer = 0,
                                TaxRate = 0,
                            });
                        }

                    }
                    if (dbTax.Comm_Taxes.Where(d => d.ID == objRate).ToList().Count != 0)
                    {
                        arrRates.Add(new TaxRate
                        {
                            ID = Convert.ToInt64(objRate),
                            RateName = dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Name,//+ " " + dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Value + " (%)",
                            Per = Convert.ToSingle(dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Value),
                            TaxOn = arrTax
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrRates;
        }

        public static date GetTaxRate(date objDate, Int64 HotelCode)
        {
            date arrDate = objDate;
            arrDate.objCharges = new ServiceCharge();
            float SupplierMarkup = 0, S2SMarkup = 0, BaseRate = 0;
            List<TaxRate> S2CTax = new List<TaxRate>();
            List<TaxRate> B2BTax = new List<TaxRate>();
            MarkupTaxManager.objMarkupCommission = objMarckupCommission;
            arrDate.HotelRates = new List<TaxRate>();
            if (arrDate.HotelRates != null)
            {
                arrDate.HotelRates = TaxManager.GetTaxRates(ListRateTypes, arrDate.Total);
            }
            arrDate.objCharges = MarkupTaxManager.GetRoomAmount(arrDate.Total, arrDate.HotelRates);
            BaseRate = arrDate.Total + arrDate.HotelRates.Select(d => d.TotalRate).Sum();
            objDate.AdminMarkup = MarkupTaxManager.GetRoomAmount(BaseRate, out SupplierMarkup, out S2SMarkup, out S2CTax, out B2BTax);
            objDate.Markup = 0;
            objDate.S2SMarkup = S2SMarkup;
            arrDate.S2CRates = S2CTax;
            arrDate.B2BRates = B2BTax;
            arrDate.Total = BaseRate + arrDate.AdminMarkup + arrDate.Markup + S2SMarkup + B2BTax.Select(d => d.TotalRate).Sum() + S2CTax.Select(d => d.TotalRate).Sum();

            return arrDate;
        }

        public static ChargeRate GetRate(float Rate, string Name)
        {
            ChargeRate objChageRate = new ChargeRate();
            List<TaxRate> S2CTax = new List<TaxRate>();
            List<TaxRate> B2BTax = new List<TaxRate>();
            float SupplierMarkup = 0, S2SMarkup = 0, BaseRate = 0;
            MarkupTaxManager.objMarkupCommission = objMarckupCommission;
            objChageRate.Charge = TaxManager.GetTaxRates(ListRateTypes, Rate); ;
            BaseRate = Rate + objChageRate.Charge.Select(d => d.TotalRate).ToList().Sum();
            float AdminMarkup = MarkupTaxManager.GetRoomAmount(BaseRate, out SupplierMarkup, out S2SMarkup, out S2CTax, out B2BTax);
            float Markup = SupplierMarkup;
            objChageRate.BaseRate = BaseRate;
            objChageRate.TotalRate = objChageRate.BaseRate + AdminMarkup + Markup + S2SMarkup + B2BTax.Select(d => d.TotalRate).Sum() + S2CTax.Select(d => d.TotalRate).Sum();
            return objChageRate;
        }

        public static ChargeRate GetRateByCancellation(float Rate, out float SupplierMarkup, out float AdminMarkup, out float S2SMarkup)
        {
            ChargeRate objChageRate = new ChargeRate();
            SupplierMarkup = 0; AdminMarkup = 0; S2SMarkup = 0; float BaseRate = 0;
            try
            {
                List<TaxRate> S2CTax = new List<TaxRate>();
                List<TaxRate> B2BTax = new List<TaxRate>();
                CutAdmin.DataLayer.MarkupTaxManager.objMarkupCommission = objMarckupCommission;
                objChageRate.Charge = TaxManager.GetTaxRates(ListRateTypes, Rate); ;
                BaseRate = Rate + objChageRate.Charge.Select(d => d.TotalRate).ToList().Sum();
                SupplierMarkup = CutAdmin.DataLayer.MarkupTaxManager.GetRoomAmount(BaseRate, out AdminMarkup, out S2SMarkup, out S2CTax, out B2BTax);
                float Markup = AdminMarkup;
                objChageRate.BaseRate = BaseRate;
                objChageRate.ListB2BTax = B2BTax;
                objChageRate.ListS2CTax = S2CTax;
                objChageRate.TotalRate = BaseRate + Markup + B2BTax.Select(d => d.TotalRate).Sum() + S2CTax.Select(d => d.TotalRate).Sum();
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return objChageRate;
        }
        public static List<TaxRate> ListRateTypes { get; set; }
        public static Int64 UserID { get; set; }
        public static Int64 noDaysGap { get; set; }
        public static MarkupCommission objMarckupCommission { get; set; }
        public static MarkupCommission objS2CMarckupTax { get; set; }
        public static HttpContext context { get; set; }
        /// <summary>
        ///  Get Rate By Hotels
        /// </summary>
        //public static List<RateGroup> GetRateList(Int64 HotelCode, string CheckIn, string CheckOut, string[] Nationality, List<tbl_commonRoomDetails> ListRoom, string SearchValid, Int64 noRooms)
        //{
        //    List<DateTime> List_SearchDate = new List<DateTime>();
        //    List<RoomType> ListRate = new List<RoomType>();
        //    List<RateGroup> ListRateGroup = new List<RateGroup>();
        //    try
        //    {
        //        objMarckupCommission = new MarkupCommission();
        //        bool OtherInventory = false;
        //        Int64 ParentID = AccountManager.GetSupplierByUser();
        //        #region Assign Markup
        //        if (HttpContext.Current.Session["LoginUser"] == null)
        //            throw new Exception("Session Expired ,Please Login and try Again!!");
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Int64 uid = AccountManager.GetSupplierByUser();

        //        using (var Context = new DBHandlerDataContext())
        //        {
        //            if (Context.Comm_CancellationDays.Where(d => d.UserID == uid).FirstOrDefault() != null)
        //                noDaysGap = Convert.ToInt64(Context.Comm_CancellationDays.Where(d => d.UserID == uid).FirstOrDefault().DaysBefore);
        //        }
        //        #endregion

        //        ListRateTypes = GetRates(HotelCode);

        //        CheckInDate = CheckIn;
        //        CheckOutDate = CheckOut;

        //        // var SupplierID = new List<Int64>();
        //        var SupplierID = new List<String>();
        //        using (var db = new ClickUrHotel_DBEntities())
        //        {
        //            SupplierID = (from obj in db.tbl_commonRoomRate
        //                          where obj.HotelID == HotelCode
        //                          select obj.SupplierId.ToString()).Distinct().ToList();
        //            DBHandlerDataContext dba = new DBHandlerDataContext();
        //            var adm = dba.tbl_AdminLogins.AsEnumerable();
        //            OtherInventory = Convert.ToBoolean((from obj in adm where obj.sid == ParentID select obj).FirstOrDefault().InventoryFlag);
        //        }
        //        foreach (var Supplier in SupplierID)
        //        {
        //            if (OtherInventory == false && Supplier != ParentID.ToString())
        //                continue;
        //            objS2CMarckupTax = new MarkupCommission();
        //            if (ParentID.ToString() != Supplier)
        //                objMarckupCommission = MarkupTaxManager.GetMarkupTax(ParentID, HotelCode);
        //            List<CommonLib.Response.HotelOccupancy> ListRates = GetRate(HotelCode, Convert.ToInt64(Supplier), CheckIn, CheckOut, ListRoom, SearchValid, Nationality[0], noRooms);
        //            if (objGlobalDefault.UserType == "Agent")
        //            {
        //                ListRateGroup.Add(new RateGroup
        //                {
        //                    AgentMarkup = 0,
        //                    AvailToken = HotelCode.ToString(),
        //                    Charge = new ServiceCharge(),
        //                    CutPrice = 0,
        //                    //Name = (from obj in db.tbl_APIDetails where obj.Hotel == true && obj.sid == Supplier select obj).FirstOrDefault().Supplier + "_" + SupplierID.IndexOf(Supplier),
        //                    Name = Supplier.ToString(),
        //                    RoomOccupancy = ListRates,
        //                    Nationality = Nationality[0].ToString(),
        //                    offerApplied = ListRates.Any(d => d.offerApplied == true),
        //                });
        //                if (ListRateGroup.LastOrDefault().RoomOccupancy.Count != 0)
        //                {
        //                    ListRateGroup.LastOrDefault().Total = GetTotal(ListRates);
        //                }
        //            }

        //            else
        //            {
        //                foreach (var objNation in Nationality)
        //                {
        //                    ListRates = GetRate(HotelCode, Convert.ToInt64(Supplier), CheckIn, CheckOut, ListRoom, SearchValid, objNation, noRooms);
        //                    ListRateGroup.Add(new RateGroup
        //                    {
        //                        AgentMarkup = 0,
        //                        AvailToken = HotelCode.ToString(),
        //                        Charge = new ServiceCharge(),
        //                        CutPrice = 0,
        //                        //Name = (from obj in db.tbl_APIDetails where obj.Hotel == true && obj.sid == Supplier select obj).FirstOrDefault().Supplier +"_" +SupplierID.IndexOf(Supplier),
        //                        Name = Supplier.ToString(),
        //                        RoomOccupancy = ListRates,
        //                        Nationality = objNation.ToString(),
        //                        offerApplied = ListRates.Any(d => d.offerApplied == true),
        //                    });
        //                    if (ListRateGroup.LastOrDefault().RoomOccupancy.Count != 0)
        //                    {
        //                        ListRateGroup.LastOrDefault().Total = GetTotal(ListRates);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
        //        throw new Exception(ex.Message);
        //    }

        //    return ListRateGroup.OrderBy(d => d.Total).ToList();
        //}

        public static List<RateGroup> GetRateList(Int64 HotelCode, string CheckIn, string CheckOut, string[] Nationality, List<tbl_commonRoomDetails> ListRoom, string SearchValid, Int64 noRooms)
        {
            List<DateTime> List_SearchDate = new List<DateTime>();
            List<RoomType> ListRate = new List<RoomType>();
            List<RateGroup> ListRateGroup = new List<RateGroup>();
            try
            {
                objMarckupCommission = new MarkupCommission();
                bool OtherInventory = false;
                Int64 ParentID = AccountManager.GetSupplierByUser();
                #region Assign Markup
                if (context == null)
                    context = HttpContext.Current;
                context = HttpContext.Current;
                if (context.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
               
                Int64 uid = AccountManager.GetSupplierByUser(); //AccountManager.GetSupplierByUser();
                ParentID = uid;
                using (var Context = new ClickUrHotel_DBEntities())
                {
                    if (Context.Comm_CancellationDay.Where(d => d.UserID == uid).FirstOrDefault() != null)
                        noDaysGap = Convert.ToInt64(Context.Comm_CancellationDay.Where(d => d.UserID == uid).FirstOrDefault().DaysBefore);
                }
                #endregion
                ListRateTypes = GetRates(HotelCode);
                CheckInDate = CheckIn;
                CheckOutDate = CheckOut;
                var SupplierID = new List<long?>();
                DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var db = new ClickUrHotel_DBEntities())
                {
                    SupplierID = (from obj in db.tbl_RatePlan
                                  where obj.HotelID == HotelCode
                                  select obj.SupplierID).Distinct().ToList();
                    OtherInventory = Convert.ToBoolean((from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault().InventoryFlag);
                }
                foreach (var Supplier in SupplierID)
                {
                    if (OtherInventory == false && Supplier != ParentID)
                        continue;
                    objS2CMarckupTax = new MarkupCommission();
                    if (ParentID != Supplier)
                        objMarckupCommission = CutAdmin.DataLayer.MarkupTaxManager.GetMarkupTax(ParentID, HotelCode);
                    List<CommonLib.Response.HotelOccupancy> ListRates = GetRate(HotelCode, Convert.ToInt64(Supplier), CheckIn, CheckOut, ListRoom, SearchValid, Nationality[0], noRooms);
                    ListRateGroup.Add(new RateGroup
                    {
                        AgentMarkup = 0,
                        AvailToken = HotelCode.ToString(),
                        Charge = new ServiceCharge(),
                        CutPrice = 0,
                        //Name = (from obj in db.tbl_APIDetails where obj.Hotel == true && obj.sid == Supplier select obj).FirstOrDefault().Supplier + "_" + SupplierID.IndexOf(Supplier),
                        Name = Supplier.ToString(),
                        RoomOccupancy = ListRates,
                        Nationality = Nationality[0].ToString(),
                    });
                    if (ListRateGroup.LastOrDefault().RoomOccupancy.Count != 0)
                    {
                        ListRateGroup.LastOrDefault().Charge.TotalPrice = GetTotal(ListRates);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }

            return ListRateGroup;
        }

        public static float GetTotal(List<CommonLib.Response.HotelOccupancy> arrRates)
        {
            float TotalRate = 0;
            try
            {
                foreach (var objPrice in arrRates.Select(d => d.MinPrice).ToList())
                {
                    var arrRate = arrRates.Where(d => d.MinPrice == objPrice).FirstOrDefault();
                    TotalRate += arrRate.MinPrice * arrRate.RoomCount;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return TotalRate;
        }
        #endregion

        #region GetRate
        public static DateTime dtCheckIn { get; set; }
        public static DateTime dtCheckOut { get; set; }

        public static List<CommonLib.Response.HotelOccupancy> GetRate(Int64 HotelCode, Int64 SupplierID, string Checkin, string Checkout, List<tbl_commonRoomDetails> rooms, string SearchValid, string Nationality, Int64 noRooms)
        {
            List<CommonLib.Response.HotelOccupancy> ListSearch = ParseCommonResponse.GetSearchOccupancy();
            List<CommonLib.Response.HotelOccupancy> DistinctSearch = ParseCommonResponse.GetDISTINST_OCCUPANCY(ListSearch);
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            objMarckupCommission = CutAdmin.DataLayer.MarkupTaxManager.GetMarkupTax(SupplierID, HotelCode);
            try
            {
                dtCheckIn = DateTime.ParseExact(Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                dtCheckOut = DateTime.ParseExact(Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-1);
                Int64 noNights = (dtCheckOut.AddDays(1) - dtCheckIn).Days;
                foreach (var Search in DistinctSearch)
                {
                    List<CommonLib.Response.RoomType> ListRoomType = new List<RoomType>();
                    Search.Rooms = new List<RoomType>();
                    var arrRate = new List<tbl_TarrifPlan>();
                    var sRate = new List<tbl_TarrifPlan>();
                    // ** Room Availbility
                    foreach (var objRoom in rooms)
                    {
                        using (var db = new ClickUrHotel_DBEntities())
                        {
                            arrRate = new List<tbl_TarrifPlan>();
                            List<CommonLib.Response.date> Dates = new List<date>();
                            List<CommonLib.Response.RoomType> OfferRATE = new List<CommonLib.Response.RoomType>();
                            List<CommonLib.Response.CancellationPolicy> CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();
                            arrRate = (from obj in db.tbl_TarrifPlan
                                       join objPlan in db.tbl_RatePlan on obj.nPlanID equals objPlan.PlanID
                                       where
                                        objPlan.ParentID == SupplierID &&
                                        objPlan.HotelID == HotelCode &&
                                        obj.sRate_Type == "GN" && (objPlan.Market.Contains("All Market")) &&
                                        obj.RoomTypeID == objRoom.RoomTypeId &&
                                        obj.nMinStay <= noNights && (obj.nMaxStay <= noNights || obj.nMaxStay == 0) && (obj.nMaxRoom <= noRooms || obj.nMaxRoom == 0)
                                       select obj).ToList();
                            if (Nationality != "")
                            {
                                var NationalityRate = (from obj in db.tbl_TarrifPlan
                                                       join objPlan in db.tbl_RatePlan on obj.nPlanID equals objPlan.PlanID
                                                       where objPlan.ParentID == SupplierID &&
                                                       objPlan.HotelID == HotelCode && obj.bStatus == true
                                                       && obj.sRate_Type == "GN" && (objPlan.Market.Contains(Nationality + ",")) &&
                                                       obj.RoomTypeID == objRoom.RoomTypeId &&
                                                       //obj.CurrencyCode == objGlobalDefault.Currency &&
                                                       obj.nMinStay <= noNights && (obj.nMaxStay <= noNights || obj.nMaxStay == 0) && (obj.nMaxRoom <= noRooms || obj.nMaxRoom == 0)
                                                       select obj).ToList();
                                foreach (var objRate in NationalityRate)
                                {
                                    arrRate.Add(objRate);
                                }
                            }
                            foreach (var Rate in arrRate)
                            {
                                var calendar = new List<DateTime>();
                                DateTime RateCheckin = DateTime.ParseExact(Rate.sFromDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                DateTime RateCheckOut = DateTime.ParseExact(Rate.sToDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                while (RateCheckin <= RateCheckOut)
                                {
                                    calendar.Add(RateCheckin);
                                    RateCheckin = RateCheckin.AddDays(1);
                                }
                                if (calendar.Contains(dtCheckIn) || calendar.Contains(dtCheckOut))
                                {
                                    List<CommonLib.Response.date> rateDates = new List<date>();
                                    // ** Occpancy Availbility
                                    if (ValidOccpancy(Convert.ToInt64(objRoom.RoomId), Convert.ToInt64(Search.AdultCount), Convert.ToInt64(Search.ChildCount), Search.ChildAges) == true || SearchValid == "General")
                                    {
                                        sRate.Add(Rate);
                                        rateDates = GetCharges(Search, Rate, out CancellationPolicy, out OfferRATE, objRoom, SearchValid, Search);
                                        foreach (var obj in rateDates)
                                        {
                                            Dates.Add(obj);
                                        }
                                    }
                                }

                            }
                            if (Dates.Count == 0)
                                continue;
                            foreach (var sMealPlan in Dates.Select(d => d.MealPlan).Distinct().ToList())
                            {
                                string RoomTypeId = "";
                                List<date> sRateDate = Dates.Where(d => d.MealPlan == sMealPlan).Distinct().ToList()
                                                        .GroupBy(d => d.datetime).ToList().Select(g => g.First()).ToList(); 
                                List<string> arrRateID = sRateDate.Select(r => r.RateTypeId.ToString()).Distinct().ToList();
                                for (int i = 0; i < arrRateID.Count; i++)
                                { 
                                    RoomTypeId += arrRateID[i] + "_";
                                }
                                bool ValidDate = true;
                                List<DateTime> col = sRateDate.Select(d => DateTime.ParseExact(d.datetime, "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();
                                DateTime currentDate = dtCheckIn;
                                var calendar = new List<DateTime>();
                                var targetDate = dtCheckIn;
                                while (targetDate <= dtCheckOut)
                                {
                                    calendar.Add(targetDate);
                                    targetDate = targetDate.AddDays(1);
                                }

                                var missingDates = (from date in calendar
                                                    where !col.Contains(date)
                                                    select date).ToList();
                                if (missingDates.Count != 0)
                                    ValidDate = false;
                                if (ValidDate)
                                {

                                    // List<string> CancellationID = ListCancellation(sRateDate);
                                   
                                    //List<TaxRate> otherRate = GetOtherRate(sRateDate);
                                
                                    if (sRateDate.Where(f => f.offerID != "" || f.offerID != null).Select(d => d.offerID).ToList().Count != 0)
                                        ListRoomType.Add(GetOffer(sRateDate, Search));
                                    float Total = sRateDate.Select(d => d.Total).Sum();// +otherRate.Select(d => d.TotalRate).Sum();
                                    CancellationPolicy = GetCancellationPolicyByRate(sRateDate);
                                    ListRoomType.Add(new RoomType
                                    {
                                        Total = Total,
                                        RoomRateTypeCurrency = sRateDate.Select(d => d).FirstOrDefault().Currency,
                                        HotelTaxRates = GetOtherRate(sRateDate.Select(d => d.HotelRates).ToList()),
                                        S2SMarkup = sRateDate.Select(d => d.S2SMarkup).Sum(),
                                        B2BMarkup = sRateDate.Select(d => d.AdminMarkup).Sum(),
                                        S2STaxRates = GetOtherRate(sRateDate.Select(d => d.S2CRates).ToList()),
                                        B2BTaxRates = GetOtherRate(sRateDate.Select(d => d.B2BRates).ToList()),
                                        dates = sRateDate,
                                        RoomTypeId = objRoom.RoomTypeId.ToString(),
                                        RoomDescriptionId = RoomTypeId.TrimEnd('_'),
                                        RoomTypeName = (from obj in db.tbl_commonRoomType where obj.RoomTypeID == objRoom.RoomTypeId select obj).FirstOrDefault().RoomType,
                                        CancellationPolicy = CancellationPolicy,
                                        RoomDescription = sMealPlan,
                                        ListCancel = CutAdmin.Common.RatesManager.CancellationPolicies(CancellationPolicy),
                                        arrOffers = new List<OfferSeason>(),
                                        arrMeals = GetMeals(sRateDate, Search, sMealPlan),
                                        objCharges = GetCharge(sRateDate),
                                    });

                                    foreach (var DatesOffer in ListRoomType.LastOrDefault().dates)
                                    {
                                        foreach (var Offer in DatesOffer.aarOffer.Distinct())
                                        {
                                            ListRoomType.LastOrDefault().arrOffers.Add(Offer);
                                        }
                                    }
                                   
                                }

                            }
                            ListRoomType.LastOrDefault().arrOffers =   ListRoomType.LastOrDefault().arrOffers.GroupBy(d => d.offerName).Select(g => g.First()).ToList(); ;
                            foreach (var RATE in OfferRATE)
                            {
                                ListRoomType.Add(RATE);
                            }
                            ListRoomType = ListRoomType.Where(D => D.Total != 0).ToList();
                            Search.Rooms = ListRoomType.OrderBy(d => d.Total).ToList();
                            if (ListRoomType.Count != 0)
                            {
                                Search.MinPrice = ListRoomType.Select(d => d.Total).ToList().Min();
                                Search.MaxPrice = ListRoomType.Select(d => d.Total).ToList().Max();
                            }

                            //}
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return DistinctSearch;
        }

        

        public static List<CommonLib.Response.date> GetCharges(CommonLib.Response.HotelOccupancy RoomSearch, EntityModal.tbl_TarrifPlan Rate, out List<CommonLib.Response.CancellationPolicy> ListCancellation, out List<CommonLib.Response.RoomType> ListOfferRate, tbl_commonRoomDetails room, string SearchValid, CommonLib.Response.HotelOccupancy objSerach)
        {
            List<CommonLib.Response.date> ListOfDay = new List<date>();
            ListCancellation = new List<CancellationPolicy>();
            ListOfferRate = new List<CommonLib.Response.RoomType>();
            List<string> CancellationID = new List<string>();
            List<string> OfferID = new List<string>();
            DateTime sCheckIn, sCheckOut;
            try
            {
                var sGeneralRate = new List<RatePrice>();
                sCheckIn = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                sCheckOut = DateTime.ParseExact(CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-1);
                double noCDays = (sCheckOut - sCheckIn).TotalDays;
                List<DateTime> ListGDates = new List<DateTime>();
                string RateCurrency = cdb.tbl_RatePlan.Where(d => d.PlanID == Rate.nPlanID).FirstOrDefault().sCurrency;
                for (int i = 0; i <= noCDays; i++)
                {
                    ListGDates.Add(sCheckIn.AddDays(i));
                    string sMonth = sCheckIn.AddDays(i).Month.ToString();
                    string sYear = sCheckIn.AddDays(i).Year.ToString();
                    IQueryable<tbl_DatesRate> arrData = cdb.tbl_DatesRate.Where(d => d.nPlanID == Rate.nPlanID &&
                    d.sMonth == sMonth && d.sYear == sYear);
                    if (arrData.ToList().Count != 0)
                    {
                        decimal RRate = RoomRateManager.GetRateByDate(sCheckIn.AddDays(i).Day.ToString(), arrData.Where(d => d.PaxType == "RR").FirstOrDefault());
                        decimal CWBRate = RoomRateManager.GetRateByDate(sCheckIn.AddDays(i).Day.ToString(), arrData.Where(d => d.PaxType == "CW").FirstOrDefault());
                         decimal EBRate = RoomRateManager.GetRateByDate(sCheckIn.AddDays(i).Day.ToString(), arrData.Where(d => d.PaxType == "EB").FirstOrDefault()); ;
                        decimal CNBRate = RoomRateManager.GetRateByDate(sCheckIn.AddDays(i).Day.ToString(), arrData.Where(d => d.PaxType == "CN").FirstOrDefault());
                        List<CommonLib.Response.InventoryType> Inventory = new List<InventoryType>();
                        GlobalDefault objGlobalDefault = (GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                        Int64 AvailBility = 0;
                        float ExchangeRate = 1;
                        if(RateCurrency != objGlobalDefault.Currency)
                            ExchangeRate= ExchangeRateManager.Exchange(RateCurrency, false) /
                            ExchangeRateManager.Exchange(objGlobalDefault.Currency, true);
                        string UserCurrency = objGlobalDefault.Currency;
                        if (objGlobalDefault.UserType != "Agent")
                        {
                            AvailBility = InventoryManager.GetInventoryCount(Rate.nHotelID.ToString(), Convert.ToDecimal(Rate.nPlanID), Rate.RoomTypeID.ToString(), sCheckIn.AddDays(i).ToString("dd-MM-yyyy"), AccountManager.GetSupplierByUser());
                        }
                        date Chk_Date = new date
                        {
                            Type = cdb.tbl_RatePlan.Where(d => d.PlanID == Rate.nPlanID).FirstOrDefault().RateType,
                            MealPlan = db.tbl_MealPlans.Where(d => d.ID == Rate.nMeal_PlanID).FirstOrDefault().Meal_Plan,
                            Currency = cdb.tbl_RatePlan.Where(d => d.PlanID == Rate.nPlanID).FirstOrDefault().sCurrency,
                            datetime = sCheckIn.AddDays(i).ToString("dd-MM-yyyy"),
                            NoOfCount = AvailBility,
                            day = sCheckIn.AddDays(i).DayOfWeek.ToString(),
                            RoomRate = Convert.ToSingle(RRate) * ExchangeRate,
                            ChidWBedRate = Convert.ToSingle(0),
                            EBRate = Convert.ToSingle(0),
                            CNBRate = Convert.ToSingle(0),
                            RoomTypeId = Convert.ToInt32(Rate.RoomTypeID),
                            RateTypeId = Convert.ToInt32(Rate.nRateID),
                            NoOfInventory = Inventory,
                            Total = Convert.ToSingle(RRate) * ExchangeRate,
                            HotelRates = new List<TaxRate>(),
                            offerID = Rate.sOffer,
                            including = GetMeals(Convert.ToInt64(Rate.nRateID), Convert.ToInt64(Rate.ParentID)),
                            UserCurrency = objGlobalDefault.Currency
                        };
                        if (Rate.nCancellation != 0)
                            CancellationID.Add(Rate.nCancellation.ToString());
                        else
                            CancellationID.Add("1");
                        if (SearchValid == "")
                            ListOfDay.Add(GetRateByOccupancy(Chk_Date, Convert.ToSingle(EBRate) * ExchangeRate, Convert.ToSingle(CWBRate) * ExchangeRate, Convert.ToSingle(CNBRate) * ExchangeRate, objSerach, Rate.nHotelID.ToString()));
                        else
                            ListOfDay.Add(Chk_Date);
                        #region Offer Manager
                        ListOfDay.Last().aarOffer = new List<OfferSeason>();
                        if(ListOfDay.Last().offerID != null &&  ListOfDay.Last().offerID !="")
                                ListOfDay.Last().aarOffer = CutAdmin.DataLayer.Datalayer.OfferManager.GetOfferByRate(ListOfDay.Last(), "AllCountry", sCheckIn, Convert.ToInt64(  noCDays));
                        #endregion
                        ListOfDay.LastOrDefault().Equals(GetTaxRate(ListOfDay.Last(), Convert.ToInt64(Rate.nHotelID)));
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListOfDay;
        }

        public static List<string> ListCancellation(List<CommonLib.Response.date> Dates)
        {
            ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
            List<string> sCancellationId = new List<string>();
            try
            {
                foreach (var RateID in Dates.Select(d => d.RateTypeId).Distinct().ToList())
                {
                    var arrRate = (from obj in db.tbl_commonRoomRate where obj.HotelRateID == RateID select obj).FirstOrDefault();
                    if (arrRate != null)
                    {
                        foreach (var sCancellation in arrRate.CancellationPolicyId.Split(',').ToList())
                        {
                            sCancellationId.Add(sCancellation);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return sCancellationId.Distinct().ToList();
        }
        #endregion

        #region ValidOccpancy
        public static bool ValidOccpancy(Int64 RoomID, Int64 AdultCount, Int64 ChildCount, string ChildAges)
        {
            bool valid = false; /* MaxOccpancy = RoomOccpancy + ChildWithoutBeds + ExtraBeds*/
            try
            {
                var sRooms = (from obj in cdb.tbl_commonRoomDetails where obj.RoomId == RoomID select obj).FirstOrDefault();
                if (sRooms != null)
                {
                    if ((sRooms.RoomOccupancy + sRooms.MaxExtrabedAllowed) >= AdultCount  && (ChildCount <= sRooms.NoOfChildWithoutBed + sRooms.MaxExtrabedAllowed) )
                        valid = true;
                }

            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return valid;
        }

        public static date GetRateByOccupancy(date sDate, float EBRate, float CWBRate, float CNBRate, CommonLib.Response.HotelOccupancy Search, string HotelCode)
        {
            date sNewDateRate = new date();
            try
            {
                var sChildAges = (from obj in cdb.tbl_CommonHotelMaster where obj.sid == Convert.ToInt64(HotelCode) select new { obj.ChildAgeTo, obj.ChildAgeFrom }).FirstOrDefault();
                var sRates = (from obj in cdb.tbl_commonRoomDetails where obj.RoomTypeId == Convert.ToInt64(sDate.RoomTypeId) && obj.HotelId == Convert.ToInt64(HotelCode) select obj).FirstOrDefault();
                Int64 MaxEB = Convert.ToInt64(sRates.MaxExtrabedAllowed);
                Int64 MaxCNB = Convert.ToInt64(sRates.NoOfChildWithoutBed);
                Int64 RoomOccupancy = Convert.ToInt64(sRates.RoomOccupancy);
                Int64 MaxOccupancy = (Convert.ToInt64(sRates.RoomOccupancy) + Convert.ToInt64(MaxEB) + Convert.ToInt64(MaxCNB));
                /*Validate Child Ages*/
                foreach (var sAges in Search.ChildAges.Split(',').ToList())
                {
                    if (sAges == "")
                        continue;
                    if (sChildAges.ChildAgeFrom >= Convert.ToInt16(sAges))
                    {
                        Search.ChildCount = Search.ChildCount - 1;

                    }
                    else if (sChildAges.ChildAgeTo <= Convert.ToInt16(sAges))
                    {
                        Search.AdultCount = Search.AdultCount + 1;
                        Search.ChildCount = Search.ChildCount - 1;
                    }
                }

                if (MaxOccupancy >= Search.AdultCount + Search.ChildCount)
                {
                    if (Convert.ToInt64(sRates.RoomOccupancy) + Convert.ToInt64(sRates.MaxExtrabedAllowed) < Search.AdultCount)
                        throw new Exception("Maximum adults exceed");
                    Int64 noAdultEB = 0;
                    RoomOccupancy = RoomOccupancy - Search.AdultCount;
                    if (RoomOccupancy < 0)
                    {
                        if (RoomOccupancy + Convert.ToInt64(sRates.MaxExtrabedAllowed) < 0)
                            throw new Exception("Room Ocuupancy No Valid");
                        noAdultEB = (-RoomOccupancy);
                        RoomOccupancy = RoomOccupancy + Convert.ToInt64(sRates.MaxExtrabedAllowed);
                        if (RoomOccupancy != 0)
                            throw new Exception("No Extrabeds exceed");
                        sDate.EBRate = noAdultEB * Convert.ToSingle(EBRate);
                        sDate.noEXB = noAdultEB;

                    }
                    if (Search.ChildCount != 0)
                    {
                        Int64 CWB = 0, CNB = 0;
                        if (noAdultEB == 0)
                        {
                            if (Convert.ToInt64(sRates.MaxExtrabedAllowed) == 1)
                            {
                                CWB = Search.ChildCount + (Convert.ToInt64(sRates.MaxExtrabedAllowed) - Search.ChildCount);
                                // CWB =  (objRate.MaxEB - ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            else if (Convert.ToInt64(sRates.MaxExtrabedAllowed) > 1 && Search.ChildCount == 1)
                            {
                                CWB = (Convert.ToInt64(sRates.MaxExtrabedAllowed) - Search.ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            else
                            {
                                CWB = Search.ChildCount + (MaxEB - Search.ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            if ((MaxEB - Search.ChildCount) < 0)
                            {
                                CNB = (-(MaxEB - Search.ChildCount));
                                sDate.CNBRate = CNB * Convert.ToSingle(CNBRate);
                                sDate.noCNB = CNB;
                            }
                        }
                        else
                        {
                            RoomOccupancy = RoomOccupancy - Search.ChildCount;
                            if (RoomOccupancy < 0)
                            {
                                CNB = -(RoomOccupancy);
                                sDate.CNBRate = CNB * Convert.ToSingle(CNBRate);
                                sDate.noCNB = CNB;
                                sDate.ChidWBedRate = 0 * Convert.ToSingle(CWBRate);
                                sDate.noCWB = 0;

                            }
                        }
                    }

                    sDate.Total = (sDate.RoomRate + sDate.EBRate + sDate.CNBRate + sDate.ChidWBedRate);
                    sNewDateRate = sDate;

                }
                else
                {
                    throw new Exception("Room Ocuupancy No Valid");
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return sNewDateRate;
        }
        #endregion

        #region GetCharges
        public static CommonLib.Response.ServiceCharge GetCharge(List<date> objDateRates)
        {
            CommonLib.Response.ServiceCharge objCharge = new ServiceCharge();
            try
            {
                objCharge.HotelTaxes = GetOtherRate(objDateRates.Select(d => d.objCharges.HotelTaxes).ToList());
                objCharge.CUHTaxes = GetOtherRate(objDateRates.Select(d => d.objCharges.CUHTaxes).ToList());
                objCharge.b2bTaxes = GetOtherRate(objDateRates.Select(d => d.objCharges.b2bTaxes).ToList());
                objCharge.S2STaxes = GetOtherRate(objDateRates.Select(d => d.objCharges.S2STaxes).ToList());
                foreach (var arrCharge in objDateRates.Select(d => d.objCharges).ToList())
                {
                    objCharge.Rate += arrCharge.Rate;
                    objCharge.HotelRate += arrCharge.HotelRate;         /* Hotel RATE*/
                    objCharge.RoomRate += arrCharge.RoomRate;

                    objCharge.S2SMarkup += arrCharge.S2SMarkup;
                    objCharge.SupplierTotal += arrCharge.SupplierTotal; /*Supplier Rate*/

                    objCharge.CUHTotal += arrCharge.CUHTotal;
                    objCharge.CUHMarkup += arrCharge.CUHMarkup;         /*ClickUrHotel Rate*/

                    objCharge.AdminMarkup += arrCharge.AdminMarkup;
                    objCharge.TotalPrice += arrCharge.TotalPrice;       /*Purchaser Rate*/

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return objCharge;
        }

        public static CommonLib.Response.ServiceCharge GetCharge(List<ServiceCharge> arrCharges)
        {
            CommonLib.Response.ServiceCharge objCharge = new ServiceCharge();
            try
            {
                objCharge.HotelTaxes = GetOtherRate(arrCharges.Select(d => d.HotelTaxes).ToList());
                objCharge.CUHTaxes = GetOtherRate(arrCharges.Select(d => d.CUHTaxes).ToList());
                objCharge.b2bTaxes = GetOtherRate(arrCharges.Select(d => d.b2bTaxes).ToList());
                objCharge.S2STaxes = GetOtherRate(arrCharges.Select(d => d.S2STaxes).ToList());
                foreach (var Charge in arrCharges)
                {
                    objCharge.Rate += Charge.Rate;
                    objCharge.HotelRate += Charge.HotelRate;         /* Hotel RATE*/
                    objCharge.RoomRate += Charge.RoomRate;

                    objCharge.S2SMarkup += Charge.S2SMarkup;
                    objCharge.SupplierTotal += Charge.SupplierTotal; /*Supplier Rate*/

                    objCharge.CUHTotal += Charge.CUHTotal;
                    objCharge.CUHMarkup += Charge.CUHMarkup;         /*ClickUrHotel Rate*/

                    objCharge.AdminMarkup += Charge.AdminMarkup;
                    objCharge.TotalPrice += Charge.TotalPrice;       /*Purchaser Rate*/

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return objCharge;
        }



        public static List<TaxRate> GetOtherRate(List<List<TaxRate>> arrTaxes)
        {
            List<TaxRate> ListRates = new List<TaxRate>();
            try
            {
                foreach (var objTax in arrTaxes)
                {
                    foreach (var objRate in objTax)
                    {
                        if (ListRates.Where(d => d.RateName == objRate.RateName).ToList().Count == 0)
                        {
                            ListRates.Add(new TaxRate { ID = objRate.ID, BaseRate = objRate.BaseRate, Per = objRate.Per, RateName = objRate.RateName, TaxOn = objRate.TaxOn, TotalRate = objRate.TotalRate, Type = objRate.Type });
                        }
                        else
                        {
                            ListRates.Where(d => d.RateName != objRate.RateName).FirstOrDefault().BaseRate += objRate.BaseRate;
                            ListRates.Where(d => d.RateName != objRate.RateName).FirstOrDefault().TotalRate += objRate.TotalRate;

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListRates;
        }


     

        #region  Meals Details
        public static List<CommonLib.Response.includedMeal> GetMeals(Int64 RateID, Int64 SupplierID)
        {
            List<CommonLib.Response.includedMeal> arrMeals = new List<includedMeal>();
            try
            {
                using (var db = new dbTaxHandlerDataContext())
                {
                    var ListMeals = (from obj in db.Comm_AddOnsRates
                                     where obj.RateID == RateID
                                     select new
                                     {
                                         MealID = obj.AddOnsID,
                                         obj.IsCumpulsury,
                                         obj.RateID,
                                         obj.RateType,
                                         obj.UserID,
                                         obj.Rate,
                                     }).ToList();
                    foreach (var objMeal in ListMeals)
                    {
                        using (var DB = new DBHandlerDataContext())
                        {
                            var MealDetails = (from obj in DB.Comm_AddOns where obj.ID == objMeal.MealID select obj).FirstOrDefault();
                            if (MealDetails != null)
                            {
                                arrMeals.Add(new includedMeal
                               {
                                   Cumpulsory = Convert.ToBoolean(objMeal.IsCumpulsury),
                                   MealID = Convert.ToInt64(objMeal.MealID),
                                   mealName = MealDetails.AddOnName,
                                   mealType = MealDetails.MealType,
                                   objcharge = new ServiceCharge { Rate = Convert.ToSingle(objMeal.Rate) },
                                   runno = 0,
                               });
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrMeals;
        }

        public static List<CommonLib.Response.includedMeal> GetMeals(List<CommonLib.Response.date> ListOfDay, CommonLib.Response.HotelOccupancy arrSearch, string Meal)
        {
            List<CommonLib.Response.includedMeal> arrMeals = new List<includedMeal>();
            try
            {
                foreach (var objDate in ListOfDay)
                {
                    foreach (var meal in objDate.including)
                    {
                        if (meal.mealType == null)
                            meal.mealType = "";
                        bool FreeMeal = false;
                        if (meal.Cumpulsory && meal.objcharge.Rate==0)
                            FreeMeal = true;
                        if (!arrMeals.Any(d => d.mealName == meal.mealName))
                        {
                            arrMeals.Add(new includedMeal
                            {
                                mealName = meal.mealName,
                                mealType = meal.mealType,
                                objcharge = new ServiceCharge { Rate = meal.objcharge.Rate },//* arrSearch.AdultCount },
                                Cumpulsory = meal.Cumpulsory,
                                Free = FreeMeal
                            });
                        }
                        else
                        {
                            arrMeals.Where(d => d.mealName == meal.mealName).FirstOrDefault().objcharge.Rate =
                                arrMeals.Where(d => d.mealName == meal.mealName).FirstOrDefault().objcharge.Rate + (meal.objcharge.Rate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrMeals;
        }

        #endregion


        public static ChargeRate GetTotalChargeTax(ListDates objDateCharge)
        {
            ChargeRate objChargeRate = new ChargeRate();
            objChargeRate.Charge = new List<TaxRate>();
            if (objDateCharge.CNBRate.BaseRate != 0)
            {
                objChargeRate.TotalRate += objDateCharge.CNBRate.TotalRate;
                foreach (var objCharge in objDateCharge.CNBRate.Charge)
                {
                    if (!objChargeRate.Charge.Exists(d => d.RateName == objCharge.RateName))
                    {
                        objChargeRate.Charge.Add(new TaxRate
                        {
                            BaseRate = objCharge.BaseRate,
                            RateName = objCharge.RateName,
                            TaxOn = objCharge.TaxOn,
                            TotalRate = objCharge.TotalRate

                        });
                    }
                }
            }
            if (objDateCharge.CWBRate.BaseRate != 0)
            {
                foreach (var objCharge in objDateCharge.CWBRate.Charge)
                {
                    if (!objChargeRate.Charge.Exists(d => d.RateName == objCharge.RateName))
                    {
                        objChargeRate.Charge.Add(new TaxRate
                        {
                            BaseRate = objCharge.BaseRate,
                            RateName = objCharge.RateName,
                            TaxOn = objCharge.TaxOn,
                            TotalRate = objCharge.TotalRate

                        });
                    }
                }
            }
            if (objDateCharge.EBRate.BaseRate != 0)
            {

            }
            return objChargeRate;
        }
        #endregion

        #region Get Inventory Details
        public static List<date> GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            List<date> ListDates = new List<date>();
            try
            {
                string HotelID = "";
                HotelOccupancy objOccupancy = GenralManager.GetRateInfo(Serach, out HotelID).RoomOccupancy.Where(d => d.RoomNo.Contains(RoomNo)).FirstOrDefault();
                RoomType objRoom = objOccupancy.Rooms.Where(d => d.RoomTypeId == RoomID && d.RoomDescription == RoomDescID).FirstOrDefault();
                foreach (var objDate in objRoom.dates)
                {
                    objDate.NoOfCount = InventoryManager.GetInventoryCount(HotelID, Convert.ToInt64(objDate.RateTypeId), RoomID, objDate.datetime, AccountManager.GetSupplierByUser());
                    ListDates.Add(objDate);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return ListDates;
        }

        public static List<date> GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo, string HotelCode, string Supplier)
        {
            List<date> ListDates = new List<date>();
            try
            {
                HotelOccupancy objOccupancy = GenralManager.GetRateInfo(Serach, Supplier, HotelCode).RoomOccupancy.Where(d => d.RoomNo.Contains(RoomNo)).FirstOrDefault();
                RoomType objRoom = objOccupancy.Rooms.Where(d => d.RoomTypeId == RoomID && d.RoomDescription == RoomDescID).FirstOrDefault();
                foreach (var objDate in objRoom.dates)
                {
                    objDate.NoOfCount = InventoryManager.GetInventoryCount(HotelCode, Convert.ToInt64(objDate.RateTypeId), RoomID, objDate.datetime, Convert.ToInt64(Supplier));
                    ListDates.Add(objDate);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return ListDates;
        }
        #endregion

        #region Get Room Image
        public static List<Image> GetRoomImage(Int64 RoomTypeID, Int64 HotelId)
        {
            List<Image> arrImages = new List<Image>();
            try
            {
                var arrRoomImage = (from obj in cdb.tbl_commonRoomDetails
                                    where obj.HotelId == HotelId &&
                                    obj.RoomTypeId == RoomTypeID
                                    select new
                                    {
                                        obj.RoomImage,
                                        obj.SubImages
                                    }).FirstOrDefault();
                string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                arrImages.Add(new Image
                {
                    Count = 1,
                    IsDefault = true,
                    Title = arrRoomImage.RoomImage,
                    Url = URL + "/RoomImages/" + arrRoomImage.RoomImage
                });
                foreach (var Image in arrRoomImage.SubImages.Split('^').ToList())
                {
                    if (Image == "")
                        continue;
                    arrImages.Add(new Image
                    {
                        Count = 1,
                        IsDefault = true,
                        Title = Image,
                        Url = URL + "/RoomImages/" + Image
                    });
                }

            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrImages;
        }
        #endregion

        #region Create  Cancellation
        public static List<CommonLib.Response.CancellationPolicy> GetCancellationPolicyByRate(List<date> arrDates)
        {
            List<CancellationPolicy> Cancellation = new List<CancellationPolicy>();
            List<CancellationPolicy> arrCancellations = new List<CancellationPolicy>();
            try
            {
                foreach (var RateID in arrDates.Select(d => d.RateTypeId).Distinct().ToList())
                {
                    List<string> CancellationID = new List<string>();
                    float TotalCancellation = 0;
                    DateTime CancelDate = DateTime.Now;
                    bool nonRefundable = false;
                    var arrRate = (from obj in cdb.tbl_TarrifPlan where obj.nRateID == RateID select obj).FirstOrDefault();
                    if (arrRate != null)
                    {
                        if (arrRate.nCancellation == 0)
                            CancellationID.Add("");
                        else
                            CancellationID.Add(arrRate.nCancellation.ToString());
                    }
                    List<date> arrChargeDate = arrDates.Where(d => d.RateTypeId == RateID).ToList();
                    float Markups = arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.AdminMarkup).ToList().Sum() +
                                          arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.Markup).ToList().Sum();
                    float TotalCharge = arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.RoomRate).ToList().Sum();
                    int noNights = arrDates.Where(d => d.RateTypeId == RateID).ToList().Count;
                    foreach (var ID in CancellationID)
                    {
                        if (ID == "")
                        {
                            nonRefundable = true;
                            CancelDate = DateTime.Now;
                            TotalCancellation = TotalCharge;
                        }
                        var sCancellation = (from obj in cdb.tbl_CommonCancelationPolicy where obj.CancelationID.ToString() == ID select obj).FirstOrDefault();

                        if (sCancellation != null && sCancellation.RefundType == "Refundable")
                        {

                            if ((DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                            //else if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddMinutes(-Convert.ToInt16(1)) <= DateTime.Now))
                            //{
                            //    CancelDate = DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            //    TotalCancellation = TotalCharge;
                            //    nonRefundable = true;
                            //}
                            //else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                            //{
                            //    CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            //    TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            //}

                        }
                        else if (sCancellation != null && sCancellation.RefundType == "NoShow")
                        {
                            if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(CheckInDate, "dd-M-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                            else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                        }
                        else
                        {
                            nonRefundable = true;
                            CancelDate = DateTime.Now;
                            TotalCancellation = TotalCharge;

                        }

                        Cancellation.Add(new CancellationPolicy
                        {
                            AmendRestricted = nonRefundable,
                            CancellationAmount = TotalCancellation,
                            CancellationDateTime = CancelDate.AddHours(-noDaysGap).AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture),
                            CancelRestricted = nonRefundable,
                            CUTCancellationAmount = TotalCancellation,
                            nonRefundable = nonRefundable,
                            arrChargeDate = arrChargeDate
                        });
                    }

                }
                float SupplierMarkup = 0, AgentMarkup = 0, S2SMarkup = 0;

                arrCancellations.Add(Cancellation[0]);
                arrCancellations.Last().objCharges = new ChargeRate();
                arrCancellations.Last().objCharges = GetRateByCancellation(arrCancellations.Last().CancellationAmount, out SupplierMarkup, out AgentMarkup, out S2SMarkup);
                arrCancellations.Last().SupplierMarkup = SupplierMarkup;
                arrCancellations.Last().AgentCancellationMarkup = AgentMarkup;
                arrCancellations.Last().S2SMarkup = S2SMarkup;
                for (int i = 1; i < Cancellation.Count; i++)
                {
                    SupplierMarkup = 0; AgentMarkup = 0; S2SMarkup = 0;
                    arrCancellations.Add(Cancellation[i]);
                    arrCancellations.Last().CancellationAmount = arrCancellations.Last().CancellationAmount + Cancellation[i - 1].CancellationAmount;
                    arrCancellations.Last().CUTCancellationAmount = arrCancellations.Last().CUTCancellationAmount + Cancellation[i - 1].CUTCancellationAmount;
                    arrCancellations.Last().CutRoomAmount = arrCancellations.Last().CutRoomAmount + Cancellation[i - 1].CUTCancellationAmount;
                    arrCancellations.Last().objCharges = new ChargeRate();
                    arrCancellations.Last().objCharges = GetRateByCancellation(arrCancellations.Last().CancellationAmount, out SupplierMarkup, out AgentMarkup, out S2SMarkup);
                    arrCancellations.Last().SupplierMarkup = SupplierMarkup;
                    arrCancellations.Last().AgentCancellationMarkup = AgentMarkup;
                    arrCancellations.Last().S2SMarkup = S2SMarkup;
                }

            }
            catch
            {

            }


            return arrCancellations;
        }


        public static DateTime OnHoldDate(List<RoomType> ListRoom)
        {
            DateTime CompareDate = new DateTime();
            List<DateTime> onHoldDate = new List<DateTime>();
            List<bool> nonRefund = new List<bool>();
            try
            {
                foreach (var objRoom in ListRoom)
                {
                    nonRefund.Add(objRoom.CancellationPolicy.Any(d => d.CancelRestricted == true || d.AmendRestricted == true && d != null));
                    int l = 0;
                    foreach (CommonLib.Response.CancellationPolicy objCancellation in objRoom.CancellationPolicy)
                    {
                        if (objCancellation.CancelRestricted == false && DateTime.ParseExact(objCancellation.CancellationDateTime, "dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture).AddDays(-2) > DateTime.Now)
                        {
                            onHoldDate.Add(DateTime.Now.AddHours(-48));
                        }
                        nonRefund.Add(objCancellation.CancelRestricted);
                    }
                }
                bool nonRefundable = nonRefund.Any(d => d == true);
                if (onHoldDate.Count != 0 && nonRefundable == false)
                {
                    DateTime date = Convert.ToDateTime(Convert.ToDateTime(onHoldDate.Min()).ToShortDateString());
                    CompareDate = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00).AddMinutes(-1).AddSeconds(-1);
                    return CompareDate;
                }

                else
                    return CompareDate;

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return CompareDate;
            }

        }
        #endregion

        #region Utilities



        public static List<CommonLib.Response.CancellationPolicy> GetCancellationPolicy(List<string> CancellationID, float TotalCharge, int noNights)
        {
            List<CancellationPolicy> Cancellation = new List<CancellationPolicy>();
            try
            {
                noNights = noNights - 1;
                float TotalCancellation = 0;
                DateTime CancelDate = DateTime.Now;
                bool nonRefundable = false;
                bool NoShow = false;
                foreach (var ID in CancellationID)
                {
                    if (ID == "")
                    {
                        nonRefundable = true;
                        CancelDate = DateTime.Now;
                        TotalCancellation = TotalCharge;
                    }
                    var sCancellation = (from obj in cdb.tbl_CommonCancelationPolicy where obj.CancelationID == Convert.ToInt64(ID) select obj).FirstOrDefault();

                    if (sCancellation.RefundType == "Refundable")
                    {

                        if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                        else if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddMinutes(-Convert.ToInt16(1)) <= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = TotalCharge;
                            nonRefundable = true;
                        }
                        else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }

                    }
                    else if (sCancellation.RefundType == "NoShow")
                    {
                        NoShow = true;
                        if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                        else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                    }
                    else
                    {
                        nonRefundable = true;
                        CancelDate = DateTime.Now;
                        TotalCancellation = TotalCharge * noNights;
                    }

                    Cancellation.Add(new CancellationPolicy
                    {
                        AmendRestricted = nonRefundable,
                        CancellationAmount = TotalCancellation,
                        //CancellationDateTime = CancelDate.ToShortDateString(),
                        CancellationDateTime = CancelDate.AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture),
                        CancelRestricted = nonRefundable,
                        CUTCancellationAmount = TotalCancellation,
                        nonRefundable = nonRefundable,
                        NoShowPolicy = NoShow
                    });
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return Cancellation;
        }

        public static float GetCancellation(tbl_CommonCancelationPolicy Cancellation, float Total)
        {
            float CancellationTotal = 0;
            if (Cancellation.ChargesType == "Amount")
            {
                CancellationTotal = Convert.ToSingle(Cancellation.AmountToCharge);
            }
            else if (Cancellation.ChargesType == "Nights")
            {
                if (Cancellation.NightsToCharge == "")
                    Cancellation.NightsToCharge = "0";
                if (Cancellation.NightsToCharge != "0")
                    CancellationTotal = (Total / Convert.ToSingle(Cancellation.NightsToCharge));
                else
                    CancellationTotal = (Total);
            }
            else if (Cancellation.ChargesType == "Percentile")
            {
                CancellationTotal = (Total * Convert.ToSingle(Cancellation.PercentageToCharge.Replace("%","")) / 100);
            }
            return CancellationTotal;
        }


        public static List<string> CancellationPolicies(List<CommonLib.Response.CancellationPolicy> ListCancellation)
        {
            List<string> Cancellation = new List<string>();

            foreach (CommonLib.Response.CancellationPolicy objCancellation in ListCancellation)
            {
                string CurrencyClass = "";
                GlobalDefault objGlobalDefault = (GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                // switch (objCancellation.arrChargeDate[0].Currency)
                switch (objGlobalDefault.Currency)
                {
                    case "AED":
                        CurrencyClass = "Currency-AED";

                        break;
                    case "SAR":
                        CurrencyClass = "Currency-SAR";
                        break;
                    case "EUR":
                        CurrencyClass = "fa fa-eur";
                        break;
                    case "GBP":
                        CurrencyClass = "fa fa-gbp";
                        break;
                    case "USD":
                        CurrencyClass = "fa fa-dollar";
                        break;
                    case "INR":
                        CurrencyClass = "fa fa-inr";
                        break;
                }
                DateTime date = Convert.ToDateTime(Convert.ToDateTime(objCancellation.CancellationDateTime).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture));
                DateTime time = Convert.ToDateTime(objCancellation.CancellationDateTime);
                DateTime dtCancel = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00).AddSeconds(-1);
                if (objCancellation != null && objCancellation.nonRefundable == false && objCancellation.NoShowPolicy == false)
                {
                    if (objCancellation.AmendRestricted == true || objCancellation.CancelRestricted == true)
                    {
                        Cancellation.Add("In the event of cancellation after " + DateTime.Now.ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture) + " no amendment/cancellation refund will be applicable after booking .");
                        break;
                    }
                    else
                    {
                        string CancelAmt = Convert.ToSingle(Math.Round((decimal)objCancellation.objCharges.TotalRate, 2, MidpointRounding.AwayFromZero)).ToString("N");
                        Cancellation.Add("<i class=" + CurrencyClass + "> </i> " + CancelAmt + " charge will apply <br/> if cancelled before " + date.AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture) + "PM");
                    }
                }//
                else if(objCancellation != null && objCancellation.NoShowPolicy == true)
                {
                    string CancelAmt = Convert.ToSingle(Math.Round((decimal)objCancellation.objCharges.TotalRate, 2, MidpointRounding.AwayFromZero)).ToString("N");
                    Cancellation.Add("<i class=" + CurrencyClass + "> </i> " + CancelAmt + " charge will apply <br/>in the event of No Show.");
                }
                else 
                {
                    Cancellation.Add("This is Non-Refundable  booking, there will be no cancellation or amendment possible once booked");
                }
            }


            return Cancellation;


        }

        public static CommonLib.Response.RoomType GetOffer(List<date> ListNight,CommonLib.Response.HotelOccupancy Search)
        {
            CommonLib.Response.RoomType ListRoomType = new CommonLib.Response.RoomType();
            List<OfferSeason> ListOffer = new List<OfferSeason>();
            var sOffer = new List<RoomType>();
            try
            {
                long noNights = ListNight.Count;
                List<string> arrOfferID = ListNight.Select(d=> d.offerID).ToList();
                List<string> sOfferID = new List<string>();
                foreach (var arrID in arrOfferID.Select(d => d.Split(',')).Distinct().ToList())
                {
                    foreach (var ID in arrID)
                    {
                        sOfferID.Add(ID);
                    }
                  
                }
                sOfferID = sOfferID.Distinct().ToList();
                using (var db = new ClickUrHotel_DBEntities())
                {
                    IQueryable<tbl_CommonHotelOffer> arrData = (from obj in db.tbl_CommonHotelOffer where
                                                                 sOfferID.Contains(obj.OfferID.ToString()) 
                                                                 && obj.Category == "Free Nights" 
                                                                 && obj.MinNight <= noNights
                                                                select obj) ;
                    foreach (var arrOffer in arrData.ToList())
                    {
                        Common.StartDate = DateTime.ParseExact(arrOffer.ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.EndDate = DateTime.ParseExact(arrOffer.ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        List<DateTime> arrDate = Common.Calander;
                        if (arrDate.Contains(dtCheckIn))
                        {
                            for (int i = 0; i < arrOffer.FreeNight; i++)
                            {
                                ListNight[i].Total = 0;
                                ListNight[i].objCharges = new ServiceCharge();
                                ListNight[i].B2BRates = new List<TaxRate>();
                                ListNight[i].S2CRates = new List<TaxRate>();
                                ListNight[i].HotelRates = new List<TaxRate>();
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListRoomType;
        }


        public static List<OfferSeason> GetValidOffer(string OfferID, string Date)
        {
            List<OfferSeason> ListSeason = new List<OfferSeason>();
            List<OfferSeason> OfferSDate = new List<OfferSeason>();
            List<DateTime> OfferBDate = new List<DateTime>();
            if (OfferID != "")
            {
                var sOfferRate = (from obj in cdb.tbl_CommonHotelOffer where obj.OfferID == Convert.ToInt64(OfferID) select obj).ToList();

                foreach (var OfferValid in sOfferRate)
                {

                    var calendar = new List<DateTime>();
                    DateTime RateCheckin = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime RateCheckOut = DateTime.ParseExact(OfferValid.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    while (RateCheckin <= RateCheckOut)
                    {
                        calendar.Add(RateCheckin.AddDays(1));
                        RateCheckin = RateCheckin.AddDays(1);
                    }
                    if (calendar.Contains(DateTime.ParseExact(Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)))
                    {
                        DateTime ValidTill = DateTime.Now;


                        // var offerDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(OfferValid.DaysPrior));
                        if (OfferValid.DateType == "Season Date")
                        {
                            DateTime offerDate = DateTime.ParseExact(Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            //for (int i = 0; i <= calendar.Count; i++)
                            //{
                            List<string> Days = OfferValid.BlockDay.Split('^').ToList();
                            bool validDay = false; /*/To check Day Valid for Current Offer Day*/
                            foreach (var Day in Days)
                            {
                                if (Day != "")
                                {
                                    if (Day == "Sunday" && offerDate.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Monday" && offerDate.DayOfWeek == DayOfWeek.Monday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Tuesday" && offerDate.DayOfWeek == DayOfWeek.Tuesday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Wednesday" && offerDate.DayOfWeek == DayOfWeek.Wednesday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Thursday" && offerDate.DayOfWeek == DayOfWeek.Thursday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Friday" && offerDate.DayOfWeek == DayOfWeek.Friday)
                                    {
                                        validDay = true;
                                    }
                                    if (Day == "Saterday" && offerDate.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        validDay = true;
                                    }
                                }
                            }
                            if (OfferValid.DaysPrior != null)
                                ValidTill = DateTime.ParseExact(OfferValid.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(OfferValid.DaysPrior));
                            else if (OfferValid.BookBefore != null)
                                ValidTill = DateTime.ParseExact(OfferValid.BookBefore, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            if (Days.Count == 0 || validDay == false && ValidTill >= DateTime.Now)

                                OfferSDate.Add(new OfferSeason
                                {
                                    offerName = OfferValid.SeasonName,
                                    offerNote = OfferValid.OfferNote,
                                    OfferTerms = OfferValid.OfferTerm,
                                    Dates = DateTime.ParseExact(Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                    DisountAmount = Convert.ToDecimal(OfferValid.DiscountAmount),
                                    DisountPercent = Convert.ToSingle(OfferValid.DiscountPercent),
                                    NewRate = Convert.ToSingle(OfferValid.NewRate),
                                    OfferType = OfferValid.OfferType
                                });
                            // }

                        }
                        else if (OfferValid.DateType == "Block Date" && ValidTill >= DateTime.Now)
                        {
                            DateTime ValidFrom = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime ValidTo = DateTime.ParseExact(OfferValid.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            double noCDays = (ValidTo - ValidFrom).TotalDays;
                            for (int i = 0; i < noCDays; i++)
                            {
                                OfferBDate.Add(DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
                            }

                        }
                    }
                }
            }


            foreach (var obj in OfferSDate)
            {
                if (!OfferBDate.Contains(obj.Dates))
                {
                    ListSeason.Add(obj);
                }

            }
            return ListSeason;
        }

        public static float GetOfferRate(OfferSeason Offer, float ActualRate)
        {
            float OfferTotal = 0;
            if (Offer.OfferType == "Discount")
            {
                if (Offer.DisountAmount != 0)
                {
                    OfferTotal = ActualRate - Convert.ToSingle(Offer.DisountAmount);
                }
                if (Offer.DisountPercent != 0)
                {
                    OfferTotal = ActualRate - (ActualRate * Convert.ToSingle(Offer.DisountPercent) / 100);
                }

            }
            if (Offer.OfferType == "Deal")
            {
                OfferTotal = Convert.ToSingle(Offer.NewRate);
            }
            if (Offer.OfferType == "Freebi")
            {
                OfferTotal = Convert.ToSingle(ActualRate);
            }

            return OfferTotal;
        }


        #endregion

        #region Inventory Update
        public static List<CommonLib.Response.InventoryType> GetInventory(Int64 HotelRateID, Int64 HotelID, Int64 Supplier, string ChekIn)
        {
            db = new DBHandlerDataContext();
            List<CommonLib.Response.InventoryType> ListInventory = new List<CommonLib.Response.InventoryType>();
            DateTime ChekInDt = DateTime.ParseExact(ChekIn, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            var sdata = (from obj in db.tbl_CommonInventories where obj.RateID == Convert.ToString(HotelRateID) && obj.HotelID == Convert.ToString(HotelID) && obj.SupplierID == Convert.ToString(Supplier) && obj.Date == Convert.ToString(ChekInDt) select obj).FirstOrDefault();
            //  foreach (var data in sdata)
            //  {
            if (sdata != null)
            {
                ListInventory.Add(new CommonLib.Response.InventoryType
                {
                    InvNoOfRoom = Convert.ToInt32(sdata.INV_NoOfRoom),
                    InventoryName = sdata.InventoryType,
                    InvDate = Convert.ToString(sdata.Date),
                    Sold = Convert.ToInt32(sdata.Sold),
                    IsStop = Convert.ToBoolean(sdata.IsStop)
                });
            }
            //  }
            return ListInventory;
        }

        #endregion

    }
}