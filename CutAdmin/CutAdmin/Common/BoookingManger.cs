﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CutAdmin.BL;
using System.Globalization;
using System.Text;
using System.Configuration;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.Common
{
    public class BoookingManger
    {
        public class Group
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }
        public static string BlockRoom(string Serach, List<Group> ListRates, string HotelCode, string Supplier)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                HttpContext.Current.Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                HotelFilter objHotel = new HotelFilter();
                if (HttpContext.Current.Session["ModelHotel" + Serach] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Serach];
                arrHotelDetails = objHotel.arrHotels.Where(d => d.HotelId == HotelCode.ToString()).FirstOrDefault();
                arrHotelDetails.RateList[0].Charge = new ServiceCharge();
                var arrCharge = new ServiceCharge();
                CommonLib.Response.RateGroup objAvailRate = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault();
                float RoomTotal = 0;
                float Markup = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                float AdminMarkup = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                List<ServiceCharge> ListCharge = new List<ServiceCharge>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate.RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.objCharges.TotalPrice == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                      
                        foreach (var objOther in arrRate.objCharges.HotelTaxes)
                        {
                            if (!objOther.RateName.Contains("VAT"))
                                continue;
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate += objOther.TotalRate;
                            }
                        }
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        ListCharge.Add(arrRate.objCharges);
                        #endregion

                        #region Check Inventory
                        foreach (var objDate in arrRate.dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, Convert.ToDecimal(objDate.RateTypeId), objDate.RoomTypeId.ToString(), objDate.datetime, Convert.ToInt64(objAvailRate.Name));
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }
                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                HttpContext.Current.Session["RateList" + Serach] = Rooms;
                var arrHotelCharge = RatesManager.GetCharge(ListCharge);
                arrHotelCharge.HotelTaxes = other;
                arrHotelCharge.RoomRate = arrHotelCharge.TotalPrice - other.Select(d => d.TotalRate).Sum();
                arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault().Charge = arrHotelCharge;// TaxManager.GetCharge(arrHotelCharge);
                HttpContext.Current.Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }


        public static string GetBookingDetails(string Serach, string Supplier)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                List<RoomType> Rooms = (List<RoomType>)HttpContext.Current.Session["RateList" + Serach];
                List<Group> ListRates = (List<Group>)HttpContext.Current.Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                var arrCharge = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault().Charge;
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, Charge = arrCharge });
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }


        public static string ValidateTransaction(List<CutAdmin.BookingHandler.Addons> arrAdons, string Serach)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (HttpContext.Current.Session["AvailDetails" + Serach] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)HttpContext.Current.Session["RatesDetails"];
                float BaseRate = objAvailRate[0].Charge.TotalPrice + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                Int64 Uid = AccountManager.GetUserByLogin();

                #endregion
                if (IsValid)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                else
                    throw new Exception("Your Balance is insufficient to Make this booking");
            }

            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }


        public static bool ValidTransact(Int64 Uid, float BaseRate)
        {
            bool IsValid = false;
            try
            {
                float AvailableCredit = 0;
                dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                var dtAvailableCredit = (from obj in dbTax.tbl_AdminCreditLimits where obj.uid == Uid select obj).FirstOrDefault();
                if (dtAvailableCredit == null)
                    throw new Exception("Please Contact  Administrator and try Again");
                AvailableCredit = Convert.ToSingle(dtAvailableCredit.AvailableCredit);
                float @Creditlimit = Convert.ToSingle(dtAvailableCredit.CreditAmount);
                bool Credit_Flag = (bool)dtAvailableCredit.Credit_Flag;
                bool OTC = (bool)dtAvailableCredit.OTC;
                float @MAXCreditlimit = Convert.ToSingle(dtAvailableCredit.MaxCreditLimit);
                if (AvailableCredit >= 0 && AvailableCredit >= BaseRate)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @MAXCreditlimit >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit + @Creditlimit) >= BaseRate && @OTC == true && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit) >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw;
            }
            return IsValid;
        }

        public static string BookHotel(List<CutAdmin.BookingHandler.Addons> arrAddOns, string Serach, List<CommonLib.Request.Customer> LisCustumer, string Supplier,string OnHold,string Remark,string Company,string Mobile,string Email)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                using (var book = DB.Database.BeginTransaction())
                {

                    List<Group> ListRates = (List<Group>)HttpContext.Current.Session["BookingRates" + Serach];
                    CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                    Int64 Uid = 0; string sTo = "";
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session expired.");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Uid = objGlobalDefault.sid;
                    DBHandlerDataContext sdb = new DBHandlerDataContext();
                    var admin = sdb.tbl_AdminLogins.AsEnumerable();
                    var Reserve = sdb.tbl_CommonHotelReservations.AsEnumerable();
                    var HotelAdons = sdb.tbl_CommonBookingHotelAddons.AsEnumerable();
                    if (objGlobalDefault.UserType != "Supplier")
                    {
                        Uid = objGlobalDefault.ParentId;
                        sTo = objGlobalDefault.uid;
                        sTo += "," + (from obj in admin where obj.sid == Uid select obj).FirstOrDefault().uid;
                        //sTo += "," + "barkatikhan66@gmail.com";                    
                    }
                    else
                    {
                        sTo = objGlobalDefault.uid;
                    }
                    var arrLastReservationID = (from obj in Reserve select obj).ToList().Count;
                    String ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                    bool response = false;

                    #region Inventory Update & Validate
                    List<RoomType> ListRate = (List<RoomType>)HttpContext.Current.Session["RateList" + Serach];
                    bool valid = InventoryManager.CheckInventory(arrHotelDetails.HotelId, ListRate, Supplier, arrHotelDetails.DateFrom);
                    if (valid)
                    {

                        response = InventoryManager.UpdateInventory(ListRate, ReservationID, arrHotelDetails.HotelId, Convert.ToInt64(Supplier));
                        List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                        List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                        for (int i = 0; i < Record.Count; i++)
                        {
                            tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                            Recordnew.BookingId = Record[i].BookingId;
                            Recordnew.InvSid = Record[i].InvSid;
                            Recordnew.SupplierId = Record[i].SupplierId;
                            Recordnew.HotelCode = Record[i].HotelCode;
                            Recordnew.RoomType = Record[i].RoomType;
                            Recordnew.RateType = Record[i].RateType;
                            Recordnew.InvType = Record[i].InvType;
                            Recordnew.Date = Record[i].Date;
                            Recordnew.Month = Record[i].Month;
                            Recordnew.Year = Record[i].Year;
                            Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                            Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                            Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                            Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                            Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                            Recordnew.UpdateOn = Record[i].UpdateOn;
                            AddRecord.Add(Recordnew);
                        }
                        DB.tbl_CommonInventoryRecord.AddRange(AddRecord);
                        DB.SaveChanges();
                        book.Commit();
                    }
                    #endregion

                    /*AB Quddus*/
                    #region AddOns  Save
                    List<tbl_CommonBookingHotelAddon> Addons = new List<tbl_CommonBookingHotelAddon>();
                    for (int l = 0; l < arrAddOns.Count; l++)
                    {

                        string[] Date = arrAddOns[l].Date.Split('_');
                        Addons.Add(new tbl_CommonBookingHotelAddon
                        {
                            BookingId = ReservationID,
                            Date = Date[0],
                            RoomNo = Date[1],
                            Name = arrAddOns[l].Name,
                            Type = arrAddOns[l].Type,
                            Quantity = arrAddOns[l].Quantity,
                            Rate = arrAddOns[l].TotalRate,
                        });
                    }
                    sdb.tbl_CommonBookingHotelAddons.InsertAllOnSubmit(Addons);
                    sdb.SubmitChanges();
                    Addons.ForEach(r => r.Rate = (Convert.ToDecimal(r.Rate) * Convert.ToDecimal(r.Quantity)).ToString());
                    #endregion

                    ServiceCharge Charges = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault().Charge;
                    RateGroup objSupplierRate = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault();
                    CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
                    objReservation.ReservationID = ReservationID;
                    objReservation.LisCustumer = new List<CommonLib.Request.Customer>();
                    objReservation.LisCustumer = LisCustumer;
                    objReservation.RateGroup = new RateGroup();
                    objReservation.RateGroup = objSupplierRate;
                    objReservation.arrAddOns = arrAddOns;
                    objReservation.SupplierID = Convert.ToInt64(Supplier);
                    objReservation.PurcheserID = Convert.ToInt64(AccountManager.GetUserByLogin());
                    objReservation.CUH_ID = 232;
                    objReservation.TotalAmount = Charges.TotalPrice;
                    objReservation.Remarks = Remark;
                    objReservation.CompanyName = Remark;
                    objReservation.Mobile = Mobile;
                    objReservation.email = Email;

                    if (OnHold == "false")
                        objReservation.BookingStatus = "";
                    else
                        objReservation.BookingStatus = "OnHold";

                    objReservation.OnVouchered = response;
                    objReservation.objCharges = new ServiceCharge();
                    objReservation.objCharges = Charges;
                    Int64 noNights = Convert.ToInt64(ListRate[0].dates.Count);
                    objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(arrHotelDetails.HotelId), Convert.ToInt64(Supplier), noNights);
                    objReservation.sCheckIn = arrHotelDetails.DateFrom;
                    objReservation.sCheckOut = arrHotelDetails.DateTo;
                    objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
                    objReservation.objCharges = new ServiceCharge();
                    objReservation.objCharges = Charges;
                    objReservation.ListRoom = new List<RoomType>();
                    objReservation.ListRoom = ListRate;
                    objReservation.noNights = ListRate[0].dates.Count;
                    var arrHotel = (from obj in DB.tbl_CommonHotelMaster where obj.sid.ToString() == arrHotelDetails.HotelId select obj).FirstOrDefault();
                    objReservation.objHotelDetails = arrHotel;
                    //objReservation.objHotelDetails = new tbl_CommonHotelMaster();
                    TransactionManager.objReservation = objReservation;
                    TransactionManager.GenrateBooking(ReservationID);
                    MailManager.SendInvoice(ReservationID, Uid, sTo);
                    return jsSerializer.Serialize(new { retCode = 1, BookingID = ReservationID });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { });
            }
        }

        public static string GroupRequest(Int64 noRoom, Int64 noPax, string HotelCode, string CheckIn, string CheckOut, string PaxName)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                string ReservationID = "";
                tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
                DateTime dCheckIn = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dCheckOut = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                double noNights = (dCheckOut - dCheckIn).TotalDays;
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    DBHandlerDataContext db = new DBHandlerDataContext();
                    var Reserve = db.tbl_CommonHotelReservations.AsEnumerable();
                    var arrLastReservationID = (from obj in Reserve select obj).ToList().Count;
                    ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                    var arrHotel = (from obj in DB.tbl_CommonHotelMaster where obj.sid.ToString() == HotelCode select obj).FirstOrDefault();

                    HotelReserv.ReservationID = Convert.ToString(ReservationID);
                    HotelReserv.HotelCode = HotelCode;
                    HotelReserv.HotelName = arrHotel.HotelName;
                    HotelReserv.mealplan = "RO";
                    HotelReserv.TotalRooms = Convert.ToInt16(noRoom);
                    HotelReserv.RoomCode = "";
                    HotelReserv.sightseeing = 0;
                    HotelReserv.Source = CutAdmin.DataLayer.AccountManager.GetSupplierByUser().ToString();
                    HotelReserv.Status = "GroupRequest";
                    HotelReserv.SupplierCurrency = "SAR";
                    HotelReserv.terms = arrHotel.HotelNote;
                    HotelReserv.ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
                    HotelReserv.Uid = AccountManager.GetUserByLogin();
                    HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
                    HotelReserv.Updateid = CutAdmin.DataLayer.AccountManager.GetUserByLogin().ToString();
                    HotelReserv.VoucherID = "";
                    HotelReserv.DeadLine = "";
                    HotelReserv.AgencyName = "";
                    HotelReserv.AgentContactNumber = "";
                    HotelReserv.AgentEmail = "";
                    HotelReserv.AgentMarkUp_Per = 0;
                    HotelReserv.AgentRef = "";
                    HotelReserv.AgentRemark = "";
                    HotelReserv.AutoCancelDate = "";
                    HotelReserv.AutoCancelTime = "";
                    HotelReserv.bookingname = PaxName;
                    HotelReserv.BookingStatus = "GroupRequest";
                    HotelReserv.CancelDate = "";
                    HotelReserv.CancelFlag = false;
                    HotelReserv.CheckIn = CheckIn;
                    HotelReserv.CheckOut = CheckOut;
                    HotelReserv.ChildAges = "";
                    HotelReserv.Children = Convert.ToInt16(0);
                    HotelReserv.City = arrHotel.CityId;
                    HotelReserv.deadlineemail = false;
                    HotelReserv.Discount = 0;
                    HotelReserv.ExchangeValue = 0;
                    HotelReserv.ExtraBed = 0;
                    HotelReserv.GTAId = "";
                    HotelReserv.holdbooking = 0;
                    HotelReserv.HoldTime = "";
                    HotelReserv.HotelBookingData = "";
                    HotelReserv.HotelDetails = "";
                    HotelReserv.Infants = 0;
                    HotelReserv.InvoiceID = "-";
                    HotelReserv.IsAutoCancel = false;
                    HotelReserv.IsConfirm = false;
                    HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
                    HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
                    HotelReserv.LuxuryTax = 0;
                    HotelReserv.mealplan_Amt = 0;
                    HotelReserv.NoOfAdults = Convert.ToInt16(noPax);
                    HotelReserv.NoOfDays = Convert.ToInt16(noNights);
                    HotelReserv.OfferAmount = 0;
                    HotelReserv.OfferId = 0;
                    HotelReserv.RefAgency = "";
                    HotelReserv.ReferenceCode = "";
                    HotelReserv.Remarks = "";
                    HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
                    HotelReserv.ReservationTime = "";
                    HotelReserv.RoomRate = 0;
                    HotelReserv.SalesTax = 0;
                    HotelReserv.Servicecharge = 0;
                    HotelReserv.ComparedFare = 0;
                    HotelReserv.ComparedCurrency = "";
                    HotelReserv.ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
                    HotelReserv.TotalFare = 0; //- objReservation.AdminCommission);
                    db.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
                    db.SubmitChanges();
                }

                #region Hotel Booking Details



                #endregion
                return jsSerializer.Serialize(new { retCode = 1, ReservationID = HotelReserv.ReservationID });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        public static bool OnRequestMail(Int64 Uid, string HotelName, string ReservationId)
        {

            try
            {
                //DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var DB = new DBHandlerDataContext())
                {
                    var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();

                    string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking OnRequest" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = sMail.BCcMail;
                    string CcTeamMail = sMail.CcMail;


                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + arrDetail.AgencyName + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + arrSupplierDetails.CompanyName + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>");
                    sb.Append("thanks for choosing our service,booking is subject to room availiblity and The confirmation of whether the room is available or not</span></p>");
                    sb.Append("will be communicated to you within within 24 business hours");
                    // sb.Append("we will confirm your booking when we have availability</span></p>");
                    // sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    //sb.Append("  No.</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    //sb.Append("  Name</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    //sb.Append("  ID</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Onrequest");
                    //sb.Append("  by</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    //sb.Append(" <tr>");
                    //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                    //sb.Append("  / Note</span></p>");
                    //sb.Append("  </td>");
                    //sb.Append(" </tr>");
                    sb.Append("</table>");
                    //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    //sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    //sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    //sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:acc.online@" + arrSupplierDetails.Uid + "\" target=\"_blank\"><span");
                    sb.Append("style='color:#954F72'>" + arrSupplierDetails.website + "</span></a>&nbsp;&amp;&nbsp;<a");
                    sb.Append("href=\"mailto:" + arrSupplierDetails.Uid + "\" target=\"_blank\"><span style='color:#954F72'>" + arrSupplierDetails.Uid + "</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Booking On-Request - " + HotelName;

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    // Email1List.Add(sReservation.customer_Email, "");
                    Email1List.Add(arrSupplierDetails.Uid, "");
                    Email1List.Add(arrDetail.uid, "");
                    Email1List.Add(BCcTeamMails, "");
                    Email2List.Add(CcTeamMail, "");

                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return false;
            }
        }
    }
}