﻿var Start = 1;
function GetData(id, data, url, columns) {
    debugger
    try {
        if (data == "")
            data = '{}';
        else
            data = JSON.stringify(data);
        var headerArray = new Array();
        $("#" + id).dataTable().fnClearTable();
        $("#" + id).dataTable().fnDestroy();
        $('#' + id).DataTable({
            bProcessing: true,
            bServerSide: true,
            columns: columns,
            bSort: true,
            sPaginationType: 'full_numbers',
            sSearch: true,
            paging: true,
            fnServerData: function (sSource, aoData, fnCallback, oSettings) {
                debugger

                try {
                    oSettings.jqXHR = $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: url + "?data="+JSON.stringify(aoData),
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            fnCallback(response);
                            $("#" + id).css("width", "100%")
                        },
                        error: function (e) {
                            console.log(e.message);
                            $("#" + id).css("width", "100%");
                        }
                    });
                }
                catch (ex) {
                    AlertDanger(ex.message)
                }

            },
            aoColumns: columns,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            responsive: true,
            fixedHeader: true,
           
        });
        //$('#' + id).removeAttr("style");


    } catch (e) {
        AlertDanger(e.message)
    }
   
}

var editIcon = function (data, type, row) {
    if (type === 'display') {
        return data + ' <i class="fa fa-pencil"/>';
    }
    return data;
};

