﻿function openModals(content, stitle, save) {
    debugger
    $.modal({
        title: stitle,
        content: content,
        resizable: false,
        scrolling: true,
        width: 600,
        spacing: 5,
        classes: ['black-gradient'],
        animateMove: 1,
        buttons: {
           
            'Close': {
                classes: 'black-gradient small',
                click: function (modal) { modal.closeModal(); }
            },
            'Save': {
                classes: 'button glossy mid-margin-right small',
                click: function (modal) { save(); }
            },
        }

    });
}

function Modal(content, stitle, success, arr) {
    debugger
    var btnName = "";
    $.modal({
        title: stitle,
        content: content,
        resizable: false,
        scrolling: true,
        width: 600,
        height: 200,
        spacing: 5,
        classes: ['black-gradient'],
        animateMove: 1,
        buttons: {
            'Close': {
                classes: 'black-gradient small',
                click: function (modal) { modal.closeModal(); }
            },
            'Save': {
                classes: 'black-gradient small',
                click: function (modal) { success(arr) },
                value: 'Change'
            },
        }

    });
}

function ConfirmModal(sMessage, success, arr) {
    $.modal.confirm(sMessage,
     function (){
         success(arr);
    }, function () {
       $('#modals').remove();
    });
}

function OpenIframe(title, URL) {
    try {
        $.modal({
            title: title,
            url: URL,
            useIframe: true,
            resizable: false,
            scrolling: true,
            //width: 800,
            height: 600,
            spacing: 5,
            classes: ['black-gradient'],
            animateMove: 1,
            buttons: {
                'Close': {
                    classes: 'black-gradient small hidden',
                    click: function (modal) { modal.closeModal(); }
                },
            }
        });
        $(".modal-iframe").css("width", "100%");
    } catch (ex) { }
}