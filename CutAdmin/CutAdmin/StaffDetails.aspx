﻿<%@ Page Title="Staff Details" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StaffDetails.aspx.cs" Inherits="CutAdmin.StaffDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <script src="../js/libs/jquery-1.10.2.min.js"></script>--%>
    <script src="Scripts/StaffDetails.js?v=1.24"></script> 
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="js/libs/TreeView/dist/simTree.js"></script>
    <link href="js/libs/TreeView/dist/simTree.css" rel="stylesheet" />
    <link href="css/jquerysctipttop.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Staff Details</h1>
            <h2><a href="AddStaff.aspx" class="button anthracite-gradient" style="float: right;"><span class="icon-user"></span>Add New</a> 
                 <a onclick="ExportStaffDetailsToExcel()" style="">
                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35">
                    </a>
            </h2>
            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_StaffDetails">
                    <thead>
                        <tr>
                            <th scope="col">Staff Detail</th>
                            <th scope="col" class="align-center hide-on-mobile">Email | Password Manage </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Unique Code </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">IsActive</th>
                            <th scope="col" width="150" class="align-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>

</asp:Content>
