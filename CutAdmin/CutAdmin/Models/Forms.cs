﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CutAdmin.BL;
namespace CutAdmin.Models
{
    public class Forms
    {
        public static List<MenuItem> GenrateMenu()
        {
            List<MenuItem> arrForms = new List<MenuItem>();
            try
            {
                List<tbl_Form> arrMenu = new List<tbl_Form>();
                using (var db = new DBHandlerDataContext())
                {
                    string UserType = CutAdmin.DataLayer.AccountManager.GetUserType();
                    if (UserType == "Supplier" || UserType == "SupplierStaff")
                    {
                        arrMenu = (from obj in db.tbl_Forms select obj).Distinct().ToList();
                        if(CutAdmin.DataLayer.AccountManager.GetUserType() == "SupplierStaff")
                        {
                            var UserID = CutAdmin.DataLayer.AccountManager.GetUser();
                            var arrMenuID = (from obj in db.tbl_Forms
                                             join
                                             objStaffRole in db.tblStaffRoleManagers on obj.ID equals
                                             objStaffRole.nFormId
                                             where objStaffRole.nUid == UserID
                                             select objStaffRole.nFormId).ToList();

                            arrMenu = (from eachDrive in arrMenu
                                       where arrMenuID.Contains(eachDrive.ID)
                                       select eachDrive).ToList();
                        }
                    }

                    //if (UserType == "Admin" || UserType == "AdminStaff" || UserType == "SuperAdmin" || UserType == "Franchisee" || UserType == "FranchiseeStaff")
                    //{
                    //    arrMenu = (from obj in db.tbl_Forms select obj).Distinct().ToList();
                    //    if(UserType != "SuperAdmin" && UserType == "Admin" || UserType == "Franchisee")
                    //    {
                    //        arrMenu = (from obj in db.tbl_Forms
                    //                   //join  objRoles in db.tbl_AdminRoleManagers on obj.ID equals objRoles.nFormId
                    //                   where objRoles.nUid == CutAdmin.DataLayer.AccountManager.GetUserByLogin()
                    //                   select obj).Distinct().ToList();
                    //    }
                    //    if (CutAdmin.DataLayer.AccountManager.GetUserType() == "AdminStaff" || CutAdmin.DataLayer.AccountManager.GetUserType() == "FranchiseeStaff")
                    //    {
                    //        var arrMenuID = (from obj in db.tbl_Forms
                    //                         join
                    //                         objStaffRole in db.tblStaffRoleManagers on obj.ID equals
                    //                         objStaffRole.nFormId
                    //                         where objStaffRole.nUid == CutAdmin.DataLayer.AccountManager.GetUserByLogin()
                    //                         select objStaffRole.nFormId).ToList();

                    //        arrMenu = (from eachDrive in arrMenu
                    //                   where arrMenuID.Contains(eachDrive.ID)
                    //                   select eachDrive).ToList();
                    //    }
                    //}

                    ///my work//
                    //  else if (UserType == "Franchisee" || UserType == "FranchiseeStaff")
                    //  {
                    //var arrmenu  = (from obj in db.tblFranchiseeForms
                    //                select obj).Distinct().ToList();
                    //foreach (var obj in arrmenu)
                    //{
                    //    arrMenu.Add( new tbl_Form
                    //    {
                    //        ID = obj.nId,
                    //        Menu1 = obj.sFormName,
                    //        Menu2 = obj.sDisplayName,
                    //        Menu3 = obj.Menu3,
                    //        Menu4 = obj.Menu4,
                    //        Form = obj.Form,
                    //    });
                    //}
                    //   if (UserType == "FranchiseeStaff")
                    //  {
                    //      var arrMenuID = (from obj in db.tblForms
                    //                           join
                    //                           objStaffRole in db.tblStaffRoleManagers on obj.nId equals
                    //                           objStaffRole.nFormId
                    //                           where objStaffRole.nUid == CutAdmin.DataLayer.AccountManager.GetUserByLogin()
                    //                           select objStaffRole.nFormId).ToList();
                    //
                    //          arrMenu = (from eachDrive in arrMenu
                    //                     where arrMenuID.Contains(eachDrive.ID)
                    //                     select eachDrive).ToList();
                    //      }
                    //  }
                }
                foreach (var smenu in arrMenu.Select(d => d.Menu1).Distinct())
                {
                    arrForms.Add(new MenuItem { Name = smenu, ChildItem = new List<MenuItem>(), Path = string.Empty });
                    if (arrMenu.Where(F => F.Menu1 == smenu).All(d => d.Menu2 == ""))
                    {
                        arrForms.LastOrDefault().ID = arrMenu.Where(d => d.Menu1 == smenu).FirstOrDefault().ID;
                        arrForms.LastOrDefault().Path = arrMenu.Where(d => d.Menu1 == smenu).FirstOrDefault().Form;
                    }
                    else
                    {
                        foreach (var sSub1 in arrMenu.Where(f => f.Menu1 == smenu).Select(d => d.Menu2).Distinct())
                        {
                            arrForms.LastOrDefault().ChildItem.Add(new MenuItem
                            {
                                Name = sSub1,
                                Path = String.Empty,
                                ChildItem = new List<MenuItem>(),
                            });
                            foreach (var objSub in arrMenu.Where(f => f.Menu2 == sSub1).Select(d => d.Menu3).Distinct())
                            {
                                if (objSub != "")
                                {
                                    arrForms.LastOrDefault().ChildItem.LastOrDefault().ChildItem.Add(new MenuItem
                                    {
                                        ID = arrMenu.Where(f => f.Menu2 == sSub1 && f.Menu3 == objSub).FirstOrDefault().ID,
                                        Name = objSub,
                                        Path = arrMenu.Where(f => f.Menu2 == sSub1 && f.Menu3 == objSub).FirstOrDefault().Form,
                                        ChildItem = new List<MenuItem>(),
                                    });
                                    foreach (var objSubMenu in arrMenu.Where(f => f.Menu3 == objSub && f.Menu4 != "").Select(d => d.Menu4).Distinct())
                                    {
                                        arrForms.LastOrDefault().ChildItem.LastOrDefault().ChildItem.LastOrDefault().ChildItem.Add(new MenuItem
                                        {
                                            ID = arrMenu.Where(f => f.Menu4 == objSubMenu).FirstOrDefault().ID,
                                            Name = objSubMenu,
                                            Path = arrMenu.Where(f => f.Menu4 == objSubMenu).FirstOrDefault().Form,
                                            ChildItem = new List<MenuItem>(),
                                        });

                                    }
                                }
                                else
                                {
                                    arrForms.LastOrDefault().ChildItem.LastOrDefault().ID = arrMenu.Where(f => f.Menu2 == sSub1 && f.Menu3 == objSub).FirstOrDefault().ID;
                                    arrForms.LastOrDefault().ChildItem.LastOrDefault().Path =
                                        arrMenu.Where(f => f.Menu2 == sSub1 && f.Menu3 == objSub).FirstOrDefault().Form;
                                    arrForms.LastOrDefault().ChildItem.LastOrDefault().Name =
                                       sSub1;
                                }
                            }
                        }
                    }

                }

             
            }
            catch (Exception ex)
            {
                GenrateMenu();
            }
            return arrForms;
        }


        public static string GetFormByMenu()
        {
            StringBuilder sMenu = new StringBuilder();
            try
            {
                List<MenuItem> arrMenu = new List<MenuItem>();
                if (HttpContext.Current.Session["Menu"] == null)
                    arrMenu = GenrateMenu();
                else
                    arrMenu = (List<MenuItem>)HttpContext.Current.Session["Menu"];
                if(arrMenu.Count ==0)
                {
                    arrMenu = GenrateMenu();
                    HttpContext.Current.Session["Menu"] = arrMenu;
                }
                
                if (HttpContext.Current.Session["Menu"] == null)
                    HttpContext.Current.Session["Menu"] = arrMenu;
                foreach (var objMenu in arrMenu)
                {
                    if (objMenu.ChildItem.Count == 0)
                    {
                        sMenu.Append("<li><a href='" + objMenu.Path + "'><span>" + objMenu.Name + "</span></a></li>");
                    }
                    else
                    {
                        sMenu.Append("<li class=\"with-right-arrow\">");
                        sMenu.Append("<span>" + objMenu.Name + "</span>");
                        sMenu.Append("<ul class=\"big-menu\">");
                        foreach (var ChildItem in objMenu.ChildItem)
                        {
                            if (ChildItem.ChildItem.Count == 0)
                            {
                                sMenu.Append("<li><a href='" + ChildItem.Path + "'><span>" + ChildItem.Name + "</span></a></li>");
                            }
                            else
                            {
                                sMenu.Append("<li class=\"with-right-arrow\">");
                                sMenu.Append("<span>" + ChildItem.Name + "</span>");
                                sMenu.Append("<ul class=\"big-menu\">");
                                foreach (var ChildItemSub in ChildItem.ChildItem)
                                {
                                    if (ChildItemSub.ChildItem.Count == 0)
                                        sMenu.Append("<li><a href='" + ChildItemSub.Path + "'><span>" + ChildItemSub.Name + "</span></a></li>");
                                    else
                                    {
                                        sMenu.Append("<li class=\"with-right-arrow\">");
                                        sMenu.Append("<span>" + ChildItemSub.Name + "</span>");
                                        sMenu.Append("<ul class=\"big-menu\">");
                                        foreach (var OBJ in ChildItemSub.ChildItem)
                                        {
                                            sMenu.Append("<li><a href='" + OBJ.Path + "'><span>" + OBJ.Name + "</span></a></li>");
                                        }
                                        sMenu.Append("</ul>");
                                        sMenu.Append("</li>");
                                    }

                                }
                                sMenu.Append("</ul>");
                                sMenu.Append("</li>");
                            }
                        }
                        sMenu.Append("</ul>");
                        sMenu.Append("</li>");
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return sMenu.ToString();
        }




    }

    public class MenuItem
    {
        public Int64 ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public List<MenuItem> ChildItem { get; set; }
    }
}