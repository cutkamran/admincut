﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="roomrate.aspx.cs" Inherits="CutAdmin.roomrate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <script src="Scripts/Exchange.js?v=1.2"></script>
   <%-- <script src="Scripts/Supplier.js?v=1.1"></script>--%>
    <script src="Scripts/roomrate.js?v=2.3"></script>
      <script src="js/UploadDoc.js"></script>
  
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
     <script src="Scripts/Hotels/GetRateType.js"></script>
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">
    <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>
      <script src="Scripts/moments.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Add rates for</b>
                <label id="lblHotel"></label>
                <span class="float-right">Room type
                    <label id="lblroom"></label>
                </span></h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="frm_Rates">

                <%-- 1st step Supplier --%>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Main</legend>
                    <div class="field-block button-height" id="chkNationality">
                        <small class="input-info">Select supplier for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Supplier</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="sel_Supplier">
                            <option value="0">Directly from hotel</option>
                        </select>
                    </div>

                    <div class="field-block button-height" id="Currency">
                        <small class="input-info">Select in which currency you wish to add / update rates</small>
                        <label for="currency" class="label"><b>Currency</b></label>
                        <%--Use dropdown--%>
                        <select name="Exchange" class="select multiple-as-single expandable-list input validate[required]" style="width: 180px" id="SelCurrency" onchange="SetCurrency(this.value)">
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select one or more nationality / market for which rates are valid</small>
                        <label for="market" class="label "><b>Valid for market</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="country" class="select multiple-as-single easy-multiple-selection check-list expandable-list input validate[required]" id="sel_Country" multiple style="width: 180px">
                        </select>

                    </div>
                    <div class="field-block button-height" id="div_Meals">
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">You may define booking / promo code as maybe provided by hotel</small>
                        <label for="ratecode" class="label "><b>Booking Code</b></label>
                        <input type="text" name="Tariff_Type" id="ratecode" value="" class="input small-margin-right input <%--validate[required]--%>">
                    </div>
                    
                      <%--<div class="field-block button-height">
                        <label for="tarifftype" class="label "><b>Tariff Policy</b></label>
                          <div class="columns">
                           <div class="column three-columns-mobile six-columns-tablet">
                                <span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="validation-number" id="txt_MinStay" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
								<button type="button" class="button number-up">+</button>
							</span>
                                <small class="">Min Stay </small> 
                                </div>
                          <div class="column three-columns-mobile six-columns-tablet">
                                <span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="validation-number" id="txt_MaxStay" value="0" size="4" class="input-unstyled validate[required,min[0],max[30]]">
								<button type="button" class="button number-up">+</button>
							</span>
                                <small class="">Max Stay </small> 
                                </div>
                        <div class="column three-columns-mobile six-columns-tablet">
                                <span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="validation-number" id="txt_MinRoom" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
								<button type="button" class="button number-up">+</button>
							</span>
                                <small class="">Min Room </small> 
                                </div>
                            </div>
                          </div>--%>

                    <div class="field-block button-height">
                      <small class="input-info">Define tariff type and you may also upload contract / promo file if any</small>
                        <label for="tarifftype" class="label "><b>Tariff Type</b></label>
                          <select name="tarifftype" class="select expandable-list mid-margin-right" style="width: 180px" id="sel_RateType">
                        </select>
                        <span class="input file">
                            <span class="file-text"></span>
                            <span class="button compact">Upload file</span>
                            <input type="file" name="file-input" id="file-input" value="" class="file withClearFunctions input ">
                        </span>
                        
                          
                      
                    </div>

                <%--    <div class="field-block button-height wizard-controls clearfix">
                        <div class="wizard-spacer" style="height: 18px"></div>
                        <button type="button" class="button glossy mid-margin-right wizard-next float-right">Next<span class="button-icon right-side"><span class="icon-forward"></span></span></button>
                    </div>--%>
                </fieldset>

                <%-- 2nd step Tariff --%>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Tariff</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Tariff for</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <label class="black sSupplier"></label>
                            </div>
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Tariff Type</label>
                                <label class="black sTariff"></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="black smarkets"></label>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">You may select more than 1 date range to enter the same rates in the different date range</small>
                        <label for="general" class="label"><b>General Tariff</b></label>
                        <div id="div_GNDate">
                        </div>
                        <div class="columns" style="display: none">
                        </div>
                    </div>
                    <div class="field-block button-height" style="padding-left: inherit;">
                        <!-- Meal Plan wise rates -->
                        <div class="side-tabs same-height tabs-active " style="height: auto !important;">
                            <!-- Tabs -->
                            <ul class="tabs" id="ul_GN">
                            </ul>
                            <div class="tabs-content" style="height: auto !important;" id="div_GNtab">
                            </div>
                        </div>
                    </div>

                    <div class="field-block button-height clear-both">
                        <small><a class="green pointer strong label" style="float: none" onclick="showspecialrate()">Special Dates</a></small>
                    </div>

                    <!-- Special Dates -->
                    <div class="field-drop button-height black-inputs" id="specialrate" style="display: none;">
                        <small class="input-info white">You may add any special dates like New Year, Diwali, etc. to feed different rates</small>
                        <label for="special" class="label"><b>Special Dates</b></label>
                        <div id="div_SPDate">
                            
                        </div>
                        <div class="wizard-spacer" style="height: 15px;"></div>

                        <div class="side-tabs same-height tabs-active tariff-tab">
                            <!-- Tabs -->
                            <ul class="tabs" id="ul_SP">
                            </ul>
                            <div class="tabs-content" style="height: auto !important;" id="div_SPtab">
                            </div>
                        </div>

                    </div>

                    <div class="wizard-spacer" style="height: 50px"></div>
                   
                </fieldset>

                <%-- 3rd step Conditions --%>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Conditions</legend>
                       <%--Offer Valid --%>
                        <%--<div class="field-block button-height">
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile">
                                <label for="mail" class="label"><b>Offers</b></label>
                                <label for="chk_offer" class="tooltip" title="Click to Add offer"><a>Is this tariff having any offer?
                                    <input type="checkbox" style="display:none" id="chk_offer" onclick="GetMasterOffer(this)">
                                    </a>
                                </label>
                                <span>
                                    <input type="checkbox" class="switch tiny"  data-text-on="YES" data-text-off="NO" id="chk_offer" onclick="GetMasterOffer(this)">
                                </span>
                            </div>
                            <div class="eight-columns twelve-columns-mobile" id="div_offer" style="display:none">
                                 <select name="Offers" class="select multiple-as-single easy-multiple-selection check-list expandable-list input" style="width: 180px;" id="sel_offer" multiple>
                                 </select>
                            </div>

                             
                        </div>
                    </div>--%>


                    <div class="field-block button-height">
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile no-margin-bottom">
                                <label for="mail" class="label"><b>Cancellation Policy</b></label>
                                <label class="">Is this tariff refundable? </label>
                                <span>
                                    <input type="checkbox" class="switch medium"  data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="chk_Cancel" onchange="ShowCancel(this)">
                             
                                </span>
                            </div>
                            <div class="eight-columns twelve-columns-mobile" id="cnlpolicy" style="display:none">

                                <input type="radio" class="radio " id="percentage" value="1" name="chk_Charge" checked="checked" onclick="GetChargeCond(this, 'percentageinput')">
                                <label for="percentage">Charges in percentage</label>

                                <input type="radio" class="radio mid-margin-left" id="amount" value="2" name="chk_Charge" onclick="GetChargeCond(this, 'amountinput')">
                                <label for="amount">Charges in amount</label>

                                <input type="radio" class="radio mid-margin-left" id="nightly" value="3" name="chk_Charge" onclick="GetChargeCond(this, 'nightlyinput')">
                                <label for="nightly">Nightly charge</label>
                            </div>
                        </div>
                    </div>
                    <%-- Charges in percentage --%>
                    <div class="field-drop button-height black-inputs" id="percentageinput" style="display:none">
                        <small class="input-info silver">Specify the charges in percentage in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in percentage (%)</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="validate[required] select expandable-list anthracite-gradient" id="sel_ChargePer">
                                        <option value="">Applicable Charges</option> 
                                        <option value="0%">No charges will apply</option>
                                        <option value="10%">10% will be charged</option>
                                        <option value="20%">20% will be charged</option>
                                        <option value="30%">30% will be charged</option>
                                        <option value="40%">40% will be charged</option>
                                        <option value="50%">50% will be charged</option>
                                        <option value="60%">60% will be charged</option>
                                        <option value="70%">70% will be charged</option>
                                        <option value="80%">80% will be charged</option>
                                        <option value="90%">90% will be charged</option>
                                        <option value="100%">100% will be charged</option>
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_DayPer">
                                       <option value="" >How long before arrival </option>
                                        <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_NoShowPer">
                                    <option value="" class="select-value">In case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="Same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>

                        <%-- Hide this if no special dates selected --%>
                        <div class="row" id="specialdates-percentage">
                            <span class="replacement" tabindex="0">
                                <span class="check-knob"></span>
                                <input type="checkbox" name="Special Dates Policy" id="sp-cnl-show" value="special date" class="checkbox SPCancel" tabindex="-1" onclick="ShowSpeicalCancel(this, 'percentage')">
                            </span>
                            <label for="sp-cnl-show">Different policy for special dates</label>

                        </div>

                        <div id="definespecialdate-percentage" class="spCancellation" style="display:none">
                            <div class="rows">
                                <small class="input-info silver">Specify the charges in percentage in the event of cancellation for special dates</small>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <select class="select expandable-list anthracite-gradient  validate[required]" id="sel_ChargeSPPer">
                                        <option value="">Applicable Charges</option>
                                         <option value="0">No charges will apply</option>
                                         <option value="10">10% will be charged</option>
                                         <option value="20">20% will be charged</option>
                                         <option value="30">30% will be charged</option>
                                         <option value="40">40% will be charged</option>
                                         <option value="50">50% will be charged</option>
                                         <option value="60">60% will be charged</option>
                                         <option value="70">70% will be charged</option>
                                         <option value="80">80% will be charged</option>
                                         <option value="90">90% will be charged</option>
                                         <option value="100">100% will be charged</option>
                                    </select>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <select class="select expandable-list anthracite-gradient  validate[required]" ID="sel_DaySPPer">
                                        <option value="">How long before arrival </option>
                                        <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                    </select>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <select class="select expandable-list anthracite-gradient validate[required]" id="sel_NoShowSPPer" >
                                        <option value="" class="select-value">In case of No Show </option>
                                        <option value="100%">100% charged in case of No Show</option>
                                        <option value="Same">Same as cancellation charge</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- Charges in amount --%>
                    <div class="field-drop button-height black-inputs" id="amountinput" style="display: none;">
                        <small class="input-info silver">Specify the charges in amount, in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in amount</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                <span class="input">
                                    <label style="width: 30px" for="roomtariff" class="button green-gradient sCurrency"></label>
                                    <input style="width: 60px" type="text" name="RoomTariff" id="cancellationamount" class="input-unstyled  validate[required]"  placeholder="12,345.00">
                                </span>
                                <label class="">will be charged</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient  validate[required]" tabindex="0" id="sel_DayAmt">
                                    <option value="" >How long before arrival </option>
                                       <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                </select>
                            </div>
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient  validate[required]" id="sel_NoShowAmt" >
                                    <option value="" class="select-value">In case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>
                        <%-- Hide this if no special dates selected --%>
                        <div class="row" id="specialdates-amount">
                            <span class="replacement" tabindex="0">
                                <span class="check-knob"></span>
                                <input type="checkbox" name="Special Dates Policy" id="sp-cnl-show-amount" value="special date SPCancel" class="checkbox" tabindex="-1" onclick="ShowSpeicalCancel(this, 'amount')">
                            </span>
                            <label for="sp-cnl-show-amount">Different policy for special dates</label>

                        </div>
                        <div id="definespecialdate-amount" class="spCancellation" style="display:none">
                            <div class="rows">
                                <small class="input-info silver">Specify the charges in amount, in the event of cancellation</small>
                            </div>
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                    <span class="input">
                                        <label style="width: 30px" for="sp-cancellationamount" class="button green-gradient sCurrency"></label>
                                        <input style="width: 60px" type="text" name="RoomTariff" id="sp-cancellationamount" class="input-unstyled" placeholder="0.00">
                                    </span>
                                    <label class="">will be charged</label>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <select class="select expandable-list anthracite-gradient  validate[required]" tabindex="0" id="sel_DaySPAmt">
                                        <option value="">How long before arrival </option>
                                        <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                    </select>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <select  class="select expandable-list anthracite-gradient validate[required]" tabindex="0">
                                        <option value="" class="select-value">In case of No Show </option>
                                        <option value="100">100% charged in case of No Show</option>
                                        <option value="Same">Same as cancellation charge</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- Nightly Charge --%>
                    <div class="field-drop button-height black-inputs" id="nightlyinput" style="display: none;">
                        <small class="input-info silver">Specify No. of night will be charged, in the event of cancellation</small>
                        <label for="" class="label"><b>Nightly charges</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="validation-number" id="validation-number" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
								<button type="button" class="button number-up">+</button>
							</span>
                                <label class="">Nights Applicable </label> 
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_DaysNight">
                                    <option value="" class="select-value">How long before arrival </option>
                                        <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                </select>
                            </div>
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select  class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_NoShowNight" >
                                        <option value="" class="select-value">In case of No Show </option>
                                        <option value="100">100% charged in case of No Show</option>
                                        <option value="Same">Same as cancellation charge</option>
                                    </select>
                            </div>
                        </div>
                        <%-- Hide this if no special dates selected --%>
                        <div class="row" id="specialdates-nightly">
                            <span class="replacement" tabindex="0">
                                <span class="check-knob"></span>
                                <input type="checkbox" name="Special Dates Policy" id="sp-cnl-show-nightly" value="special date" class="checkbox SPCancel" tabindex="-1" onclick="ShowSpeicalCancel(this, 'nightly')">
                            </span>
                            <label for="sp-cnl-show-nightly">Different policy for special dates</label>

                        </div>
                        <div id="definespecialdate-nightly"  class="spCancellation" style="display:none" >
                            <div class="rows">
                                <small class="input-info silver">Specify No. of night will be charged, in the event of cancellation</small>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                <span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="validation-number" id="validation-spnumber" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
								<button type="button" class="button number-up">+</button>
							</span>
                                <label class="">Nights Applicable </label> 
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <select class="select expandable-list anthracite-gradient validate[required]" id="sel_DaysSPNight">
                                        <option value="" class="select-value">How long before arrival </option>
                                        <option value="1days">until 1 day before arrival date</option>
                                        <option value="2days">until 2 days before arrival date</option>
                                        <option value="3days">until 3 days before arrival date</option>
                                        <option value="5days">until 5 days before arrival date</option>
                                        <option value="10days">until 10 days before arrival date</option>
                                        <option value="15days">until 15 days before arrival date</option>
                                        <option value="20days">until 20 days before arrival date</option>
                                        <option value="25days">until 25 days before arrival date</option>
                                        <option value="30days">until 30 days before arrival date</option>
                                        <option value="45days">until 45 days before arrival date</option>
                                        <option value="60days">until 60 days before arrival date</option>
                                        <option value="72days">until 72 days before arrival date</option>
                                        <option value="90days">until 90 days before arrival date</option>
                                        <option value="10:00 AM">until 10:00 AM on day of arrival</option>
                                        <option value="14:00 PM">until 14:00 PM on day of arrival</option>
                                </select>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <select  class="select expandable-list anthracite-gradient validate[required]"" tabindex="0" id="sel_NoShowSPNight">
                                        <option value="" class="select-value">In case of No Show </option>
                                        <option value="100%">100% charged in case of No Show</option>
                                        <option value="Same">Same as cancellation charge</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <br /><br />
                    <%-- Additional Meal Charges --%>
                    <div class="field-block button-height">
                        <label for="" class="label"><b>Meal Rates</b></label>
                        <p>
                            Do you want to define extra meal rates? 
                            <span>
                                <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="chk_mealrates" onchange="ShowMeal(this)">
                            </span>
                        </p>
                    </div>
                    <div class="field-drop button-height black-inputs chk_Meal" style="display:none">
                        <label for="" class="label"><b>Additional Meal Rates</b></label>
                        <small class="input-info">Here you can define extra meal rates if you wish to sell them online</small>
                        <label for="" class="label large-margin-top"><b>Adult</b></label>
                        <div class="columns" id="div_AdultMeals">
                           
                        </div>
                        <%-- Hide if different rate checkbox is not selected --%>
                           <label for="" class="label cChildMeals" style="display:none"><b>Child</b></label>
                        <div class="columns cChildMeals" style="display:none" id="div_ChildMeals">
                        </div>
                    </div>

                    <div class="field-block button-height">
                        <label for="tarifftype" class="label "><b>Stay restrictions</b></label>
                   
                            <p>
                                <span>This rate can be booked if min</span>
                                <span class="number input small-margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input type="text" name="validation-number" id="txt_MinStay" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <span>night(s) & maximum</span>
                                <span class="number input margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input type="text" name="validation-number" id="txt_MaxStay" value="0" size="4" class="input-unstyled validate[required,min[0],max[30]]">
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <span>night(s) are booked</span>
                            </p>
                            <small class="input-info silver">KEEP IT '0' (ZERO) IF YOU DON'T HAVE MAXIMUM NIGHTS STAY RESTRICTIONS</small>
              
                    </div>
                    <div class="field-block button-height">
                        <label for="tarifftype" class="label"><b>Rooms restrictions</b></label>
               
                            <p>
                                <span>This rate can be booked if min</span>
                                <span class="number input small-margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input type="text" name="validation-number" id="txt_MinRoom" value="1" size="4" class="input-unstyled validate[required,min[1],max[30]]">
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <span>room(s) & maximum</span>
                                <span class="number input margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input type="text" name="validation-number" id="txt_MaxRoom" value="0" size="4" class="input-unstyled validate[required,min[0],max[30]]">
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <span>room(s) are booked</span>
                            </p>
                            <small class="input-info silver">KEEP IT '0' (ZERO) IF YOU DON'T HAVE MAXIMUM ROOMS BOOKING RESTRICTIONS</small>
                        </div>
          
                 

                </fieldset>

                <%-- 4th step Preview --%>
                <fieldset class="wizard-fieldset fields-list" id="div_RatePreview">
                    <legend class="legend">Preview</legend>
                    
                 

                   
                   

                </fieldset>

            </form>
        </div>
    </section>

   

    <script>

        $(document).ready(function () {
            debugger
            //setTimeout(function() {

            //},1000)
            // Elements
            var form = $('.wizard'),

                // If layout is centered
                centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
             * Center function
             * @param boolean animate whether or not to animate the position change
             * @return void
             */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });
            // Validation

            if ($.validationEngine) {
                debugger
                form.validationEngine();
            }
            form.showWizardPrevStep(true)
        });
    </script>


    <script src="js/developr.collapsible.js"></script>
    <script>
        function showspecialrate() {
            var x = document.getElementById("specialrate");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
    <script>
        function showdaywise(MealPlanID) {
            debugger

            setTimeout(function () {
                $('#div_tab' + MealPlanID).refreshTabs();
            }, 100)
            var x = document.getElementById("ro-daywise" + MealPlanID);
            if (x.style.display === "none") {
                x.style.display = "block";
                $("#chk_day" + MealPlanID).prop("checked", true);
            } else {
                x.style.display = "none";
                $("#chk_day" + MealPlanID).prop("checked", false);
            }

        }
        function showdaywiseMin(MealPlanID) {
            debugger

            setTimeout(function () {
                $('#div_tab' + MealPlanID).refreshTabs();
            }, 100)
            var x = document.getElementById("ro-daywiseMin" + MealPlanID);
            if (x.style.display === "none") {
                x.style.display = "block";
                $("#chk_dayMin" + MealPlanID).prop("checked", true);
            } else {
                x.style.display = "none";
                $("#chk_dayMin" + MealPlanID).prop("checked", false);
            }

        }
    </script>
    <script>
        function addtax() {
            var x = document.getElementById("addtax");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
    <script>
        function genminsell(MealPlanID) {
            setTimeout(function () {
                $('#div_tab' + MealPlanID).refreshTabs();
            }, 100)
            var x = document.getElementById("genminsell" + MealPlanID);
            if (x.style.display === "none") {
                x.style.display = "block";
                $('.sMinCellDate').show();
                $("#ro-daywiseMin" + MealPlanID).hide();
                $("#chk_Min" + MealPlanID).prop("checked", true);
            } else {
                x.style.display = "none";
                $('.sMinCellDate').hide();
                $("#ro-daywiseMin" + MealPlanID).hide();
                $("#chk_Min" + MealPlanID).prop("checked", false);
            }
        }
    </script>
</asp:Content>
