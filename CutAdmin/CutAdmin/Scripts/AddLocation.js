﻿$(document).ready(function () {
    GetLocation();
});
var contryname;
var cityname;
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetLocCountry() {
    post("GenralHandler.asmx/GetCountry", {}, function (result) {
        arrCountry = result.Country;
        $('#selCountry').append($('<option selected="selected" disabled></option>').val("").html("-Please Select Country-"));
        $(arrCountry).each(function (index, sCountry) {
            $('#selCountry').append($('<option></option>').val(sCountry.Country).html(sCountry.Countryname));
        });
        $('#selCountry').change();
    }, function (error) {
        AlertDanger("server not responding.")
    });
}

function GetLocCity(reccountry) {
    if (reccountry != "") {
        $('#selCity').empty();
        post("GenralHandler.asmx/GetCity2", { country: reccountry }, function (result) {
            arrCity = result.CityList;
            $('#selCity').append($('<option selected="selected" disabled></option>').val("").html("-Please Select City-"));
            $(arrCity).each(function (index, City) {
                $('#selCity').append($('<option></option>').val(City).html(City));
            });
            $('#selCity').change();
        }, function (error) {
            AlertDanger("server not responding.")
        })
    }
}

function SaveLocation() {

    if ($("#frm_Location").validationEngine('validate')) {
        /*array Location detail for tbl_araegroup*/
        arrCity = {
            Name: $("#Area_Name").val(),
            CountryName: $("#selCountry option:selected").text(),
            Country_Code: $("#selCountry").val(),
            latitude: $("#lat_Area").val(),
            longitude: $("#log_Area").val(),
            Placeid: $("#AreaPlaceID").val(),
        };
        /*array Location detail for tbl_commomlocation*/
        arrLocation = {
            LocationName: $("#Location_Name").val(),
            City: $('#selCity').val(),
            CityCode: $('#selCountry').val(),
            Country: $("#selCountry option:selected").text(),
            CountryCode: $('#selCountry').val(),
            Latitude: $("#lat_Location").val(),
            Longitutde: $("#log_Location").val(),
            Placeid: $("#LocationPlaceID").val(),
        };
        var Data = {
            arrLocation: arrLocation,
            arrCity: arrCity,
        }
        post("../LocationHandler.asmx/Save", Data, function (result) {
            Success("Location Updated succesfully");
            $("#modals").remove();
            GetLocation();
        }, function (error) {
            AlertDanger("Something Going Wrong");
        })
    }
}

function GetLocation() {
    $("#tbl_GetLocation").dataTable().fnClearTable();
    $("#tbl_GetLocation").dataTable().fnDestroy();
    post("../LocationHandler.asmx/GetLocation", {}, function (result) {
        var arrActLocation = result.LocationList;
        var tRow = '';
        for (var i = 0; i < arrActLocation.length; i++) {
            tRow += '        <tr>';
            tRow += '    <td style="width:3%" align="center">' + (i + 1) + '</td>';
            tRow += '    <td style="width:11%" align="center">' + arrActLocation[i].LocationName + '</td>';
            tRow += '    <td style="width:11%" align="center">' + arrActLocation[i].City + '</td>';
            tRow += '    <td style="width:10%" align="center">' + arrActLocation[i].Country + '</td>';
            tRow += '<td class="align-center" style="vertical-align:middle;width:5%">';
            tRow += '<div class="button-group compact">';
            //tRow += '<a class="button icon-pencil with-tooltip" title="Edit Details"  onclick="GetDetails(\'' + arrActLocation[i].Lid + '\')"></a>'
            tRow += '<a class="button icon-marker with-tooltip" title="View On Map"  onclick="ViewMap(\'' + arrActLocation[i].Latitude + '\',\'' + arrActLocation[i].Longitutde + '\')"></a>'
            tRow += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeleteLocation(\'' + arrActLocation[i].Lid + '\',\'' + arrActLocation[i].LocationName + '\')"></a>'
            tRow += '</div ></td>';
            tRow += '</tr>';

        }
        $("#tbl_GetLocation tbody").append(tRow);
        $('[data-toggle="tooltip"]').tooltip()
        $("#tbl_GetLocation").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
        $("#tbl_GetLocation").css("width", "100%")
    }, function (error) {
        $("#tbl_GetLocation").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
    })
}

function DeleteLocation(LocationId, LocationName) {
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/><span class=\"orange\">' + LocationName + '</span>?</p>', function () {
        post("../LocationHandler.asmx/DeleteLocation", { LocationId: LocationId }, function (result) {
            Success("Location has been deleted successfully.");
            GetLocation();
        }, function (error) {
            AlertDanger(error.ex);
        })
    }, function () {
        $('#modals').remove();
    });
}

var id;
function GetDetails(LocationID) {
    id = LocationID;
    $.ajax({
        url: "../ActivityHandller.asmx/GetDetails",
        type: "post",
        data: '{"LocationID":"' + LocationID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var data = result.dttable;

                $("#txt_Location").val(data[0].LocationName);
                $("#selCountry option:selected").val(data[0].Country);

                var CountryName = $.grep(arrCountry, function (p) { return p.Countryname == data[0].Country; })
                    .map(function (p) { return p.Country; });
                $("#DivCountry .select span")[0].textContent = data[0].Country;
                for (var i = 0; i < arrCountry.length; i++) {
                    if (arrCountry[i].Countryname == data[0].Country.toUpperCase()) {
                        $("#DivCountry .select span")[0].textContent = arrCountry[i].Countryname;
                        $('input[value="' + arrCountry[i].Country + '"][class="OfferType"]').prop("selected", true);
                        $("#selCountry").val(arrCountry[i].Country);
                        GetLocCity(arrCountry[i].Country)
                    }


                }


                $("#City .select span")[0].textContent = data[0].City;
                for (var i = 0; i < arrCity.length; i++) {
                    if (arrCity[i] == data[0].City.toUpperCase()) {
                        $("#City .select span")[0].textContent = arrCity[i];
                        $('input[value="' + arrCity[i] + '"][class="OfferType"]').prop("selected", true);
                        $("#selCity").val(arrCity[i]);
                    }
                }

                $("#selCity option:selected").val(data[0].City);
                //$("#txt_Longitude").val(data[0].Longitude);
                //$("#txt_Latitude").val(data[0].Latitude);
                $("#btn_Save").hide();
                $("#btn_Update").show();

            } else if (result.retCode == 0) {
                alert("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            alert('Error occured while processing your request! Please try again.');
        }
    });
}

function NewLocation() {
    try {
        openModals(content, "Add Location", function (result) {
            SaveLocation();
        });
        setTimeout(function () {
            GetLocCountry();
            $("#frm_Location").validationEngine();
        }, 500)
    } catch (e) { }
}
var content = '<form id="frm_Location">' +
    '<input type="hidden" id="Hdn_ID" value="0" />' +
    '<div class="columns">' +
    '<div class="twelve-columns twelve-columns-mobile">' +
    '<label>Country:</label>' +
    '<select name="select90" id="selCountry" onchange="GetLocCity(this.value);" class="select country full-width validate[required]" data-prompt-position="topLeft"></select>' +
    '</div></div>' +

    '<div class="columns"><div class="twelve-columns twelve-columns-mobile">' +
    '<label>City:</label>' +
    '<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>' +
    '<select id="selCity" name="validation-select" class="select multiple-as-single easy-multiple-selection check-list expandable-list full-width validate[required]" data-prompt-position="topLeft" onchange="codeAddress()" ></select>' +
    '</div></div>' +

    '<div class="columns"><div class="twelve-columns twelve-columns-mobile">' +
    '<label>Location:</label>' +
    '<span class="input full-width">' +
    '<input type="text" id="txt_Location" class="input-unstyled validate[required]" data-prompt-position="topLeft" style="width:90%" value=""><label style="float:right" class="button grey-gradient glossy icon-marker"></label>'
'</span></div></div>' +
'<div class="clear"></div>' +
'</form>';
function codeAddress() {
    if ($("#selCity").val() != "" && $("#selCity").val() != null) {
        geocoder = new google.maps.Geocoder();
        var AddNew = $("#selCity option:selected").text() + ", " + $("#selCountry option:selected").text();
        var address = AddNew.split(",");
        address = address[(address.length - 1)];
        address = address.trim();
        geocoder.geocode({ 'address': AddNew }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                debugger
                $("#Area_Name").val(results[0].address_components[0].short_name);
                var a = results[0].geometry.location.lat();
                var b = results[0].geometry.location.lng();
                $("#lat_Area").val(a);
                $("#log_Area").val(b);
                $("#AreaPlaceID").val(results[0].place_id);
                var Bounds = results[0].geometry.bounds;
                for (var i = 0; i < results[0].address_components.length; i++) {
                    if (results[0].address_components[i].short_name == $("#selCountry").val()) {
                        Restrict = { 'country': results[0].address_components[i].short_name };
                        $("#txt_Location").val("");
                        AutoCompleteForPlace(Restrict, Bounds)
                    }
                }
            }
        });
    }
}

function AutoCompleteForPlace(Restrict, Bounds) {
    setInterval(function () {
        $('.pac-container').css("z-index", "10000000");
    }, 500)
    google.maps.event.addDomListener
    var places2 = new google.maps.places.Autocomplete(
        (document.getElementById('txt_Location')),
        {
            icon: true,
            bounds: Bounds,
            strictBounds: true,
            componentRestrictions: Restrict,
        });
    google.maps.event.addListener(places2, 'place_changed', function () {
        debugger;
        var place = places2.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var Placeid = place.place_id;
        $('#lat_Location').val(latitude);
        $('#log_Location').val(longitude);
        $('#Location_Name').val(place.name);
        $('#LocationPlaceID').val(place.place_id);
    });
}

function ViewMap(Latitude, Longitude) {
    try {
        $.modal({
            content: '<div > <div id="map"></div></div>',
            title: "",
            width: 800,
            height: 400,
            scrolling: false,
            buttons: {
                'Close': {
                    classes: 'black-gradient small hidden',
                    click: function (modal) { modal.closeModal(); }
                },
            }
        });
        debugger;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(Latitude, Longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow();
        var marker;
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(Latitude, Longitude),
            map: map,
            zoom: 12,
        });
        setTimeout(function myfunction() {
            $("#map").removeAttr("style");
        }, 500)
    } catch (e) { }
}