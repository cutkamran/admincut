﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities = "", selectedFacilities = [];
var arrRoomList = [];
var UserType = "";
$(document).ready(function () {
    $("#frm_Inventory").validationEngine();
    HotelCode = GetQueryStringParams('sHotelID');
    if (HotelCode != undefined)
        GetHotel(HotelCode);
    else
        GetHotelList()
});

function GetHotelList() {
    var trRequest = "";
    $("#tbl_HotelList").dataTable().fnClearTable();
    $("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetMappedHotels",
        //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.MappedHotelList;
                OfferList = result.OfferList;
                RoomList = result.RoomList;
                arrRoomList = RoomList;
                arrUserType = UserType;
                //Hotels List
                if (arrHotellist.length > 0) {
                    for (i = 0; i < arrHotellist.length; i++) {
                        if (arrHotellist[i].Approved == false) {
                            continue;
                        }
                        arr = arrHotellist[i].HotelFacilities;
                        arrFacilities = arr.split(',');
                        trRequest += '<tr >';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelName + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CountryId + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CityId + '</td>';
                        //trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>';

                        // Ratings
                        if (arrHotellist[i].HotelCategory == 'Other' || arrHotellist[i].HotelCategory == '48055' || arrHotellist[i].HotelCategory == '0') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '1EST' || arrHotellist[i].HotelCategory == '559' || arrHotellist[i].HotelCategory == '1') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '2EST' || arrHotellist[i].HotelCategory == '560' || arrHotellist[i].HotelCategory == '2') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '3EST' || arrHotellist[i].HotelCategory == '561' || arrHotellist[i].HotelCategory == '3') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '4EST' || arrHotellist[i].HotelCategory == '562' || arrHotellist[i].HotelCategory == '4') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '5EST' || arrHotellist[i].HotelCategory == '563' || arrHotellist[i].HotelCategory == '5') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }
                        else {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>'
                            // trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }


                        //Ratings End


                        //Update
                        // trRequest += '<td class="align-center"><a target="_blank" class="button" title="Edit" href="UpdateApprovedHotel.aspx?sHotelID=' + arrHotellist[i].sid + '&Parent=' + arrHotellist[i].ParentID + '"><span class="icon-pencil"></span></a></td>';
                        // End Update

                        //trRequest += '<td class="align-center"><p><label for="hotelAcitve' + i + '"><input type="checkbox" name="switch-tiny" id="hotelAcitve' + i + '" class="switch tiny mid-margin-right" value="1"></label></p></td>'; //(Active/Deactive)
                        if (arrHotellist[i].Active == 'True') {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'False\')" disabled></td>';
                        }
                        else {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'True\')" disabled></td>';
                        }

                        //  trRequest += '<td class="align-center"><a href="#" onclick="SetMarkup(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelCode + '\')" class="button" title="Inventory"><span class="icon-read"></span></a></td>';

                        //trRequest += '<td class="align-center"><a target="_blank" class="button" title="Inventory" href="ShowInventory.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '&HAddress=' + arrHotellist[i].HotelAddress + '"><span class="icon-read"></span></a></td>';
                        //trRequest += '<td class="align-center"><a href="#" onclick="AddInventory(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelName + '\',\'' + arrHotellist[i].HotelCode + '\',\'' + arrHotellist[i].HotelAddress + '\')" class="button" title="Inventory"><span class="icon-read"></span></a></td>';

                        //Update//
                        trRequest += '<td class="align-center" style="vertical-align:middle;">';
                        trRequest += '<div class="button-group compact">';
                        trRequest += '<a href="hotel.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '" class="button icon-pencil with-tooltip" title="Edit Details "></a>';
                        trRequest += '<a href="#" class="button icon-gear with-tooltip" title="Markup" onclick="SetMarkup(\'' + arrHotellist[i].sid + '\')"></a>';
                        trRequest += '<a class="button icon-cart with-tooltip" title="Inventory" href="availability_pricing.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '"></a>';
                        // trRequest += '<a  class="button icon-cart with-tooltip" title="Inventory" href="InventoryDetails.aspx?sHotelID=' + arrHotellist[i].sid + '"></a>';
                        trRequest += '<a href="#" class="button icon-trash" onclick="DeleteHotelModel(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelName + '\')" title="Delete"></a>';
                        trRequest += '</div></td>'
                        trRequest += '<br></tr>'

                        //

                        trRequest += '<br></tr>';
                    }
                    $("#tbl_HotelList tbody").append(trRequest);
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                }

                $("#tbl_HotelList").dataTable(
                    {

                        sSort: true, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList").dataTable(
                    {

                        bSort: false, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }

        }

    });
}


function GetHotel(HotelCode) {
    var trRequest = "";
    $("#tbl_HotelList").dataTable().fnClearTable();
    $("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHotel",
        //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
        data: '{"HotelCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.MappedHotelList;
                OfferList = result.OfferList;
                RoomList = result.RoomList;
                arrRoomList = RoomList;
                arrUserType = UserType;
                //Hotels List
                if (arrHotellist.length > 0) {
                    for (i = 0; i < arrHotellist.length; i++) {
                        if (arrHotellist[i].Approved == false) {
                            continue;
                        }
                        arr = arrHotellist[i].HotelFacilities;
                        arrFacilities = arr.split(',');
                        trRequest += '<tr >';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelName + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CountryId + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CityId + '</td>';
                        //trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>';

                        // Ratings
                        if (arrHotellist[i].HotelCategory == 'Other' || arrHotellist[i].HotelCategory == '48055' || arrHotellist[i].HotelCategory == '0') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '1EST' || arrHotellist[i].HotelCategory == '559' || arrHotellist[i].HotelCategory == '1') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '2EST' || arrHotellist[i].HotelCategory == '560' || arrHotellist[i].HotelCategory == '2') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '3EST' || arrHotellist[i].HotelCategory == '561' || arrHotellist[i].HotelCategory == '3') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '4EST' || arrHotellist[i].HotelCategory == '562' || arrHotellist[i].HotelCategory == '4') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '5EST' || arrHotellist[i].HotelCategory == '563' || arrHotellist[i].HotelCategory == '5') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }
                        else {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>'
                            // trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }


                        //Ratings End


                        //Update
                        // trRequest += '<td class="align-center"><a target="_blank" class="button" title="Edit" href="UpdateApprovedHotel.aspx?sHotelID=' + arrHotellist[i].sid + '&Parent=' + arrHotellist[i].ParentID + '"><span class="icon-pencil"></span></a></td>';
                        // End Update

                        //trRequest += '<td class="align-center"><p><label for="hotelAcitve' + i + '"><input type="checkbox" name="switch-tiny" id="hotelAcitve' + i + '" class="switch tiny mid-margin-right" value="1"></label></p></td>'; //(Active/Deactive)
                        if (arrHotellist[i].Active == 'True') {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'False\')" disabled></td>';
                        }
                        else {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'True\')" disabled></td>';
                        }

                        //  trRequest += '<td class="align-center"><a href="#" onclick="SetMarkup(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelCode + '\')" class="button" title="Inventory"><span class="icon-read"></span></a></td>';

                        //trRequest += '<td class="align-center"><a target="_blank" class="button" title="Inventory" href="ShowInventory.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '&HAddress=' + arrHotellist[i].HotelAddress + '"><span class="icon-read"></span></a></td>';
                        //trRequest += '<td class="align-center"><a href="#" onclick="AddInventory(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelName + '\',\'' + arrHotellist[i].HotelCode + '\',\'' + arrHotellist[i].HotelAddress + '\')" class="button" title="Inventory"><span class="icon-read"></span></a></td>';

                        //Update//
                        trRequest += '<td class="align-center" style="vertical-align:middle;">';
                        trRequest += '<div class="button-group compact">';
                        trRequest += '<a href="hotel.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '" class="button icon-pencil with-tooltip" title="Edit Details "></a>';
                        trRequest += '<a href="#" class="button icon-gear with-tooltip" title="Markup" onclick="SetMarkup(\'' + arrHotellist[i].sid + '\')"></a>';
                        trRequest += '<a class="button icon-cart with-tooltip" title="Inventory" href="availability_pricing.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '"></a>';
                        // trRequest += '<a  class="button icon-cart with-tooltip" title="Inventory" href="InventoryDetails.aspx?sHotelID=' + arrHotellist[i].sid + '"></a>';
                        trRequest += '<a href="#" class="button icon-trash" onclick="DeleteHotelModel(\'' + arrHotellist[i].sid + '\',\'' + arrHotellist[i].HotelName + '\')" title="Delete"></a>';
                        trRequest += '</div></td>'
                        trRequest += '<br></tr>'

                        //

                        trRequest += '<br></tr>';
                    }
                    $("#tbl_HotelList tbody").append(trRequest);
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                }

                $("#tbl_HotelList").dataTable(
                    {

                        sSort: true, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList").dataTable(
                    {

                        bSort: false, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }

        }

    });
}

function ActivateHotel(HotelId, Status) {
    var hotel = HotelId;
    var s = status;
    var Data = { HotelId: HotelId, Status: Status };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelActivation",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var ActiveHotel = obj.ActiveHotel;
            if (obj.retCode == 1) {
                if (ActiveHotel == "True") {
                    Success("Hotel status has been changed successfully!");
                    GetHotelList()
                    //location.reload();
                }
                else {
                    Success("Hotel status has been changed successfully!");
                    GetHotelList()
                }

                //GetAllSeasons();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}
function DropRowwithId(ID) {
    var trDropleft = "";
    var trDropmiddle = "";
    var trDropRight = "";
    $.ajax({
        type: "POST",
        url: "../handler/RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRoomDetails = result.RoomList;
                var HotelAddress = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.HotelAddress; });
                var HotelCity = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.CityId; });
                var HotelCountry = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.CountryId; }); trDropRight = "";
                var HotelName = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.HotelName; }); trDropRight = "";
                trDropRight = "";
                //Left
                trDropleft += '<label class="font11 silver">' + HotelAddress + '<br>' + HotelCity + ',' + HotelCountry + '&nbsp;&nbsp;&nbsp;<a onclick="GetMap(\'' + ID + '\')" class="icon-marker icon-size2 font11 silver" title="Map"></a></label>';
                if (arrRoomDetails.length > 0) {
                    for (var i = 0; i < arrRoomDetails.length; i++) {
                        trDropRight += '<table class="room-table">';
                        trDropRight += '<tbody>'
                        trDropRight += '<tr class="silver">';
                        trDropRight += '<td style="width:175px;" class="with-small-padding">' + arrRoomDetails[i].RoomType + '</td>';
                        trDropRight += '<td style="width:50px;" class="with-small-padding"> ' + arrRoomDetails[i].RoomOccupancy + '</td>';
                        if (arrRoomDetails[i].Approved == true) {
                            trDropRight += '<td style="width:50px;" class="with-small-padding"> <input type="checkbox" class="switch mini" checked  onclick="ActivateRoom(this,\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'False\')"></td>';
                        }
                        else {
                            trDropRight += '<td style="width:50px;" class="with-small-padding"> <input type="checkbox" class="switch mini" onclick="ActivateRoom(this,\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'True\')"></td>';
                        }
                        trDropRight += '<td class="with-small-padding"><div class="font12"><a href="AddRooms.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '&RoomId=' + arrRoomDetails[i].RoomId + '" class="silver icon-pencil" title="Edit Room Details"></a>';
                        if (UserType != "Admin") {
                            // trDropRight += '<a onclick="AddInventory(\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'' + HotelName + '\',\'' + HotelAddress + '\')" class="silver icon-cart mid-margin-left" title="Add Inventory"></a>';
                            trDropRight += '<a class="silver pointer icon-cart mid-margin-left Inventory" onclick="SetInventory(\'' + ID + '\',\'' + HotelName + '-' + HotelAddress + ',' + HotelCity + ', ' + HotelCountry + '\',\'' + arrRoomDetails[i].RoomTypeID + '\',\'' + arrRoomDetails[i].RoomType + '\')" title="Add Inventory"></a>';
                            trDropRight += '<a href="#" class="silver icon-trash mid-margin-left" onclick="DeleteRoomModal(\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'' + arrRoomDetails[i].RoomType + '\')" title="Delete Room"></a>';
                            trDropRight += '<a href="roomrate.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '&RoomId=' + arrRoomDetails[i].RoomTypeID + '&RoomType=' + arrRoomDetails[i].RoomType + '" class="silver fa fa-money mid-margin-left" title="Add Rates"></a>';
                        }

                        trDropRight += '</div></td></tr></tbody></table>';
                    }
                    trDropRight += '<a href="AddRooms.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '" class="mid-margin-left font11 white pointer">Add room</a> ';
                    if (UserType != "Admin")
                    {
                      //  trDropRight += ' |<a href="Ratelist.aspx?sHotelID=' + ID + '&HName=' + HotelName + '" class="mid-margin-left font11 white pointer">View Rates</a>';
                        //trDropRight += ' |<a onclick="AddInventory(\'' + ID + '\',\'' +'0' + '\',\'' + HotelName + '\',\'' + HotelAddress + '\')" class="mid-margin-left font11 white pointer">Add Inventory</a>';
                    }
                   
                }
                else {
                    trDropRight += '<span>No Room Available </span><a target="_blank" href="AddRooms.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '" class="mid-margin-left font11 white">Add room</a>';
                }
                $('#DropLeft').append(trDropleft);
                $('#Dropmiddle').append(trDropmiddle);
                $('#DropRight').append(trDropRight);
                $('.Inventory').menuTooltip($('#Inventoryfilter').hide(), {
                    classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
                });
            }
            $(".mini").click(function () {
                $(this).find("input:checkbox").click();
            })
        }
    });
}

function SetInventory(HotelCode, HotelName, RoomID, RoomName) {
    try {
        $("#HotelCode").val(HotelCode);
        $("#HotelName").val(HotelName);
        $("#txt_RoomID").val(RoomID);
        $("#txt_RoomName").val(RoomName);
    } catch (e) { }
}

function AddInventory() {
    try {
        if ($("#frm_Inventory").validationEngine('validate')) {
            var sIntype = "";
            if ($("#sel_Inventory").val() == "Inventory_freesale.aspx")
                sIntype = "FR";
            else if ($("#sel_Inventory").val() == "Inventory_allocation.aspx")
                sIntype = "AL";
            else if ($("#sel_Inventory").val() == "Inventory_allotment.aspx")
                sIntype = "AT";
            window.location.href = $("#sel_Inventory").val() + "?sHotelID=" + $("#HotelCode").val() + "&HName=" + $("#HotelName").val() + "," + $("#HotelAddress").val() + "&RoomID=" + $("#txt_RoomID").val() + "&RName=" + $("#txt_RoomName").val() + "&sIntype=" + sIntype;
        }
    }
    catch (e) {
    }
}
//complex modal
function GetMappedHotels() {
    GetMappedHotels();
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Facilities:</label>' +
                                 '<select id="ddl_HtlFacilities"  class="full-width select multiple-as-single easy-multiple-selection allow-empty blue-gradient check-list" multiple>' +
                                       '<option  value="" selected="selected" disabled>Select Facilities</option>' +
                            '</select>' +
                  '</p>' +
                  '<p class="button-height align-right">' +
						'<button onclick="AddtoTextArea();"  class="button black-gradient glossy">Add</button>' +
				 '</p>' +
                  '<p class="button-height inline-label ">' +
                         '<label class="label">Present Facilities:</label>' +
                         '<textarea id="addedFacilities" class="input full-width align-right">' + arr + '</textarea>' +
                 '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveFacilities(' + HotelCode + ');"  class="button black-gradient glossy">Save</button>' +
                         '<label id="lblFacilitiesMsg" style="color:red;display:none">Facilities Added</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};


function AddRatings(HotelCode, Ratings) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Ratings + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedRatings" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveRatings(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblRatingsMsg" style="color:red;display:none">Saved Ratings</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};

function ShowRow(dropID) {
    $(".row-drop").css("display", "none");
    $('#DropRow' + dropID).css("display", "");
}

function UpdateImage(HotelCode, Url) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Url + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedUrl" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveImageUrl(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblImageMsg" style="color:red;display:none">Image Saved </button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};

function fnFacilities() {

    var HtlFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < HtlFacilities.length; i++) {
        if (HtlFacilities[i].selected == true) {
            selectedFacilities[i] = HtlFacilities[i].value;
            pFacilities += selectedFacilities[i] + ',';
        }


    }


}

function AddtoTextArea() {
    fnFacilities();
    var sFacilities = pFacilities.replace(/^,|,$/g, '');
    var nFacilities = arr + ',' + sFacilities
    $('#addedFacilities').val(nFacilities);
    // document.getElementById("ddl_HtlFacilities").disabled = true;
}

function getFacilities() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#ddl_HtlFacilities").empty();
                    var ddlRequest = '<option selected="selected" value="-" disabled>Select Facilities</option>';
                    var j = 0;
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        ddlRequest += '<option type="checkbox" class="HtlFacilities" value="' + arrFacilitiesList[i].HotelFacilityName + '">' + arrFacilitiesList[i].HotelFacilityName + '</option>';

                    }
                    $("#ddl_HtlFacilities").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });

}

function SaveFacilities(HotelCode) {
    var sFacilities = $('#addedFacilities').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sFacilities: sFacilities
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveFacilities",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblFacilitiesMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function SaveRatings(HotelCode) {
    var sRatings = $('#addedRatings').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sRatings: sRatings
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveRatings",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblRatingsMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function getRooms(drophotelId) {


    $.ajax({
        type: "POST",
        url: "../RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + drophotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;
                arrAmenities = result.RoomAmenityList;
                $("#tbl_RoomList tbody").empty();
                //Hotels List
                var trRequest = "";
                if (arrRoomList.length > 0) {
                    for (i = 0; i < arrRoomList.length; i++) {
                        trRequest += '<tr>';
                        for (j = 0; j < arrRoomTypes.length; j++) {


                        }


                    }

                }

            }
            else if (result.retCode == 0) {

            }

        }

    });
}

function SaveImageUrl(HotelCode) {
    var addedUrl = $('#addedUrl').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        addedUrl: addedUrl
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveImageUrl",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblImageMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function DeleteModel(sid, name) {

    $.modal({

        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to Delete <span class=\"orange\"> "' + name + '</span>?</p>' + '<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="DeleteHote(\'' + sid + '\')">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function DeleteHote(sid) {
    $.ajax({
        url: "../HotelHandler.asmx/DeleteHote",
        type: "post",
        data: '{"Id":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Hotel Deleted successfully!")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function SetMarkup(sid) {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetMarkup",
        data: '{"HotelCode":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                MrkCOunt = result.MrkCOunt;
                if (MrkCOunt.length != 0)
                {
                    $.modal({
                        content:
                            '<div class="columns">' +
                            '<div class="four-columns">' +
                            '<label>Markup Per</label><br>' +
                            '<input type="text" id="txt_Per" class="input" value="' + MrkCOunt[0].Per + '" style="width: 92%" autocomplete="off">' +
                            '</div>' +
                            '<div class="four-columns">' +
                            '<label>Markup Amnt</label><br>' +
                            '<input type="text" id="txt_Amt" class="input" value="' + MrkCOunt[0].Amt + '" style="width: 92%" autocomplete="off">' +
                            '</div>' +

                              '<br/><input id="btn_Save" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveHotelMarkup(\'' + sid + '\');" />',

                        title: 'Markup',
                        width: 400,
                        height: 200,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient glossy displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    }, function () {
                        $('#modals').remove();
                    });
                }
                else
                {
                    $.modal({
                        content:
                            '<div class="columns">' +
                            '<div class="four-columns">' +
                            '<label>Markup Per</label><br>' +
                            '<input type="text" id="txt_Per" class="input" value="" style="width: 92%" autocomplete="off">' +
                            '</div>' +
                            '<div class="four-columns">' +
                            '<label>Markup Amnt</label><br>' +
                            '<input type="text" id="txt_Amt" class="input" value="" style="width: 92%" autocomplete="off">' +
                            '</div>' +

                              '<br/><input id="btn_Save" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveHotelMarkup(\'' + sid + '\');" />',

                        title: 'Markup',
                        width: 400,
                        height: 200,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient glossy displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    }, function () {
                        $('#modals').remove();
                    });
                }


            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
        }
    });

}

function SaveHotelMarkup(HotelCode) {
    var Per = $("#txt_Per").val();
    var Amt = $("#txt_Amt").val();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SetMarkup",
        data: '{"HotelCode":"' + HotelCode + '","Per":"' + Per + '","Amt":"' + Amt + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Markup Added Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
        }
    });
}

function DeleteHotelModel(HotelId, Hotelname) {

    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Delete <span class=\"orange\"> "' + Hotelname + '"</span> Hotel?</p>',
        function () {
            DeleteHotel(HotelId);
        },
        function () {
            $('#modals').remove();
        }
    );

}


function DeleteHotel(HotelId) {
    $.ajax({
        url: "../handler/RoomHandler.asmx/DeleteHotel",
        type: "post",
        data: '{"HotelId":"' + HotelId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Hotel Deleted successfully!")
                setTimeout(function () {
                    window.location.reload();
                }, 1000);

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function DeleteRoomModal(ID, RoomId, Roomname) {

    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Delete <span class=\"orange\"> "' + Roomname + '"</span> Room?</p>',
        function () {
            DeleteRoom(RoomId, ID);
        },
        function () {
            $('#modals').remove();
        }
    );

}

function DeleteRoom(Roomid, ID) {
    $.ajax({
        url: "../handler/RoomHandler.asmx/DeleteRoom",
        type: "post",
        data: '{"RoomId":"' + Roomid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Room Deleted successfully!")
                window.location.href = "hotellist.aspx?sHotelID=" + ID + "";
                //setTimeout(function () {
                //    window.location.reload();
                //}, 1000);

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}


function ActivateRoom(val,ID, RoomId, Status) {

    var Data = { RoomId: RoomId, Status: Status };
    $.ajax({
        type: "POST",
        url: "../handler/RoomHandler.asmx/ActivateRoom",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
             
                Success("Room status has been changed successfully!");
                var Params = [];
                Params.push("this")
                Params.push(ID)
                Params.push(RoomId)
               
                if (Status == "False")
                {
                    $(val).prop("checked", false);
                    $(val).change();
                  //  Params.push(Status.replace("False", "True"))
                }
                else
                {
                    $(val).prop("checked", true);
                    $(val).change();
                 //   Params.push(Status.replace("True", "False"))
                }
              //  $(val).click(Params, ActivateRoom)
               
            }
            else {
                Success("Something went Wrong.");
                // GetHotelList()
            }
        },
    });
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}