﻿$(function () {
    GetGroup()
});
var arrGroup = new Array();
var ListHotels;
var Details;
var sid = 0;
function SaveHotelGroup() {

    var Name = $("#txt_GroupName").val();
    var Hotels = $("#ddl_HotelList").val();
    var Email = $("#txt_mail").val();
    var Phone = $("#txt_Phone").val();
    var Country = $("#selCountry").val();
    var City = $("#selCity").val();

    if (Validate()) {
        {
            $.ajax({
                type: "POST",
                url: "handler/HotelGroupHandler.asmx/SaveHotelGroup",
                data: '{"Name":"' + Name + '","Hotels":"' + Hotels + '","Email":"' + Email + '","Phone":"' + Phone + '","Country":"' + Country + '","City":"' + City + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        sid = 0;
                        Success("Saved Successfully");
                        $("#btn_reset").click()
                        GetGroup();
                    }
                    else if (result.retCode == 2) {
                        Success(result.ErrorMsg);
                        GetGroup();
                    }
                    else {
                        Success(result.ErrorMsg);
                        setTimeout(function () {
                            window.location.href = "default.aspx";
                        }, 2000);
                    }
                }
            });
        }
    }
}

function Validate() {
    var bValid = true;
    if ($("#txt_GroupName").val() == "") {
        Success("*Please insert Group Name");
        bValid = false;
    }
    if ($("#ddl_HotelList").val() == "") {
        Success("*Please Select Hotels");
        bValid = false;
    }
    if ($("#txt_mail").val() == "") {
        Success("*Please insert Email");
        bValid = false;
    }
    if ($("#txt_Phone").val() == "") {
        Success("*Please insert Phone no");
        bValid = false;
    }
    if ($("#selCountry").val() == "") {
        Success("*Please select Country");
        bValid = false;
    }
    if ($("#selCity").val() == "") {
        Success("*Please select City");
        bValid = false;
    }

    return bValid;
}
function GetGroup() {
    $("#tbl_HotelGroup").dataTable().fnClearTable();
    $("#tbl_HotelGroup").dataTable().fnDestroy();
    $.ajax({
        url: "handler/HotelGroupHandler.asmx/GetGroup",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrGroup = result.arrGroup
                for (var i = 0; i < arrGroup.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:8%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrGroup[i].ContactPerson + ' </td>'
                    html += '<td style="width:20%"  align="center"><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + arrGroup[i].sid + '\',\'' + arrGroup[i].uid + '\',\'' + arrGroup[i].ContactPerson + '\',\'' + arrGroup[i].password + '\'); return false" title="Click to edit Password">' + arrGroup[i].uid + '</a></td>'
                    html += '<td style="width:20%" align="center">'
                    html += '<span class="button-group">'
                    html += '<a href="#" class="button" title="Edit" onclick="UpdateHotelModal(\'' + arrGroup[i].sid + '\',\'' + arrGroup[i].ContactPerson + '\')"><span class="icon-pencil"></a> '
                    html += '</span>'
                    html += '</td></tr>'
                    $("#tbl_HotelGroup").append(html);
                }
                $('[data-toggle="tooltip"]').tooltip()
                $("#tbl_HotelGroup").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
                $('#tbl_HotelGroup').removeAttr("style");
            }
            else {
                Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}

function Update(ID, Name) {

    //var Group = $.grep(arrGroup, function (p) { return p.Group_id == ID })
    //                                        .map(function (p) { return p; })[0];
    $("#txt_GroupName").val(Name);
    $("#txt_Groupid").val(ID)
    $('#btn_AddHHotelGroup').attr("value", "Update");
}

function UpdateHotelGroup(Groupid) {

    var Name = $("#txt_GroupName").val();
    var Hotels = $("#ddl_HotelList").val();
   // var id = $("#txt_Groupid").val();
    if (Validate()) {    
      
           $.ajax({
               type: "POST",
               url: "handler/HotelGroupHandler.asmx/UpdateHotelGroup",
               data: '{"Name":"' + Name + '","Hotels":"' + Hotels + '","Groupid":"' + Groupid + '"}',
               contentType: "application/json; charset=utf-8",
               datatype: "json",
               success: function (response) {
                   var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                   if (result.retCode == 1) {
                       sid = 0;
                       Success("Saved Successfully");
                       $("#btn_reset").click()
                       GetGroup();
                   }
                   else if (result.retCode == 2) {
                       Success(result.ErrorMsg);
                       GetGroup();
                   }
                   else {
                       Success(result.ErrorMsg);
                       setTimeout(function () {
                           window.location.href = "default.aspx";
                       }, 2000);
                   }
               }
           });
    }
}

function Chek() {

    if ($("#btn_AddHHotelGroup").val() == "Save") {
        SaveHotelModal();
        //SaveHotelGroup()
    }
    else {
        UpdateHotelModal();
        // UpdateHotelGroup()
    }
}

function SaveHotelModal() {
    var Name = $("#txt_GroupName").val();
    if (Validate()) {
        $.ajax({
            type: "POST",
            url: "handler/HotelGroupHandler.asmx/GetHotels",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {

                var ddlRequest = '';
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    ListHotels = result.ListHotels;
            
                }
                $.modal({
                    content:
                        '<div class="columns">' +
                        '<div class="four-columns">' +
                        '<label>Group Name</label><br>' +
                        '<input type="text" id="txt_GroupName" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +
                        '<div class="four-columns">' +
                        '<label>mail</label><br>' +
                        '<input type="text" id="txt_mail" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +

                        '<div class="four-columns">' +
                        '<label>Phone</label><br>' +
                        '<input type="text" id="txt_Phone" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +
                        '</div> ' +
                         //'<div class="columns">'+
                         //'<div class="eight-columns full-width">'+
                         //'<label>Address</label><br />'+
                         //'<input type="text" id="txt_Address" class="input" style="width: 97.5%" autocomplete="off" />'+
                         //'</div>' +
                         //'<div class="four-columns full-width">' +
                         //'<label>Pin Code</label><br />' +
                         //'<input type="text" id="txtPinCode" class="input" style="width: 92%" autocomplete="off" />' +
                         //'</div>' +
                         //'</div>' +
                         '<div class="columns">' +
                         '<div class="four-columns" id="drp_Nationality">' +
                         '<label>Nationality</label>' +
                         '<p class="button-height">' +
                         '<select id="selCountry" class="select full-width replacement select-styled-list tracked OfferType">' +
                         '</select>' +
                         '</div>' +
                         '<div class="four-columns" id="drp_City">' +
                         '<label>City</label>' +
                         '<p class="button-height">' +
                         '<select id="selCity" class="select full-width replacement select-styled-list tracked OfferType">' +
                         '</select>' +
                         '</div>' +
                         '' +
                         '<div class="four-columns">                                                                                             ' +
                          '<label>Hotels</label>' +
                         '<select id="ddl_HotelList"  class="full-width select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality" multiple>' +
                         '</select>' +
                         '</div>                                                                                                                 ' +
                         '</div>' +
                          '<br/><input id="btn_Save" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveHotelGroup();" />',

                    title: 'Hotels',
                    width: 600,
                    height: 300,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient glossy displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                }, function () {
                    $('#modals').remove();
                });
                if (ListHotels.length > 0) {
                    $("#ddl_HotelList").empty();
                    for (i = 0; i < ListHotels.length; i++) {
                        ddlRequest += '<option value="' + ListHotels[i].sid + '">' + ListHotels[i].HotelName + '</option>';
                    }
                    $("#ddl_HotelList").append(ddlRequest);
                }
                GetCountry();
                $('#selCountry').change(function () {
                    var sndcountry = $('#selCountry').val();
                    GetCity(sndcountry);
                });
            },
            error: function () {
                Success("An error occured while loading Hotels")
            }
        });
    }
}

function UpdateHotelModal(sid, Name) {
    //var Name = $("#txt_GroupName").val();
    //var id = $("#txt_Groupid").val();
    if (Validate()) {
        $.ajax({
            type: "POST",
            url: "handler/HotelGroupHandler.asmx/GetDetails",
            data: '{"Name":"' + Name + '","sid":"' + sid + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {

                var ddlRequest = '';
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    ListHotels = result.ListHotels;
                    Details = result.Details;
                    if (ListHotels.length > 0) {
                        $('#modals').empty();
                       
                    }
                }
                $.modal({
                    content:
                        '<div class="columns">' +
                        '<div class="four-columns">' +
                        '<label>Group Name</label><br>' +
                        '<input type="text" id="txt_GroupName" value="' + Details[0].Group_Name + '" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +
                        '<div class="four-columns">' +
                        '<label>mail</label><br>' +
                        '<input type="text" id="txt_mail" value="' + Details[0].uid + '" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +

                        '<div class="four-columns">' +
                        '<label>Phone</label><br>' +
                        '<input type="text" id="txt_Phone" value="' + Details[0].Mobile + '" class="input" style="width: 92%" autocomplete="off">' +
                        '</div>' +
                        '</div> ' +
                         //'<div class="columns">'+
                         //'<div class="eight-columns full-width">'+
                         //'<label>Address</label><br />'+
                         //'<input type="text" id="txt_Address" class="input" style="width: 97.5%" autocomplete="off" />'+
                         //'</div>' +
                         //'<div class="four-columns full-width">' +
                         //'<label>Pin Code</label><br />' +
                         //'<input type="text" id="txtPinCode" class="input" style="width: 92%" autocomplete="off" />' +
                         //'</div>' +
                         //'</div>' +
                         //'<div class="columns">' +
                         //'<div class="four-columns" id="drp_Nationality">' +
                         //'<label>Nationality</label>' +
                         //'<p class="button-height">' +
                         //'<select id="selCountry" class="select full-width replacement select-styled-list tracked OfferType">' +
                         //'</select>' +
                         //'</div>' +
                         //'<div class="four-columns" id="drp_City">' +
                         //'<label>City</label>' +
                         //'<p class="button-height">' +
                         //'<select id="selCity" class="select full-width replacement select-styled-list tracked OfferType">' +
                         //'</select>' +
                         //'</div>' +
                         //'' +
                         '<div class="four-columns">                                                                                             ' +
                          '<label>Hotels</label>' +
                         '<select id="ddl_HotelList"  class="full-width select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality" multiple>' +
                         '</select>' +
                         '</div>                                                                                                                 ' +
                         '</div>' +
                          '<br/><input id="btn_Save" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="UpdateHotelGroup(' + Details[0].Group_id + ');" />',

                    title: 'Hotels',
                    width: 500,
                    height: 200,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient glossy full-width',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                }, function () {
                    $('#modals').remove();
                });
                $("#ddl_HotelList").empty();
                for (i = 0; i < ListHotels.length; i++) {
                    ddlRequest += '<option value="' + ListHotels[i].sid + '">' + ListHotels[i].HotelName + '</option>';
                }
                $("#ddl_HotelList").append(ddlRequest);
           //     GetCountry();
            //    GetCity(Details[0].sCountry);
            },
            error: function () {
                Success("An error occured while loading Hotels")
            }
        });

      
    }
   // GetUpdateHotels(id);
}

function GetUpdateHotels(id) {
    $.ajax({
        type: "POST",
        url: "handler/HotelGroupHandler.asmx/GetUpdateHotels",
        data: '{"id":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                GetSelectHotels(result.ListHotels);
            }
            else if (result.retCode == 2) {
                Success(result.ErrorMsg);

            }
            else {
                Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx";
                }, 2000);
            }
        }
    })
}

function GetSelectHotels(List) {

    try {
        var NameNew = [];

        for (var i = 0; i < List.length; i++) {
            var Code = List[i].Hotel_Code;

            var checkclass = document.getElementsByClassName('check');

            var Name = $.grep(ListHotels, function (p) { return p.sid == Code; })
                                            .map(function (p) { return p; });
            NameNew.push(Name);

        }
      
        for (var j = 0; j < NameNew.length; j++) {
            $("#Hotels .select span")[0].textContent = NameNew[j][0].HotelName;
            $('input[value="' + NameNew[j][0].sid + '"][class="chkNationality"]').prop("selected", true);
            $("#ddl_HotelList").val(NameNew[j][0].sid);
        }
     //   $("#ddl_HotelList").multipleSelect("setSelects", NameNew);
    }
    catch (ex)
    { }

}

function PasswordModal(sid, uid, StaffName, password) {
    HiddenId = sid;
    GetPassword(sid, uid, StaffName, password);

};

function GetPassword(sid, uid, StaffName, password) {
    $("#txt_Password").val('');
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/GetPassword",
        data: '{"password":"' + password + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                pass = result.password;
                $.modal({
                    content: '<div class="modal-body">' +
            '<div class="scrollingDiv">' +
            '<div class="columns">' +
            '<div class="three-columns bold">User ID:</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="Agency-value" value="' + uid + '" class="input-unstyled full-width" type="text">' + '</div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Group Name</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="' + StaffName + '" class="input-unstyled full-width" type="text"></div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Password</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + pass + '" class="input-unstyled full-width" type="text"></div></div> ' +
            '</div>' +
            '<div class="columns">' +
            '<div class="three-columns bold">&nbsp;</div>' +
            //'<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="openEmail()">Email</button>' +
            '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePassword()">Change&nbsp;Password</button></div>' +
            '</div></div></div>',

                    title: 'Edit Password',
                    width: 500,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
        error: function () {
            Success("An error occured while loading password.")
        }
    });
}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $('#lbl_ErrPassword').css("display", "none");
            $.modal.confirm('Are you sure you want to change password?', function () {
                $.ajax({
                    type: "POST",
                    url: "HotelAdmin/Handler/GenralHandler.asmx/StaffChangePassword",
                    data: '{"sid":"' + HiddenId + '","password":"' + $('#txt_Password').val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.Session != 1) {
                            Success("Session Expired");
                        }
                        if (result.retCode == 1) {
                            Success("Password changed successfully");
                            setTimeout(function () {
                                window.location.reload();
                            }, 500)
                        }
                        if (result.retCode == 0) {
                            Success("Something went wrong while processing your request! Please try again.");
                        }
                    },
                    error: function () {
                    }
                });
            }, function () {
                $('#modals').remove();
            });
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.Success('No Change found in Password!', {
            buttons: {
                'Cancel': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            }
        });
    }

}