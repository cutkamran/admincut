﻿var HotelCode, HotelName, RoomName, RoomCode;
$(function () {
    var sIntype = GetQueryStringParams('sIntype');
    $("#frm_Inventory").validationEngine()
    HotelCode = GetQueryStringParams('sHotelID');
    HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
    RoomCode = GetQueryStringParams('RoomID');
    RoomName = GetQueryStringParams('RName').replace(/%20/g, ' ');
    GetTypeByRate(sIntype, HotelCode, RoomCode);
    $("#spn_Hotel").text(HotelName);
    $("#spn_Hotel").append('<br /><span class="red" id="spn_Room">' + RoomName + '</span>');
    GenrateDates('Inventory');
});

function SelectDays(Days) {
    try {
        if ($(Days).is(":checked")) {
            $('.divDays').hide(500);
        }
        else
            $('.divDays').show(500);

    } catch (e) { }
}

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function GenrateDates(Type) {
    debugger
    var html = "";
    try {
        var elem = $(".dte" + Type);
        html += '<div class="columns dte' + Type + '">'
        html += '<div class="four-columns ten-columns-mobile five-columns-tablet">'
        html += '<span class="input">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + Type + 'From" value="">'
        html += '</span>'
        html += '</div>'
        html += '<div class="five-columns twelve-columns-mobile seven-columns-tablet">'
        html += '<span class="input">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + Type + 'To" value="">'
        html += '</span>';
        if (elem.length == 0) {

            html += '<span class="mid-margin-left icon-size2 icon-plus-round pointer icon-black" onclick="GenrateDates(\'' + Type + '\')" id="btn_Add' + Type + 'Date"></span>'
        }
        else
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red pointer remCF" ></span>';
        html += '</div>'
        html += '</div>'
        $("#div_" + Type + 'Date').append(html);
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });
        var elem_FromDate = $("#div_" + Type + "Date").find("." + Type + "From");
        var elem_ToDate = $("#div_" + Type + "Date").find("." + Type + "To");
        for (var i = 0; i < elem_FromDate.length; i++) {
            /*Previous Date Select Date*/
            var previousFrom = 0;
            var previousTo = 0;
            if (i != 0)
                previousTo = moment($(elem_ToDate[i - 1]).val(), "DD-MM-YYYY");
            var minDate = 0;
            if (previousTo != 0)
                minDate = previousTo._i;
            $(elem_FromDate[i]).datepicker($.extend({
                minDate: minDate,
                onSelect: function () {
                    var minDate = $(this).datepicker('getDate');
                    minDate.setDate(minDate.getDate() + 1); //add One days
                    $(elem_ToDate[i - 1]).datepicker("option", "minDate", minDate);
                }, beforeShow: function () {
                    //var minDate = moment($($('#div_' + Type + 'Date').find("." + Type + "From")[i - 1]).val(), "DD-MM-YYYY")._i;
                    //var maxDate = moment($($('#div_' + Type + 'Date').find("." + Type + "To")[i - 1]).val(), "DD-MM-YYYY")._i;
                    //$(this).datepicker("option", "minDate", minDate);
                    //$(this).datepicker("option", "maxDate", maxDate);
                },
            }, datepickersOpt));

            $(elem_ToDate[i]).datepicker($.extend({
                onSelect: function () {
                    var maxDate = $(this).datepicker('getDate');
                    maxDate.setDate(maxDate.getDate());
                    UpdateFlag = false;
                }, beforeShow: function () {
                    //var minDate = moment($($("#div_" + Type + "Date").find("." + Type + "From")[i - 1]).val(), "DD-MM-YYYY")._i;
                    //var maxDate = moment($($("#div_" + Type + "Date").find("." + Type + "To")[i - 1]).val(), "DD-MM-YYYY")._i;
                    //$(this).datepicker("option", "minDate", minDate);
                    //$(this).datepicker("option", "maxDate", maxDate);
                },
            }, datepickersOpt));
        }
    } catch (e) { }
}


function Showconditions() {
    try {
        if ($("#conditions").is(":checked")) {
            $('.conditions').show(500);
        }
        else {
            $('.conditions').hide(500);
        }

    } catch (e) { }
}
var InvLiveOrReq = "Live";
function SaveInventory(InType) {
    if ($("#frm_Inventory").validationEngine('validate')) {
        var Room = [];
        Room.push(RoomCode)
        var RateType = [];
        RateType.push($("#sel_RateType").val());
        var MaxRoom = "";
        var DtTill = "";
        var OptRoomperDate = "";
        var InventoryState = "";
        if ($("#open").is(":checked") && (InType == "FreeSale"))
            InventoryState = "fs";
        else if (InType == "FreeSale")
            InventoryState = "ss";
        else
            InventoryState = "Allotment";
        if ($("#conditions").is(":checked")) {
            MaxRoom = $('#txt_MaxRoomperbook').val();
            if (MaxRoom == undefined)
                MaxRoom = "";
            DtTill = $('#txt_Till').val();
            if (DtTill == undefined)
                DtTill = "";
            OptRoomperDate = $('#txt_MaxRoomperDate').val();
            if (OptRoomperDate == undefined)
                OptRoomperDate = "";

        }
        if ($("#chk_Request").is(":checked"))
            InvLiveOrReq = "OnRequest";
        if (InType == "Allocation" || InType == "Allotment") {
            MaxRoom = $('#txt_TotalRooms').val();
            if ($("#conditions").is(":checked"))
                InvLiveOrReq = "OnRequest";
        }

        var DateInvFr = [];
        var DateInvTo = [];
        var ndDates = $(".dteInventory");

        $(ndDates).each(function (index, ndDate) {
            var ndFromDates = $(ndDate).find(".InventoryFrom")[0];
            var ndToDates = $(ndDate).find(".InventoryTo")[0];
            DateInvFr.push($(ndFromDates).val());
            DateInvTo.push($(ndToDates).val());
        });;
        var arrDays = [];
        if ($("#chk_Days").is(":checked")) {
            $('.divDays input[type=checkbox]').each(function () {
                if ($(this).is(":checked")) {
                    arrDays.push($(this).val());
                }
            });
        }
        var data =
             {
                 HotelCode: HotelCode,
                 Room: Room,
                 MaxRoom: MaxRoom,
                 DtTill: DtTill,
                 DateInvFr: DateInvFr,
                 DateInvTo: DateInvTo,
                 pRateType: RateType,
                 OptRoomperDate: OptRoomperDate,
                 InvLiveOrReq: InvLiveOrReq,
                 InType: InType,
                 InventoryState: InventoryState,
                 arrDays: new Array()
             }
        post("../InventoryHandler.asmx/SaveInventory", data, function (data) {
            if (data.retCode == 1) {
                Success("Inventory Added Sucessfully");
                if (GetQueryStringParams('Modal') == undefined) {
                    setTimeout(function () {
                        window.location.href = "Hotellist.aspx";
                    }, 2000);
                }
                else {
                    window.parent.closeIframe();
                }

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }, function (error) {

        })
    };
}

function CancelInventory() {
    debugger
    if (GetQueryStringParams('Modal') == undefined) {
        setTimeout(function () {
            window.location.href = "Hotellist.aspx";
        }, 2000);
    }
    else {
        window.parent.closeIframe();
    }
}