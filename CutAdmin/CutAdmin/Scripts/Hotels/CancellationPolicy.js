﻿var data = new Array();
var HotelAdminID = "";
$(function () {
    debugger;
    $("#frm_Inventory").validationEngine();
    data = window.parent.arrCancelDetails;
    HotelAdminID = data.HotelAdminID;
    var arrCancel = data.arrcancel;
    if (arrCancel.length != 0 && arrCancel[0].Type != "non-Refundable")
        $("#chk_Cancel").click();
    $(arrCancel).each(function (c, Policy) {
        if (Policy.RefundType == "Refundable") {
            if (Policy.Type == "Percentile") {
                $("#sel_ChargePer").val(Policy.Percentage);
                $("#sel_DayPer").val(Policy.DayTill);
                $("#percentage").click();
                $("#sel_ChargePer").change();
                $("#sel_DayPer").change();
            }
            else if (Policy.Type == "Amount") {
                $("#cancellationamount").val(Policy.Amount);
                $("#sel_DayAmt").val(Policy.DayTill);
                $("#amount").click();
                $("#sel_DayAmt").change();
            }
            else if (Policy.Type == "Night") {
                $("#validation-number").val(Policy.noNights);
                $("#sel_DayAmt").val(Policy.DayTill);
                $("#nightly").click();
                $("#sel_DayAmt").change();
            }
        }
        else if (Policy.RefundType == "NoShow") {
            var RefundPolicy = $.grep(arrCancel, function (p) { return p.RefundType == "Refundable"; })
                     .map(function (p) { return p; })[0];

            if (RefundPolicy.Type == "Percentile") {
                if (RefundPolicy.Percentage == Policy.Percentage)
                    $("#sel_NoShowPer").val("Same");
                else
                    $("#sel_NoShowPer").val("100%");
                $("#sel_NoShowPer").change();
            }
            else if (RefundPolicy.Type == "Amount") {
                if (RefundPolicy.Amount == Policy.Amount)
                    $("#sel_NoShowAmt").val("Same");
                else
                    $("#sel_NoShowAmt").val("100%");
                $("#sel_NoShowAmt").change();
            }
            else if (RefundPolicy.Type == "Night") {
                if (RefundPolicy.noNights == Policy.noNights)
                    $("#sel_NoShowNight").val("Same");
                else
                    $("#sel_NoShowNight").val("100%");
                $("#sel_NoShowNight").change();
            }
        }
        else {

        }
    });
});
function ShowCancel(elem) {
    try {
        if ($(elem).is(":checked")) {
            $('#cnlpolicy').slideToggle();
            $("#cnlpolicy").css("display", "")
            $("#percentage").prop("checked", true);
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }
        else {
            $("#cnlpolicy").css("display", "none")
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }

    } catch (e) { }
}/*Cancel Show*/
function GetChargeCond(Input) {
    try {
        if (Input == "percentageinput") {
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }
        else if (Input == "amountinput") {
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "")
            $("#nightlyinput").css("display", "none")
        }
        else if (Input == "nightlyinput") {
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "")
        }
    } catch (e) {
    }
}/* Charge Condition*/

function SaveCancellation() {
    try {
        if ($("#frm_Inventory").validationEngine('validate')) {
            var arrData = GetCancellationPolicy();
            var Data = {
                arrCancellationPolicy: arrData,
                TarrifID: data.TarrifID,
                sFrom: data.fromdate,
            }
            post("../handler/Roomratehandler.asmx/SaveCancellation", Data, function (result) {
                Success("Cancellation Changed.")
                setTimeout(function () {
                    window.parent.closeCancelIframe();
                }, 500)
            }, function (error) {
                AlertDanger(error.ex)
            })
        }
    } catch (e) {
        AlertDanger("Server no reponding.")
    }
}

function GetCancellationPolicy() {
    var arrCancellation = new Array();
    try {
        var CancelationPolicy = "REF/FD";
        var NoShowPolicy = "No Show/";
        if ($("#chk_Cancel").is(":checked")) {
            if ($("#percentage").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayPer").val().replace("days", "") + ")/(" + $("#sel_ChargePer").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });
                if ($("#sel_NoShowPer").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowPer").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: 0, PercentageToCharge: $("#sel_NoShowPer").val() });/*No Show*/
                }
                else {
                    NoShowPolicy += $("#sel_ChargePer").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });

                }
            }
            else if ($("#nightly").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DaysNight").val().replace("days", "") + ")/N(" + $("#validation-number").val() + ")";
                arrCancellation.push({ RefundType: "Refundable", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), NightsToCharge: $("#validation-number").val(), ChargesType: "Nights", SupplierID: HotelAdminID });
                if ($("#sel_NoShowNight").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowNight").val();
                    arrCancellation.push({ RefundType: "NoShow", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowNight").val() });;/*No Show*/
                }
                else {
                    NoShowPolicy += $("#validation-number").val();
                    arrCancellation.push({ RefundType: "NoShow", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), NightsToCharge: $("#validation-number").val(), ChargesType: "Nights", SupplierID: HotelAdminID });

                }
            }
            else if ($("#amount").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayAmt").val().replace("days", "") + ")/(" + $("#cancellationamount").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                if ($("#sel_NoShowAmt").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowAmt").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowAmt").val() });;/*No Show*/
                }
                else {
                    NoShowPolicy += $("#cancellationamount").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                }
            }
        }
        return arrCancellation;
    } catch (e) { return new Array() }
}