﻿var arrFrmData = new Array();
$(function () {
    debugger
    arrFrmData = window.parent.arrRateDetails;
    console.log(arrFrmData)
    $("#dt_Start").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        onSelect: insertDepartureDate,
        //dateFormat: "yy-m-d",
        minDate: "dateToday",
        maxDate: "+12M +10D"
    });
    $("#dt_Start").datepicker("setDate", new Date());
    var dateObject = $("#dt_Start").datepicker("getDate", '+1d');
    dateObject.setDate(dateObject.getDate() + 1);
    $("#dt_End").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        beforeShow: insertDepartureDate,
        minDate: "+1D",
        maxDate: "+12M +20D"
    });
    $("#dt_End").datepicker("setDate", dateObject);
   
    $($("#div_GNDate").find(".GNFrom")).val(arrFrmData.Startdt);
    $($("#div_GNDate").find(".GNTo")).val(arrFrmData.Enddt);
    $(".supp").text(arrFrmData.SupplierName);
    $(".RateType").text(arrFrmData.RateType);
    $(".Currency").text(arrFrmData.sCurrency);
    if(arrFrmData.PaxType =="RR")
        $(".sRateType").text("Adult Rate");
    else if(arrFrmData.PaxType =="EB")
        $(".sRateType").text("Extra Bed");
    else if(arrFrmData.PaxType =="CW")
        $(".sRateType").text("Child With Bed");
    else if(arrFrmData.PaxType =="CN")
        $(".sRateType").text("Child No Bed");
})
var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function insertDepartureDate(value, date, inst) {
    var firstDate = new Date($('#dt_Start').datepicker("getDate"));
    var dateAdjust = 1;
    var current_date = new Date();
    current_time = current_date.getTime();
    days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
    if (days < 0) {
        var add_day = 1;
    } else {
        var add_day = 2;
    }
    days = parseInt(days);
    $('#dt_Start').datepicker("option", "minDate", days + add_day);
    $('#dt_End').datepicker("option", "maxDate", days + 365);
    dateAdjust = parseInt(dateAdjust);
    var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
    $('#dt_End').datepicker('setDate', secondDate);
}
function SaveRate() {
    try {
        var arrRatePlan = new Array();
        if ($("#frm_Rates").validationEngine('validate')) {
            post("../handler/Roomratehandler.asmx/UpdatePaxRate",
                {
                    PaxType: arrFrmData.PaxType,
                    Rate: $("#Rate").val(),
                    arrPlanID: arrFrmData.arrPlanID,
                    SellType: arrFrmData.SellType,
                    sFrom: $("#dt_Start").val(),
                    sTo: $("#dt_End").val(),
                    MealPlanID: arrFrmData.MealPlan,
                },
                function (result) {
                    Success("Rates Updated Successfully.");
                    setTimeout(function() {
                        window.parent.closeIframe();
                    },500)
                   
                }, function (error) {
                    AlertDanger(error.ex)
                });
        }
    } catch (ex) { }
}