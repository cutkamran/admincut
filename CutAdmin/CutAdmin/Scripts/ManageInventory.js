﻿var HotelCode, HotelName;
function AddInventory(HotelID, RoomID, Hotel, Address) {
     HotelCode = HotelID;
     HotelName = Hotel;


    $.modal({
        content:
            '<div class="modal-body">' +
            '<div class="columns">' +
            '<div class="four-columns twelve-columns-mobile">' +
            '<input type="radio" class="radio" name="chk_Inv" id="chk_freesale" checked="checked" value="1" class="" onchange="ShowSwitch()">' +
            '<label for="chk_freesale" class="label">Free Sale</label>' +
            '</div>' +
            '<div class="four-columns twelve-columns-mobile">' +
            '<input type="radio" class="radio" name="chk_Inv" id="chk_allocation" value="1" class="" onchange="ShowAllocation()">' +
            '<label for="chk_allocation" class="label">Allocation</label>' +
            '</div>' +
            '<div class="four-columns twelve-columns-mobile">' +
            '<input type="radio" class="radio" name="chk_Inv" id="chk_allotment" value="1" class="" onchange="ShowAllotment()">' +
            '<label for="chk_allotment" class="label">Allotment</label>' +
            '</div>' +
           
            '</div>' +

            '<div class="six-columns twelve-columns-mobile" id="Switch">' +
            '<span class="button-group">' +
            '<label for="chk_Start" class="button grey-active">' +
            '<input type="radio" name="button-radio" id="chk_Start" value="Start" onclick="Redirect(this.value)">' +
            'Start Sale' +
            '</label>' +
            '</label>' +
            '</span>' +
            '</div>' +
            '</div>',
        title: Hotel + ',<small>' + Address+ '</small>',
        width: 400,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });

}

function Redirect(value) {
    if (value == "Start") {
        window.location.href = "StartSale.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
    }
    else if (value == "Stop") {
        window.location.href = "StopSale.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
    }
}
function ShowSwitch() {
    if ($("#chk_freesale").is(':checked')) {
        $("#Switch").show();
    }
    else {
        $("#Switch").hide();
    }
}

function Allocation() {
    window.location.href = "Allocation.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
}

function Allotment() {
    window.location.href = "Allotment.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
}


function ShowAllocation() {
    Allocation();
    //$.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Add Allocation?</p>',
    //    function () {
    //        Allocation();
    //    },
    //    function () {
    //        $('#modals').remove();
    //    }
    //);
}

function ShowAllotment() {
    Allotment();
    //$.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Add Allotment?</p>',
    //    function () {
    //        Allotment();
    //    },
    //    function () {
    //        $('#modals').remove();
    //    }
    //);
}