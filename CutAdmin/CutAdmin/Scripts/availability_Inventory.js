﻿var arrModelData = new Array();
var Room = [];

$(function () {
    setTimeout(function () {
        GetHotels();
    }, 1000)
})

function GetHotels() {
    try {
        post("../handler/GenralHandler.asmx/GetHotels", { AdminID: HotelAdminID }, function (data) {
            $('#sel_Hotel').empty();
            arrHotels = data.arrHotels;
            $(data.arrHotels).each(function (index, Hotel) { // GETTING Sucees HERE
                $('#sel_Hotel').append($('<option></option>').val(Hotel.HotelID).html(Hotel.Name + " , " + Hotel.Address));
            });
            if (location.href.indexOf('?') != -1) {
                HotelCode = GetQueryStringParams('sHotelID');
                RateType = GetQueryStringParams('RateType');
                if (RateType != undefined)
                {
                    GetInventoryRateType(RateType);
                }
              
                $("#sel_Hotel").val(HotelCode);
                $('#sel_Hotel').change();
            }
            else {
                HotelCode = $("#sel_Hotel").val();
                $('#sel_Hotel').change();
            }
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    } catch (e) {
        AlertDanger('Something we wrong while getting Hotels');
    }
}
var InventoryState = "";
function SetInventory(RoomID, ndRateType, elem, day) {
    try {
        var RateType = $('#sel_RateType').val();
        if (RateType == "")
            throw new DOMException('Sorry Rates is Not Available,Please Add Rates first');
        var InType = "";
        var html = "";
        /*Check Inventory Type*/
        if ($("#Freesale_Id").is(":checked")) {
            InType = "FreeSale";
        }
        else if ($("#Allocation_Id").is(":checked")) {
            InType = "Allocation";
        }
        else if ($("#Allotmnet_Id").is(":checked"))
            InType = "Allotment";
        /*Check Inventory Open or Close*/

      
        if ($(elem).hasClass('room-available') && (InType == "FreeSale" || InType == "Allocation"))
            InventoryState = "ss";
        else if ($(elem).hasClass('room-not-available') && (InType == "FreeSale" || InType == "Allocation"))
            InventoryState = "fs";
        else
            InventoryState = "Allotment";

        if (InType == "FreeSale")
            html = "<b>Are you sure want to " + InventoryState.replace("fs", "Open").replace("ss", "Close") + " Inventory for " + arrDates[day].LongDate.toString() + "</b>";
        else {
            var arrCount = $(elem).text().split('/')[1];
            if (arrCount == 0)
                arrCount = 1;
            if (InType == "Allocation") {
                html += '<span class="">'
                html += '<input type="radio" class="" name="startstop" id="startradio" value="Start" checked>'
                html += '<label for="startradio" class="label">Open Inventory</label>'
                html += '<input type="radio" class="" name="startstop" id="stopradio" value="Stop">'
                html += '<label for="stopradio" class="label mid-margin-right">Close Inventory</label>'
                html += '</span>'
            }
            html += '<div class="field-block ">' +
                 ' ' +
                 ' <span class="number input small-margin-right">' +
                 '     <button type="button" class="button number-down">-</button>' +
                 '     <input type="text" value=' + arrCount + ' size="3" class="input-unstyled validate[required,min[1],max[30]]" id="txt_TotalRooms">' +
                 '<button type="button" class="button number-up">+</button>' +
                 ' </span> <small class="input-info">Input total no of rooms you want to open for booking</small>' +
              '</div>'
        }
        var MaxRoom = ""
     
        ConfirmModal(html, function () {
         //   $('#txt_TotalRooms').val(arrCount);
            if (InType == "Allocation" || InType == "Allotment")
                MaxRoom = $('#txt_TotalRooms').val();
            if (InType == "Allocation") {
                if ($("#startradio").is(":checked"))
                    InventoryState = "fs";
                else
                    InventoryState = "ss";
            }
            var DateInvFr = []
            DateInvFr.push(arrDates[day].LongDate.toString());
            var DateInvTo = [];
            DateInvTo.push(arrDates[day].LongDate.toString());
            var Status = '';

            var arrRateType = [], Room = [];
            arrRateType.push(RateType);
            Room.push(RoomID);
            var arrDays = [];
            var data =
                 {
                     HotelCode: HotelCode,
                     Room: Room,
                     MaxRoom: MaxRoom,
                     DtTill: "",
                     DateInvFr: DateInvFr,
                     DateInvTo: DateInvTo,
                     pRateType: arrRateType,
                     OptRoomperDate: "",
                     InvLiveOrReq: 'Live',
                     InType: InType,
                     InventoryState: InventoryState,
                     arrDays: arrDays
                 }
            $(elem).addClass("loading");
            post("../InventoryHandler.asmx/SaveInventory", data, function (data) {
                Success("Inventory Updated Sucessfully");
                $(elem).removeClass("loading");
                if (InventoryState == "ss" && InType != "FreeSale") {
                    $(elem).removeClass("room-available");
                    $(elem).addClass("room-not-available");
                }
                else if (InventoryState == "fs" && InType != "FreeSale") {
                   $(elem).addClass("room-available");
                   $(elem).removeClass("room-not-available");
                }
                else if ($(elem).hasClass("room-available") && InType == "FreeSale") {
                    $(elem).addClass("room-not-available");
                    $(elem).removeClass("room-available");
               }
                else if ($(elem).hasClass("room-not-available") && InType == "FreeSale") {
                   $(elem).removeClass("room-not-available");
                   $(elem).addClass("room-available");
                }
                if ($(elem).text() != "0") {
                    var arrCount = $(elem).text().split('/');
                    if (MaxRoom != "")
                        $(elem).text(arrCount[0] + "/" + MaxRoom);
                    else
                        $(elem).text(arrCount[0]);
                }


            }, function (error) {
                AlertDanger("Something Went Wrong")
            });
        })

    } catch (e) { AlertDanger(e.message) }
} /*Individual Date Inventory Update*/


function SaveAvailability(RoomID, sDate) {
    var HotelName = $("#sel_Hotel option:selected").text();
    var RoomName = $.grep(arrRooms, function (p) { return p.RoomID == RoomID; })
                 .map(function (p) { return p.RoomName; })[0];
    var sPage = "";
    var sInvtype = '';
    if (Freesale_Id.checked) {
        sPage = "freesale";
        sInvtype = "FR"
    }
    if (Allocation_Id.checked) {
        sPage = "Allocation";
        sInvtype = "AL"
    }

    if (Allotmnet_Id.checked) {
        sPage = "Allotment";
        sInvtype = "AT"
    }
    //$.modal({
    //    title: ,
    //    url: ,
    //    useIframe: true,
    //    resizable: false,
    //    scrolling: true,
    //    width: 800,
    //    height:600,
    //    spacing: 5,
    //    classes: ['black-gradient'],
    //    animateMove: 1,
    //    buttons: {

    //        'Close': {
    //            classes: 'black-gradient small hidden',
    //            click: function (modal) { modal.closeModal(); }
    //        },
    //    }
    //});

    OpenIframe(sPage + "-   " + HotelName + ':<label class="green underline right" style="margin-left: 1%;">' + RoomName + '</label><br/>',
        '../Inventory_' + sPage + '.html?sHotelID=' + HotelCode + '&HName=' + HotelName + ',&RoomID=' + RoomID + '&RName=' + RoomName + '&sIntype=' + sInvtype + "&Modal=yes")
}
function closeIframe() {
    setTimeout(function () {
        $('#modals').remove();
    }, 2000); 
    return false;
}

function GetInventoryType() {
    var sInvtype = "";
    try {
        if (Freesale_Id.checked) {
            sInvtype = "FR"
        }
        else if (Allocation_Id.checked) {
            sInvtype = "AL"
        }
        else if (Allotmnet_Id.checked) {
            sInvtype = "AT"
        }
    } catch (e) { }
    return sInvtype;
}

function GetInventoryRateType(RateType)
{
    try {
        var  Supplier = $("#Sel_Supplier").val();
        post("../handler/GenralHandler.asmx/GetInventoryRateType", { AdminID:Supplier, RateType: RateType }, function (data) {
            arrRateType = data.arrRateTypes;
            $(data.arrRateTypes).each(function (index, Hotel) { // GETTING Sucees HERE
                InvType = Hotel.sRateType.split(",");
                for (var i = 0; i < InvType.length - 1; i++) {
                    if(InvType[i] == "FR")
                    {
                        $("#Freesale_Id").prop("checked", true);
                        $("#Freesale_Id").change();
                        break;
                    }
                    else if (InvType[i] == "AL")
                    {
                        $("#Allocation_Id").prop("checked", true);
                        $("#Allocation_Id").change();
                        break;
                    }
                    else if (InvType[i] == "AT")
                    {
                        $("#Allotmnet_Id").prop("checked", true);
                        $("#Allotmnet_Id").change();
                        break;
                    }
                }
            });
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    } catch (e) {
        AlertDanger('Something we wrong while getting Hotels');
    }
}
