﻿$(function() {
    GetTaxMapping();
})
var arrTax = new Array();
var arrTaxRate = new Array();
function GetTaxMapping() {
    $.ajax({
        type: "POST",
        url: "../handler/TaxHandler.asmx/GetSupplierTax",
        data:'{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrTax = result.arrTax;
                arrTaxRate = result.arrTaxRate;
                if (arrTaxRate.length == 0)
                    AddTax(0, "div_TaxDetails");
                else
                    GenrateTax();
            }
        }
    })
}

function GenrateTax() {
    AddTax(0, "div_TaxDetails");
    var RatesType = [];
    for (var i = 0; i < arrTaxRate.length; i++) {
        RatesType.push(arrTaxRate[i].TaxID);
    }
    var unique = RatesType.filter(function (itm, i, RatesType) {
        return i == RatesType.indexOf(itm);
    });
    for (var j = 0; j < unique.length; j++) {
        var Rates = $.grep(arrTaxRate, function (H) { return H.TaxID == unique[j] })
           .map(function (H) { return H.TaxOn; });
        var RatesVal = $.grep(arrTaxRate, function (H) { return H.TaxID == unique[j] })
         .map(function (H) { return H.Value; });
        var arrRate = [];
        arrRate.push(unique[j])
        var ndDiv = $("#div_" + j);
        var ndSelTax = $(ndDiv).find('.sel_TaxType')[0];
        var ndSelOn = $(ndDiv).find('.sel_TaxOn')[0];
        var ndTaxVal = $(ndDiv).find('.TaxValue')[0];
        $(ndSelOn).multipleSelect("setSelects", Rates);
        $(ndTaxVal).val(RatesVal[0]);
    }
} /*Genrate Tax*/

function AddTax(ID, el) {
    try {
        if (arrTax.length == 0)
            AlertDanger("No Tax Found Kindly Add Tax before tax configuration.");
        for (var ID = 0; ID < arrTax.length; ID++) {
            var html = '';
            html += '<div class="columns TaxDetails" id="div_'+ ID+'">'
            html += '<div class="four-columns three-columns-mobile SelBox">'
            if (ID == 0)
                html += '<label class="input-info">Tax Type&nbsp;&nbsp;&nbsp;&nbsp;</label>';
            html += '<input type="hidden" class="sel_TaxType TaxId' + ID + '" value="'+arrTax[ID].ID+'"/>'
            html += '<input  name="validation-select" class="input full-width" value="' + arrTax[ID].Name + '"  redonly>'
            html += '</select>'
            html += '</div>'

            html += '<div class="two-columns  three-columns-mobile ">'
            if (ID == 0)
                html += '<label class="input-info">Value(%)&nbsp;&nbsp;&nbsp;&nbsp;</label>';
            html += '<input type="number" min="1" class="input full-width TaxValue"  value="' + arrTax[ID].Value + '"/>'
            html += '</div>'

            html += ' <div class="four-columns three-columns-mobile SelBox" id="TaxOn' + ID + '">'
            if (ID == 0)
                html += '<label class="input-info">Tax On</label>';
            html += '<select  name="validation-select" multiple="multiple" class="  full-width sel_TaxOn">'
            html += ' </select>'
            html += '</div>'
            html += '</div>'
            $("#" + el).append(html);
            var ndDiv = $("#div_" + ID);
            var ndSel = $(ndDiv).find('.sel_TaxOn')[0];
            GenrateTaxOn(ID, arrTax[ID].ID)
            $(ndSel).change(function () {
                console.log($(this).val());
            }).multipleSelect({
                selectAll: false,
                multiple: false,
                width: '100%',
                delimiter: '+ ',
                filter: true,
                multiple: false,
                minimumCountSelected: 10,
                keepOpen: false,
                single: false,
            });
        }
    }
    catch (ex) {
        Success(ex.message)
    }
} /*Add Tax*/

function GenrateChargeType(ID) {
   var Opt = '';
   var ndDiv = $("#div_" + ID);
    var ndSel = $(ndDiv).find('.sel_TaxType')[0];
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '</option>'
    }
    $(ndSel).append(Opt)
    $(ndSel).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: false,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        multiple: false,
        minimumCountSelected: 10,
        keepOpen: false,
        single: true,
    });
}

function GenrateTaxOn(ID,TaxID) {
    var ndDiv = $("#div_" + ID);
    var ndSel = $(ndDiv).find('.sel_TaxOn')[0];
    var ndTaxVal = $(ndDiv).find('.TaxValue')[0];
    $(ndSel).empty();
    var Opt = '';
    Opt += '<option value="0">On Total</option>'
    Opt += '<option value="-1">On Markup</option>'
    for (var i = 0; i < arrTax.length; i++) {
        if (arrTax[i].ID != TaxID)
            Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
        else
        {
            $(ndTaxVal).val(arrTax[i].Value);
        }
    }
    $(ndSel).append(Opt);
    
    $(ndSel).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: false,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        multiple: false,
        minimumCountSelected: 10,
        keepOpen: false,
    });

}


function Save() {
    var arrSupplierTax = GetSupplierRates();
    $.ajax({
        type: "POST",
        url: "../handler/TaxHandler.asmx/SaveSupplierTax",
        data: JSON.stringify({ arrTax: arrSupplierTax }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Tax Configration Saved.")
            }
            else if(result.retCode == 0)
            {
                AlertDanger(result.ex);
            }
        }
    })
}

function GetSupplierRates() {
    var arrDetails = $(".TaxDetails");
    var arrRates = new Array();
    for (var i = 0; i < arrDetails.length; i++) {
        var Tax = $(arrDetails[i]).find(".sel_TaxType")[0]
        var TaxOn = $(arrDetails[i]).find(".sel_TaxOn")[0]
        var ndRate = $(arrDetails[i]).find(".TaxValue")[0]
        for (var j = 0; j < TaxOn.length; j++) {
            if (TaxOn[j].selected) {
                if ($(Tax).val() != "" && $(TaxOn[j]).val() != "") {
                    arrRates.push({
                        TaxID: $(Tax).val(),
                        TaxOn: $(TaxOn[j]).val(),
                        Value: $(ndRate).val(),
                    });
                }
            }
        }
    }
    return arrRates;
}