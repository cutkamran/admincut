﻿
$(document).ready(function () {
    GetBeddingType();
});

function AddBeddingType() {
    var bValid = true;
    var BeddingTypeName = $("#txt_Beddingtype").val();
    if (BeddingTypeName == "") {
        Success("Please Insert Room Type");
        bValid = false;
        $("#txt_Beddingtype").focus()
    }
    // var arrRoomType = new Array();
    //arrRoomType.push({
    //    RoomTypeID: 0,
    //    RoomTypeName: $("#txt_Roomtype").val(),
    //});
    var arrBeddingType = {
        BeddingType: $("#txt_Beddingtype").val(),
        BeddingTypeID: 0,
    }
    post("../HotelHandler.asmx/AddBeddingMaster", { arrBeddingType: arrBeddingType }, function (data) {
        Success("Bedding Type Added Successfully");
        $("#txt_Beddingtype").val("");
        GetBeddingType();

    }, function (data) {
        AlertDanger(error.ex);
    })
}

function GetBeddingType() {
    debugger;
    $("#tbl_BeddingMaster").dataTable().fnClearTable();
    $("#tbl_BeddingMaster").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetBeddingMaster",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_BeddingType = result.BeddingType;

                //var List_Amunities = Amunities[0];
                var tRow = '';
                //var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
                for (var i = 0; i < List_BeddingType.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td style="width:15%" class="align-center hide-on-mobile">' + (i + 1) + '</td>';
                    tRow += '<td style="width:35%"  class="align-center">' + List_BeddingType[i].BeddingType + '</td>';
                    tRow += '<td style="width:15%" class="align-center hide-on-mobile"><span class="button-group compact"><a onclick="GetDetails(\'' + List_BeddingType[i].BeddingTypeID + '\')" class="button icon-pencil"></a></span></td>';
                    //tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory+ '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-volume-up").replace("False", "glyphicon glyphicon-lock") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].AgencyName + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    // tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sDepartment=' + List_StaffDetails[i].Department + '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-eye-open").replace("False", "glyphicon glyphicon-eye-close") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_BeddingMaster tbody").html(tRow);
                $("#tbl_BeddingMaster").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });


            }

        },
        error: function () {
        }

    });
}

function GetDetails(BeddingTypeID) {
    id = BeddingTypeID;
    //icon = $("#File_FacilityImage").val();
    $.ajax({
        url: "HotelHandler.asmx/GetBeddingDetails",
        type: "post",
        data: '{"BeddingTypeID":"' + BeddingTypeID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arrdata = result.Arr;

                $("#txt_Beddingtype").val(arrdata[0].BeddingType);
                $("#beddingTypeId").val(arrdata[0].BeddingTypeID);
                $("#btn_Add").hide();
                $("#btn_Update").show();

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function UpdateBeddingType() {
    var bValid = true;
    var BeddingTypeName = $("#txt_Beddingtype").val();
    var BeddingTypeID = $("#beddingTypeId").val();
    //var AmunityIcon = File1;

    if (BeddingTypeName == "") {
        Success("Please Insert Bedding Type");
        bValid = false;
        $("#txt_Beddingtype").focus()
    }

    var arrBeddingType = {
        BeddingType: $("#txt_Beddingtype").val(),
        BeddingTypeId: $("#beddingTypeId").val()
    }

    if (bValid == true) {
        post("../HotelHandler.asmx/UpdateBeddingMaster", { arrBeddingType: arrBeddingType }, function (data) {
            Success("Bedding Type Update Successfully")
            $("#btn_Add").show();
            $("#btn_Update").hide();
            $("#txt_Beddingtype").val("");
            $("#txt_Beddingtype").val("");
            GetBeddingType();
        }, function (data) {
            AlertDanger(error.ex);
        })
    }
}