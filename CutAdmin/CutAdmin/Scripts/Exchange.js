﻿$(function() {
    Exchange();
})
function Exchange() {
    post("../handler/ExchangeRateHandler.asmx/GetExchange", {}, function (data) {
        $('#SelCurrency').empty();
        $(data.arrCurrency).each(function (index, item) { // GETTING Sucees HERE
            if (index == 0)
                $('#SelCurrency').append($('<option selected="selected"></option>').val(item.CurrencyCode).html(item.CurrencyCode + " (" + item.CurrencyDetails + ")"));
            else
                $('#SelCurrency').append($('<option></option>').val(item.CurrencyCode).html(item.CurrencyCode + " (" + item.CurrencyDetails + ")"));
            $('#SelCurrency').css("width", "100px");
            $('#SelCurrency').change();

        });
    }, function (responsedata) {// GETTING ERROR HERE
        AlertDanger(responsedata.ex)
    });
}