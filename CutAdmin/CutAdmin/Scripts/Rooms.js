﻿var arrRoom = new Array();
function GetRoomByCode(HotelCode) {
    $(".room").remove();
    post("../handler/RoomHandler.asmx/GetRoombyHotel",
        { HotelCode: HotelCode },
        function (response) {
            $("#validateRoom").css("display", "none");
            arrRoom = response.arrRoom;
            $(response.arrRoom).each(function (index, Room) {
                $("#div_Room").append('<input type="checkbox" name="rooms" class="checkbox  validate[required]" id="chk' + Room.RoomID + '" value="' + Room.RoomName + '"> <label class="font12 room" for="chk' + Room.RoomID + '">' + Room.RoomName + '</label>')
            })
        },
        function (error) {
            $("#validateRoom").css("display", "")
            AlertDanger(error.ex)
        });
}

function GetRoomDropDown(HotelCode) {
    $(".room").remove();
    post("../handler/RoomHandler.asmx/GetRoombyHotel",
        { HotelCode: HotelCode },
        function (response) {
            $("#validateRoom").css("display", "none");
            arrRoom = response.arrRoom;
            $(response.arrRoom).each(function (index, Room) {
                $('#sel_RoomsType').append($('<option></option>').val(Room.RoomID).html(Room.RoomName));
                $('#sel_RoomsType').change();
            })
        },
        function (error) {
            $("#validateRoom").css("display", "")
            AlertDanger(error.ex)
        });
}