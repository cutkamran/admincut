﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using CutAdmin.Common;
using CutAdmin.BL;
using System.Data;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelMappingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelMappingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        DBHelper.DBReturnCode retCode;

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Hotel Parsing

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string HotelList(string session)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = int.MaxValue;
            try
            {
                if (Session["session" + session] != null && session == Session["session" + session].ToString() && Session["newCommonHotels" + session] != null)
                {
                    List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                    List_CommonHotelDetails = (List<CommonLib.Response.CommonHotelDetails>)Session["newCommonHotels" + session];
                    //List_CommonHotelDetails = HotelMappingManager.skipMappedHotels(List_CommonHotelDetails);
                    Session["newCommonHotels" + session] = List_CommonHotelDetails;
                    string htlname = session.Split('_')[7];
                    if (htlname != "")
                    {

                        List_CommonHotelDetails = List_CommonHotelDetails.Where(c => c.HotelName.Contains(htlname)).ToList();
                    }
                    return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, List_CommonHotelDetails = List_CommonHotelDetails });
                }
                else
                {
                    Session["FiterHistory"] = null;
                    Session["List_HotelDetail_Filter"] = null;
                    GlobalDefault objGlobalDefault = null;
                    string jsonString = "";
                    string Response = "";
                    bool bResponse_MGH = false;
                    Session["session"] = session;
                    Session["session" + session] = session;
                    objSerlizer.MaxJsonLength = Int32.MaxValue;

                    List<CommonLib.Response.Facility> lstFacility = new List<CommonLib.Response.Facility>();
                    List<CommonLib.Response.Category> lstCategory = new List<CommonLib.Response.Category>();
                    List<CommonLib.Response.Location> lstLocation = new List<CommonLib.Response.Location>();

                    if (CutAdmin.Common.Common.Session(out objGlobalDefault))
                    {
                        // 

                        DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                        if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
                        {
                            objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                        }
                        List<HotelLib.Response.HotelDetail> List_HotelDetail;
                        float min_price;
                        float max_price;
                        int m_counthotel;
                        float min_price_Common;
                        float max_price_Common;
                        int m_counthotel_Common;
                        Models.Hotel objHotel = new Models.Hotel { };
                        Models.MGHHotel objMGHHotel = new Models.MGHHotel { };
                        Models.DoTWHotel objDoTWHotel = new Models.DoTWHotel { };
                        Models.EANHotels objEANHotels = new Models.EANHotels { };
                        Models.GRNHotels ObjGRNHotel = new Models.GRNHotels { };
                        //Models.GRNAvailReq


                        HttpContext context = HttpContext.Current;
                        //var parentThreadCulture = System.Globalization.CultureInfo.InvariantCulture;
                        var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                        //................................HotelBeds..........B..............................................//
                        Models.StatusCode objStatusCode;
                        Common.XMLHelper objXmlHelper = new XMLHelper();
                        objXmlHelper.session = session;
                        Common.XMLHelper.context = context;


                        objXmlHelper.objGlobalDefault = objGlobalDefault;
                        objXmlHelper.CultureInfo = parentThreadCulture;
                        objXmlHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;

                        Models.MGHStatusCode objMGHStatusCode;
                        Common.MghXMLHelper objMghXMLHelper = new MghXMLHelper();
                        objMghXMLHelper.session = session;
                        Common.MghXMLHelper.context = context;
                        objMghXMLHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;

                        HttpContext ctx = HttpContext.Current;
                        Models.DOTWStatusCode objDOTWStatusCode = new Models.DOTWStatusCode();
                        Common.DOTWXMLHelper objDOTWXMLHelper = new DOTWXMLHelper();
                        objDOTWXMLHelper.session = session;
                        objDOTWXMLHelper.context = context;
                        objDOTWXMLHelper.objGlobalDefault = objGlobalDefault;
                        objDOTWXMLHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;
                        objDOTWXMLHelper.CultureInfo = parentThreadCulture;
                        // Thread DotwTh = new Thread(new ThreadStart(objDOTWXMLHelper.AvailRequest));
                        //  DotwTh.Priority = ThreadPriority.Lowest;
                        // bool bResponse_Dotw = false; ;
                        // bResponse_Dotw = Common.DOTWXMLHelper.AvailRequest(session, out objDOTWStatusCode);
                        //LogManager.Add(0, objDOTWStatusCode.RequestHeader, objDOTWStatusCode.Request, objDOTWStatusCode.ResponseHeader, objDOTWStatusCode.Response, objGlobalDefault.sid, objDOTWStatusCode.Status);
                        // end respons/
                        var Inverient = System.Globalization.CultureInfo.InvariantCulture;
                        #region Expidia Request & Respons
                        List<EANLib.Response.HotelSummary> List_EANHotels;
                        Models.EANStatusCode objEANStatusCode;
                        //List<EANLib.Response.ValueAdd> Category = new List<EANLib.Response.ValueAdd>();
                        Common.EANXMLhelper objEANXMLhelper = new EANXMLhelper();
                        EANXMLhelper.Context = context;
                        objEANXMLhelper.session = session;
                        //GRN
                        JsonHelper ObjGRNHelper = new JsonHelper();
                        JsonHelper.Context = context;
                        ObjGRNHelper.session = session;

                        GRNHotelList ObjGRNHotelList = new GRNHotelList();
                        GtaHotelList ObjGtaHoteList = new GtaHotelList();
                        HotelBedsList ObjHotelBedsList = new HotelBedsList();
                        ExpediaHotelList ObjExpediaHotelList = new ExpediaHotelList();

                        JsonHelper.Context = context;
                        ObjGtaHoteList.Session = session;
                        ObjGRNHotelList.Session = session;
                        ObjHotelBedsList.Session = session;
                        ObjExpediaHotelList.Session = session;
                        //while (objDOTWXMLHelper.Response == false) { objDOTWXMLHelper.AvailRequest(true); }
                        // objXmlHelper.AvailRequest();
                        Parallel.For(0, 7, i =>
                        {
                            Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                            if (i == 0)
                                objXmlHelper.AvailRequest();

                            else if (i == 1)
                                objMghXMLHelper.AvailRequest();
                            else if (i == 2)
                                objDOTWXMLHelper.AvailRequest(true);
                            else if (i == 3)
                                //ObjGRNHelper.AvailReq();.
                                ObjGRNHotelList.GetGRNHotels();
                            else if (i == 4)
                                ObjGtaHoteList.GetGtaHotels();
                            ////////else if( i== 5)
                            ////////   ObjGRNHelper.AvailReq();
                            else if (i == 5)
                                ObjHotelBedsList.GetHotelBedsList();
                            else if (i == 6)
                                ObjExpediaHotelList.GetExpediaHotelList();

                        });

                        //for (int i = 0; i < 4;i++ )
                        //{
                        //    if (i == 0)
                        //        objXmlHelper.AvailRequest();
                        //    else if (i == 1)
                        //        objMghXMLHelper.AvailRequest();
                        //    else if (i == 2)
                        //        objDOTWXMLHelper.AvailRequest(true);
                        //    else if (i == 3)
                        //        ObjGRNHelper.AvailReq();
                        //}

                        if (objEANXMLhelper.bResponse)
                        {
                            Session["EanAvail"] = objEANXMLhelper.objEANHotels;
                            objEANHotels = objEANXMLhelper.objEANHotels;
                        }


                        #endregion Expidia Request & Respons



                        // Parallel.Invoke(() => objXmlHelper.AvailRequest(), () => objMghXMLHelper.AvailRequest(), () => objDOTWXMLHelper.AvailRequest(true));

                        //HotelBedsTh.Start(); 
                        //MghTh.Start(); 
                        //DotwTh.Start();
                        Boolean HotelBedsIsAlive = true;
                        Boolean MghIsAlive = true;
                        Boolean DotwIsAlive = true;
                        Models.CommonStatusCode objCommonStatusCode;
                        bool bResponse_Common = Common.CommonXMLHelper.AvailRequest(session, out objCommonStatusCode);
                        //objStatusCode = new Models.CommonStatusCode {  DisplayRequest = objMGHAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
                        int count;
                        //int countCommon;





                        if (objMghXMLHelper.Response)
                        {
                            Session["MGHStatus"] = objMghXMLHelper.objStatusCode;
                            Session["MGHAvail"] = objMghXMLHelper.objMGHHotel;
                            Session["MGHRooms"] = objMghXMLHelper.objStatusCode.DisplayRequest.Room;
                            objMGHHotel = objMghXMLHelper.objMGHHotel;
                            // LogManager.Add(0, objMghXMLHelper.objStatusCode.RequestHeader, objMghXMLHelper.objStatusCode.Request, objMghXMLHelper.objStatusCode.ResponseHeader, objMghXMLHelper.objStatusCode.Response, objXmlHelper.objGlobalDefault.sid, objMghXMLHelper.objStatusCode.Status);
                            //}
                        }
                        if (objDOTWXMLHelper.Response)
                        {
                            Session["DoTW_Night"] = objDOTWXMLHelper.objStatusCode.DisplayRequest.Night;
                            Session["DoTW_Occupancy"] = objDOTWXMLHelper.objStatusCode.Occupancy;
                            Session["DoTW_NumberOfRoom"] = objDOTWXMLHelper.objStatusCode.DisplayRequest.room.Count;
                            Session["DoTWAvail"] = objDOTWXMLHelper.objDoTWHotel;
                            objDoTWHotel = objDOTWXMLHelper.objDoTWHotel;
                            // LogManager.Add(0, objDOTWXMLHelper.objStatusCode.RequestHeader, objDOTWXMLHelper.objStatusCode.Request, objDOTWXMLHelper.objStatusCode.ResponseHeader, objDOTWXMLHelper.objStatusCode.Response, objXmlHelper.objGlobalDefault.sid, objDOTWXMLHelper.objStatusCode.Status);
                        }
                        if (ObjGRNHelper.isResponse)
                        {
                            Session["GRN_Night"] = ObjGRNHelper.objStatusCode.DisplayRequest.Night;
                            Session["GRN_Occupancy"] = ObjGRNHelper.objStatusCode.Occupancy;
                            Session["GRN_NumberOfRoom"] = ObjGRNHelper.objStatusCode.NoofRoom;
                            Session["GRNAvail"] = ObjGRNHelper.objHotel;
                            ObjGRNHotel = ObjGRNHelper.objHotel;
                        }
                        List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                        if (objMghXMLHelper.Response == true || objDOTWXMLHelper.Response == true)
                        {
                            // int countCommon = objXmlHelper.objStatusCode.DisplayRequest.Night;
                            ParseCommonResponse objParseCommonResponse = new ParseCommonResponse();
                            List_CommonHotelDetails = objParseCommonResponse.GetCommon(objHotel, objMGHHotel, objDoTWHotel, objEANHotels, ObjGRNHotel, out lstFacility, out lstCategory, out lstLocation);
                            List_CommonHotelDetails = List_CommonHotelDetails.OrderBy(d => d.HotelName).ToList();
                        }
                        if (ObjGtaHoteList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjGtaHoteList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        //GRN Chnages
                        if (ObjGRNHotelList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjGRNHotelList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        if (ObjHotelBedsList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjHotelBedsList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        if (ObjExpediaHotelList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjExpediaHotelList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        List_CommonHotelDetails = HotelMappingManager.skipMappedHotels(List_CommonHotelDetails);
                        if (List_CommonHotelDetails != null)
                            Session["newCommonHotels" + session] = List_CommonHotelDetails;
                        else
                            return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                        string htlname = session.Split('_')[7];
                        if (htlname != "")
                        {
                            List_CommonHotelDetails = List_CommonHotelDetails.Where(c => c.HotelName.Contains(htlname)).ToList();
                        }

                        return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, List_CommonHotelDetails = List_CommonHotelDetails, objUser = objUser });
                    }
                    else
                        return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0, error = ex.Message });
            }


        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMappedHotel(string HotelCode, string session)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            if (Session["newCommonHotels" + session] != null)
            {
                List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                List_CommonHotelDetails = (List<CommonLib.Response.CommonHotelDetails>)Session["newCommonHotels" + session];
                List<CommonLib.Response.CommonHotelDetails> objMapped = new List<CommonLib.Response.CommonHotelDetails>();


                foreach (string HotelId in HotelCode.Split(','))
                {
                    objMapped.Add(List_CommonHotelDetails.Single(d => d.HotelId == HotelId));
                }
                return objSerialize.Serialize(new { Session = 1, retCode = 1, CommonHotel = objMapped });

            }
            else
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetSelectedHotels(string sHotelDescription, string sSubImages, string sHotelFacilitiesText)
        {
            ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
            using (var Select = DB.Database.BeginTransaction())
            {
                //string[] arrFacilities;
                //char[] splitchar = { ',' };
                //arrFacilities = sHotelFacilitiesText.Split(splitchar);

                JavaScriptSerializer objSerialize = new JavaScriptSerializer();
                List<CommonLib.Response.CommonHotelDetails> SelectedHotels = new List<CommonLib.Response.CommonHotelDetails>();
                Session["SelectedHotelDetails"] = sHotelDescription + "|||" + sSubImages;
                if (sHotelFacilitiesText != "")
                {
                    foreach (string Facility in sHotelFacilitiesText.Split(','))
                    {
                        try
                        {
                            var facility = (from obj in DB.tbl_commonFacility where obj.HotelFacilityName == Facility select obj).ToList();
                            if (facility.Count() > 0)
                            {
                                facility[0].HotelFacilityName = Facility;
                                DB.SaveChanges();
                                Select.Commit();
                                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                            }
                            else
                            {
                                tbl_commonFacility tblFacility = new tbl_commonFacility();

                                tblFacility.HotelFacilityName = Facility;

                                DB.tbl_commonFacility.Add(tblFacility);

                                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                                DB.SaveChanges();
                                Select.Commit();
                            }


                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                        //return json;

                    }
                }



                return objSerialize.Serialize(new { Session = 1, retCode = 1 });
            }
        }

        #endregion Hotel Parsing

        #region Add Mapping with sp

        //[WebMethod(true)]
        //public string AddHotelMapping(string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sMainImage, string sSubImages, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sCountryCode, string sCityCode, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime)
        //{
        //    //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

        //    DBHelper.DBReturnCode retCode = HotelMappingManager.AddHotelMapping(sHotelName, sHotelAddress, sDescription, sRatings, sFacilities, sLangitude, sLatitude, sMainImage, sSubImages, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sCountryCode, sCityCode, sZipCode, sPatesAllowed, sLiquorPolicy, sTripAdviserLink, sHotelGroup, sAdultMinAge, sChildAgeFrom,sChildAgeTo, sCheckinTime, sCheckoutTime);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    else
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //}


        #endregion

        #region Add Mapping with linq
        [WebMethod(true)]
        public string AddHotelMapping(Int64 sHotelCode, string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sGTACode, string sCountryCode, string sCityCode, Int64 LocationID, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string Smooking, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime, int TotalRooms, int TotalFloors, string BuildYear, string RenovationYear, string HotelPhone, string HotelMobile, string HotelFax, string HotelPerson, string HotelEmail, string ReservationPhone, string ReservationMobile, string ReservationFax, string ReservationPerson, string ReservationEmail, string AccountsPhone, string AccountsMobile, string AccountsFax, string AccountsPerson, string AccountsEmail, List<Comm_TaxMapping> RatesDetails, List<Comm_TaxMapping> MealRates)
        {
            DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sUserID = 0;
            bool Approved = true;
            if (objUser.UserType=="Supplier")
            {
                sUserID = objUser.sid;
                Approved = true;
            }
            else if (objUser.UserType=="SupplierStaff")
            {
                sUserID = objUser.ParentId;
                Approved = true;
            }
            else if (objUser.UserType == "Admin")
            {
                sUserID = objUser.sid;
                Approved = true;
            }
            
            retCode = HotelMappingManager.SearchHotelMapp(sHotelName,sUserID, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sGTACode, out dtResult);

           
           
            string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                    ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                    using (var Add = DB.Database.BeginTransaction())
                    {
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        else
                        {
                            TaxManager.DeleteTaxDetails(sHotelCode);
                        }
                        if (MealRates.Count != 0)
                        {
                            MealRates.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(MealRates);
                        }
                        else
                        {
                            TaxManager.DeleteTaxDetailsAddOns(sHotelCode);
                        }

                        tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMaster.Single(x => x.sid == sHotelCode);
                        //tbl_Comm_HotelGroupMapping Map = DB.tbl_Comm_HotelGroupMappings.Single(x => x.Hotel_Code == sHotelCode.ToString());
                        List<tbl_CommonHotelContacts> Contacts = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        DBHandlerDataContext db = new DBHandlerDataContext();
                        ClickUrHotel_DBEntities cdb = new ClickUrHotel_DBEntities();
                        List<Comm_HotelContacts> Contacts1 = (from obj in cdb.Comm_HotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        if (sHotelName != "")
                        {
                            if (objUser.UserType == "Supplier")
                                Hotel.HotelName = Hotel.HotelName;
                            else if (objUser.UserType != "SupplierStaff")
                                Hotel.HotelName = Hotel.HotelName;
                            else
                                Hotel.HotelName = sHotelName;
                        }
                        if (sHotelAddress != "")
                        {
                            Hotel.HotelAddress = sHotelAddress;
                        }
                        if (sFacilities != "")
                        {
                            Hotel.HotelFacilities = sFacilities;
                        }
                        if (sCheckinTime != "")
                        {
                            Hotel.CheckinTime = sCheckinTime;
                        }
                        if (sCheckoutTime != "")
                        {
                            Hotel.CheckoutTime = sCheckoutTime;
                        }
                        if (sDescription != "")
                        {
                            Hotel.HotelDescription = sDescription;
                        }
                        if (sAdultMinAge != 0)
                        {
                            Hotel.AdultMinAge = sAdultMinAge;
                        }
                        if (sChildAgeFrom != 0)
                        {
                            Hotel.ChildAgeFrom = sChildAgeFrom;
                        }
                        if (sChildAgeTo != 0)
                        {
                            Hotel.ChildAgeTo = sChildAgeTo;
                        }
                        if (TotalRooms != 0)
                        {
                            Hotel.TotalRooms = TotalRooms;
                        }
                        if (TotalFloors != 0)
                        {
                            Hotel.TotalFloors = TotalFloors;
                        }
                        if (BuildYear != "")
                        {
                            Hotel.BuildYear = BuildYear;
                        }
                        if (RenovationYear != "")
                        {
                            Hotel.RenovationYear = RenovationYear;
                        }

                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        // Hotel.ParentID = sUserID;
                       // Hotel.Approved = Approved;
                        //Hotel.HotelImage = sMainImage;
                        //Hotel.SubImages = sSubImages;
                        if (sDotwCode != "")
                        {
                            Hotel.DotwCode = sDotwCode;
                        }
                        if (sHotelBedsCode != "")
                        {
                            Hotel.HotelBedsCode = sHotelBedsCode;
                        }
                        if (sMGHCode != "")
                        {
                            Hotel.MGHCode = sMGHCode;
                        }
                        if (sGRNCode != "")
                        {
                            Hotel.GRNCode = sGRNCode;
                        }
                        if (sExpediaCode != "")
                        {
                            Hotel.ExpediaCode = sExpediaCode;
                        }
                        if (sGTACode != "")
                        {
                            Hotel.GTACode = sGTACode;
                        }
                        if (Hotel.BaseSupplier != "")
                        {
                            Hotel.BaseSupplier = "ClickUrtrip";
                        }
                        //Hotel.UpdatedBy = sUserID;
                        Hotel.UpdatedDate = DateTimes;
                        if (sHotelGroup != "" || sHotelGroup != "Select Any Group")
                        {
                            Hotel.HotelGroup = sHotelGroup;
                        }
                        else
                        {
                            Hotel.HotelGroup = sHotelName;
                        }
                        if (sPatesAllowed != "")
                        {
                            Hotel.PatesAllowed = sPatesAllowed;
                        }
                        if (sTripAdviserLink != "")
                        {
                            Hotel.TripAdviserLink = sTripAdviserLink;
                        }
                        if (sLiquorPolicy != "")
                        {
                            Hotel.LiquorPolicy = sLiquorPolicy;
                        }
                        if (Smooking != "")
                        {
                            Hotel.Smooking = Smooking;
                        }
                     
                        if (Contacts1.Count() > 0)                                  //update contact details in table 'Comm_HotelContacts'
                        {
                            Contacts1[0].Type = "HotelContact";
                            if (HotelPhone != "")
                            {
                                Contacts1[0].Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contacts1[0].MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contacts1[0].Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contacts1[0].ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contacts1[0].email = HotelEmail;
                            }

                            Contacts1[0].HotelCode = Hotel.sid;

                            DB.SaveChanges();
                            Add.Commit();
                            //
                            Contacts1[1].Type = "ReservationContact";
                            if (ReservationPhone != "")
                            {
                                Contacts1[1].Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contacts1[1].MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contacts1[1].Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contacts1[1].ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contacts1[1].email = ReservationEmail;
                            }

                            Contacts1[1].HotelCode = Hotel.sid;
                            DB.SaveChanges();
                            Add.Commit();
                            //
                            Contacts1[2].Type = "AccountsContact";
                            if (AccountsPhone != "")
                            {
                                Contacts1[2].Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contacts1[2].MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contacts1[2].Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contacts1[2].ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contacts1[2].email = AccountsEmail;
                            }

                            Contacts1[2].HotelCode = Hotel.sid;

                            DB.SaveChanges();
                            Add.Commit();
                        }
                        else
                        {
                            ClickUrHotel_DBEntities dbc = new ClickUrHotel_DBEntities();
                            Comm_HotelContacts Contact1 = new Comm_HotelContacts();
                            Comm_HotelContacts Contact2 = new Comm_HotelContacts();
                            Comm_HotelContacts Contact3 = new Comm_HotelContacts();

                            Contact1.Type = "HotelContact";
                            Contact1.SupplierCode = sUserID;
                            if (HotelPhone != "")
                            {
                                Contact1.Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contact1.MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contact1.Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contact1.ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contact1.email = HotelEmail;
                            }

                            Contact1.HotelCode = Hotel.sid;
                            dbc.Comm_HotelContacts.Add(Contact1);
                            dbc.SaveChanges();

                            //
                            Contact2.Type = "ReservationContact";
                            Contact2.SupplierCode = sUserID;
                            if (ReservationPhone != "")
                            {
                                Contact2.Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contact2.MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contact2.Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contact2.ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contact2.email = ReservationEmail;
                            }

                            Contact2.HotelCode = Hotel.sid;
                            dbc.Comm_HotelContacts.Add(Contact2);
                            dbc.SaveChanges();

                            //
                            Contact3.Type = "AccountsContact";
                            Contact3.SupplierCode = sUserID;
                            if (AccountsPhone != "")
                            {
                                Contact3.Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contact3.MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contact3.Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contact3.ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contact3.email = AccountsEmail;
                            }

                            Contact3.HotelCode = Hotel.sid;
                            dbc.Comm_HotelContacts.Add(Contact3);
                            dbc.SaveChanges();
                        }

                        //Map.Group_id = Convert.ToInt64(sHotelGroup);
                        //Map.Hotel_Code = Hotel.sid.ToString();
                        //DB.SubmitChanges();

                        //DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"Update\"}";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }



            }
            else
            {
                try
                {
                    ClickUrHotel_DBEntities db = new ClickUrHotel_DBEntities();
                    DBHandlerDataContext DB = new DBHandlerDataContext();
                    using (var Add = db.Database.BeginTransaction())
                    {
                        tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
                        tbl_CommonHotelContacts Contacts = new tbl_CommonHotelContacts();
                        Comm_HotelContacts Contact1 = new Comm_HotelContacts();
                        Comm_HotelContacts Contact2 = new Comm_HotelContacts();
                        Comm_HotelContacts Contact3 = new Comm_HotelContacts();
                        tbl_Comm_HotelGroupMapping Map = new tbl_Comm_HotelGroupMapping();


                        Hotel.HotelName = sHotelName;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.HotelDescription = sDescription;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelFacilities = sFacilities;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        Hotel.CheckinTime = sCheckinTime;
                        Hotel.CheckoutTime = sCheckoutTime;
                        Hotel.AdultMinAge = sAdultMinAge;
                        Hotel.ChildAgeFrom = sChildAgeFrom;
                        Hotel.ChildAgeTo = sChildAgeTo;
                        Hotel.TotalRooms = TotalRooms;
                        Hotel.TotalFloors = TotalFloors;
                        Hotel.BuildYear = BuildYear;
                        Hotel.RenovationYear = RenovationYear;
                        Hotel.DotwCode = sDotwCode;
                        Hotel.HotelBedsCode = sHotelBedsCode;
                        Hotel.MGHCode = sMGHCode;
                        Hotel.GRNCode = sGRNCode;
                        Hotel.ExpediaCode = sExpediaCode;
                        Hotel.GTACode = sGTACode;
                        Hotel.CreatedBy = sUserID;
                        Hotel.ParentID = sUserID;
                        Hotel.CreatedDate = DateTimes;
                        if (sHotelGroup != "Select Any Group")
                        {
                            Hotel.HotelGroup = sHotelGroup;
                        }
                        else
                        {
                            Hotel.HotelGroup = sHotelName;
                        }
                        Hotel.TripAdviserLink = sTripAdviserLink;
                        Hotel.PatesAllowed = sPatesAllowed;
                        Hotel.LiquorPolicy = sLiquorPolicy;
                        Hotel.Smooking = Smooking;
                        Hotel.Approved = true;
                        Hotel.Active = "True";

                        db.tbl_CommonHotelMaster.Add(Hotel);
                        db.SaveChanges();
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.ForEach(c => c.HotelID = Hotel.sid);
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        if (MealRates.Count != 0)
                        {
                            MealRates.ForEach(c => c.HotelID = Hotel.sid);
                            TaxManager.SaveTaxDetails(MealRates);
                        }
                        Contact1.Type = "HotelContact";
                        Contact1.SupplierCode = sUserID;
                        if (HotelPhone != "")
                        {
                            Contact1.Phone = HotelPhone;
                        }
                        if (HotelMobile != "")
                        {
                            Contact1.MobileNo = HotelMobile;
                        }
                        if (HotelFax != "")
                        {
                            Contact1.Fax = HotelFax;
                        }
                        if (HotelPerson != "")
                        {
                            Contact1.ContactPerson = HotelPerson;
                        }
                        if (HotelEmail != "")
                        {
                            Contact1.email = HotelEmail;
                        }

                        Contact1.HotelCode = Hotel.sid;
                        db.Comm_HotelContacts.Add(Contact1);
                        db.SaveChanges();

                        //
                        Contact2.Type = "ReservationContact";
                        Contact2.SupplierCode = sUserID;
                        if (ReservationPhone != "")
                        {
                            Contact2.Phone = ReservationPhone;
                        }
                        if (ReservationMobile != "")
                        {
                            Contact2.MobileNo = ReservationMobile;
                        }
                        if (ReservationFax != "")
                        {
                            Contact2.Fax = ReservationFax;
                        }
                        if (ReservationPerson != "")
                        {
                            Contact2.ContactPerson = ReservationPerson;
                        }
                        if (ReservationEmail != "")
                        {
                            Contact2.email = ReservationEmail;
                        }

                        Contact2.HotelCode = Hotel.sid;
                        db.Comm_HotelContacts.Add(Contact2);
                        db.SaveChanges();

                        //
                        Contact3.Type = "AccountsContact";
                        Contact3.SupplierCode = sUserID;
                        if (AccountsPhone != "")
                        {
                            Contact3.Phone = AccountsPhone;
                        }
                        if (AccountsMobile != "")
                        {
                            Contact3.MobileNo = AccountsMobile;
                        }
                        if (AccountsFax != "")
                        {
                            Contact3.Fax = AccountsFax;
                        }
                        if (AccountsPerson != "")
                        {
                            Contact3.ContactPerson = AccountsPerson;
                        }
                        if (AccountsEmail != "")
                        {
                            Contact3.email = AccountsEmail;
                        }

                        Contact3.HotelCode = Hotel.sid;
                        db.Comm_HotelContacts.Add(Contact3);
                        db.SaveChanges();

                       // Map.Group_id = Convert.ToInt64(Hotel.HotelGroup);
                        //Map.Hotel_Code = Hotel.sid.ToString();
                        //DB.tbl_Comm_HotelGroupMappings.InsertOnSubmit(Map);
                        //DB.SubmitChanges();



                        if (sHotelGroup == "Select Any Group")
                        {
                            var CityCode = (from objc in DB.tbl_HCities where objc.Countryname == sCountryCode && objc.Description == sCityCode select objc).FirstOrDefault();
                            string Name = sHotelName;
                            string Hotels = Hotel.sid.ToString();
                            string Email = HotelEmail;
                            string Phone = HotelMobile;
                            string Country = CityCode.Country;
                            string City = CityCode.Code;
                            CutAdmin.handler.HotelGroupHandler obj = new handler.HotelGroupHandler();
                            obj.SaveHotelGroup(Name, Hotels, Email, Phone, Country, City);
                        }

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelCode\":\"" + Contact3.HotelCode + "\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"Add\" }";
                        
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;

        }



        [WebMethod(true)]
        public string AddHotelApproved(Int64 sHotelCode,Int64 sParent, string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sGTACode, string sCountryCode, string sCityCode, Int64 LocationID, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string Smooking, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime, int TotalRooms, int TotalFloors, string BuildYear, string RenovationYear, string HotelPhone, string HotelMobile, string HotelFax, string HotelPerson, string HotelEmail, string ReservationPhone, string ReservationMobile, string ReservationFax, string ReservationPerson, string ReservationEmail, string AccountsPhone, string AccountsMobile, string AccountsFax, string AccountsPerson, string AccountsEmail, List<Comm_TaxMapping> RatesDetails, List<Comm_TaxMapping> MealRates)
        {
            DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sUserID = 0;
            bool Approved = true;
  
            retCode = HotelMappingManager.SearchHotelMapp(sHotelName, sParent, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sGTACode, out dtResult);



            string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                    DBHandlerDataContext db = new DBHandlerDataContext();
                    ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                    using (var Hotels = DB.Database.BeginTransaction())
                    {
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        if (MealRates.Count != 0)
                        {
                            MealRates.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(MealRates);
                        }

                        tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMaster.Single(x => x.sid == sHotelCode);
                        List<tbl_CommonHotelContacts> Contacts = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        tbl_Comm_HotelGroupMapping Map = db.tbl_Comm_HotelGroupMappings.Single(x => x.Hotel_Code == sHotelCode.ToString());
                        List<Comm_HotelContacts> Contacts1 = (from obj in DB.Comm_HotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        if (sHotelName != "")
                        {
                            Hotel.HotelName = sHotelName;
                        }
                        if (sHotelAddress != "")
                        {
                            Hotel.HotelAddress = sHotelAddress;
                        }
                        if (sFacilities != "")
                        {
                            Hotel.HotelFacilities = sFacilities;
                        }
                        if (sCheckinTime != "")
                        {
                            Hotel.CheckinTime = sCheckinTime;
                        }
                        if (sCheckoutTime != "")
                        {
                            Hotel.CheckoutTime = sCheckoutTime;
                        }
                        if (sDescription != "")
                        {
                            Hotel.HotelDescription = sDescription;
                        }
                        if (sAdultMinAge != 0)
                        {
                            Hotel.AdultMinAge = sAdultMinAge;
                        }
                        if (sChildAgeFrom != 0)
                        {
                            Hotel.ChildAgeFrom = sChildAgeFrom;
                        }
                        if (sChildAgeTo != 0)
                        {
                            Hotel.ChildAgeTo = sChildAgeTo;
                        }
                        if (TotalRooms != 0)
                        {
                            Hotel.TotalRooms = TotalRooms;
                        }
                        if (TotalFloors != 0)
                        {
                            Hotel.TotalFloors = TotalFloors;
                        }
                        if (BuildYear != "")
                        {
                            Hotel.BuildYear = BuildYear;
                        }
                        if (RenovationYear != "")
                        {
                            Hotel.RenovationYear = RenovationYear;
                        }

                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                       // Hotel.ParentID = sUserID;
                        Hotel.Approved = Approved;
                        //Hotel.HotelImage = sMainImage;
                        //Hotel.SubImages = sSubImages;
                        if (sDotwCode != "")
                        {
                            Hotel.DotwCode = sDotwCode;
                        }
                        if (sHotelBedsCode != "")
                        {
                            Hotel.HotelBedsCode = sHotelBedsCode;
                        }
                        if (sMGHCode != "")
                        {
                            Hotel.MGHCode = sMGHCode;
                        }
                        if (sGRNCode != "")
                        {
                            Hotel.GRNCode = sGRNCode;
                        }
                        if (sExpediaCode != "")
                        {
                            Hotel.ExpediaCode = sExpediaCode;
                        }
                        if (sGTACode != "")
                        {
                            Hotel.GTACode = sGTACode;
                        }
                        if (Hotel.BaseSupplier != "")
                        {
                            Hotel.BaseSupplier = "ClickUrtrip";
                        }
                        Hotel.UpdatedBy = sUserID;
                        Hotel.UpdatedDate = DateTimes;
                        if (sHotelGroup != "")
                        {
                            Hotel.HotelGroup = sHotelGroup;
                        }
                        if (sPatesAllowed != "")
                        {
                            Hotel.PatesAllowed = sPatesAllowed;
                        }
                        if (sTripAdviserLink != "")
                        {
                            Hotel.TripAdviserLink = sTripAdviserLink;
                        }
                        if (sLiquorPolicy != "")
                        {
                            Hotel.LiquorPolicy = sLiquorPolicy;
                        }
                        if (Smooking != "")
                        {
                            Hotel.Smooking = Smooking;
                        }
                     

                        if (Contacts1.Count() > 0)                                  //update contact details in table 'Comm_HotelContacts'
                        {
                            Contacts1[0].Type = "HotelContact";
                            if (HotelPhone != "")
                            {
                                Contacts1[0].Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contacts1[0].MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contacts1[0].Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contacts1[0].ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contacts1[0].email = HotelEmail;
                            }

                            Contacts1[0].HotelCode = Hotel.sid;

                            DB.SaveChanges();
                            Hotels.Commit();
                            //
                            Contacts1[1].Type = "ReservationContact";
                            if (ReservationPhone != "")
                            {
                                Contacts1[1].Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contacts1[1].MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contacts1[1].Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contacts1[1].ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contacts1[1].email = ReservationEmail;
                            }

                            Contacts1[1].HotelCode = Hotel.sid;
                            DB.SaveChanges();
                            Hotels.Commit();
                            //
                            Contacts1[2].Type = "AccountsContact";
                            if (AccountsPhone != "")
                            {
                                Contacts1[2].Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contacts1[2].MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contacts1[2].Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contacts1[2].ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contacts1[2].email = AccountsEmail;
                            }

                            Contacts1[2].HotelCode = Hotel.sid;

                            DB.SaveChanges();
                            Hotels.Commit();
                        }
                        else
                        {
                            ClickUrHotel_DBEntities DBC = new ClickUrHotel_DBEntities();
                            Comm_HotelContacts Contact1 = new Comm_HotelContacts();
                            Comm_HotelContacts Contact2 = new Comm_HotelContacts();
                            Comm_HotelContacts Contact3 = new Comm_HotelContacts();

                            Contact1.Type = "HotelContact";
                            Contact1.SupplierCode = sUserID;
                            if (HotelPhone != "")
                            {
                                Contact1.Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contact1.MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contact1.Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contact1.ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contact1.email = HotelEmail;
                            }

                            Contact1.HotelCode = Hotel.sid;
                            DBC.Comm_HotelContacts.Add(Contact1);
                            DBC.SaveChanges();

                            //
                            Contact2.Type = "ReservationContact";
                            Contact2.SupplierCode = sUserID;
                            if (ReservationPhone != "")
                            {
                                Contact2.Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contact2.MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contact2.Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contact2.ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contact2.email = ReservationEmail;
                            }

                            Contact2.HotelCode = Hotel.sid;
                            DBC.Comm_HotelContacts.Add(Contact2);
                            DBC.SaveChanges();

                            //
                            Contact3.Type = "AccountsContact";
                            Contact3.SupplierCode = sUserID;
                            if (AccountsPhone != "")
                            {
                                Contact3.Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contact3.MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contact3.Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contact3.ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contact3.email = AccountsEmail;
                            }

                            Contact3.HotelCode = Hotel.sid;
                            DBC.Comm_HotelContacts.Add(Contact3);
                            DBC.SaveChanges();
                        }

                        Map.Group_id = Convert.ToInt64(sHotelGroup);
                        Map.Hotel_Code = Hotel.sid.ToString();
                        DB.SaveChanges();
                        Hotels.Commit();
                        //DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"AddApproved\" }";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }



            }
            else
            {
                try
                {
                    ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                    DBHandlerDataContext db = new DBHandlerDataContext();
                    using (var AddHotel = DB.Database.BeginTransaction())
                    {
                        tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
                        tbl_CommonHotelContacts Contacts = new tbl_CommonHotelContacts();
                        Comm_HotelContacts Contact1 = new Comm_HotelContacts();
                        Comm_HotelContacts Contact2 = new Comm_HotelContacts();
                        Comm_HotelContacts Contact3 = new Comm_HotelContacts();
                        tbl_Comm_HotelGroupMapping Map = new tbl_Comm_HotelGroupMapping();

                        Hotel.HotelName = sHotelName;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.HotelDescription = sDescription;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelFacilities = sFacilities;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        Hotel.CheckinTime = sCheckinTime;
                        Hotel.CheckoutTime = sCheckoutTime;
                        Hotel.AdultMinAge = sAdultMinAge;
                        Hotel.ChildAgeFrom = sChildAgeFrom;
                        Hotel.ChildAgeTo = sChildAgeTo;
                        Hotel.TotalRooms = TotalRooms;
                        Hotel.TotalFloors = TotalFloors;
                        Hotel.BuildYear = BuildYear;
                        Hotel.RenovationYear = RenovationYear;
                        Hotel.DotwCode = sDotwCode;
                        Hotel.HotelBedsCode = sHotelBedsCode;
                        Hotel.MGHCode = sMGHCode;
                        Hotel.GRNCode = sGRNCode;
                        Hotel.ExpediaCode = sExpediaCode;
                        Hotel.GTACode = sGTACode;
                       // Hotel.CreatedBy = sUserID;
                       // Hotel.ParentID = sUserID;
                        Hotel.CreatedDate = DateTimes;
                        Hotel.HotelGroup = sHotelGroup;
                        Hotel.TripAdviserLink = sTripAdviserLink;
                        Hotel.PatesAllowed = sPatesAllowed;
                        Hotel.LiquorPolicy = sLiquorPolicy;
                        Hotel.Smooking = Smooking;
                        Hotel.Approved = Approved;

                        DB.tbl_CommonHotelMaster.Add(Hotel);
                        DB.SaveChanges();
                        AddHotel.Commit();
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.Select(c => { c.HotelID = sHotelCode; return c; });
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                       
                        Contact1.Type = "HotelContact";
                        Contact1.SupplierCode = sUserID;
                        if (HotelPhone != "")
                        {
                            Contact1.Phone = HotelPhone;
                        }
                        if (HotelMobile != "")
                        {
                            Contact1.MobileNo = HotelMobile;
                        }
                        if (HotelFax != "")
                        {
                            Contact1.Fax = HotelFax;
                        }
                        if (HotelPerson != "")
                        {
                            Contact1.ContactPerson = HotelPerson;
                        }
                        if (HotelEmail != "")
                        {
                            Contact1.email = HotelEmail;
                        }

                        Contact1.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.Add(Contact1);
                        DB.SaveChanges();

                        //
                        Contact2.Type = "ReservationContact";
                        Contact2.SupplierCode = sUserID;
                        if (ReservationPhone != "")
                        {
                            Contact2.Phone = ReservationPhone;
                        }
                        if (ReservationMobile != "")
                        {
                            Contact2.MobileNo = ReservationMobile;
                        }
                        if (ReservationFax != "")
                        {
                            Contact2.Fax = ReservationFax;
                        }
                        if (ReservationPerson != "")
                        {
                            Contact2.ContactPerson = ReservationPerson;
                        }
                        if (ReservationEmail != "")
                        {
                            Contact2.email = ReservationEmail;
                        }

                        Contact2.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.Add(Contact2);
                        DB.SaveChanges();

                        //
                        Contact3.Type = "AccountsContact";
                        Contact3.SupplierCode = sUserID;
                        if (AccountsPhone != "")
                        {
                            Contact3.Phone = AccountsPhone;
                        }
                        if (AccountsMobile != "")
                        {
                            Contact3.MobileNo = AccountsMobile;
                        }
                        if (AccountsFax != "")
                        {
                            Contact3.Fax = AccountsFax;
                        }
                        if (AccountsPerson != "")
                        {
                            Contact3.ContactPerson = AccountsPerson;
                        }
                        if (AccountsEmail != "")
                        {
                            Contact3.email = AccountsEmail;
                        }

                        Contact3.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.Add(Contact3);
                        DB.SaveChanges();


                        Map.Group_id = Convert.ToInt64(sHotelGroup);
                        Map.Hotel_Code = Hotel.sid.ToString();
                        db.tbl_Comm_HotelGroupMappings.InsertOnSubmit(Map);
                        db.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelCode\":\"" + Contact3.HotelCode + "\",\"UserType\":\"Admin\",\"value\":\"UpdateApproved\" }";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string AddLocation(string val, string sCountryCode, string sCityCode)
        {
            try
            {
                ClickUrHotel_DBEntities DB = new ClickUrHotel_DBEntities();
                using (var Add = DB.Database.BeginTransaction())
                {
                    var arrLocation = DB.tbl_CommonLocations.Where(d => d.LocationName == val && d.City == sCityCode && d.Country == sCountryCode).FirstOrDefault();
                    if (arrLocation != null)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Lid\":\"" + arrLocation.Lid + "\"}";
                    }
                    else
                    {
                        tbl_CommonLocations Locationn = new tbl_CommonLocations();
                        Locationn.LocationName = val;
                        Locationn.Country = sCountryCode;
                        Locationn.City = sCityCode;
                        DB.tbl_CommonLocations.Add(Locationn);
                        DB.SaveChanges();
                        Add.Commit();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Lid\":\"" + Locationn.Lid + "\"}";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion

        #region CountryCity
        [WebMethod(true)]
        public string GetCountry()
        {

            DBHelper.DBReturnCode retcode = HotelMappingManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelMappingManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityList\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string GetCityCountry(string Location)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
           try
            {
                using (var DB = new ClickUrHotel_DBEntities())
                {
                    var LocationName = (from obj in DB.tbl_CommonLocations where obj.LocationName == Location select obj).FirstOrDefault();
                    return objSerialize.Serialize(new { Session = 1, retCode = 1, LocationName = LocationName });
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }
        #endregion


    }

    
}

