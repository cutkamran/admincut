﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class cancellationRules
    {
        public string fromDate { get; set; }
        public float cancelCharge { get; set; }
        public float CUTcancelCharge { get; set; }
        public float AgentMarkup { get; set; }
        public bool AmendRestricted { get; set; }
        public bool CancelRestricted { get; set; }
        public string NoShowPolicy { get; set; }
    }
}
