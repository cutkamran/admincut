﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class ServiceResponse
    {
        public List<string> allocationDetails { get; set; }
        public List<string> tariffNotes { get; set; }
        //public List<booking> booking { get; set; }
        public List<string> status { get; set; }
    }
}
