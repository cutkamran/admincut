﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class RoomCapacityInfo
    {
        public Int64 roomPaxCapacity { get; set; }
        public Int64 allowedAdultsWithoutChildren { get; set; }
        public Int64 allowedAdultsWithChildren { get; set; }
        public Int64 maxExtraBed { get; set; }
    }
}
