﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Attraction
    {
        public Int64 ItemNo { get; set; }
        public string Name { get; set; }
        public List<Dist> dist { get; set; }
        public int Count { get; set; }
    }
}
