﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DOTWLib.Response;
using System.Web;
//using CUT.DataLayer;
//using CUT.BL;
using System.Data;

namespace DOTWLib.Response
{
    public class ParseCancelResponse
    {
        public List<Service> GetCancelResponse(string m_response)
        {
            //DBHelper.DBReturnCode retCode;
            //DataTable dtResult;
            XmlDocument doc = new XmlDocument();

            bool nStatus;
            Cancellation Cancellation = new Cancellation();
            List<Service> lstService = new List<Service>();
            doc.LoadXml(m_response);
            XmlNode ndResponseStatus = doc.SelectSingleNode("result/successful");
            if (ndResponseStatus == null)
            {
                XmlNode ndResponseStatus_F = doc.SelectSingleNode("result/request/successful");
                nStatus = Convert.ToBoolean(ndResponseStatus_F.InnerText);
            }
            else
            {
                nStatus = Convert.ToBoolean(ndResponseStatus.InnerText);
            }
            if (nStatus == true)
            {
                XmlNodeList nService = doc.SelectNodes("result/services/service");
                Service[] objService = new Service[nService.Count];
                for (int i = 0; i < nService.Count; i++)
                {
                    objService[i] = new Service();
                    objService[i].code = Convert.ToInt32(nService[i].Attributes.GetNamedItem("code").Value);
                    objService[i].charge = Convert.ToSingle(nService[i].SelectSingleNode("cancellationPenalty/penaltyApplied").FirstChild.InnerText);
                    objService[i].currency = Convert.ToInt32(nService[i].SelectSingleNode("cancellationPenalty/currency").InnerText);
                    objService[i].currencyShort = nService[i].SelectSingleNode("cancellationPenalty/currencyShort").InnerText;
                    lstService.Add(objService[i]);
                }
                //currency = doc.SelectSingleNode("result/Ser/currencyShort").InnerText;
                //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                //float dolarRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());


                //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                //string DoTWcurrency = objGlobalDefault.Currency;
                //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                //float ExchangeRate = 0;
                //if (DoTWcurrency == "INR")
                //{
                //    ExchangeRate = 1;
                //}
                //else
                //{
                //    ExchangeRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());
                //}
            }
            return lstService;
        }
    }
}