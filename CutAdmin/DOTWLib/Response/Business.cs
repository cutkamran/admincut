﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Business
    {
        public Int64 BusinessId { get; set; }
        public string BusinessName { get; set; }
    }
}
