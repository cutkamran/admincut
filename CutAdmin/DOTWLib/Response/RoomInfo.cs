﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class RoomInfo
    {
        public Int64 maxOccupancy { get; set; }
        public Int64 maxAdultWithChildren { get; set; }
        public Int64 minChildAge { get; set; }
        public Int64 maxChildAge { get; set; }

        public Int64 maxAdult { get; set; }
        public Int64 maxExtraBed { get; set; }
        public Int64 maxChildren { get; set; }
    }
}
