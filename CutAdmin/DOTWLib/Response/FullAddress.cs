﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class FullAddress
    {
        public string hotelStreetAddress { get; set; }
        public string hotelZipCode { get; set; }
        public string hotelCountry { get; set; }
        public string hotelCity { get; set; }
    }
}
