﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class DOTWHotelDetails
    {
        public Int64 HotelId { get; set; }
        public string Currency { get; set; }
        public string HotelName { get; set; }
        public string CityCode { get; set; }
        public FullAddress FullAddress { get; set; }
        public string Description { get; set; }
        public List<Attraction> Attraction { get; set; }
        public List<Amenitie> Amenitie { get; set; }
        public List<Business> Business { get; set; }
        public string HotelPhone { get; set; }
        public string HotelCheckIn { get; set; }
        public string HotelCheckOut { get; set; }
        public string Rating { get; set; }
        public List<Image> HotelImages { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public List<Room> Room { get; set; }
        //public List<RoomType> RoomType { get; set; }
        //public List<cancellationRules> cancellationRules { get; set; }
        public float RoomPrice { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public List<float> StaffMarkup { get; set; }
        public string Supplier { get; set; }
    }
}
