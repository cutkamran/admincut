﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class emergencyContact
    {
        public int salutationid { get; set; }
        public string salutationname { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
    }
}
