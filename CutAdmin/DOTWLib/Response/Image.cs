﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Image
    {
        public string thumb { get; set; }
        public Int64 CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Url { get; set; }
    }
}
