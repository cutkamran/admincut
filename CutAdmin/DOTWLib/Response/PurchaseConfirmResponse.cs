﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class PurchaseConfirmResponse
    {
        public string confirmationText { get; set; }
        public string returnedCode { get; set; }
        public List<booking> booking { get; set; }
        public bool successful { get; set; }
    }
}
