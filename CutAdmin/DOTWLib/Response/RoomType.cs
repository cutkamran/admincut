﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class RoomType
    {
        public Int64 RoomTypeCode { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomDescription { get; set; }
        public string RoomDescriptionId { get; set; }
        public string RoomRateType { get; set; }
        public string RoomRateTypeCurrency { get; set; }
        public Int64 RoomRateTypeCurrencyId { get; set; }
        public string RoomAllocationDetails { get; set; }
        public List<RoomAmenities> RoomAmenities { get; set; }
        public RoomInfo RoomInfo { get; set; }
        public List<special> special { get; set; }
        public RoomCapacityInfo RoomCapacityInfo { get; set; }
        public List<cancellationRules> cancellationRules { get; set; }
        public string tariffNotes { get; set; }
        public float Total { get; set; }
        public int LeftToSell { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public string ChildrenAges { get; set; }
        public Int64 Children { get; set; }
        public Int64 Adults { get; set; }
        public int roomNumber { get; set; }
        public List<rateBasis> rateBasis { get; set; }
        public Int64 specialsApplied { get; set; }
        //public rateBasis rateBasis_s { get; set; }
        public string minStay { get; set; }
        public string status { get; set; }
        public string dateApplyMinStay { get; set; }
        public int passengerNamesRequiredForBooking { get; set; }
        public validForOccupancy validForOccupancy { get; set; }
        public string changedOccupancy { get; set; }
    }
}
