﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class booking
    {
        public Int64 runno { get; set; }
        public Int64 bookingCode { get; set; }
        public string bookingReferenceNumber { get; set; }
        public int bookingStatus { get; set; }
        public float servicePrice { get; set; }
        public float mealsPrice { get; set; }
        public float price { get; set; }
        public Int64 currency { get; set; }
        public string type { get; set; }
        public string voucher { get; set; }
        public string paymentGuaranteedBy { get; set; }
        public List<emergencyContact> emergencyContact { get; set; }
    }
}
