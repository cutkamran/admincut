﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Dist
    {
        public string Attr { get; set; }
        public string Value { get; set; }
    }
}
