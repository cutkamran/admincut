﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Room
    {
        public Int64 Count { get; set; }
        //public string ChildrenAges { get; set; }
        //public Int64 Children { get; set; }
        //public Int64 Adults { get; set; }
        public List<RoomType> RoomType { get; set; }
        public float RoomPrice { get; set; }
        public float CUTRoomPrice { get; set; }
        public float AgentMarkup { get; set; }
    }
}
