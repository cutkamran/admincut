﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class RoomAmenities
    {
        public Int64 RoomAmenitiesId { get; set; }
        public string RoomAmenitiesName { get; set; }
    }
}
