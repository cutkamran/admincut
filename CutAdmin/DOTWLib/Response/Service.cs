﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Response
{
    public class Service
    {
        public int code { get; set; }
        public float charge { get; set; }
        public int currency { get; set; }
        public string currencyShort { get; set; }
    }
}
