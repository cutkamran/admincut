﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO.Compression;
using System.Configuration;
using System.IO;

namespace DOTWLib.Common
{
  public class WebClient
    {
      public bool Post(string m_data, out string m_response, out string RequestHeader, out string ResponseHeader, out int Status)
      {
          m_response = "";
          RequestHeader = "";
          ResponseHeader = "";
          Status = 0;
          try
          {
              var oRequest = WebRequest.Create(ConfigurationManager.AppSettings["APIURL"]);
              oRequest.Method = "POST";
              oRequest.ContentType = "application/xml";
              oRequest.Headers["Accept-Encoding"] = "gzip, deflate";
              RequestHeader = "ContentType:application/xml Accept-Encoding:gzip, deflate";
              byte[] oRequestBytes = Encoding.ASCII.GetBytes(m_data);
              oRequest.ContentLength = oRequestBytes.Length;
              RequestHeader = RequestHeader + " ContentLength:" + oRequest.ContentLength;
              using (var oRequestStream = oRequest.GetRequestStream())
              {
                  oRequestStream.Write(oRequestBytes, 0, oRequestBytes.Length);
              }
              using (var oResponse = oRequest.GetResponse())
              {
                  using (var oResponseStream = oResponse.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oResponse.ContentType + " Content-Length:" + oResponse.ContentLength;
                      if (m_response.Contains("ErrorList"))
                      {
                          Status = 0;
                          return false;
                      }
                      else
                      {
                          Status = 1;
                          return true;
                      }
                  }
              }
          }
          catch (WebException oWebException)
          {
              if (oWebException.Response != null)
              {
                  using (var oResponseStream = oWebException.Response.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oWebException.Response.ContentType + " Content-Length:" + oWebException.Response.ContentLength;
                  }
              }
              return false;
          }
          catch (Exception oException)
          {
              m_response = oException.Message;
              return false;
          }
      }
    }
}
