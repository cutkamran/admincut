﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Request
{
    public class City
    {
        public string name { get; set; }
        public int code { get; set; }
        public string countryname { get; set; }
        public int countrycode { get; set; }
    }
}
