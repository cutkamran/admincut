﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOTWLib.Request
{
    public class ServiceAddRequest : Common.DOTWWebClient
    {
        public string username { get; set; }
        public string password { get; set; }
        public string id { get; set; }
        public int source { get; set; }
        public string product { get; set; }
        public string command { get; set; }
        public string fDate { get; set; }
        public string tDate { get; set; }
        public Currency currency { get; set; }
        public string rating { get; set; }
        public string hotelname { get; set; }
        public Int64 hotelId { get; set; }
        //public string roomTypeCode { get; set; }
        public Int64 selectedRateBasis { get; set; }
        public int Night { get; set; }
        public List<Room> room { get; set; }
        public List<DOTWLib.Response.RoomType> RoomType { get; set; }
        //public List<DOTWLib.Request.Guest> Guest_listDoTW { get; set; }
        public City city { get; set; }
        public List<HotelOccupancy> HotelOccupancy { get; set; }
        public string GenerateXMLforBooking(List<DOTWLib.Request.Guest> Guest_listDoTW)
        {
            string m_xml = "";
            string[] FirstName;
            //DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
            //DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
            //string ffd = fd.ToString("yyyy-MM-dd");
            //string ttd = td.ToString("yyyy-MM-dd");
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();

                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);

                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                textWriter.WriteValue(fDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                textWriter.WriteValue(tDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("currency");
                textWriter.WriteValue(currency.value);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("rooms");
                textWriter.WriteAttributeString("no", room.Count.ToString());
                #region Rooms tag
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(-1);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("roomTypeSelected");
                    textWriter.WriteStartElement("code");
                    textWriter.WriteValue(RoomType[i].RoomTypeCode);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("selectedRateBasis");
                    textWriter.WriteValue(selectedRateBasis);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("allocationDetails");
                    textWriter.WriteValue(RoomType[i].RoomAllocationDetails);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// for room Tag
                }
                #endregion Rooms tag

                #region Room tag

                textWriter.WriteEndElement();// for rooms Tag
                #endregion Room tag

                textWriter.WriteStartElement("productId");
                textWriter.WriteValue(hotelId); /// Please mark here
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();// for bookingDetails Tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag

                //textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }

        public string GenerateXMLforBooking()
        {
            string m_xml = "";
            //string[] FirstName;
            //DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
            //DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
            //string ffd = fd.ToString("yyyy-MM-dd");
            //string ttd = td.ToString("yyyy-MM-dd");
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();

                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);

                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                textWriter.WriteValue(fDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                textWriter.WriteValue(tDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("currency");
                textWriter.WriteValue(currency.value);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("rooms");
                textWriter.WriteAttributeString("no", room.Count.ToString());
                #region Rooms tag
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(-1);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    //textWriter.WriteStartElement("roomTypeSelected");
                    //textWriter.WriteStartElement("code");
                    //textWriter.WriteValue(RoomType[i].RoomTypeCode);
                    //textWriter.WriteEndElement();
                    //textWriter.WriteStartElement("selectedRateBasis");
                    //textWriter.WriteValue(selectedRateBasis);
                    //textWriter.WriteEndElement();
                    //textWriter.WriteStartElement("allocationDetails");
                    //textWriter.WriteValue(RoomType[i].RoomAllocationDetails);
                    //textWriter.WriteEndElement();
                    //textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// for room Tag
                }
                #endregion Rooms tag

                #region Room tag

                textWriter.WriteEndElement();// for rooms Tag
                #endregion Room tag

                textWriter.WriteStartElement("productId");
                textWriter.WriteValue(hotelId); /// Please mark here
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();// for bookingDetails Tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag

                //textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }

        public string GenerateXMLforChangeOccupancyRequest()
        {
            string m_xml = "";
            string[] FirstName;
            string[] ArrChangedOccupancy;
            //DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
            //DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
            //string ffd = fd.ToString("yyyy-MM-dd");
            //string ttd = td.ToString("yyyy-MM-dd");
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();

                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);

                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                textWriter.WriteValue(fDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                textWriter.WriteValue(tDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("currency");
                textWriter.WriteValue(currency.value);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("rooms");
                textWriter.WriteAttributeString("no", room.Count.ToString());
                #region Rooms tag
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());


                    //////////////////////////////////////////////////////////////////

                    //if (RoomType[i].validForOccupancy != null)
                    //{
                    //    ArrChangedOccupancy = RoomType[i].changedOccupancy.Split(',');

                    //    if (Convert.ToInt32(ArrChangedOccupancy[3]) > 0)
                    //    {
                    //        if (RoomType[i].validForOccupancy.extraBedOccupant == "adult")
                    //        {
                    //            textWriter.WriteStartElement("adultsCode");
                    //            textWriter.WriteValue(Convert.ToInt32(ArrChangedOccupancy[0]) + 1);
                    //            textWriter.WriteEndElement();
                    //        }
                    //    }
                    //    else
                    //    {
                    //        textWriter.WriteStartElement("adultsCode");
                    //        textWriter.WriteValue(ArrChangedOccupancy[0]);
                    //        textWriter.WriteEndElement();
                    //    }

                    //    if (Convert.ToInt32(ArrChangedOccupancy[1]) > 0)
                    //    {
                    //        string[] ArrChildOccupant = ArrChangedOccupancy[2].Split('_');
                    //        textWriter.WriteStartElement("children");  //children start
                    //        textWriter.WriteAttributeString("no", RoomType[i].validForOccupancy.children.ToString());
                    //        for (int l = 0; l < ArrChildOccupant.Length; l++)
                    //        {
                    //            textWriter.WriteStartElement("child"); //child start
                    //            textWriter.WriteAttributeString("runno", l.ToString());
                    //            textWriter.WriteValue(ArrChildOccupant[l].ToString());
                    //            textWriter.WriteEndElement(); //child end
                    //        }
                    //        textWriter.WriteEndElement();  //children end
                    //    }
                    //    else
                    //    {
                    //        textWriter.WriteStartElement("children");  //children start
                    //        textWriter.WriteAttributeString("no", "0");
                    //        //textWriter.WriteValue(room[i].children.Count);
                    //        textWriter.WriteEndElement();  //children end
                    //    }

                    //    //textWriter.WriteStartElement("extraBed");
                    //    //textWriter.WriteValue(RoomType[i].validForOccupancy.extraBed);
                    //    //textWriter.WriteEndElement();
                    //}
                    //else
                    //{
                    //    textWriter.WriteStartElement("adultsCode");
                    //    textWriter.WriteValue(room[i].adults);
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteStartElement("children");
                    //    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    //    for (int j = 0; j < room[i].children.Count; j++)
                    //    {
                    //        textWriter.WriteStartElement("child");
                    //        textWriter.WriteAttributeString("runno", j.ToString());
                    //        textWriter.WriteValue(room[i].children[j].Age);
                    //        textWriter.WriteEndElement();
                    //    }
                    //    textWriter.WriteEndElement();

                    //    //textWriter.WriteStartElement("extraBed");
                    //    //textWriter.WriteValue(0);
                    //    //textWriter.WriteEndElement();
                    //}

                    //////////////////////////////////////////////////////////////////

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(-1);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("roomTypeSelected");
                    textWriter.WriteStartElement("code");
                    textWriter.WriteValue(RoomType[i].RoomTypeCode);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("selectedRateBasis");
                    textWriter.WriteValue(selectedRateBasis);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("allocationDetails");
                    textWriter.WriteValue(RoomType[i].RoomAllocationDetails);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// for room Tag
                }
                #endregion Rooms tag

                #region Room tag

                textWriter.WriteEndElement();// for rooms Tag
                #endregion Room tag

                textWriter.WriteStartElement("productId");
                textWriter.WriteValue(hotelId); /// Please mark here
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();// for bookingDetails Tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag

                //textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
