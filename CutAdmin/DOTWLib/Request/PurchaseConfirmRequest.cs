﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOTWLib.Request
{
    public class PurchaseConfirmRequest : Common.DOTWWebClient
    {
        public string username { get; set; }
        public string password { get; set; }
        public string id { get; set; }
        public int source { get; set; }
        public string product { get; set; }
        public string command { get; set; }
        public string fDate { get; set; }
        public string tDate { get; set; }
        public Currency currency { get; set; }
        public string rating { get; set; }
        public string hotelname { get; set; }
        public Int64 hotelId { get; set; }
        public Int64 selectedRateBasis { get; set; }
        public int Night { get; set; }
        public List<Room> room { get; set; }
        public List<DOTWLib.Response.RoomType> RoomType { get; set; }
        public City city { get; set; }
        public List<HotelOccupancy> HotelOccupancy { get; set; }
        public string GenerateXMLforBooking(List<DOTWLib.Request.Guest> Guest_listDoTW)
        {
            string m_xml = "";
            string[] FirstName;
            string[] ArrChangedOccupancy;
            //DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
            //DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
            //string ffd = fd.ToString("yyyy-MM-dd");
            //string ttd = td.ToString("yyyy-MM-dd");
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();
                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);

                textWriter.WriteStartElement("bookingDetails");

                //textWriter.WriteStartElement("parent");
                //textWriter.WriteValue("");
                //textWriter.WriteEndElement();

                //textWriter.WriteStartElement("bookingCode");
                ////textWriter.WriteValue("");
                //textWriter.WriteEndElement();

                //textWriter.WriteStartElement("addToBookedItn");
                ////textWriter.WriteValue("");
                //textWriter.WriteEndElement();

                //textWriter.WriteStartElement("bookedItnParent");
                ////textWriter.WriteValue("");
                //textWriter.WriteEndElement();

                textWriter.WriteStartElement("fromDate");
                textWriter.WriteValue(fDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                textWriter.WriteValue(tDate);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("currency");
                textWriter.WriteValue(currency.value);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("productId");
                textWriter.WriteValue(hotelId); /// Please mark here
                textWriter.WriteEndElement();


                //textWriter.WriteStartElement("customerReference");
                //textWriter.WriteEndElement();

                textWriter.WriteStartElement("rooms");
                textWriter.WriteAttributeString("no", room.Count.ToString());
                #region Rooms tag
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("roomTypeCode");
                    textWriter.WriteValue(RoomType[i].RoomTypeCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("selectedRateBasis");
                    textWriter.WriteValue(selectedRateBasis);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("allocationDetails");
                    textWriter.WriteValue(RoomType[i].RoomAllocationDetails);
                    textWriter.WriteEndElement();
                    if (RoomType[i].validForOccupancy != null)
                    {
                        ArrChangedOccupancy = RoomType[i].changedOccupancy.Split(',');

                        textWriter.WriteStartElement("adultsCode");
                        textWriter.WriteValue(ArrChangedOccupancy[0]);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("actualAdults");
                        textWriter.WriteValue(room[i].adults);
                        textWriter.WriteEndElement();

                        if (Convert.ToInt32(ArrChangedOccupancy[1]) > 0)
                        {
                            string[] ArrChildOccupant = ArrChangedOccupancy[2].Split('_');
                            textWriter.WriteStartElement("children");  //children start
                            textWriter.WriteAttributeString("no", RoomType[i].validForOccupancy.children.ToString());
                            for (int l = 0; l < ArrChildOccupant.Length; l++)
                            {
                                    textWriter.WriteStartElement("child"); //child start
                                    textWriter.WriteAttributeString("runno", l.ToString());
                                    textWriter.WriteValue(ArrChildOccupant[l].ToString());
                                    textWriter.WriteEndElement(); //child end
                            }
                            textWriter.WriteEndElement();  //children end
                        }
                        else
                        {
                            textWriter.WriteStartElement("children");  //children start
                            textWriter.WriteAttributeString("no", "0");
                            //textWriter.WriteValue(room[i].children.Count);
                            textWriter.WriteEndElement();  //children end

                            //textWriter.WriteStartElement("children");  //children start
                            //textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                            ////textWriter.WriteValue(room[i].children.Count);
                            //for (int j = 0; j < room[i].children.Count; j++)
                            //{
                            //    textWriter.WriteStartElement("child"); //child start
                            //    textWriter.WriteAttributeString("runno", j.ToString());
                            //    textWriter.WriteValue(room[i].children[j].Age);
                            //    textWriter.WriteEndElement(); //child end
                            //}
                            //textWriter.WriteEndElement();  //children end
                        }


                        textWriter.WriteStartElement("actualChildren");
                        textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                        //textWriter.WriteValue(room[i].children.Count);
                        for (int j = 0; j < room[i].children.Count; j++)
                        {
                            textWriter.WriteStartElement("actualChild");
                            textWriter.WriteAttributeString("runno", j.ToString());
                            textWriter.WriteValue(room[i].children[j].Age);
                            textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("extraBed");
                        textWriter.WriteValue(RoomType[i].validForOccupancy.extraBed);
                        textWriter.WriteEndElement();
                    }
                    else
                    {
                        textWriter.WriteStartElement("adultsCode");
                        textWriter.WriteValue(room[i].adults);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("actualAdults");
                        textWriter.WriteValue(room[i].adults);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("children");
                        textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                        //textWriter.WriteValue(room[i].children.Count);
                        for (int j = 0; j < room[i].children.Count; j++)
                        {
                            textWriter.WriteStartElement("child");
                            textWriter.WriteAttributeString("runno", j.ToString());
                            textWriter.WriteValue(room[i].children[j].Age);
                            textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("actualChildren");
                        textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                        //textWriter.WriteValue(room[i].children.Count);
                        for (int j = 0; j < room[i].children.Count; j++)
                        {
                            textWriter.WriteStartElement("actualChild");
                            textWriter.WriteAttributeString("runno", j.ToString());
                            textWriter.WriteValue(room[i].children[j].Age);
                            textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("extraBed");
                        textWriter.WriteValue(0);
                        textWriter.WriteEndElement();
                    }


                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    //textWriter.WriteEndElement();

                    #region passengersDetails
                    //foreach (DOTWLib.Request.Guest guestroom in Guest_listDoTW)
                    //{
                    int k = 0;
                    textWriter.WriteStartElement("passengersDetails");
                    foreach (DOTWLib.Request.Customers customer in Guest_listDoTW[i].sCustomer)
                    {
                        if (RoomType[i].passengerNamesRequiredForBooking == 1)
                        {
                            if (k == 0)
                            {
                                FirstName = customer.Name.Split(' ');
                                //textWriter.WriteStartElement("passengersDetails");
                                textWriter.WriteStartElement("passenger");
                                textWriter.WriteAttributeString("leading", "yes");

                                textWriter.WriteStartElement("salutation");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(147);
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(149);
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(15134);
                                }
                                else
                                {
                                    textWriter.WriteValue(14632);
                                }
                                //textWriter.WriteValue(FirstName[0]);
                                textWriter.WriteEndElement();

                                textWriter.WriteStartElement("firstName");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mr", "").Trim());
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mrs", "").Trim());
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Miss", "").Trim());
                                }
                                else
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Master", "").Trim());
                                }
                                textWriter.WriteEndElement();


                                textWriter.WriteStartElement("lastName");
                                textWriter.WriteValue(customer.LastName);
                                textWriter.WriteEndElement();

                                textWriter.WriteEndElement(); // for passenger tag
                                //textWriter.WriteEndElement(); // for passengersDetails tag
                            }
                        }
                        else
                        {
                            if (k == 0)
                            {
                                FirstName = customer.Name.Split(' ');
                                //textWriter.WriteStartElement("passengersDetails");
                                textWriter.WriteStartElement("passenger");
                                textWriter.WriteAttributeString("leading", "yes");

                                textWriter.WriteStartElement("salutation");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(147);
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(149);
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(15134);
                                }
                                else
                                {
                                    textWriter.WriteValue(14632);
                                }
                                //textWriter.WriteValue(FirstName[0]);
                                textWriter.WriteEndElement();

                                textWriter.WriteStartElement("firstName");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mr", "").Trim());
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mrs", "").Trim());
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Miss", "").Trim());
                                }
                                else
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Master", "").Trim());
                                }
                                textWriter.WriteEndElement();


                                textWriter.WriteStartElement("lastName");
                                textWriter.WriteValue(customer.LastName);
                                textWriter.WriteEndElement();

                                textWriter.WriteEndElement(); // for passenger tag
                                //textWriter.WriteEndElement(); // for passengersDetails tag
                            }
                            else
                            {
                                FirstName = customer.Name.Split(' ');
                                //textWriter.WriteStartElement("passengersDetails");
                                textWriter.WriteStartElement("passenger");
                                textWriter.WriteAttributeString("leading", "no");

                                textWriter.WriteStartElement("salutation");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(147);
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(149);
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(15134);
                                }
                                else
                                {
                                    textWriter.WriteValue(14632);
                                }
                                //textWriter.WriteValue(FirstName[0]);
                                textWriter.WriteEndElement();

                                textWriter.WriteStartElement("firstName");
                                if (FirstName[0] == "Mr")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mr", "").Trim());
                                }
                                else if (FirstName[0] == "Mrs")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Mrs", "").Trim());
                                }
                                else if (FirstName[0] == "Miss")
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Miss", "").Trim());
                                }
                                else
                                {
                                    textWriter.WriteValue(customer.Name.Replace("Master", "").Trim());
                                }
                                textWriter.WriteEndElement();


                                textWriter.WriteStartElement("lastName");
                                textWriter.WriteValue(customer.LastName);
                                textWriter.WriteEndElement();

                                textWriter.WriteEndElement(); // for passenger tag
                                //textWriter.WriteEndElement(); // for passengersDetails tag
                            }
                        }

                        k++;
                    }
                    textWriter.WriteEndElement(); // for passengersDetails tag
                    //}
                    #endregion passengersDetails

                    #region selectedExtraMeals

                    //textWriter.WriteStartElement("selectedExtraMeals");
                    //textWriter.WriteStartElement("mealPlanDate");
                    //textWriter.WriteAttributeString("mealplandatetime", "12 bje"); // here room no goes

                    //textWriter.WriteStartElement("mealPlan");
                    //textWriter.WriteAttributeString("applicablefor", "");
                    //textWriter.WriteAttributeString("childage", "");
                    //textWriter.WriteAttributeString("ispassenger", "");
                    //textWriter.WriteAttributeString("mealscount", "");
                    //textWriter.WriteAttributeString("passengernumber", "");
                    //textWriter.WriteAttributeString("runno", "");

                    //textWriter.WriteStartElement("meal");
                    //textWriter.WriteAttributeString("runno", "");

                    //textWriter.WriteStartElement("mealTypeCode");
                    //textWriter.WriteValue("M-1234");
                    //textWriter.WriteEndElement();

                    //textWriter.WriteStartElement("units");
                    //textWriter.WriteValue("2");
                    //textWriter.WriteEndElement();

                    //textWriter.WriteStartElement("mealPrice");
                    //textWriter.WriteValue("200");
                    //textWriter.WriteEndElement();

                    //textWriter.WriteEndElement(); // for meal
                    //textWriter.WriteEndElement(); // for mealPlan
                    //textWriter.WriteEndElement(); // for mealPlanDate
                    //textWriter.WriteEndElement(); // for selectedExtraMeals tag

                    #endregion selectedExtraMeals

                    #region specialRequests
                    //textWriter.WriteStartElement("specialRequests");
                    //textWriter.WriteStartElement("req");
                    //textWriter.WriteAttributeString("no", "1");
                    //textWriter.WriteValue("yes");

                    //textWriter.WriteEndElement();
                    //textWriter.WriteEndElement(); // for specialRequests tag
                    #endregion specialRequests

                    textWriter.WriteStartElement("beddingPreference");
                    textWriter.WriteValue(0);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// for room Tag
                }
                #endregion Rooms tag

                #region Room tag

                textWriter.WriteEndElement();// for rooms Tag
                #endregion Room tag

                textWriter.WriteEndElement();// for bookingDetails Tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag

                // textWriter.WriteEndDocument();




            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
