﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Request
{
    public class Currency
    {
        public string shortcut { get; set; }
        public int value { get; set; }
        public string text { get; set; }
    }
}
