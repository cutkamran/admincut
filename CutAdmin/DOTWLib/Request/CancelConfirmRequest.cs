﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace DOTWLib.Request
{
    public class CancelConfirmRequest : Common.DOTWWebClient
    {
        public string Language { get; set; }


        public string UserName { get; set; }
        public string Password { get; set; }
        public string ComapnyId { get; set; }
        public string EchoToken { get; set; }
        public string RoomCode { get; set; }
        public string IncomingOffice { get; set; }
        public string CancelCharge { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();

                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(ComapnyId);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue("1");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", "cancelbooking");


                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("bookingType");
                textWriter.WriteValue("1");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("bookingCode");
                textWriter.WriteValue(RoomCode);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("confirm");
                textWriter.WriteValue("no");
                textWriter.WriteEndElement();


                //textWriter.WriteStartElement("testPricesAndAllocation");

                //textWriter.WriteStartElement("service");
                //textWriter.WriteAttributeString("referencenumber", RoomCode);

                //textWriter.WriteStartElement("penaltyApplied");
                //textWriter.WriteValue(CancelCharge);
                //textWriter.WriteEndElement(); // for penaltyApplied

                //textWriter.WriteEndElement(); // for service
                //textWriter.WriteEndElement(); // for testPricesAndAllocation
                textWriter.WriteEndElement(); // for bookingDetails tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag



                //textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }

        public string GenerateCXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();

                textWriter.WriteStartElement("customer");


                textWriter.WriteStartElement("username");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(ComapnyId);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue("1");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", "cancelbooking");


                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("bookingType");
                textWriter.WriteValue("1");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("bookingCode");
                textWriter.WriteValue(RoomCode);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("confirm");
                textWriter.WriteValue("yes");
                textWriter.WriteEndElement();


                textWriter.WriteStartElement("testPricesAndAllocation");

                textWriter.WriteStartElement("service");
                textWriter.WriteAttributeString("referencenumber", RoomCode);
                //if (Convert.ToSingle(CancelCharge) > 0)
                //{
                textWriter.WriteStartElement("penaltyApplied");
                textWriter.WriteValue(CancelCharge);
                textWriter.WriteEndElement(); // for penaltyApplied
                //}
                //else
                //{
                //    textWriter.WriteStartElement("penaltyApplied");
                //    //textWriter.WriteValue(CancelCharge);
                //    textWriter.WriteEndElement(); // for penaltyApplied
                //}

                textWriter.WriteEndElement(); // for service
                textWriter.WriteEndElement(); // for testPricesAndAllocation
                textWriter.WriteEndElement(); // for bookingDetails tag
                textWriter.WriteEndElement(); // for request Tag
                textWriter.WriteEndElement();// for customer tag



                //textWriter.WriteEndDocument();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}
