﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Request
{
    public class Rooms
    {
        public int roomno { get; set; }
        public List<Room> room { get; set; }
    }
}
