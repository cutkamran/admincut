﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.Xml;

namespace DOTWLib.Request
{
    public class DOTWValuedAvailRQ : Common.DOTWWebClient
    {
        public string username { get; set; }
        public string password { get; set; }
        public string id { get; set; }
        public int source { get; set; }
        public string product { get; set; }
        public string command { get; set; }
        public string fDate { get; set; }
        public string tDate { get; set; }
        public Currency currency { get; set; }
        public string rating { get; set; }
        public string hotelname { get; set; }
        //public Rooms rooms { get; set; }
        public List<Room> room { get; set; }
        public City city { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();
                textWriter.WriteStartElement("customer");

                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);

                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                textWriter.WriteValue("2015-06-30");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                textWriter.WriteValue("2015-07-01");
                textWriter.WriteEndElement();

                if (currency != null)
                {
                    textWriter.WriteStartElement("currency");
                    textWriter.WriteValue(currency.value);
                    textWriter.WriteEndElement();
                }

                textWriter.WriteStartElement("rooms");
                textWriter.WriteAttributeString("no", room.Count.ToString());

                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    //textWriter.WriteValue(room[i].children.Count);
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(room[i].rateBasis);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();              
                }
                textWriter.WriteEndElement(); // rooms end

                textWriter.WriteEndElement(); //bookingdetails end
               
                textWriter.WriteStartElement("return"); 

                textWriter.WriteStartElement("filters");

                textWriter.WriteStartElement("city");
                textWriter.WriteValue(city.code);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("noPrice");
                textWriter.WriteValue("true");
                textWriter.WriteEndElement();

                textWriter.WriteEndElement(); //filter end
                textWriter.WriteStartElement("fields");

                textWriter.WriteStartElement("field");
                textWriter.WriteValue("preferred");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("hotelName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("address");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("location");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("cityCode");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("amenitie");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("leisure");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("rating");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("images");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("geoPoint");
                textWriter.WriteEndElement();

                textWriter.WriteEndElement(); // fields end

                textWriter.WriteEndElement(); // return end
              
                textWriter.WriteEndElement(); //request end

                textWriter.WriteEndElement(); //customer end
               // textWriter.WriteEndDocument();

            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;
        }

    }
}
