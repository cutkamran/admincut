﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOTWLib.Request
{
    public class Room
    {
        public int adults { get; set; }
        public List<Child> children { get; set; }
        public int rateBasis { get; set; }
        public passengerNationality passengerNationality { get; set; }
        public passengerCountryOfResidence passengerCountryOfResidence { get; set; }
    }
}
