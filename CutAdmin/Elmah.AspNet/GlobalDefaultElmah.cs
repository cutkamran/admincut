﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Elmah
{
    public class GlobalDefaultElmah
    {
        public Int64 sid;
        public string uid;
        public string UserType;
        public string password;
        public string Supplier;
        public string AgencyName;
        public string AgencyLogo;
        public string AgencyType;
        public string MerchantID;
        public string ContactPerson;
        public string Last_Name;
        public string Designation;
        public Int64 ContactID;
        public Int64 RoleID;
        public DateTime Validity;
        public DateTime dtLastAccess;
        public string Remarks;
        public string ValidationCode;
        public bool LoginFlag;
        public string RefAgency;
        public string PANNo;
        public string ServiceTaxNO;
        public string Donecarduser;
        public string MerchantURL;
        public bool TDSFlag;
        public bool EnableDeposit;
        public bool EnableCard;
        public bool EnableCheckAccount;
        public string Agentuniquecode;
        public string Staffuniquecode;
        public DateTime Updatedate;
        public string FailedReports;
        public string BookingDate;
        public string Bankdetails;
        public string IATANumber;
        public string DistAgencyName;
        public string agentCategory;
        public Int64 ParentId;
        public Int64 GroupId;
        public float AvailableCrdit;
        public float CreditLimit;
        public float OTC;
        public bool IsFirstLogIn;
        public string Currency;
        public string Franchisee;
        public bool SupplierVisible;
        public string CityAllocated;
        public string TerritoryType;
        public string CountryID;
        public string CitiesID; 
        public string StateID;
        public string Agents;
        public string AgentCity;
        public bool IsB2C;
        public string AdminURL;
        // GST Details
        public string SupplierState { get; set; }
        public string PurchaserGstState { get; set; }


        // FIOR B2C
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string UserName;
        public string B2C_Id;
        public string Phone;
        public string City;
        public string Address;
        public string Password;
        public string Confirm_Password;

    }
  
}