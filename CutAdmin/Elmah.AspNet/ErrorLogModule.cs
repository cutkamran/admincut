#region License, Terms and Author(s)
//
// ELMAH - Error Logging Modules and Handlers for ASP.NET
// Copyright (c) 2004-9 Atif Aziz. All rights reserved.
//
//  Author(s):
//
//      Atif Aziz, http://www.raboof.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#endregion

[assembly: Elmah.Scc("$Id: ErrorLogModule.cs 923 2011-12-23 22:02:10Z azizatif $")]

namespace Elmah
{
    using Elmah;

    #region Imports

    using System;

    using System.Web;

    using Trace = System.Diagnostics.Trace;
    using System.Data.SqlClient;
    using System.Configuration;
    using System.Data;
    #endregion

    /// <summary>
    /// HTTP module implementation that logs unhandled exceptions in an
    /// ASP.NET Web application to an error log.
    /// </summary>

    

    public class ErrorLogModule :  HttpModuleBase, IExceptionFiltering  
    {
        public event ExceptionFilterEventHandler Filtering;
        public event ErrorLoggedEventHandler Logged;

        /// <summary>
        /// Initializes the module and prepares it to handle requests.
        /// </summary>
        
        protected override void OnInit(HttpApplication application)
        {
            if (application == null)
                throw new ArgumentNullException("application");
            
            application.Error += OnError;
            ErrorSignal.Get(application).Raised += OnErrorSignaled;
        }

        /// <summary>
        /// Gets the <see cref="ErrorLog"/> instance to which the module
        /// will log exceptions.
        /// </summary>
        
        protected virtual ErrorLog GetErrorLog(HttpContextBase context)
        {
            return ErrorLog.GetDefault(context);
        }

        /// <summary>
        /// The handler called when an unhandled exception bubbles up to 
        /// the module.
        /// </summary>

        protected virtual void OnError(object sender, EventArgs args)
        {
            var application = (HttpApplication) sender;
            LogException(application.Server.GetLastError(), new HttpContextWrapper(application.Context));
        }

        /// <summary>
        /// The handler called when an exception is explicitly signaled.
        /// </summary>

        protected virtual void OnErrorSignaled(object sender, ErrorSignalEventArgs args)
        {
            using (args.Exception.TryScopeCallerInfo(args.CallerInfo))
                LogException(args.Exception, args.Context);
        }

        //public static int GenerateRandomNumber()
        //{
        //    int rndnumber = 0;
        //    Random rnd = new Random((int)DateTime.Now.Ticks);
        //    rndnumber = rnd.Next();
        //    return rndnumber;
        //}

        protected virtual void LogException(Exception e, HttpContextBase context)
        {
            if (e == null)
                throw new ArgumentNullException("e");

            var args = new ExceptionFilterEventArgs(e, context);
            OnFiltering(args);
            
            if (args.Dismissed)
                return;

            ErrorLogEntry entry = null;

            try
            {
                var error = new Error(e, context);

                if (error == null)
                    throw new ArgumentNullException("error");

                string errorXml = ErrorXml.EncodeString(error);

                var objUser = (dynamic)context.Session["LoginUser"];
                //int ErrorID = GenerateRandomNumber();
               
                string UserID = "";
                if (context.Session["LoginUser"] != null)
                {
                    UserID = objUser.sid.ToString();
                }

                int rowsAffected = 0;
                SqlParameter[] SQLParams = new SqlParameter[9];
                SQLParams[0] = new SqlParameter("@Application", "");
                SQLParams[1] = new SqlParameter("@Host", error.HostName);
                SQLParams[2] = new SqlParameter("@Type", error.Type);
                SQLParams[3] = new SqlParameter("@Source", error.Source);
                SQLParams[4] = new SqlParameter("@Message", error.Message);
                SQLParams[5] = new SqlParameter("@User", UserID);
                SQLParams[6] = new SqlParameter("@AllXml", errorXml);
                SQLParams[7] = new SqlParameter("@StatusCode", error.StatusCode);
                SQLParams[8] = new SqlParameter("@TimeUtc", error.Time);
                
                DBHelperElmah.DBReturnCode retCode = DBHelperElmah.ExecuteNonQuery("Proc_ELMAH_LogError", out rowsAffected, SQLParams);
                //  return retCode;


               // MySqlParameterCollection parameters = command.Parameters;

               // parameters.Add("?Application", MySqlDbType.String, 60).Value = "";
               // parameters.Add("?Host", MySqlDbType.String, 30).Value = error.HostName;
               // parameters.Add("?Type", MySqlDbType.String, 100).Value = error.Type;
               // parameters.Add("?Source", MySqlDbType.String, 60).Value = error.Source;
               // parameters.Add("?Message", MySqlDbType.String, 500).Value = error.Message;
               // parameters.Add("?User", MySqlDbType.String, 50).Value = error.User;
               // parameters.Add("?StatusCode", MySqlDbType.Int64).Value = error.StatusCode;
               // parameters.Add("?TimeUtc", MySqlDbType.Datetime).Value = error.Time.ToUniversalTime();
               // parameters.Add("?AllXml", MySqlDbType.String).Value = errorXml;

               // connection.Open();

               // return
               //command.ExecuteNonQuery().ToString();



                //var log = GetErrorLog(context);
                //error.ApplicationName = log.ApplicationName;
                //var id = log.Log(error);
                //entry = new ErrorLogEntry(log, id, error);
            }
            catch (Exception localException)
            {
                //
                // IMPORTANT! We swallow any exception raised during the 
                // logging and send them out to the trace . The idea 
                // here is that logging of exceptions by itself should not 
                // be  critical to the overall operation of the application.
                // The bad thing is that we catch ANY kind of exception, 
                // even system ones and potentially let them slip by.
                //

                Trace.WriteLine(localException);
            }

            if (entry != null)
                OnLogged(new ErrorLoggedEventArgs(entry));
        }

        /// <summary>
        /// Raises the <see cref="Logged"/> event.
        /// </summary>

        protected virtual void OnLogged(ErrorLoggedEventArgs args)
        {
            var handler = Logged;

            if (handler != null)
                handler(this, args);
        }

        /// <summary>
        /// Raises the <see cref="Filtering"/> event.
        /// </summary>

        protected virtual void OnFiltering(ExceptionFilterEventArgs args)
        {
            var handler = Filtering;
            
            if (handler != null)
                handler(this, args);
        }

        /// <summary>
        /// Determines whether the module will be registered for discovery
        /// in partial trust environments or not.
        /// </summary>

        protected override bool SupportDiscoverability
        {
            get { return true; }
        }
    }

    public delegate void ErrorLoggedEventHandler(object sender, ErrorLoggedEventArgs args);

    [ Serializable ]
    public sealed class ErrorLoggedEventArgs : EventArgs
    {
        private readonly ErrorLogEntry _entry;

        public ErrorLoggedEventArgs(ErrorLogEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");

            _entry = entry;
        }

        public ErrorLogEntry Entry
        {
            get { return _entry; }
        }
    }
}
