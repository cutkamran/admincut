﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request.Availability
{
    public class Room
    {
        public int adults { get; set; }
        public List<int> children_ages { get; set; }
    }
}
