﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request.Availability
{
    public class Occupancy
    {
        public int RoomCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public List<Customer> GuestList { get; set; }
    }
}
