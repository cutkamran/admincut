﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace GRNLib.Request.Availability 
{
    public class AvailRequest : Common.WebClient
    {
        public GRNAvailReq request { get; set; }
        public string GenerateJson()
        {
           
            string Json = JsonConvert.SerializeObject(request);
            return Json;
        }
       
    }

    public class GRNAvailReq
    {
        //public string API_Key { get; set; }
        //public string Accept { get; set; }
        //public string ContentType { get; set; }
        public string destination_code { get; set; }
        public string hotelname { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string client_nationality { get; set; }
        public string response { get; set; }
        public string currency { get; set; }
        public string rates { get; set; }
        //public List<int> hotel_category { get; set; }
        public List<Room> rooms { get; set; }
    }
}
