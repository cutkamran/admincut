﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request.Booking
{
    public class HotelBookingRequest:Common.WebClient
    {
        public HotelBookingReq request;
        public string GenerateJson()
        {
            string Json = JsonConvert.SerializeObject(request);
            return Json;
        }
    }

    public class HotelBookingReq
    {
        public string search_id { get; set; }
        public string hotel_code { get; set; }
        public string city_code { get; set; }
        public string group_code { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string booking_comments { get; set; }
        public string payment_type { get; set; }
        public List<BookingItem> booking_items { get; set; }
        public Holder holder { get; set; }
    }
}
