﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request.Booking
{
    public class Pax
    {
        public string title { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string type { get; set; }
        public int age { get; set; }
    }
}
