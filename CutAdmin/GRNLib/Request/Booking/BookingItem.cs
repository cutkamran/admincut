﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request.Booking
{
    public class BookingItem
    {
        public string room_code { get; set; }
        public string rate_key { get; set; }
        public List<Room> rooms { get; set; }
    }
}
