﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Request
{
    public class CancellBooking : Common.WebClient
    {
        public string comments { get; set; }
        public string GenerateJson()
        {
            string Json = JsonConvert.SerializeObject(comments);
            return Json;
        }
    }
}
