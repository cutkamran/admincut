﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class SurchargeOrTax
    {
        public string name { get; set; }
        public bool mandatory { get; set; }
        public bool included { get; set; }
        public string currency { get; set; }
        public string amount_type { get; set; }
        public double amount { get; set; }
    }
}
