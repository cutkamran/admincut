﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class RateComments
    {
        public string checkinInstruction  { get; set; }
        public string comments { get; set; }
    }
}
