﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class Net
    {
        public string name { get; set; }
        public bool included { get; set; }
        public string currency { get; set; }
        public string amount_type { get; set; }
        public float amount { get; set; }
    }
}
