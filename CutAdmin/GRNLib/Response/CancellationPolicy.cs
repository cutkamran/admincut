﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class CancellationPolicy
    {
        public bool under_cancellation { get; set; }
        public List<Detail> details { get; set; }
        public string cancel_by_date { get; set; }
        public string amount_type { get; set; }
    }
}
