﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class HotelList
    {
        public string search_id { get; set; }
        public List<string> FacilitiesList { get; set; }
        public int no_of_rooms { get; set; }
        public int no_of_nights { get; set; }
        public int no_of_hotels { get; set; }
        public int no_of_children { get; set; }
        public int no_of_adults { get; set; }
        public bool more_results { get; set; }
        public List<Hotel> hotels { get; set; }
        public string checkout { get; set; }
        public string checkin { get; set; }
    }
}
