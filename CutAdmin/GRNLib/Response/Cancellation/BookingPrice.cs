﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Cancellation
{
    public class BookingPrice
    {
        public string currency { get; set; }
        public double amount { get; set; }
    }
}
