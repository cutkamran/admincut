﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Cancellation
{
    public class CancellationResponse
    {
        public string status { get; set; }
        public string cancellation_reference { get; set; }
        public string cancellation_comments { get; set; }
        public CancellationCharges cancellation_charges { get; set; }
        public string booking_reference { get; set; }
        public BookingPrice booking_price { get; set; }
        public string booking_id { get; set; }
    }
}
