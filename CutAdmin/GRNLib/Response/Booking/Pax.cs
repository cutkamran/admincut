﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class Pax
    {
        public string type { get; set; }
        public string surname { get; set; }
        public int pax_id { get; set; }
        public string name { get; set; }
    }
}
