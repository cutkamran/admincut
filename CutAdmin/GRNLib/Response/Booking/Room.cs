﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class Room
    {
        public string room_type { get; set; }
        public string room_reference { get; set; }
        public List<int> pax_ids { get; set; }
        public int no_of_children { get; set; }
        public int no_of_adults { get; set; }
        public string description { get; set; }
    }
}
