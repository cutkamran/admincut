﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class Price
    {
        public double total { get; set; }
        public Breakdown breakdown { get; set; }
    }
}
