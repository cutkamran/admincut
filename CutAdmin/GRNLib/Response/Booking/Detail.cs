﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class Detail
    {
        public string from { get; set; }
        public double flat_fee { get; set; }
        public string currency { get; set; }
    }
}
