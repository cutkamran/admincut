﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class BookingItem
    {
        public List<Room> rooms { get; set; }
        public float price { get; set; }
        public bool non_refundable { get; set; }
        public bool includes_boarding { get; set; }
        public string currency { get; set; }
        public CancellationPolicy cancellation_policy { get; set; }
        public List<string> boarding_details { get; set; }
    }
}
