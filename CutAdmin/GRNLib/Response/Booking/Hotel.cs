﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class Hotel
    {
        public List<Pax> paxes { get; set; }
        public string name { get; set; }
        public string hotel_code { get; set; }
        public Geolocation geolocation { get; set; }
        public string description { get; set; }
        public string city_code { get; set; }
        public int category { get; set; }
        public List<BookingItem> booking_items { get; set; }
        public string address { get; set; }
    }
}
