﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.Booking
{
    public class BookingResponse
    {
        public bool supports_cancellation { get; set; }
        public bool supports_amendment { get; set; }
        public string status { get; set; }
        public Price price { get; set; }
        public string payment_type { get; set; }
        public string payment_status { get; set; }
        public Hotel hotel { get; set; }
        public GRNLib.Request.Booking.Holder holder { get; set; }
        public string currency { get; set; }
        public string checkout { get; set; }
        public string checkin { get; set; }
        public string booking_reference { get; set; }
        public string booking_id { get; set; }
        public string booking_date { get; set; }
        public string booking_comment { get; set; }
        public AdditionalInfo additional_info { get; set; }
    }
}
