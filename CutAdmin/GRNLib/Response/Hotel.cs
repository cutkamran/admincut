﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class Hotel
    {
        public bool recommended { get; set; }
        public List<Rate> rates { get; set; }
        public string name { get; set; }
        public Images images { get; set; }
        public string hotel_code { get; set; }
        public Geolocation geolocation { get; set; }
        public string facilities { get; set; }
        public string description { get; set; }
        public string country { get; set; }
        public string city_code { get; set; }
        public double category { get; set; }
        public string address { get; set; }
        public float MaxCutPrice { get; set; }
        public float AgentMarkup { get; set; }
        public List<string> FacilitiesList { get; set; }
    }
}
