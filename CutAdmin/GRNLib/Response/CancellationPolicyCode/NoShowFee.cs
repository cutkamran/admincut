﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.CancellationPolicyCode
{
    public class NoShowFee
    {
        public float flat_fee { get; set; }
        public string currency { get; set; }
        public string amount_type { get; set; }
    }
}
