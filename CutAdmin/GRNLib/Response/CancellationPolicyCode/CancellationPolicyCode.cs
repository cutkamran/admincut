﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.CancellationPolicyCode
{
    public class CancellationPolicyCode
    {
        public bool under_cancellation { get; set; }
        public NoShowFee no_show_fee { get; set; }
        public List<Detail> details { get; set; }
        public string cancellation_policy_code { get; set; }
        public string amount_type { get; set; }
    }
}
