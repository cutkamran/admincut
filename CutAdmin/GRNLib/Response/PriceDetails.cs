﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class PriceDetails
    {
        public List<Net> NetRate { get; set; }
        public List<GST> GST { get; set; }
        public List<HotelCharge> hotel_charges { get; set; }
        public List<SurchargeOrTax> surcharge_or_tax { get; set; }
    }
}
