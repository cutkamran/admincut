﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class Rate
    {
        public bool supports_cancellation { get; set; }
        public bool supports_amendment { get; set; }
        public List<HotelRoom> rooms { get; set; }
        public string room_code { get; set; }
        public string rate_type { get; set; }
        public string rate_key { get; set; }
        public List<RateComments> rate_comments { get; set; }
        public PriceDetails price_details { get; set; }
        public float price { get; set; }
        public List<string> payment_type { get; set; }
        public bool non_refundable { get; set; }
        public int no_of_rooms { get; set; }
        public bool includes_boarding { get; set; }
        public bool has_promotions { get; set; }
        public string group_code { get; set; }
        public string currency { get; set; }
        public CancellationPolicy cancellation_policy { get; set; }
        public List<string> boarding_details { get; set; }
        public int allotment { get; set; }
        public string cancellation_policy_code { get; set; }
        public string availability_status { get; set; }
        public List<string> promotions_details { get; set; }
        public List<string> allowed_card_types { get; set; }
        public bool includes_wifi { get; set; }
        // For CUT use only
        public float CUTRoomPrice { get; set; }
        public float CUTRoomMarkup { get; set; }
        public float AgentPrice { get; set; }
        public float AgentMarkup { get; set; }
        public float ServiceTax { get; set; }
    }
}
