﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class Location
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
