﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class Detail
    {
        public string from { get; set; }
        public float flat_fee { get; set; }
        public Int64 percent { get; set; }
        public Int64 nights { get; set; }
        public float CUTCancellationPrice { get; set; }
        public float AgentCancellationPrice { get; set; }
        public string currency { get; set; }
        public bool allowed_cancellation { get; set; }
    }
}
