﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response.ImagesRes
{
    public class Images
    {
        public List<Small> small { get; set; }
        public List<Regular> regular { get; set; }
        public List<Large> large { get; set; }
    }
}
