﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRNLib.Response
{
    public class HotelRoom
    {
        public string room_type { get; set; }
        public int no_of_rooms { get; set; }
        public int no_of_children { get; set; }
        public int no_of_adults { get; set; }
        public string description { get; set; }
        public List<int> children_ages { get; set; }
    }
}
