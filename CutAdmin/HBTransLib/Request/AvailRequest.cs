﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.Xml;

namespace HBTransLib.Request
{
    public class AvailRequest : Common.WebClient
    {

        public string Language { get; set; }
        public string sessionId { get; set; }
        public string echoToken { get; set; }
        public int PageNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Return { get; set; }
        public string PickUpDate { get; set; }
        public string ArrivalPickUpTime { get; set; }
        public string ArrivalDropTime { get; set; }

        public string PickUpTime { get; set; }
        public string AdultCount { get; set; }
        public string ChildCount { get; set; }
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }

        public string PickUpTerminalCode { get; set; }
        public string DropTerminalCode { get; set; }

        public string PickUpHotelCode { get; set; }
        public string DropHotelCode { get; set; }

        public string PickUpLatitude { get; set; }
        public string PickUpLongitude { get; set; }
        public string DropLatitude { get; set; }
        public string DropLongitude { get; set; }

        public string PickUpAddress { get; set; }
        public string PickUpAddressDescription { get; set; }
        public string PickUpCountry { get; set; }
        public string PickUpCity { get; set; }
        public string PickUpState { get; set; }
        public string PickUpZipCode { get; set; }

        public string DropAddress { get; set; }
        public string DropAddressDescription { get; set; }
        public string DropCountry { get; set; }
        public string DropCity { get; set; }
        public string DropState { get; set; }
        public string DropZipCode { get; set; }

        public string ReturnDate { get; set; }
        public string DeparturePickUpTime { get; set; }
        public string DepartureDropTime { get; set; }

        public string ChildAge1 { get; set; }
        public string ChildAge2 { get; set; }


        public string GenerateXML()
        {
            string m_xml = "";

            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("TransferValuedAvailRQ");
                textWriter.WriteAttributeString("echoToken", echoToken);
                textWriter.WriteAttributeString("sessionId", sessionId);
                textWriter.WriteAttributeString("version", "2013/12");
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages TransferValuedAvailRQ.xsd");

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue("ENG");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Credentials");// start of credentials

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement(); // end of credentials



                #region In

                textWriter.WriteStartElement("AvailData"); // start of AvailData
                textWriter.WriteAttributeString("type", "IN");

                textWriter.WriteStartElement("ServiceDate");
                textWriter.WriteAttributeString("date", PickUpDate);
                textWriter.WriteAttributeString("time", "0600");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Occupancy");// start of occupancy

                textWriter.WriteStartElement("AdultCount");
                textWriter.WriteValue(AdultCount);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("ChildCount");
                textWriter.WriteValue(ChildCount);
                textWriter.WriteEndElement();

                if (Convert.ToInt64(ChildCount) > 0)
                {
                    textWriter.WriteStartElement("GuestList");
                    //for (int i = 0; i < Convert.ToInt64(ChildCount); i++)
                    //{


                        
                        if (Convert.ToInt64(ChildCount) == 1)
                        {
                            textWriter.WriteStartElement("Customer");
                            textWriter.WriteAttributeString("type", "CH");
                            textWriter.WriteStartElement("Age");
                            textWriter.WriteValue(ChildAge1);
                            textWriter.WriteEndElement();
                            textWriter.WriteEndElement();
                        }
                        else
                        {
                            textWriter.WriteStartElement("Customer");
                            textWriter.WriteAttributeString("type", "CH");
                            textWriter.WriteStartElement("Age");
                            textWriter.WriteValue(ChildAge1);
                            textWriter.WriteEndElement();
                            textWriter.WriteEndElement();

                            textWriter.WriteStartElement("Customer");
                            textWriter.WriteAttributeString("type", "CH");
                            textWriter.WriteStartElement("Age");
                            textWriter.WriteValue(ChildAge2);
                            textWriter.WriteEndElement();
                            textWriter.WriteEndElement();
                        }
                        

                        
                    //}
                    textWriter.WriteEndElement();
                }

                textWriter.WriteEndElement(); //end of occupancy

                // here will'll have to apply condition according to product type...

                #region PickUp

                if (PickUpProductType == "ProductTransferTerminal")
                {
                    textWriter.WriteStartElement("PickupLocation"); // start of pickup location
                    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(PickUpTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("date", PickUpDate);
                    if (ArrivalPickUpTime != "")
                    {
                        textWriter.WriteAttributeString("time", ArrivalPickUpTime);
                    }
                    else
                    {
                        textWriter.WriteAttributeString("time", "0600");
                    }

                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of pickuplocation
                }
                else if (PickUpProductType == "ProductTransferHotel")
                {
                    textWriter.WriteStartElement("PickupLocation"); // start of pickup location
                    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(PickUpHotelCode);
                    textWriter.WriteEndElement();

                    //textWriter.WriteStartElement("DateTime");
                    //textWriter.WriteAttributeString("date", PickUpDate);
                    //textWriter.WriteAttributeString("time", "1101");
                    //textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of pickuplocation

                }
                else
                {
                    textWriter.WriteStartElement("DestinationLocation"); // start of Pickup location
                    textWriter.WriteAttributeString("xsi:type", PickUpProductType);
                    textWriter.WriteStartElement("Coordinates");
                    textWriter.WriteAttributeString("latitude", PickUpLatitude);
                    textWriter.WriteAttributeString("longitude", PickUpLongitude);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("Description");
                    textWriter.WriteStartElement("<![CDATA[" + PickUpAddressDescription + "]]>");
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Address");
                    textWriter.WriteStartElement("<![CDATA[" + PickUpAddress + "]]>");
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("City");
                    textWriter.WriteStartElement(PickUpCity);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("ZipCode");
                    textWriter.WriteStartElement(PickUpZipCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Country");
                    textWriter.WriteStartElement(PickUpCountry);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of PickUp location
                }
                #endregion

                #region Drop

                if (DropProductType == "ProductTransferTerminal")
                {
                    textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                    textWriter.WriteAttributeString("xsi:type", DropProductType);
                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(DropTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("date", PickUpDate);
                    if (ArrivalDropTime != "")
                    {
                        textWriter.WriteAttributeString("time", ArrivalDropTime);
                    }
                    else
                    {
                        textWriter.WriteAttributeString("time", "2000");
                    }
                    //textWriter.WriteAttributeString("time", "2000");
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Drop location
                }
                else if (DropProductType == "ProductTransferHotel")
                {
                    textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                    textWriter.WriteAttributeString("xsi:type", DropProductType);
                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(DropHotelCode);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();//end of Drop location
                }

                else
                {
                    textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                    textWriter.WriteAttributeString("xsi:type", DropProductType);
                    textWriter.WriteStartElement("Coordinates");
                    textWriter.WriteAttributeString("latitude", DropLatitude);
                    textWriter.WriteAttributeString("longitude", DropLongitude);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("Description");
                    textWriter.WriteStartElement("<![CDATA[" + DropAddressDescription + "]]>");
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Address");
                    textWriter.WriteStartElement("<![CDATA[" + DropAddress + "]]>");
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("City");
                    textWriter.WriteStartElement(DropCity);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("ZipCode");
                    textWriter.WriteStartElement(DropZipCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Country");
                    textWriter.WriteStartElement(DropCountry);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Drop location


                }
                #endregion


                textWriter.WriteEndElement();//for available data
                #endregion

                if (ReturnDate != "")
                {
                    #region OUT
                    textWriter.WriteStartElement("AvailData"); // start of AvailData
                    textWriter.WriteAttributeString("type", "OUT");

                    textWriter.WriteStartElement("ServiceDate");
                    textWriter.WriteAttributeString("date", ReturnDate);
                    textWriter.WriteAttributeString("time", "0600");
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Occupancy");// start of occupancy

                    textWriter.WriteStartElement("AdultCount");
                    textWriter.WriteValue(AdultCount);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("ChildCount");
                    textWriter.WriteValue(ChildCount);
                    textWriter.WriteEndElement();

                    if (Convert.ToInt64(ChildCount) > 0)
                    {
                        textWriter.WriteStartElement("GuestList");
                        //for (int i = 0; i < Convert.ToInt64(ChildCount); i++)
                        //{
                            if (Convert.ToInt64(ChildCount) == 1)
                            {
                                textWriter.WriteStartElement("Customer");
                                textWriter.WriteAttributeString("type", "CH");
                                textWriter.WriteStartElement("Age");
                                textWriter.WriteValue(ChildAge1);
                                textWriter.WriteEndElement();
                                textWriter.WriteEndElement();
                            }
                            else
                            {
                                textWriter.WriteStartElement("Customer");
                                textWriter.WriteAttributeString("type", "CH");
                                textWriter.WriteStartElement("Age");
                                textWriter.WriteValue(ChildAge1);
                                textWriter.WriteEndElement();
                                textWriter.WriteEndElement();

                                textWriter.WriteStartElement("Customer");
                                textWriter.WriteAttributeString("type", "CH");
                                textWriter.WriteStartElement("Age");
                                textWriter.WriteValue(ChildAge2);
                                textWriter.WriteEndElement();
                                textWriter.WriteEndElement();
                            }
                        //}
                        textWriter.WriteEndElement();
                    }

                    textWriter.WriteEndElement(); //end of occupancy

                    // here will'll have to apply condition according to product type...

                    #region PickUp

                    if (DropProductType == "ProductTransferTerminal")
                    {
                        textWriter.WriteStartElement("PickupLocation"); // start of pickup location
                        textWriter.WriteAttributeString("xsi:type", DropProductType);

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(DropTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("date", ReturnDate);
                        if (DeparturePickUpTime != "")
                        {
                            textWriter.WriteAttributeString("time", DeparturePickUpTime);
                        }
                        else
                        {
                            textWriter.WriteAttributeString("time", "0600");
                        }
                        //textWriter.WriteAttributeString("time", PickUpTime);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of pickuplocation
                    }
                    else if (DropProductType == "ProductTransferHotel")
                    {
                        textWriter.WriteStartElement("PickupLocation"); // start of pickup location
                        textWriter.WriteAttributeString("xsi:type", DropProductType);

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(DropHotelCode);
                        textWriter.WriteEndElement();

                        //textWriter.WriteStartElement("DateTime");
                        //textWriter.WriteAttributeString("date", ReturnDate);
                        //textWriter.WriteAttributeString("time", PickUpTime);
                        //textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of pickuplocation

                    }
                    else
                    {
                        textWriter.WriteStartElement("DestinationLocation"); // start of Pickup location
                        textWriter.WriteAttributeString("xsi:type", PickUpProductType);
                        textWriter.WriteStartElement("Coordinates");
                        textWriter.WriteAttributeString("latitude", PickUpLatitude);
                        textWriter.WriteAttributeString("longitude", PickUpLongitude);
                        textWriter.WriteEndElement();
                        textWriter.WriteStartElement("Description");
                        textWriter.WriteStartElement("<![CDATA[" + PickUpAddressDescription + "]]>");
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Address");
                        textWriter.WriteStartElement("<![CDATA[" + PickUpAddress + "]]>");
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("City");
                        textWriter.WriteStartElement(PickUpCity);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("ZipCode");
                        textWriter.WriteStartElement(PickUpZipCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Country");
                        textWriter.WriteStartElement(PickUpCountry);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of PickUp location
                    }
                    #endregion



                    #region Drop

                    if (PickUpProductType == "ProductTransferTerminal")
                    {
                        textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                        textWriter.WriteAttributeString("xsi:type", PickUpProductType);
                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(PickUpTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("date", ReturnDate);
                        if (DepartureDropTime != "")
                        {
                            textWriter.WriteAttributeString("time", DepartureDropTime);
                        }
                        else
                        {
                            textWriter.WriteAttributeString("time", "2000");
                        }
                        //textWriter.WriteAttributeString("time", "2000");
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Drop location
                    }
                    else if (PickUpProductType == "ProductTransferHotel")
                    {
                        textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                        textWriter.WriteAttributeString("xsi:type", PickUpProductType);
                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(PickUpHotelCode);
                        textWriter.WriteEndElement();
                        textWriter.WriteEndElement();//end of Drop location
                    }

                    else
                    {
                        textWriter.WriteStartElement("DestinationLocation"); // start of Drop location
                        textWriter.WriteAttributeString("xsi:type", DropProductType);
                        textWriter.WriteStartElement("Coordinates");
                        textWriter.WriteAttributeString("latitude", DropLatitude);
                        textWriter.WriteAttributeString("longitude", DropLongitude);
                        textWriter.WriteEndElement();
                        textWriter.WriteStartElement("Description");
                        textWriter.WriteStartElement("<![CDATA[" + DropAddressDescription + "]]>");
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Address");
                        textWriter.WriteStartElement("<![CDATA[" + DropAddress + "]]>");
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("City");
                        textWriter.WriteStartElement(DropCity);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("ZipCode");
                        textWriter.WriteStartElement(DropZipCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Country");
                        textWriter.WriteStartElement(DropCountry);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Drop location


                    }
                    #endregion


                    textWriter.WriteEndElement();//for available data

                    #endregion
                }


                textWriter.WriteStartElement("ReturnContents");
                textWriter.WriteValue("Y");
                textWriter.WriteEndElement();

                textWriter.WriteEndElement();// end of main tag


            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;

        }

    }
}
