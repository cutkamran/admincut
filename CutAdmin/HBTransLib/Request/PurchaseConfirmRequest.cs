﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.Xml;


namespace HBTransLib.Request
{
    public class PurchaseConfirmRequest : Common.WebClient
    {
        public string Language { get; set; }
        public string sessionId { get; set; }
        public string echoToken { get; set; }
        public int PageNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Return { get; set; }
        public string PickUpDate { get; set; }
        public string PickUpTime { get; set; }
        public string AdultCount { get; set; }
        public string ChildCount { get; set; }
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }

        public string PickUpTerminalCode { get; set; }
        public string DropTerminalCode { get; set; }

        public string PickUpHotelCode { get; set; }
        public string DropHotelCode { get; set; }

        public string PickUpLatitude { get; set; }
        public string PickUpLongitude { get; set; }
        public string DropLatitude { get; set; }
        public string DropLongitude { get; set; }

        public string PickUpAddress { get; set; }
        public string PickUpAddressDescription { get; set; }
        public string PickUpCountry { get; set; }
        public string PickUpCity { get; set; }
        public string PickUpState { get; set; }
        public string PickUpZipCode { get; set; }

        public string DropAddress { get; set; }
        public string DropAddressDescription { get; set; }
        public string DropCountry { get; set; }
        public string DropCity { get; set; }
        public string DropState { get; set; }
        public string DropZipCode { get; set; }
        public string SPUI { get; set; }

        public string ReturnDate { get; set; }
        public string PurchaseToken { get; set; }
        public string CustomerId { get; set; }
        public string CustomerAge { get; set; }
        public string CustomerType { get; set; }

        public string HolderFname { get; set; }
        public string HolderLname { get; set; }
        public string HolderAge { get; set; }
        public string AgencyReferenceNo { get; set; }
        public string[] AdultList { get; set; }
        public string[] ChildList { get; set; }
        //public string TravelCompanyName { get; set; }
        //public string TravelNumber { get; set; }

        //Return
        public string TransferTypeT { get; set; }
        public string PickUpTerminalCodeT { get; set; }
        public string DropTerminalCodeT { get; set; }

        public string PickUpHotelCodeT { get; set; }
        public string DropHotelCodeT { get; set; }

        public string PickUpLatitudeT { get; set; }
        public string PickUpLongitudeT { get; set; }
        public string DropLatitudeT { get; set; }
        public string DropLongitudeT { get; set; }

        public string PickUpAddressT { get; set; }
        public string PickUpAddressDescriptionT { get; set; }
        public string PickUpCountryT { get; set; }
        public string PickUpCityT { get; set; }
        public string PickUpStateT { get; set; }
        public string PickUpZipCodeT { get; set; }

        public string DropAddressT { get; set; }
        public string DropAddressDescriptionT { get; set; }
        public string DropCountryT { get; set; }
        public string DropCityT { get; set; }
        public string DropStateT { get; set; }
        public string DropZipCodeT { get; set; }

        public string SPUIOut { get; set; }

        public string ArrivalPickUpTime { get; set; }
        public string ArrivalDropTime { get; set; }

        public string DeparturePickUpTime { get; set; }
        public string DepartureDropTime { get; set; }

        public string FlightCodeArrF { get; set; }
        public string FlightCodeDepT { get; set; }
        public string FlightCodeDepF { get; set; }
        public string FlightCodeArrT { get; set; }

        //End Return

        public string GenerateXML()
        {
            string m_xml = "";

            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("PurchaseConfirmRQ");
                textWriter.WriteAttributeString("version", "2013/12");
                textWriter.WriteAttributeString("xsi:schemaLocation", "http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd");
                textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                textWriter.WriteAttributeString("xmlns", "http://www.hotelbeds.com/schemas/2005/06/messages");
                textWriter.WriteAttributeString("echoToken", echoToken);
                //textWriter.WriteAttributeString("echoToken", "DummyEchoToken");

                //textWriter.WriteAttributeString("sessionId", sessionId);

                textWriter.WriteStartElement("Language");
                textWriter.WriteValue("ENG");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Credentials");// start of credentials

                textWriter.WriteStartElement("User");
                textWriter.WriteValue(UserName);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Password");
                textWriter.WriteValue(Password);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement(); // end of credentials

                string[] Adult = new string[0];
                string[] Child = new string[0];

                List<string> AdFn = new List<string>();
                List<string> AdLn = new List<string>();
                List<string> AdAge = new List<string>();
                List<string> ChFn = new List<string>();
                List<string> ChLn = new List<string>();
                List<string> ChAge = new List<string>();
                List<string> CustomerTypeN = new List<string>();
                List<int> AdIdNo = new List<int>();
                List<int> ChIdNo = new List<int>();

                for (int i = 0; i < AdultList.Length; i++)
                {
                    Adult = AdultList[i].Split('^');
                    AdFn.Add(Adult[0]);
                    AdLn.Add(Adult[1]);
                    AdAge.Add(Adult[2]);
                    AdIdNo.Add(i + 1);
                    CustomerTypeN.Add("AD");
                }

                for (int i = 0; i < ChildList.Length; i++)
                {
                    Child = ChildList[i].Split('^');
                    ChFn.Add(Child[0]);
                    ChLn.Add(Child[1]);
                    ChAge.Add(Child[2]);
                    ChIdNo.Add(AdIdNo.LastIndexOf(AdIdNo.Last()) + 1);
                    CustomerTypeN.Add("CH");
                }



                textWriter.WriteStartElement("ConfirmationData"); // start of ConfirmationData
                textWriter.WriteAttributeString("purchaseToken", PurchaseToken);

                textWriter.WriteStartElement("Holder"); // start of Holder
                textWriter.WriteAttributeString("type", CustomerType);

                textWriter.WriteStartElement("CustomerId");
                textWriter.WriteValue(CustomerId);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Age");
                textWriter.WriteValue(HolderAge);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("Name");
                textWriter.WriteValue(HolderFname);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("LastName");
                textWriter.WriteValue(HolderLname);
                textWriter.WriteEndElement();

                textWriter.WriteEndElement(); // end of Holder

                textWriter.WriteStartElement("AgencyReference");
                textWriter.WriteValue(AgencyReferenceNo);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("ConfirmationServiceDataList");// start of ConfirmationServiceDataList


                #region In
                textWriter.WriteStartElement("ServiceData");// start of ServiceData
                textWriter.WriteAttributeString("xsi:type", "ConfirmationServiceDataTransfer");
                textWriter.WriteAttributeString("SPUI", SPUI);

                textWriter.WriteStartElement("CustomerList");// start of CustomerList

                //textWriter.WriteStartElement("Customer");// start of Customer
                //textWriter.WriteAttributeString("type", CustomerType);

                //textWriter.WriteStartElement("CustomerId");
                //textWriter.WriteValue(CustomerId);
                //textWriter.WriteEndElement();



                //for (int i = 0; i < AdultList.Length-1; i++ )
                //{
                //    textWriter.WriteStartElement("Age");
                //    textWriter.WriteValue(AdAge[i+1]);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("Name");
                //    textWriter.WriteValue(AdFn[i + 1]);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("LastName");
                //    textWriter.WriteValue(AdLn[i + 1]);
                //    textWriter.WriteEndElement();
                //}

                int j = 1;

                for (int i = 0; i < AdultList.Length; i++)
                {
                    textWriter.WriteStartElement("Customer");// start of Customer
                    textWriter.WriteAttributeString("type", CustomerTypeN[i]);

                    textWriter.WriteStartElement("CustomerId");
                    //textWriter.WriteValue(AdIdNo[i]); //was original
                    textWriter.WriteValue(j);
                    textWriter.WriteEndElement();


                    textWriter.WriteStartElement("Age");
                    textWriter.WriteValue(AdAge[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Name");
                    textWriter.WriteValue(AdFn[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("LastName");
                    textWriter.WriteValue(AdLn[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// end of Customer

                    j++;
                }


                //was actual code.....
                //for (int i = 0; i < ChildList.Length; i++)
                //{
                //    textWriter.WriteStartElement("Customer");// start of Customer
                //    textWriter.WriteAttributeString("type", CustomerType);

                //    textWriter.WriteStartElement("CustomerId");
                //    textWriter.WriteValue(ChIdNo[i]);
                //    textWriter.WriteEndElement();


                //    textWriter.WriteStartElement("Age");
                //    textWriter.WriteValue(ChAge[i]);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("Name");
                //    textWriter.WriteValue(ChFn[i]);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("LastName");
                //    textWriter.WriteValue(ChLn[i]);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteEndElement();// end of Customer
                //}
                //till here was actual code

                for (int i = 0; i < ChildList.Length; i++)
                {
                    textWriter.WriteStartElement("Customer");// start of Customer
                    textWriter.WriteAttributeString("type", CustomerType);

                    textWriter.WriteStartElement("CustomerId");
                    //textWriter.WriteValue(ChIdNo[i]);     //was original
                    textWriter.WriteValue(j);
                    textWriter.WriteEndElement();


                    textWriter.WriteStartElement("Age");
                    textWriter.WriteValue(ChAge[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("Name");
                    textWriter.WriteValue(ChFn[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("LastName");
                    textWriter.WriteValue(ChLn[i]);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();// end of Customer   

                    j++;
                }

                //textWriter.WriteEndElement();// end of Customer

                textWriter.WriteEndElement();// end of CustomerList

                //......
                textWriter.WriteStartElement("CommentList"); // start of CommentList

                textWriter.WriteStartElement("Comment");  // start of Comment
                textWriter.WriteAttributeString("type", "INCOMING");
                textWriter.WriteValue("Comment for the transfer incoming office.");
                textWriter.WriteEndElement(); // end of Comment

                textWriter.WriteEndElement(); // end of CommentList
                //......



                // here will'll have to apply condition according to product type...

                #region PickUp

                if (PickUpProductType == "ProductTransferTerminal" && DropProductType == "ProductTransferHotel")
                {
                    textWriter.WriteStartElement("ArrivalTravelInfo");// start of ArrivalTravelInfo

                    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                    textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(PickUpTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("time", ArrivalPickUpTime);
                    textWriter.WriteAttributeString("date", PickUpDate);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Arrival Info

                    textWriter.WriteStartElement("TravelNumber");
                    //textWriter.WriteValue("ABY-102");
                    textWriter.WriteValue(FlightCodeArrF);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end Arrival Info
                }
                else if (PickUpProductType == "ProductTransferHotel" && DropProductType == "ProductTransferTerminal")
                {
                    textWriter.WriteStartElement("DepartureTravelInfo");// start of ArrivalTravelInfo

                    textWriter.WriteStartElement("DepartInfo"); // start of Arrival Info
                    textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(DropTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("time", ArrivalDropTime);
                    textWriter.WriteAttributeString("date", PickUpDate);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Arrival Info

                    textWriter.WriteStartElement("TravelNumber");
                    //textWriter.WriteValue("ABY-102");
                    textWriter.WriteValue(FlightCodeDepF);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end Arrival Info
                }
                else if (PickUpProductType == "ProductTransferTerminal" && DropProductType == "ProductTransferTerminal")
                {
                    textWriter.WriteStartElement("ArrivalTravelInfo");// start of ArrivalTravelInfo

                    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                    textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(PickUpTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("time", ArrivalPickUpTime);
                    textWriter.WriteAttributeString("date", PickUpDate);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Arrival Info

                    textWriter.WriteStartElement("TravelNumber");
                    //textWriter.WriteValue("ABY-102");
                    textWriter.WriteValue(FlightCodeArrF);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end Arrival Info

                    //...........////

                    textWriter.WriteStartElement("DepartureTravelInfo");// start of ArrivalTravelInfo

                    textWriter.WriteStartElement("DepartInfo"); // start of Arrival Info
                    textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                    textWriter.WriteStartElement("Code");
                    textWriter.WriteValue(DropTerminalCode);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("DateTime");
                    textWriter.WriteAttributeString("time", ArrivalDropTime);
                    textWriter.WriteAttributeString("date", PickUpDate);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end of Arrival Info


                    //.....................
                    //textWriter.WriteStartElement("TravelCompanyName");
                    //textWriter.WriteValue("Air Arabia");
                    //textWriter.WriteEndElement();

                    textWriter.WriteStartElement("TravelNumber");
                    //textWriter.WriteValue("ABY-102");
                    textWriter.WriteValue(FlightCodeDepF);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();//end Arrival Info
                }
                //else
                //{
                //    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                //    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                //    textWriter.WriteStartElement("Code");
                //    textWriter.WriteValue(PickUpTerminalCode);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("DateTime");
                //    textWriter.WriteAttributeString("time", "1103");
                //    textWriter.WriteAttributeString("date", PickUpDate);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteEndElement();//end of Arrival Info
                //}
                #endregion

                #region Drop

                //if (DropProductType == "ProductTransferTerminal")
                //{
                //    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                //    textWriter.WriteAttributeString("xsi:type", DropProductType);

                //    textWriter.WriteStartElement("Code");
                //    textWriter.WriteValue(PickUpTerminalCode);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteStartElement("DateTime");
                //    textWriter.WriteAttributeString("time", "1103");
                //    textWriter.WriteAttributeString("date", PickUpDate);
                //    textWriter.WriteEndElement();

                //    textWriter.WriteEndElement();//end of Arrival Info
                //}
                ////else
                ////{
                ////    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                ////    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                ////    textWriter.WriteStartElement("Code");
                ////    textWriter.WriteValue(PickUpTerminalCode);
                ////    textWriter.WriteEndElement();

                ////    textWriter.WriteStartElement("DateTime");
                ////    textWriter.WriteAttributeString("time", "1103");
                ////    textWriter.WriteAttributeString("date", PickUpDate);
                ////    textWriter.WriteEndElement();

                ////    textWriter.WriteEndElement();//end of Arrival Info
                ////}

                #endregion

                //textWriter.WriteStartElement("TravelCompanyName");
                //textWriter.WriteValue("Air Arabia");
                //textWriter.WriteEndElement();



                textWriter.WriteEndElement();// end of service data

                #endregion


                if (ReturnDate != "")
                {
                    #region OUT

                    textWriter.WriteStartElement("ServiceData");// start of ServiceData
                    textWriter.WriteAttributeString("xsi:type", "ConfirmationServiceDataTransfer");
                    textWriter.WriteAttributeString("SPUI", SPUIOut);

                    textWriter.WriteStartElement("CustomerList");// start of CustomerList


                    for (int i = 0; i < AdultList.Length; i++)
                    {
                        textWriter.WriteStartElement("Customer");// start of Customer
                        textWriter.WriteAttributeString("type", CustomerTypeN[i]);

                        textWriter.WriteStartElement("CustomerId");
                        //textWriter.WriteValue(AdIdNo[i]);   //was original line
                        textWriter.WriteValue(j);
                        textWriter.WriteEndElement();


                        textWriter.WriteStartElement("Age");
                        textWriter.WriteValue(AdAge[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Name");
                        textWriter.WriteValue(AdFn[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("LastName");
                        textWriter.WriteValue(AdLn[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();// end of Customer

                        j++;
                    }

                    for (int i = 0; i < ChildList.Length; i++)
                    {
                        textWriter.WriteStartElement("Customer");// start of Customer
                        textWriter.WriteAttributeString("type", CustomerType);

                        textWriter.WriteStartElement("CustomerId");
                        //textWriter.WriteValue(ChIdNo[i]); //was original line
                        textWriter.WriteValue(j);
                        textWriter.WriteEndElement();


                        textWriter.WriteStartElement("Age");
                        textWriter.WriteValue(ChAge[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("Name");
                        textWriter.WriteValue(ChFn[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("LastName");
                        textWriter.WriteValue(ChLn[i]);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();// end of Customer

                        j++;
                    }

                    //textWriter.WriteEndElement();// end of Customer

                    textWriter.WriteEndElement();// end of CustomerList

                    //......
                    textWriter.WriteStartElement("CommentList"); // start of CommentList

                    textWriter.WriteStartElement("Comment");  // start of Comment
                    textWriter.WriteAttributeString("type", "INCOMING");
                    textWriter.WriteValue("Comment for the transfer incoming office.");
                    textWriter.WriteEndElement(); // end of Comment

                    textWriter.WriteEndElement(); // end of CommentList
                    //......



                    // here will'll have to apply condition according to product type...

                    #region PickUp

                    if (PickUpProductType == "ProductTransferTerminal" && DropProductType == "ProductTransferHotel")
                    {
                        textWriter.WriteStartElement("DepartureTravelInfo");// start of ArrivalTravelInfo

                        textWriter.WriteStartElement("DepartInfo"); // start of Arrival Info
                        textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(PickUpTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("time", DepartureDropTime);
                        textWriter.WriteAttributeString("date", ReturnDate);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Arrival Info


                        //.....................
                        //textWriter.WriteStartElement("TravelCompanyName");
                        //textWriter.WriteValue("Air Arabia");
                        //textWriter.WriteEndElement();

                        textWriter.WriteStartElement("TravelNumber");
                        //textWriter.WriteValue("ABY-102");
                        textWriter.WriteValue(FlightCodeDepT);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end Arrival Info
                    }
                    else if (PickUpProductType == "ProductTransferHotel" && DropProductType == "ProductTransferTerminal")
                    {
                        textWriter.WriteStartElement("ArrivalTravelInfo");// start of ArrivalTravelInfo

                        textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                        textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(DropTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("time", DeparturePickUpTime);
                        textWriter.WriteAttributeString("date", ReturnDate);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Arrival Info


                        //.....................
                        //textWriter.WriteStartElement("TravelCompanyName");
                        //textWriter.WriteValue("Air Arabia");
                        //textWriter.WriteEndElement();

                        textWriter.WriteStartElement("TravelNumber");
                        //textWriter.WriteValue("ABY-102");
                        textWriter.WriteValue(FlightCodeArrT);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end Arrival Info
                    }
                    else if (PickUpProductType == "ProductTransferTerminal" && DropProductType == "ProductTransferTerminal")
                    {
                        textWriter.WriteStartElement("ArrivalTravelInfo");// start of ArrivalTravelInfo

                        textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                        textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(DropTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("time", DeparturePickUpTime);
                        textWriter.WriteAttributeString("date", ReturnDate);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Arrival Info


                        //.....................
                        //textWriter.WriteStartElement("TravelCompanyName");
                        //textWriter.WriteValue("Air Arabia");
                        //textWriter.WriteEndElement();

                        textWriter.WriteStartElement("TravelNumber");
                        //textWriter.WriteValue("ABY-102");
                        textWriter.WriteValue(FlightCodeArrT);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end Arrival Info

                        //////.....//////
                        textWriter.WriteStartElement("DepartureTravelInfo");// start of ArrivalTravelInfo

                        textWriter.WriteStartElement("DepartInfo"); // start of Arrival Info
                        textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                        textWriter.WriteStartElement("Code");
                        textWriter.WriteValue(PickUpTerminalCode);
                        textWriter.WriteEndElement();

                        textWriter.WriteStartElement("DateTime");
                        textWriter.WriteAttributeString("time", DepartureDropTime);
                        textWriter.WriteAttributeString("date", ReturnDate);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end of Arrival Info


                        //.....................
                        //textWriter.WriteStartElement("TravelCompanyName");
                        //textWriter.WriteValue("Air Arabia");
                        //textWriter.WriteEndElement();

                        textWriter.WriteStartElement("TravelNumber");
                        //textWriter.WriteValue("ABY-102");
                        textWriter.WriteValue(FlightCodeDepT);
                        textWriter.WriteEndElement();

                        textWriter.WriteEndElement();//end Arrival Info
                    }
                    //else
                    //{
                    //    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                    //    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                    //    textWriter.WriteStartElement("Code");
                    //    textWriter.WriteValue(PickUpTerminalCode);
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteStartElement("DateTime");
                    //    textWriter.WriteAttributeString("time", "1103");
                    //    textWriter.WriteAttributeString("date", PickUpDate);
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteEndElement();//end of Arrival Info
                    //}
                    #endregion

                    #region Drop

                    //if (PickUpProductType == "ProductTransferTerminal" && DropProductType == "ProductTransferTerminal")
                    //{
                    //    textWriter.WriteStartElement("ArrivalTravelInfo");// start of ArrivalTravelInfo

                    //    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                    //    textWriter.WriteAttributeString("xsi:type", "ProductTransferTerminal");

                    //    textWriter.WriteStartElement("Code");
                    //    textWriter.WriteValue(PickUpTerminalCode);
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteStartElement("DateTime");
                    //    textWriter.WriteAttributeString("time", "1103");
                    //    textWriter.WriteAttributeString("date", PickUpDate);
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteEndElement();//end of Arrival Info

                    //    //.....................
                    //    //textWriter.WriteStartElement("TravelCompanyName");
                    //    //textWriter.WriteValue("Air Arabia");
                    //    //textWriter.WriteEndElement();

                    //    textWriter.WriteStartElement("TravelNumber");
                    //    //textWriter.WriteValue("ABY-102");
                    //    textWriter.WriteValue("UX-122");
                    //    textWriter.WriteEndElement();

                    //    textWriter.WriteEndElement();//end Arrival Info
                    //}
                    ////else
                    ////{
                    ////    textWriter.WriteStartElement("ArrivalInfo"); // start of Arrival Info
                    ////    textWriter.WriteAttributeString("xsi:type", PickUpProductType);

                    ////    textWriter.WriteStartElement("Code");
                    ////    textWriter.WriteValue(PickUpTerminalCode);
                    ////    textWriter.WriteEndElement();

                    ////    textWriter.WriteStartElement("DateTime");
                    ////    textWriter.WriteAttributeString("time", "1103");
                    ////    textWriter.WriteAttributeString("date", PickUpDate);
                    ////    textWriter.WriteEndElement();

                    ////    textWriter.WriteEndElement();//end of Arrival Info
                    ////}

                    #endregion




                    textWriter.WriteEndElement();// end of service data

                    #endregion
                }

               

                textWriter.WriteEndElement();// end of ConfirmationService Data List

                textWriter.WriteEndElement();// end Confirmation Data





                textWriter.WriteEndElement();// end of main tag

            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;

        }
    }
}
