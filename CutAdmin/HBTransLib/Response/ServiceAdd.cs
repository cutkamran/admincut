﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class ServiceAdd
    {
        public string purchaseToken { get; set; }
        public string timeToExpiration { get; set; }
        public string PurchaseStatus { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyBranch { get; set; }
        public string Language { get; set; }
        public string CreationUser { get; set; }
        public string Currency { get; set; }
        public List<ServiceAddList> ServiceAddList { get; set; }

        public string SPUI { get; set; }
        public string PaymentTypeCode { get; set; }
        public string InvoiceCompany { get; set; }
        public string ServiceDescription { get; set; }
        public string TotalPrice { get; set; }
        public string PendingAmount { get; set; }

    }
}
