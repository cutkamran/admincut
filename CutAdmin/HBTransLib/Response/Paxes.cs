﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class Paxes
    {
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public List<GuestList> GuestList { get; set; }
    }
}
