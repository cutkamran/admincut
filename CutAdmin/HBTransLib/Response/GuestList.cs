﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class GuestList
    {
        public string type { get; set; }
        public int CustomerId { get; set; }
        public int CustomerAge { get; set; }
    }
}
