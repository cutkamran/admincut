﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class TransferInfo
    {
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }
        public string transferType { get; set; }
        public string Code { get; set; }
        public List<TransInfoDescription> TransInfoDescription { get; set; }
        public List<Image> ImageList { get; set; }
        public string Typecode { get; set; }
        public string VehicleTypecode { get; set; }
        public string TransferSpecificContentid { get; set; }
        public List<GenericTransferBulletPoint> GenericTransferBulletPoint { get; set; }
        public List<SpecificTransferBulletPoint> SpecificTransferBulletPoint { get; set; }
        public int MaximumWaitingTime { get; set; }
        public int MaximumWaitingTimeSupplierDomestic { get; set; }
        public int MaximumWaitingTimeSupplierInternational { get; set; }
        
    }
}
