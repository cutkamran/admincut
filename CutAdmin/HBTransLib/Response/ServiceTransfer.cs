﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class ServiceTransfer
    {
        public List<string> TransferType { get; set; }
        public string AvailToken { get; set; }
        public string ContractName { get; set; }
        public string IncomingOfficeCode { get; set; }
        public string DateFrom { get; set; }
        public string DateFromtime { get; set; }
        public string Currency { get; set; }
        public float TotalAmount { get; set; }

        public float CutAmount { get; set; }
        public float AgentMarkup { get; set; }

        public string NetPrice { get; set; }
        public string Commission { get; set; }
        public string SellingPrice { get; set; }
        public string SellingPricemandatory { get; set; }
        public List<TransferInfo> TransferInfo { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }

        public List<PickupLocation> PickupLocation { get; set; }
        //public string PickUpType { get; set; }
        //public string PickUpCode { get; set; }
        //public string PickUpName { get; set; }
        //public string PickUpTransferZoneType { get; set; }
        //public string PickUpTransferZoneCode { get; set; }
        //public string PickUpTerminalType { get; set; }
        //public string PickUpLatitude { get; set; }
        //public string PickUpLongitude { get; set; }
        //public string PickDescription { get; set; }
        //public string PickUpAddress { get; set; }
        //public string PickUpCity { get; set; }
        //public string PickUpZipCode { get; set; }
        //public string PickUpCountry { get; set; }

        public List<DestinationLocation> DestinationLocation { get; set; }
        //public string DropType { get; set; }
        //public string DropCode { get; set; }
        //public string DropName { get; set; }
        //public string DropTransferZoneType { get; set; }
        //public string DropTransferZoneCode { get; set; }
        //public string DropTerminalType { get; set; }
        //public string DropLatitude { get; set; }
        //public string DropLongitude { get; set; }
        //public string Dropscription { get; set; }
        //public string DropAddress { get; set; }
        //public string DropCity { get; set; }
        //public string DropZipCode { get; set; }
        //public string DropCountry { get; set; }

        public Decimal ComissionPercentage { get; set; }
        public Decimal RetailPrice { get; set; }
        public Decimal ComissionVAT { get; set; }
        public ProductSpecifications ProductSpecifications { get; set; }
        public List<TransferPickupInformation> TransferPickupInformation { get; set; }
        public string Description { get; set; }

        public List<ArrivalTravelInfo> ArrivalTravelInfo { get; set; }
        //public string ArrivalType { get; set; }
        //public string ArrivalCode { get; set; }
        //public string ArrivalName { get; set; }
        //public string ArrivalTransferZoneType { get; set; }
        //public string ArrivalTransferZoneCode { get; set; }
        //public string ArrivalTerminalType { get; set; }
        //public string ArrivalDate { get; set; }
        //public string ArrivalTime { get; set; }

        public List<DepartureTravelInfo> DepartureTravelInfo { get; set; }

        //public float CancellationAmount { get; set; }
        //public float CancellationCutAmount { get; set; }
        //public float CancellationAgentMarkup { get; set; }

        public List<CancellationPolicy> CancellationPolicy { get; set; }
    }
}
