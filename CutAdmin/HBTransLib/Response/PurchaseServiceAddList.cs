﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class PurchaseServiceAddList
    {
        public string xsitype { get; set; }
        public string transferType { get; set; }
        public string SPUI { get; set; }
        public string ServiceStatus { get; set; }
        public string ReferenceFileNumber { get; set; }
        public string ReferenceIncomingOffice { get; set; }

        public string ContractName { get; set; }
        public string ContractIncomingOffice { get; set; }
        public string ContractSequence { get; set; }

        public string SupplierName { get; set; }
        public string SupplierVatNumber { get; set; }
        public string DateFrom { get; set; }
        public string Currency { get; set; }
        public string CurrencyCode { get; set; }
        

        public string TotalAmount { get; set; }
        public string NetPrice { get; set; }
        public string Commission { get; set; }
        public string SellingPrice { get; set; }
        public string SellingPricemandatory { get; set; }
        public List<AdditionalCostList> AdditionalCostList { get; set; }
        public List<ModificationPolicyList> ModificationPolicyList { get; set; }
        public TransferInfo TransferInfo { get; set; }
        public Paxes Paxes { get; set; }

        public string PickUpType { get; set; }
        public string PickUpCode { get; set; }
        public string PickUpName { get; set; }
        public string PickUpTransferZoneType { get; set; }
        public string PickUpTransferZoneCode { get; set; }
        public string PickUpTerminalType { get; set; }
        public string PickUpLatitude { get; set; }
        public string PickUpLongitude { get; set; }
        public string PickDescription { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpCity { get; set; }
        public string PickUpZipCode { get; set; }
        public string PickUpCountry { get; set; }

        public string DropType { get; set; }
        public string DropCode { get; set; }
        public string DropName { get; set; }
        public string DropTransferZoneType { get; set; }
        public string DropTransferZoneCode { get; set; }
        public string DropTerminalType { get; set; }
        public string DropLatitude { get; set; }
        public string DropLongitude { get; set; }
        public string Dropscription { get; set; }
        public string DropAddress { get; set; }
        public string DropCity { get; set; }
        public string DropZipCode { get; set; }
        public string DropCountry { get; set; }

        public Decimal ComissionPercentage { get; set; }
        public Decimal RetailPrice { get; set; }
        public Decimal ComissionVAT { get; set; }
        public ProductSpecifications ProductSpecifications { get; set; }
        public TransferPickupTime TransferPickupTime { get; set; }
        public string Description { get; set; }

        public ArrivalTravelInfo ArrivalTravelInfo { get; set; }

        public DepartureTravelInfo DepartureTravelInfo { get; set; }

        //public string ArrivalType { get; set; }
        //public string ArrivalCode { get; set; }
        //public string ArrivalName { get; set; }
        //public string ArrivalTransferZoneType { get; set; }
        //public string ArrivalTransferZoneCode { get; set; }
        //public string ArrivalTerminalType { get; set; }
        //public string ArrivalDate { get; set; }
        //public string ArrivalTime { get; set; }

        //public string DepartureType { get; set; }
        //public string DepartureCode { get; set; }
        //public string DepartureName { get; set; }
        //public string DepartureTransferZoneType { get; set; }
        //public string DepartureTransferZoneCode { get; set; }
        //public string DepartureTerminalType { get; set; }
        //public string DepartureDate { get; set; }
        //public string DepartureTime { get; set; }

        public List<CancellationPolicy> CancellationPolicy { get; set; }
        public List<ContactInfoList> ContactInfoList { get; set; }


    }
}
