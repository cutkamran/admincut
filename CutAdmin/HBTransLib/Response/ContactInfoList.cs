﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class ContactInfoList
    {
        public string Type { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string OperationDays { get; set; }
        public string Language { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }
}
