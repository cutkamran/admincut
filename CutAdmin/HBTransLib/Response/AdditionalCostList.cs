﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class AdditionalCostList
    {
        public string type { get; set; }
        public string Amount { get; set; }
    }
}
