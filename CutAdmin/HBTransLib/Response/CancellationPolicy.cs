﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class CancellationPolicy
    {
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }
        public string transferType { get; set; }

        public float amount { get; set; }
        public string dateFrom { get; set; }
        public string time { get; set; }

        public float CutAmount { get; set; }
        public float AgentMarkup { get; set; }
    }
}
