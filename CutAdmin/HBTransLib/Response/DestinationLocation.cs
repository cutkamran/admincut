﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class DestinationLocation
    {
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }
        public string transferType { get; set; }

        public string DropType { get; set; }
        public string DropCode { get; set; }
        public string DropName { get; set; }
        public string DropTransferZoneType { get; set; }
        public string DropTransferZoneCode { get; set; }
        public string DropTerminalType { get; set; }
        public string DropLatitude { get; set; }
        public string DropLongitude { get; set; }
        public string Dropscription { get; set; }
        public string DropAddress { get; set; }
        public string DropCity { get; set; }
        public string DropZipCode { get; set; }
        public string DropCountry { get; set; }

    }
}
