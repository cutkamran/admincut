﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class DepartureTravelInfo
    {
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }
        public string transferType { get; set; }

        public string ArrivalType { get; set; }
        public string ArrivalCode { get; set; }
        public string ArrivalName { get; set; }
        public string ArrivalTransferZoneType { get; set; }
        public string ArrivalTransferZoneCode { get; set; }
        public string ArrivalTerminalType { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }

        public string DepartureType { get; set; }
        public string DepartureCode { get; set; }
        public string DepartureName { get; set; }
        public string DepartureTransferZoneType { get; set; }
        public string DepartureTransferZoneCode { get; set; }
        public string DepartureTerminalType { get; set; }
        public string DepartureDate { get; set; }
        public string DepartureTime { get; set; }

        public string TravelNumber { get; set; }
    }
}
