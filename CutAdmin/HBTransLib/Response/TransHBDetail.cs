﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class TransHBDetail
    {
        public string ProcessTime { get; set; }
        public string Timestamp { get; set; }
        public string RequestHost { get; set; }
        public string ServerName { get; set; }
        public string ServerId { get; set; }
        public string SchemaRelease { get; set; }
        public string HydraCoreRelease { get; set; }
        public string HydraEnumerationsRelease { get; set; }
        public string MerlinRelease { get; set; }
        public List<ServiceTransfer> ServiceTransfer { get; set; }
    }
}
