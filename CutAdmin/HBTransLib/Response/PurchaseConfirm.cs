﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class PurchaseConfirm
    {
        public string purchaseToken { get; set; }
        public string timeToExpiration { get; set; }
        public string FileNumber { get; set; }
        public string IncomingOffice { get; set; }
        public string PurchaseStatus { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyBranch { get; set; }
        public string Language { get; set; }
        public string CreationUser { get; set; }
        public string Currency { get; set; }

        public string HolderType { get; set; }
        public string HolderAge { get; set; }
        public string HolderName { get; set; }
        public string HolderLastName { get; set; }
        public string AgencyReference { get; set; }

        public List<PurchaseServiceAddList> PurchaseServiceAddList { get; set; }
        public string PaymentTypeCode { get; set; }
        public string InvoiceCompanyCode { get; set; }
        public string InvoiceCompanyName { get; set; }
        public string InvoiceCompanyRegistrationNumber { get; set; }


        public string ServiceDescription { get; set; }
        public string TotalPrice { get; set; }
        public string PendingAmount { get; set; }

    }
}
