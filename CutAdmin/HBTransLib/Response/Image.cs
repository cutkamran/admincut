﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
   public class Image
    {
       public string Type { set; get; }
       public string Url { set; get; }
    }
}
