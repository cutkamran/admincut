﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class SpecificTransferBulletPoint
    {

        public string id { get; set; }
        public string order { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Metric { get; set; }
    }
}
