﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
   public class ProductSpcTransferBulletPoint
    {
        public string id { get; set; }
        public string order { get; set; }
        public string Description { get; set; }
    }
}
