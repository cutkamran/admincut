﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class PickupLocation
    {
        public string PickUpProductType { get; set; }
        public string DropProductType { get; set; }
        public string transferType { get; set; }

        //public string Type { get; set; }
        //public string Code { get; set; }
        //public string Name { get; set; }
        //public string TransferZoneType { get; set; }
        //public string TransferZoneCode { get; set; }
        //public string TerminalType { get; set; }

        public string PickUpType { get; set; }
        public string PickUpCode { get; set; }
        public string PickUpName { get; set; }
        public string PickUpTransferZoneType { get; set; }
        public string PickUpTransferZoneCode { get; set; }
        public string PickUpTerminalType { get; set; }
        public string PickUpLatitude { get; set; }
        public string PickUpLongitude { get; set; }
        public string PickDescription { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpCity { get; set; }
        public string PickUpZipCode { get; set; }
        public string PickUpCountry { get; set; }
            
    }
}
