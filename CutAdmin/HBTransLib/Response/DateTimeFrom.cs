﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class DateTimeFrom
    {
        public string date { get; set; }
        public string time { get; set; }
    }
}
