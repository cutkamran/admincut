﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class ProductSpecifications
    {
        public string MasterServiceTypeCode { get; set; }
        public string MasterServiceTypename { get; set; }
        public string MasterProductTypeCode { get; set; }
        public string MasterProductTypename { get; set; }
        public string MasterVehicleTypeCode { get; set; }
        public string MasterVehicleTypename { get; set; }
        public List<ProductSpcTransferBulletPoint> TransferGeneralInfoList { get; set; }
    }
}
