﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
   public class TransInfoDescription
    {
       public string Description { set; get; }
       public string type { set; get; }
       public string languageCode { set; get; }
    }
}
