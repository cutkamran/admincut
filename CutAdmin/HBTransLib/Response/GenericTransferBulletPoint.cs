﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBTransLib.Response
{
    public class GenericTransferBulletPoint
    {
        public string id { get; set; }
        public string Description { get; set; }
        public string DetailedDescription { get; set; }
        
    }
}
