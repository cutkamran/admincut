﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class HotelInfo
    {
       // public HotelInfo();

        public string HotelName { get; set; }
        public long sid { get; set; }
        public string SupplierName { get; set; }
    }
}
