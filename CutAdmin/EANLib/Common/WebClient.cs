﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO.Compression;
using System.Configuration;
using System.IO;
using EANLib.Request;

namespace EANLib.Common
{
  public  class WebClient
    {
      public bool Post(string m_data,string RequestType, out string m_response, out string RequestHeader, out string ResponseHeader, out int Status)
      {
          m_response = "";
          RequestHeader = "";
          ResponseHeader = "";
          Status = 0;
          try
          {
              //var oRequest = WebRequest.Create(ConfigurationManager.AppSettings["EMNURL"]);
              var sig = MD5GenerateHash.MD5Generate("6vgq7v92jk67ri7mhbvvl71a3h" + "duu091elm9gq1" + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
              var oRequest=WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/");
              if (RequestType != "res")
                  oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/" + RequestType + "?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=" + m_data);
              else
              {
                  
                  oRequest = WebRequest.Create(" https://book.api.ean.com/ean-services/rs/hotel/v3/res?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=" + m_data);
                  oRequest.Method = "POST";
              }
                               // Create the web request
              HttpWebRequest request = WebRequest.Create(oRequest.RequestUri) as HttpWebRequest;

                // Get response
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    if (reader.ToString().Contains("EanWsError"))
                    {
                        m_response = reader.ReadToEnd();
                        Status = 0;
                        return false;
                    }
                    else
                    {
                        Status = 1;
                        m_response = reader.ReadToEnd();
                        return true;
                    }
                }
          }
          catch (WebException oWebException)
          {
              if (oWebException.Response != null)
              {
                  using (var oResponseStream = oWebException.Response.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oWebException.Response.ContentType + " Content-Length:" + oWebException.Response.ContentLength;
                  }
              }
              return false;
          }
          catch (Exception oException)
          {
              m_response = oException.Message;
              return false;
          }
      }
      public bool PostBookingRequest(string m_data, string RequestType, out string m_response, out string RequestHeader, out string ResponseHeader, out int Status)
      {
          bool Response = false;
          m_response = "";
          RequestHeader = "";
          ResponseHeader = "";
          Status = 0;
          try
          {
              var sig = MD5GenerateHash.MD5Generate("6vgq7v92jk67ri7mhbvvl71a3h" + "duu091elm9gq1" + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
              ////var oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/res?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=" + m_data);
              //var oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/res?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=%3CHotelRoomReservationRequest%3E%3ChotelId%3E470142%3C%2FhotelId%3E%3CarrivalDate%3E12%2F23%2F2016%3C%2FarrivalDate%3E%3CdepartureDate%3E12%2F24%2F2016%3C%2FdepartureDate%3E%3CsupplierType%3EE%3C%2FsupplierType%3E%3CrateKey%3E6f462cfb-6042-4776-8db2-d5dac94f9702-5001%3C%2FrateKey%3E%3CroomTypeCode%3E200752832%3C%2FroomTypeCode%3E%3CrateCode%3E203811157%3C%2FrateCode%3E%3CchargeableRate%3E36.94%3C%2FchargeableRate%3E%3CRoomGroup%3E%3CRoom%3E%3CnumberOfAdults%3E2%3C%2FnumberOfAdults%3E%3CfirstName%3EMr%20test%3C%2FfirstName%3E%3ClastName%3Etester%3C%2FlastName%3E%3CbedTypeId%3E13%3C%2FbedTypeId%3E%3C%2FRoom%3E%3C%2FRoomGroup%3E%3CReservationInfo%3E%3Cemail%3Etest%40travelnow.com%3C%2Femail%3E%3CfirstName%3Etest%3C%2FfirstName%3E%3ClastName%3Etester%3C%2FlastName%3E%3ChomePhone%3E2145370159%3C%2FhomePhone%3E%3CworkPhone%3E2145370159%3C%2FworkPhone%3E%3CcreditCardType%3ECA%3C%2FcreditCardType%3E%3CcreditCardNumber%3E5401999999999999%3C%2FcreditCardNumber%3E%3CcreditCardIdentifier%3E123%3C%2FcreditCardIdentifier%3E%3CcreditCardExpirationMonth%3E11%3C%2FcreditCardExpirationMonth%3E%3CcreditCardExpirationYear%3E2018%3C%2FcreditCardExpirationYear%3E%3C%2FReservationInfo%3E%3CAddressInfo%3E%3Caddress1%3Etravelnow%3C%2Faddress1%3E%3Ccity%3ENagpur%3C%2Fcity%3E%3CstateProvinceCode%3EMAH%3C%2FstateProvinceCode%3E%3CcountryCode%3EIN%3C%2FcountryCode%3E%3CpostalCode%3E98004%3C%2FpostalCode%3E%3C%2FAddressInfo%3E%3C%2FHotelRoomReservationRequest%3E");
  
              //oRequest.Method = "GET";
              //HttpWebRequest request = WebRequest.Create(oRequest.RequestUri) as HttpWebRequest;
              //using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
              //{
              //    // Get the response stream
              //    StreamReader reader = new StreamReader(response.GetResponseStream());
              //    if (reader.ToString().Contains("EanWsError"))
              //    {
              //        m_response = reader.ReadToEnd();
              //        Status = 0;
              //        return false;
              //    }
              //    else
              //    {
              //        Status = 1;
              //        m_response = reader.ReadToEnd();
              //        return true;
              //    }
              //}
              var oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/res?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml="+ m_data);
              oRequest.Method = "POST";
              oRequest.ContentType = "application/xml";
              oRequest.Headers["Accept-Encoding"] = "gzip, deflate";
              RequestHeader = "ContentType:application/xml Accept-Encoding:gzip, deflate";
              //byte[] oRequestBytes = Encoding.ASCII.GetBytes(m_data);
              byte[] oRequestBytes = Encoding.UTF8.GetBytes(m_data);
              oRequest.ContentLength = oRequestBytes.Length;
              RequestHeader = RequestHeader + " ContentLength:" + oRequest.ContentLength;
              using (var oRequestStream = oRequest.GetRequestStream())
              {
                  oRequestStream.Write(oRequestBytes, 0, oRequestBytes.Length);
              }
              using (var oResponse = oRequest.GetResponse())
              {
                  using (var oResponseStream = oResponse.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oResponse.ContentType + " Content-Length:" + oResponse.ContentLength;
                      if (m_response.Contains("ErrorList"))
                      {
                          Status = 0;
                          return false;
                      }
                      else
                      {
                          Status = 1;
                          return true;
                      }
                  }
              }

          }
          catch (WebException oWebException)
          {
              if (oWebException.Response != null)
              {
                  using (var oResponseStream = oWebException.Response.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oWebException.Response.ContentType + " Content-Length:" + oWebException.Response.ContentLength;
                  }
              }
              return false;
          }
          return Response;
      }

      public bool PostCancellationRequest(string m_data, string RequestType, out string m_response, out string RequestHeader, out string ResponseHeader, out int Status)
      {
          bool Response = false;
          m_response = "";
          RequestHeader = "";
          ResponseHeader = "";
          Status = 0;
          try
          {
              //var oRequest = WebRequest.Create(ConfigurationManager.AppSettings["EMNURL"]);
              var sig = MD5GenerateHash.MD5Generate("6vgq7v92jk67ri7mhbvvl71a3h" + "duu091elm9gq1" + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
              var oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/");
              if (RequestType != "res")
                  oRequest = WebRequest.Create("https://book.api.ean.com/ean-services/rs/hotel/v3/" + RequestType + "?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=" + m_data);
              else
              {

                  oRequest = WebRequest.Create(" https://book.api.ean.com/ean-services/rs/hotel/v3/res?cid=453858&minorRev=99&apiKey=6vgq7v92jk67ri7mhbvvl71a3h&locale=en_US&currencyCode=USD&sig=" + sig + "&xml=" + m_data);
                  oRequest.Method = "POST";
              }
              // Create the web request
              HttpWebRequest request = WebRequest.Create(oRequest.RequestUri) as HttpWebRequest;

              // Get response
              using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
              {
                  // Get the response stream
                  StreamReader reader = new StreamReader(response.GetResponseStream());
                  if (reader.ToString().Contains("ErrorAttributes"))
                  {
                      m_response = reader.ReadToEnd();
                      Status = 0;
                      return false;
                  }
                  else
                  {
                      Status = 1;
                      m_response = reader.ReadToEnd();
                      return true;
                  }
              }
          }
          catch (WebException oWebException)
          {
              if (oWebException.Response != null)
              {
                  using (var oResponseStream = oWebException.Response.GetResponseStream())
                  {
                      var response = new GZipStream(oResponseStream, CompressionMode.Decompress);
                      System.IO.StreamReader sr = new System.IO.StreamReader(response);
                      m_response = sr.ReadToEnd();
                      ResponseHeader = "Content-Type:" + oWebException.Response.ContentType + " Content-Length:" + oWebException.Response.ContentLength;
                  }
              }
              return false;
          }
          catch (Exception oException)
          {
              m_response = oException.Message;
              return false;
          }
          return Response;
      }

    }
}
