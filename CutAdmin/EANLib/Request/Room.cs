﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Request
{
   public class Room
    {
       public string FirstName { get; set; }
       public string Type { get; set; }
       public string LastName { get; set; }
       public int RoomCount { get; set; }
       public int numberOfAdults { get; set; }
       public int numberOfChildren { get; set; }
       public int childAges { get; set; }
       public string ratekey { get; set; }
    }
}
