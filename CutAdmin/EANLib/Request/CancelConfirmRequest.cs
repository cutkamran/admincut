﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EANLib.Request
{
    public class CancelConfirmRequest : Common.WebClient
    {
        public string itineraryId { get; set; }
        public string email { get; set; }
        public string confirmationNumber { get; set; }

      public string GenerateXML()
      {
          string m_xml = "";
          StringWriter sw = new StringWriter();
          using (XmlTextWriter textWriter = new XmlTextWriter(sw))
          {
              textWriter.WriteStartDocument();

              textWriter.WriteStartElement("HotelRoomCancellationRequest");

              textWriter.WriteStartElement("itineraryId"); //  itineraryId //
              textWriter.WriteValue(itineraryId);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("email"); //  email //
              textWriter.WriteValue(email);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("confirmationNumber"); //  confirmationNumber //
              textWriter.WriteValue(confirmationNumber);
              textWriter.WriteEndElement();

              textWriter.WriteEndDocument();
          }
          m_xml = sw.ToString();
          sw.Close();
          sw.Dispose();
          return m_xml;
      }
    }
}
