﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.Xml;
using EANLib.Response;

namespace EANLib.Request
{
    public class AvailRequest : Common.WebClient
    {
        public string city { get; set; }
        public string countryCode { get; set; }
        public string arrivalDate { get; set; }
        public string departureDate { get; set; }
        public List<Room> room { get; set; }
       //public string apiExperience { get; set; }
       //public string arrivalDate { get; set; }
       //public string departureDate { get; set; }
       //public int numberOfResults { get; set; }
       //
       //public Boolean includeDetails { get; set; }
        //public Boolean includeHotelFeeBreakdown { get; set; }
        #region HotelSeacrh Request
        public string GenerateXML()
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();
                textWriter.WriteStartElement("HotelListRequest");
                textWriter.WriteStartElement("city");
                textWriter.WriteValue(city);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("stateProvinceCode");
                textWriter.WriteValue("");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("countryCode");
                textWriter.WriteValue(countryCode);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("arrivalDate");
                textWriter.WriteValue(arrivalDate);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("departureDate");
                textWriter.WriteValue(departureDate);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("includeDetails");
                textWriter.WriteValue("true");
                textWriter.WriteEndElement();
                #region Rooms tag
                // Room Group Start Here //
                textWriter.WriteStartElement("RoomGroup");

                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("Room");
                   // textWriter.WriteAttributeString("runno", i.ToString());
                    textWriter.WriteStartElement("numberOfAdults");
                    textWriter.WriteValue(room[i].numberOfAdults);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("numberOfChildren");
                    textWriter.WriteValue(room[i].numberOfChildren);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("childAges");
                    textWriter.WriteValue(room[i].childAges);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();// for room Tag
                }


                textWriter.WriteEndElement();
                // Room Group Ends //
                #endregion Rooms tag

                textWriter.WriteStartElement("numberOfResults");
                textWriter.WriteValue(200);
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();


            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;

        }
        #endregion

        #region Romm Request
        public string GenerateRoomXML(string HotelId)
        {
            string m_xml = "";
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();
                textWriter.WriteStartElement("HotelRoomAvailabilityRequest");
                textWriter.WriteStartElement("hotelId");
                textWriter.WriteValue(HotelId);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("arrivalDate");
                textWriter.WriteValue(arrivalDate);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("departureDate");
                textWriter.WriteValue(departureDate);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("includeDetails");
                textWriter.WriteValue(true);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("includeRoomImages");
                textWriter.WriteValue(true);
                textWriter.WriteEndElement();
                #region Rooms tag
                // Room Group Start Here //
                textWriter.WriteStartElement("RoomGroup");

                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("Room");
                    //textWriter.WriteAttributeString("runno", i.ToString());
                    textWriter.WriteStartElement("numberOfAdults");
                    textWriter.WriteValue(room[i].numberOfAdults);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("numberOfChildren");
                    textWriter.WriteValue(room[i].numberOfChildren);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("childAges");
                    textWriter.WriteValue(room[i].childAges);
                    textWriter.WriteEndElement();
                    textWriter.WriteEndElement();// for room Tag
                }


                textWriter.WriteEndElement();
                // Room Group Ends //
                #endregion Rooms tag
                textWriter.WriteEndElement();
            }
            m_xml = sw.ToString();
            sw.Close();
            sw.Dispose();
            return m_xml;

        }

        #endregion


        #region HotelRoomReservationRequest
        public string HotelId { get; set; }
        public string supplierType { get; set; }
        public string rateKey { get; set; }
        public string roomTypeCode { get; set; }
        public string rateCode { get; set; }
        public string chargeableRate { get; set; }
        public string bedTypeId { get; set; }
        public string smokingPreference { get; set; }
        public string firstName { get; set; }
        public string LastName { get; set; }
        public string GetBookingDetails()
        {
          string  m_xml = "";
           StringWriter sw = new StringWriter();
           using (XmlTextWriter textWriter = new XmlTextWriter(sw))
           {
               //textWriter.WriteStartDocument();
               textWriter.WriteStartElement("HotelRoomReservationRequest");
               textWriter.WriteStartElement("hotelId");
               textWriter.WriteValue(HotelId);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("arrivalDate");
               textWriter.WriteValue(arrivalDate);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("departureDate");
               textWriter.WriteValue(departureDate);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("supplierType");
               textWriter.WriteValue("E");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("rateKey");
               textWriter.WriteValue(rateKey);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("roomTypeCode");
               textWriter.WriteValue(roomTypeCode);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("rateCode");
               textWriter.WriteValue(rateCode);
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("chargeableRate");
               textWriter.WriteValue(chargeableRate);
               textWriter.WriteEndElement();
               #region Rooms tag
               // Room Group Start Here //
               textWriter.WriteStartElement("RoomGroup");

               for (int i = 0; i < room.Count; i++)
               {
                   textWriter.WriteStartElement("Room");
                   //textWriter.WriteAttributeString("runno", i.ToString());
                   textWriter.WriteStartElement("numberOfAdults");
                   textWriter.WriteValue(room[i].numberOfAdults);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("numberOfChildren");
                   textWriter.WriteValue(room[i].numberOfChildren);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("childAges");
                   textWriter.WriteValue(room[i].childAges);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("firstName");
                   textWriter.WriteValue(room[i].FirstName);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("lastName");
                   textWriter.WriteValue(room[i].LastName);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("bedTypeId");
                   textWriter.WriteValue(bedTypeId);
                   textWriter.WriteEndElement();
                   textWriter.WriteStartElement("smokingPreference");
                   textWriter.WriteValue(smokingPreference);
                   textWriter.WriteEndElement();
                   //textWriter.WriteStartElement("smokingPreference");
                   //textWriter.WriteValue("NS");
                   //textWriter.WriteEndElement();
                   textWriter.WriteEndElement();// for room Tag
               }


               textWriter.WriteEndElement();
               // Room Group Ends //
               #endregion Rooms tag

               #region Reservation Details
               textWriter.WriteStartElement("ReservationInfo"); // Reservation Details Start
               textWriter.WriteStartElement("email");
               textWriter.WriteValue("imran@clickurtrip.com");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("firstName");
               textWriter.WriteValue("Mohd");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("lastName");
               textWriter.WriteValue("Imran");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("homePhone");
               textWriter.WriteValue("2145370159");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("workPhone");
               textWriter.WriteValue("2145370159");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("creditCardType");
               textWriter.WriteValue("VI");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("creditCardNumber");
               textWriter.WriteValue("4375514498806008");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("creditCardIdentifier");
               textWriter.WriteValue("575");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("creditCardExpirationMonth");
               textWriter.WriteValue("06");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("creditCardExpirationYear");
               textWriter.WriteValue("2020");
               textWriter.WriteEndElement();

               textWriter.WriteEndElement(); // Reservation Details End
               #endregion Reservation Details

               #region Address Info
               textWriter.WriteStartElement("AddressInfo");
               textWriter.WriteStartElement("address1");
               textWriter.WriteValue("travelnow");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("city");
               textWriter.WriteValue("Nagpur");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("stateProvinceCode");
               textWriter.WriteValue("MAH");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("countryCode");
               textWriter.WriteValue("IN");
               textWriter.WriteEndElement();
               textWriter.WriteStartElement("postalCode");
               textWriter.WriteValue("98004");
               textWriter.WriteEndElement();
               textWriter.WriteEndElement();
               #endregion
               textWriter.WriteEndElement();
           }
           //StringBuilder sb = new StringBuilder();
           //sb.Append("&hotelId=" + HotelId);
           //sb.Append("&arrivalDate=" + arrivalDate);
           //sb.Append("&departureDate=" + departureDate);
           //sb.Append("&supplierType=E" );
           //sb.Append("&rateKey=" + rateKey);
           //sb.Append("&roomTypeCode=" + roomTypeCode);
           //sb.Append("&rateCode=" + rateCode);
           //sb.Append("&chargeableRate=" + chargeableRate);
           //for (int i = 0; i < room.Count; i++)
           //{
           //    sb.Append("&room" + (i+1) + "=" + room[i].numberOfAdults);
           //    sb.Append("&room" + (i+1) + "FirstName=" + room[i].FirstName);
           //    sb.Append("&room" + (i+1) + "LastName=" + room[i].LastName);
           //    sb.Append("&room" + (i+1) + "BedTypeId=" + bedTypeId);
           //    //sb.Append("&room" + (i+1) + "SmokingPreference=" + "NS");
           //}
           //sb.Append("&email=test@travelnow.com");
           //sb.Append("&firstName=test");
           //sb.Append("&lastName=tester");
           //sb.Append("&homePhone=2145370159");
           //sb.Append("&workPhone=2145370159");
           //sb.Append("&creditCardType=CA");
           //sb.Append("&creditCardNumber=5401999999999999");
           //sb.Append("&creditCardIdentifier=123");
           //sb.Append("&creditCardExpirationMonth=11");
           //sb.Append("&creditCardExpirationYear=2018");
           //sb.Append("&address1=travelnow");
           //sb.Append("&city=Bellevue");
           //sb.Append("&stateProvinceCode=WA");
           //sb.Append("&countryCode=US");
           //sb.Append("&postalCode=98004");
          // m_xml = sb.ToString();
            sw.Close();
            sw.Dispose();
            //return m_xml.ToString(); ;
            return m_xml = sw.ToString();
        }
        #endregion HotelRoomReservationRequest



    }
}
