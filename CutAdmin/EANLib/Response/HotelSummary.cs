﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class HotelSummary
    {
        public string @order { get; set; }
        public string @ubsScore { get; set; }
        public string hotelId { get; set; }
        public string name { get; set; }
        public List<string> address1 { get; set; }
        public string city { get; set; }
        public string stateProvinceCode { get; set; }
        public string postalCode { get; set; }
        public string countryCode { get; set; }
        public string airportCode { get; set; }
        public string supplierType { get; set; }
        public string propertyCategory { get; set; }
        public string hotelRating { get; set; }
        public string confidenceRating { get; set; }
        public int amenityMask { get; set; }
        public string tripAdvisorRating { get; set; }
        public string tripAdvisorReviewCount { get; set; }
        public string tripAdvisorRatingUrl { get; set; }
        public string locationDescription { get; set; }
        public string shortDescription { get; set; }
        public string highRate { get; set; }
        public string lowRate { get; set; }
        public string rateCurrencyCode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public decimal proximityDistance { get; set; }
        public string proximityUnit { get; set; }
        public string hotelInDestination { get; set; }
        public string thumbNailUrl { get; set; }
        public string deepLink { get; set; }
        public RoomRateDetailsList RoomRateDetailsList { get; set; }
        public string Supplier { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
      
    }
}
