﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public  class CancelConfirmResponse
    {
        public string Status { get; set; }
        public string customerSessionId { get; set; }
        public string cancellationNumber { get; set; }
    }
}
