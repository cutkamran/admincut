﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class HotelList
    {
        public string @size { get; set; }
        public string @activePropertyCount { get; set; }
        public List<HotelSummary> HotelSummary { get; set; }

    }
}
