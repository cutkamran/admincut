﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class NightlyRate
    {
       public string @baseRate { get; set; }
       public string @rate { get; set; }
       public string @promo { get; set; }
    }
}
