﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class cachedSupplierResponse
    {
       public string @supplierCacheTolerance { get; set; }
       public string @cachedTime { get; set; }
       public string @supplierRequestNum { get; set; }
       public string @supplierResponseNum { get; set; }
       public string @supplierResponseTime { get; set; }
       public string @candidatePreptime { get; set; }
       public string @otherOverheadTime { get; set; }
       public string @tpidUsed { get; set; }
       public string @matchedCurrency { get; set; }
       public string @matchedLocale { get; set; }
    }
}
