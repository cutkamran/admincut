﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class NightlyRatesPerRoom
    {
       public string @size { get; set; }
       public List<NightlyRate> NightlyRate { get; set; }
    }
}
