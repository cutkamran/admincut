﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class CancellationInfoList
    {
       public Int64 Count { get; set; }
       public List<CancellationInfo> CancellationInfo { get; set; }
    }
}
