﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class HotelRoomAvailabilityResponse
    {
       public Int64 size { get; set; } 
       public string customerSessionId { get; set; }
       public string latitude { get; set; }
       public string  longitude { get; set; }
       public string hotelRating { get; set; }
       public string hotelId { get; set; }
       public string arrivalDate { get; set; }
       public string departureDate { get; set; }
       public Int64 noNight { get; set; }
       public string hotelName { get; set; }

       public string Discription { get; set; }
       public string hotelAddress { get; set; }
       public string hotelCity { get; set; }
       public string hotelCountry { get; set; }
       public Int64 numberOfRoomsRequested { get; set; }
       public string checkInInstructions { get; set; }
       public string specialCheckInInstructions { get; set; }
       public string tripAdvisorRating { get; set; }
       public string tripAdvisorReviewCount { get; set; }
       public string tripAdvisorRatingUrl { get; set; }
       public List<HotelRoomResponse> HotelRoomResponse { get; set; }
       public BedTypes BedTypes { get; set; }
       public List<string> ValueAdd { get; set; }
       public string rateOccupancyPerRoom { get; set; }
       public string quotedOccupancy { get; set; }
       public string minGuestAge { get; set; }
       public string ChargeableRateInfo { get; set; }
       public List<string> HotelImages { get; set; }
    }
}
