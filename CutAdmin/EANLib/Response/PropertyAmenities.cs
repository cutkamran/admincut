﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class PropertyAmenities
    {
       public Int64 Size { get; set; }
       public List<PropertyAmenity> PropertyAmenity { get; set; }
    }
}
