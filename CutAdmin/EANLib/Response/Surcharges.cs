﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class Surcharges
    {
        public string @size { get; set; }
        public List< Surcharge> Surcharge { get; set; }
    }
}
