﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class BedTypes
    {
       public Int64 size { get; set; }
       public List<BedType> BedType { get; set; }
    }
}
