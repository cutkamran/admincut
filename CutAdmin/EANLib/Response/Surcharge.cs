﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class Surcharge
    {
       public string @type { get; set; }
       public string @amount { get; set; }
    }
}
