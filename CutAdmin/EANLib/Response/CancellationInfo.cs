﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
  public  class CancellationInfo
    {
      public string VersionId { get; set; }
    
      public string CancelTime { get; set; }
      public string StartWindowHours { get; set; }
      public string NightCount { get; set; }
      public string amount { get; set; }
      public string Percentage { get; set; }
      public string CurrencyCode { get; set; }
      public string TimeZoneDesc { get; set; }

      public DateTime CancelDate { get; set; }
      public float CancellationPrice { get; set; }
      public float CUTCancellationPrice { get; set; }
      public float AgentCancellationPrice { get; set; }
    }
}
