﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
  public  class HotelListResponse
    {
      public string customerSessionId { get; set; }
      public int numberOfRoomsRequested { get; set; }
      public bool moreResultsAvailable { get; set; }
      public string cacheKey { get; set; }
      public string cacheLocation { get; set; }
      public cachedSupplierResponse SuplierResponse { get; set; }
      public HotelList HotelList { get; set; }


    }
}
