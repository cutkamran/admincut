﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class PurchaseConform
    {
        public string ArivelDate { get; set; }
        public string checkInInstructions { get; set; }
        public string confirmationNumbers { get; set; }
        public string customerSessionId { get; set; }
        public string departureDate { get; set; }
        public string existingItinerary { get; set; }
        public string hotelAddress { get; set; }
        public string hotelCity { get; set; }
        public string hotelCountry { get; set; }
        public string hotelCountryCode { get; set; }
        public string hotelName { get; set; }
        public string hotelPostalCode   { get; set; }
        public string itineraryId { get; set; }
        public string numberOfRoomsBooked { get; set; }
        public string processedWithConfirmation { get; set; }
        public RateInfos RateInfos { get; set; }
        public string rateOccupancyPerRoom { get; set; }
        public string reservationStatusCode { get; set; }
        public string roomDescription { get; set; }
        public string supplierType   { get; set; }
        public string tripAdvisorRating { get; set; }
        public string tripAdvisorRatingUrl { get; set; }
        public string tripAdvisorReviewCount { get; set; }
        public ValueAdds ValueAdds { get; set; }
    }
}
