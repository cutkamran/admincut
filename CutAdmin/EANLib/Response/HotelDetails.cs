﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class HotelDetails
    {
       public string numberOfRooms { get; set; }
       public string numberOfFloors { get; set; }
       public string propertyInformation { get; set; }
       public string propertyDescription { get; set; }
       public string hotelPolicy { get; set; }
       public string roomInformation { get; set; }
       public string drivingDirections { get; set; }
       public string checkInInstructions { get; set; }
       public string locationDescription { get; set; }
       public string diningDescription { get; set; }
       public string amenitiesDescription { get; set; }
       public string businessAmenitiesDescription { get; set; }
       public string roomDetailDescription { get; set; }
       public string specialCheckInInstructions { get; set; }
    }
}
