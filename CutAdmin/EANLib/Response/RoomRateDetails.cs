﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class RoomRateDetails
    {
       public Int64 roomTypeCode { get; set; }
       public Int64 rateCode { get; set; }
       public Int64 maxRoomOccupancy { get; set; }
       public Int64 quotedRoomOccupancy { get; set; }
       public Int64 minGuestAge { get; set; }
       public string roomDescription { get; set; }
       public bool propertyAvailable { get; set; }
       public bool propertyRestricted { get; set; }
       public bool expediaPropertyId { get; set; }
       public RateInfos RateInfos { get; set; }
       public ValueAdds ValueAdds { get; set; }
       public  BedTypes BedTypes { get; set; }
       public string smokingPreferences { get; set; }   
    }
}
