﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class Facility
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
