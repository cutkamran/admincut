﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
  public  class RoomDetails
    {
      public string retcode { get; set; }
      public string roomTypeCode { get; set; }
      public string roomType { get; set; }
      public string SupplierType { get; set; }
      public string PropertyIs { get; set; }
    }
}
