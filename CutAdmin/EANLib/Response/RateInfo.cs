﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class RateInfo
    {
        public string @priceBreakdown { get; set; }
        public string @promo { get; set; }
        public string @rateChange { get; set; }
        public RoomGroup RoomGroup { get; set; }
        public ChargeableRateInfo ChargeableRateInfo { get; set; }
        public bool nonRefundable { get; set; }
        public float  MandatoryTax { get; set; }
        public string rateType { get; set; }
        public Int64 currentAllotment { get; set; }
        public string CancellationPolicy { get; set; }
        public CancellationInfoList CancellationInfoList { get; set; }
        public string promoDescription { get; set; }
        public string promoId { get; set; }

    }
}
