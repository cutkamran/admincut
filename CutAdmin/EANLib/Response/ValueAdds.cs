﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class ValueAdds
    {
       public Int64 @Size { get; set; }
       public List<ValueAdd> ValueAdd { get; set; }
    }
}
