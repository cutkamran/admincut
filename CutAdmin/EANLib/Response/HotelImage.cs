﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class HotelImage
    {
        public string hotelImageId { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string thumbnailUrl { get; set; }
    }
}
