﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class HotelRoomResponse
    {
        public string rateCode { get; set; }
        public string roomTypeCode { get; set; }
        public string rateDescription { get; set; }
        public string roomTypeDescription { get; set; }
        public string supplierType { get; set; }
        public string propertyId { get; set; }
        public BedTypes BedTypes { get; set; }
        public RateInfos RateInfos { get; set; }
        public ValueAdds ValueAdds { get; set; }
        public string deeplink { get; set; }
        public List<HotelImage> HotelImage { get; set; }
        public string smokingPreferences { get; set; }

    }
}
