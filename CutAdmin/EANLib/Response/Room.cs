﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class Room
    {
       public Int64 numberOfAdults { get; set; }
       public Int64 numberOfChildren { get; set; }
       public string rateKey { get; set; }
       public Int64 childAges { get; set; }
       public float CUTPrice { get; set; }
       public float  AgentMarkup { get; set; }
       public List<CancellationInfo> CancellationInfo { get; set; }
       public DateTime ChargeDate { get; set; }
    }
}
