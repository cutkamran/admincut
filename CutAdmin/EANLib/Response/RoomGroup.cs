﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
    public class RoomGroup
    {
        public List<Room> Room { get; set; }
    }
}
