﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public  class HotelRoomDetails
    {
       public int noRooms { get; set; }
       public List<RoomDetails> RoomDetails { get; set; }
    }
}
