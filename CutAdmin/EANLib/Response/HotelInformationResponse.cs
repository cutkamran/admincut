﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class HotelInformationResponse
    {
       public string HotelId { get; set; }
       public string SessionId { get; set; }
       public HotelSummary HotelSummary { get; set; }
       public HotelDetails HotelDetails { get; set; }
       public PropertyAmenities PropertyAmenities { get; set; }
       public HotelImages HotelImages { get; set; }
    }
}
