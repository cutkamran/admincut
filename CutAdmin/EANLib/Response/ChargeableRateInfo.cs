﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EANLib.Response
{
   public class ChargeableRateInfo
    {

       public string @averageBaseRate { get; set; }
       public string @averageRate { get; set; }
       public string @commissionableUsdTotal { get; set; }
       public string @currencyCode { get; set; }
       public string @maxNightlyRate { get; set; }
       public string @nightlyRateTotal { get; set; }
       public string @grossProfitOffline { get; set; }
       public string @grossProfitOnline { get; set; }
       public string @surchargeTotal { get; set; }
       public string @total { get; set; }
       public NightlyRatesPerRoom NightlyRatesPerRoom {get;set;}
       public Surcharges Surcharges { get; set; }
       public string CutRoomAmmount { get; set; }
       public string AgentRoomMarkup { get;set; }

       // New Charges 
       public float LessCommEarned { get; set; }
       public float ServiceTax { get; set; }
       public float TDS { get; set; }
       public float NetPayble { get; set; }

    }
}
